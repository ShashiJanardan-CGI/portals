﻿namespace Posten.Portal.Skicka.Services.Cart
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System;    
    //using System.Net.Http;
    //using System.Net.Http.Headers;
    

    using Microsoft.SharePoint;
    using Posten.Portal.Skicka.Services.BusinessEntities.CartEntities;

    public class CartAgent
    {
        #region Methods

        
        public string AddItemToCart(string ApplicationId, string ApplicationName, string BasketId, string ConfirmationUrl,
            string CurrencyCode, string CustomApplicationLink, string CustomApplicationLinkText, string Details, string EditUrl,
            string FreeTextField1, string FreeTextField2, string Name, string OrderNumber, string ProduceUrl, string ServiceImageUrl,
            IEnumerable<ServiceOrderLineItem> ServiceOrderLines,
            string SubTotal,
            string Vat,
            string Total)
        {
            string serviceEndpointUrl = SPContext.Current.Site.Url + "/_vti_bin/posten/cart/CartTransaction.svc/AddItemToCart";
            string basketId = string.Empty;

           
            System.Net.WebClient client = new System.Net.WebClient();
            client.Headers.Add("content-type", "application/json");
            var response = client.UploadData(serviceEndpointUrl, "POST", Encoding.Default.GetBytes("{\"EmailId\": \"admin@admin.com\",\"Password\": \"pass#123\"}"));
            basketId = Encoding.ASCII.GetString(response);

            //WebClient client = new WebClient();
            //client.UseDefaultCredentials = true;
            //client.Headers["Content-type"] = "application/json";
            //client.Encoding = Encoding.UTF8;
            //string response = basketId = client.DownloadString(serviceEndpointUrl);

            return basketId;
        }

        #endregion Methods
    }
}