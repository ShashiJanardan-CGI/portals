﻿namespace Posten.Portal.Skicka.Services.PVT
{
    using System.Collections.Generic;

    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Utils;
using System.Web;

    public interface IPVTService
    {
        #region Methods

        //AccountValidationResponseBE GetAccountValidationResponse(string accountNumber, string reference, string type);
        DestinationsBE GetDestinations();

        PriceAndRanksBE GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight);

        /// <summary>
        /// Get services using country iso 2 code
        /// </summary>
        /// <param name="countryCode">country code in iso 2</param>
        /// <returns>All services for a specific country</returns>
        ProductsBE GetProducts(string countryCode);

        /// <summary>
        /// Get a list of Price and Ranking criteria
        /// </summary>
        /// <param name="countryCode">country code in iso 2</param>
        /// <param name="measurements">size of product</param>
        /// <param name="weight">weight of product</param>
        /// <param name="filters">additional delivery conditions for the product</param>
        /// <returns></returns>
        PriceAndRanksBE GetProductSuggestions(string countryCode, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters);

        string GetVersion();

        string TestService();

        ValidationResponseBE ValidateItem(SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver);

        //ValidationResponseBE ValidateService(string code, int length, int width, int height, int weight, List<AdditionalServiceBE> additionalServices, double valueAmount, string content, double codAmount, int diameter, string destinationId, int destinationZip, AddressBE fromAddress, AddressBE toaddress, double insuranceAmount, string marketplace);
        string ZipToCity(string zipCode);

        #endregion Methods
    }
}