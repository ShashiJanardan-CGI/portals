﻿namespace Posten.Portal.Skicka.Services.PVT
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.ServiceModel;
    using System.Web;
    using System.Web.Script.Serialization;

    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

    using Posten.Portal.Platform.Common.Services;
    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Logging;
    using Posten.Portal.Skicka.Services.Utils;

    public class PVTServiceAgent : ServiceAgentBase, IPVTService
    {
        #region Fields

        private static LogController logger;

        private PvtServiceBeanClient pvtClient;
        private string transactionLogID;

        #endregion Fields

        #region Constructors

        public PVTServiceAgent()
        {
            try
            {
                this.pvtClient = (PvtServiceBeanClient)ConfigureServiceClient<PvtServiceBeanClient, PvtServiceBean>();
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }

        public PVTServiceAgent(HttpContext context)
        {
            try
            {
                this.pvtClient = new PvtServiceBeanClient();//(PvtServiceBeanClient)ConfigureServiceClient<PvtServiceBeanClient, PvtServiceBean>();
                this.pvtClient.ClientCredentials.UserName.UserName = "PwpItemNotesUser";//"ISP";
                this.pvtClient.ClientCredentials.UserName.Password = "2Eddy1SwatT_";//"ISP";

                LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }

        #endregion Constructors

        #region Properties

        public string TransactionLogID
        {
            get { return transactionLogID; }
            set { transactionLogID = value; }
        }

        #endregion Properties

        #region Methods


        // DOne Mattias
        public DestinationsBE GetDestinations()
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetDestinations");  //TODO: add parameters
                DestinationsBE destinations = new DestinationsBE();
                DestinationResponse response = new DestinationResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.listDestinations();
                }
                destinations = new DestinationsBE(response);
                return destinations;
            }
            catch (Exception ex)
            {
                //Posten.Portal.Skicka.Services.Utils.Util.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetDestinations");
            }
        }

        // DOne Mattias
        public PriceAndRanksBE GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetPreferredProduct");  //TODO: add parameters
                PriceAndRanksBE result = null;
                PriceAndRankResponse response = new PriceAndRankResponse();

                // TODO: ASK ISP TO CHANGE SIGNATURE OF getPreferredProduct TO USE SIZE
                ParcelSize size = TranslateSizeBEtoParcelSize(measurements);

                preferredProductRequest request = new preferredProductRequest();
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
                request.productIdSpecified = true;
                request.productId = productId;
                request.destinationId = destinationID;
                request.size = size;
                request.weightSpecified = true;
                request.weight = weight;

                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.getPreferredProduct(request);
                }
                result = new PriceAndRanksBE(response);
                return result;
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetPreferredProduct");
            }
        }

        //DOne Mattias
        public ProductsBE GetProducts(string countryISO2Code)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetProducts");  //TODO: add parameters
                ProductsBE products = null;
                productsRequest request = new productsRequest();                
                request.destinationId = countryISO2Code;
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;

                ServicesResponse result = new ServicesResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    result = this.pvtClient.getProducts(request);
                }
                products = new ProductsBE(result);
                return products;
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetProducts");
            }
        }

        //DOne Mattias
        public PriceAndRanksBE GetProductSuggestions(string countryISO2Code, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetProductSuggestions");  //TODO: add parameters

                ParcelSize size = TranslateSizeBEtoParcelSize(measurements);
                characteristics[] characteristics = Array.ConvertAll(
                    filters,
                    s => (characteristics)Enum.Parse(typeof(characteristics), Enum.GetName(typeof(ConstantsAndEnums.Characteristics), s)));

                PriceAndRanksBE productSuggestions = null;

                productSuggestionsRequest request = new productSuggestionsRequest();                
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
                request.destinationId = countryISO2Code;
                request.size = size;
                request.weightSpecified = true;
                request.weight = weight;
                request.filters = characteristics;

                PriceAndRankResponse result = new PriceAndRankResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    result = this.pvtClient.getProductSuggestions(request);
                }
                productSuggestions = new PriceAndRanksBE(result);
                return productSuggestions;
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetProductSuggestions");
            }
        }

        public string GetVersion()
        {
            string version = "Version not set";
            try
            {
                version = this.pvtClient.getVersion();

                // Logging
                Posten.Portal.Skicka.Services.Utils.LogUtility.LogVerbose(version, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                Posten.Portal.Skicka.Services.Utils.LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                if (ExceptionPolicy.HandleException(ex, "SkickaExceptionPolicy"))
                {
                    throw;
                }
            }

            return version;
        }

        public string TestService()
        {
            try
            {
                return "Agent";
            }
            catch (Exception ex)
            {
                Posten.Portal.Skicka.Services.Utils.LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return ex.ToString();
            }
        }

        // DOne
        public ValidationResponseBE ValidateItem(SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver)
        {
            try
            {
                orderRequest request = new orderRequest();
                ValidationResponseBE validationResponse = new ValidationResponseBE();
                itemResponse response = new itemResponse();
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:ValidateItem");  //TODO: add parameters

                InitiateValidateItemRequest(size, weight, additionalServiceIDs, account, countryISOCode, selectedServiceID, sender, receiver, request);
                
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.validateItem(request);
                }

                if (response.status == responseStatus.SUCCESS)
                {
                    //Initiate validation response                    
                    InitiateValidationResponse(response, validationResponse);
                }
                else
                {
                    validationResponse.Valid = false;
                    logger.LogMessage(this.transactionLogID, "Result:ValidateItem - INVALID"); 
                }

                return validationResponse;
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:ValidateItem");
            }
        }

        // DO Mattias
        public string ZipToCity(string zipCode)
        {
            zipCode = zipCode.Replace(" ", string.Empty).Trim();
            string jsonString = string.Empty;
            string returnValue = string.Empty;
            try
            {
                string url = ConfigurationManager.AppSettings[ConstantsAndEnums.SkickaZipCodeServiceUrlAppKey];
                WebRequest request = HttpWebRequest.Create(url + zipCode);
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        jsonString = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                var error = e.Message;
            }

            if (!string.IsNullOrEmpty(jsonString))
            {
                var zipAndCityDictionary = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(jsonString);
                if (zipAndCityDictionary != null && zipAndCityDictionary.ContainsKey("postalcity"))
                {
                    returnValue = zipAndCityDictionary["postalcity"];
                }
            }
            return returnValue;
        }

        private static void InitiateValidateItemRequest(SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver, orderRequest request)
        {
            request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
            request.order = new Order();
            if (additionalServiceIDs != null && additionalServiceIDs.Count > 0)
            { //This section is only provided to request order object if additional services have been selected
                request.order.additionalServiceIds = additionalServiceIDs.ToArray();               
            }

            if (additionalServiceIDs.Contains("01") || selectedServiceID.Equals(2))//Postforkott - COD: cash on delivery
            {
                CodInformation codInfo = new CodInformation();
                codInfo.accountNo = account.AccountNumber;
                codInfo.amount = account.Amount;
                codInfo.amountSpecified = true;
                codInfo.reference = account.Reference;
                codInfo.giroType = account.ReturnAccountGiroType();
                codInfo.giroTypeSpecified = true;

                request.order.codInformation = codInfo;
            }

            request.order.destinationId = countryISOCode;
            //request.order.marketPlaceId = ConstantsAndEnums.SkickaMarketPlaceID;
            request.order.productId = selectedServiceID;
            request.order.productIdSpecified = true;
            request.order.receiver = receiver.ToParty();
            request.order.sender = sender.ToParty();
            request.order.size = size.BusinessEntityToDataAccessObject();
            request.order.weight = weight;
            request.order.weightSpecified = true;
        }

        private static void InitiateValidationResponse(itemResponse response, ValidationResponseBE validationResponse)
        {
            validationResponse.Valid = true;
            validationResponse.SapId = response.item.baseService.sapProductId;
            validationResponse.ReferenceNumberSerialType = response.item.referenceNumberSerialType;
            validationResponse.StatusMsg = response.statusMsg;
            validationResponse.ProductCode = response.item.baseService.serviceId;//response.item.compoundPvtId;
            validationResponse.CompanyCode = response.item.baseService.companyCode;

            validationResponse.Price = new PriceBE();
            if (response.item.totalPrice != null)
            {
                validationResponse.Price.Amount = response.item.totalPrice.amount;
                validationResponse.Price.AmountNoVat = response.item.totalPrice.amountNoVat;
                validationResponse.Price.VATAmount = response.item.totalPrice.amount - response.item.totalPrice.amountNoVat;
                validationResponse.Price.Currency = response.item.totalPrice.currency;
                validationResponse.Price.VATPercentage = (decimal)response.item.totalPrice.vat;
            }

            validationResponse.BaseServicePrice = new PriceBE();
            if (response.item.baseService.totalPrice != null) //baseService is an order item
            {
                validationResponse.BaseServicePrice.Amount = response.item.baseService.totalPrice.amount;
                validationResponse.BaseServicePrice.AmountNoVat = response.item.baseService.totalPrice.amountNoVat;
                validationResponse.BaseServicePrice.VATAmount = response.item.baseService.totalPrice.amount - response.item.baseService.totalPrice.amountNoVat;
                validationResponse.BaseServicePrice.Currency = response.item.baseService.totalPrice.currency;
                validationResponse.BaseServicePrice.VATPercentage = (decimal)response.item.baseService.totalPrice.vat;                
            }

            if (response.item.deliveryTime != null)
            {
                validationResponse.DeliveryInfo = new DeliveryInfoBE();
                validationResponse.DeliveryInfo.Delayed = response.item.deliveryTime.delayed;
                validationResponse.DeliveryInfo.DeliveryDay = response.item.deliveryTime.deliveryDay;
                validationResponse.DeliveryInfo.DeliveryHour = response.item.deliveryTime.deliveryHour;
            }

            //Base Service
            validationResponse.BaseService = new AdditionalServiceBE();
            validationResponse.BaseService.AdditionalServiceCode = response.item.baseService.serviceId;
            validationResponse.BaseService.ServiceKey = response.item.baseService.serviceId;//response.item.compoundPvtId;
            validationResponse.BaseService.Name = response.item.baseService.name;
            validationResponse.BaseService.SapId = response.item.baseService.sapProductId;
            validationResponse.BaseService.CompanyCode = response.item.baseService.companyCode;
            validationResponse.BaseService.Price = new PriceBE();
            validationResponse.BaseService.Price.Currency = response.item.baseService.price.currency;
            validationResponse.BaseService.Price.Amount = response.item.baseService.price.amount;
            validationResponse.BaseService.Price.AmountNoVat = response.item.baseService.price.amountNoVat;
            validationResponse.BaseService.Price.VATAmount = response.item.baseService.price.amount - response.item.baseService.price.amountNoVat;
            validationResponse.BaseService.Price.VATPercentage = (decimal)response.item.baseService.price.vat;
            validationResponse.BaseService.PvtId = response.item.baseService.pvtId;

            validationResponse.AdditionalServices = new List<AdditionalServiceBE>();
            if (response.item.additionalServices != null)
            {
                foreach (LineItem additionalService in response.item.additionalServices)
                {
                    AdditionalServiceBE additionalServiceBE = new AdditionalServiceBE();
                    additionalServiceBE.AdditionalServiceCode = additionalService.serviceId;
                    additionalServiceBE.ServiceKey = response.item.baseService.serviceId; //additionalService.serviceId; // response.item.baseService.pvtId;//response.item.code;
                    additionalServiceBE.Name = additionalService.name;
                    additionalServiceBE.SapId = additionalService.sapProductId;
                    additionalServiceBE.CompanyCode = additionalService.companyCode;
                    additionalServiceBE.Price = new PriceBE();
                    additionalServiceBE.Price.Currency = additionalService.price.currency;
                    additionalServiceBE.Price.Amount = additionalService.price.amount;
                    additionalServiceBE.Price.AmountNoVat = additionalService.price.amountNoVat;
                    additionalServiceBE.Price.VATAmount = additionalService.price.amount - additionalService.price.amountNoVat;
                    additionalServiceBE.Price.VATPercentage = (decimal)additionalService.price.vat;
                    additionalServiceBE.PvtId = additionalService.pvtId;

                    validationResponse.AdditionalServices.Add(additionalServiceBE);
                }
            }

            if (response.responseInfos != null)
            {
                validationResponse.AdditionalInfo = new List<InfoBE>();
                foreach (responseInfo info in response.responseInfos)
                {
                    InfoBE infoBe = new InfoBE();
                    //infoBe.Attribute = info.description;
                    infoBe.Code = info.code.ToString();
                    infoBe.Value = info.description;
                    validationResponse.AdditionalInfo.Add(infoBe);
                }
            }
        }

        private static CodInformation TranslateAccountBEtoCodInformation(AccountBE account)
        {
            CodInformation translatedObject = new CodInformation()
            {
                accountNo = account.AccountNumber,

                amountSpecified = true,
                amount = account.Amount,

                giroTypeSpecified = true,
                giroType = (i2GiroType)Enum.Parse(typeof(i2GiroType), Enum.GetName(typeof(AccountBE.GiroTypeEnum), account.AccountType)),

                reference = account.Reference,
            };

            return translatedObject;
        }

        // TODO: REMOVE WHEN ISP HAS FIXED THE GetPreferredProduct signature to use SIZE
        private static ParcelSize TranslateSizeBEtoParcelSize(SizeBE sizeBE)
        {
            ParcelSize translatedSize = new ParcelSize();

            translatedSize.diameter = sizeBE.Diameter;

            translatedSize.height = sizeBE.Height;

            translatedSize.length = sizeBE.Length;

            translatedSize.width = sizeBE.Width;

            return translatedSize;
        }

        #endregion Methods

        #region Other

        //private static size TranslateSizeBEtoSize(SizeBE sizeBE)
        //{
        //    size translatedObject = new size();
        //    translatedObject.diameterSpecified = true;
        //    translatedObject.diameter = sizeBE.Diameter;
        //    translatedObject.heightSpecified = true;
        //    translatedObject.height = sizeBE.Height;
        //    translatedObject.lengthSpecified = true;
        //    translatedObject.length = sizeBE.Length;
        //    translatedObject.widthSpecified = true;
        //    translatedObject.width = sizeBE.Width;
        //    return translatedObject;
        //}

        #endregion Other
    }
}