﻿namespace Posten.Portal.Skicka.Services.Logging
{
    using System.Collections.Generic;
    using System.Security.Permissions;

    using Microsoft.SharePoint.Administration;
    using Microsoft.SharePoint.Security;

    using Posten.Portal.Skicka.Services.Resources;

    public class LoggingService : SPDiagnosticsServiceBase
    {
        #region Constructors

        [SharePointPermission(SecurityAction.InheritanceDemand, ObjectModel = true)]
        [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
        public LoggingService(string title, SPFarm farm)
            : base(title, farm)
        {
        }

        public LoggingService()
        {
        }

        #endregion Constructors

        #region Methods

        protected override IEnumerable<SPDiagnosticsArea> ProvideAreas()
        {
            var defaultCategory = new SPDiagnosticsCategory(Constants.LogApplicationName, TraceSeverity.Medium, EventSeverity.Information);

            var areas = new List<SPDiagnosticsArea>{
                    new SPDiagnosticsArea(Constants.LogApplicationName,
                      new List<SPDiagnosticsCategory>{defaultCategory})
            };

            return areas;
        }

        #endregion Methods
    }
}