﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class ResponseInfoBE
    {
        #region Constructors

        public ResponseInfoBE()
        {
        }

        public ResponseInfoBE(responseInfo responseInfo)
        {
            this.Code = responseInfo.code;
            this.Description = responseInfo.description;
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public int Code
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        #endregion Properties
    }
}