﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class SizeBE
    {
        #region Properties

        [DataMember]
        public int Diameter
        {
            get;
            set;
        }

        [DataMember]
        public int Height
        {
            get;
            set;
        }

        [DataMember]
        public int Length
        {
            get;
            set;
        }

        [DataMember]
        public int Width
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public ParcelSize BusinessEntityToDataAccessObject()
        {
            ParcelSize dataAccessObject = new ParcelSize();
            dataAccessObject.diameter = this.Diameter;
            dataAccessObject.height = this.Height;
            dataAccessObject.length = this.Length;
            dataAccessObject.width = this.Width;

            return dataAccessObject;
        }

        public override string ToString()
        {
            if (this.Diameter == 0)
            {
                string length = (((double)this.Length) / 10).ToString("0.0");
                length = length.Replace('.', ',');
                if (length.EndsWith(",0"))
                {
                    length = length.Substring(0, length.Length - 2);
                }

                string width = (((double)this.Width) / 10).ToString("0.0");
                width = width.Replace('.', ',');
                if (width.EndsWith(",0"))
                {
                    width = width.Substring(0, width.Length - 2);
                }

                string height = (((double)this.Height) / 10).ToString("0.0");
                height = height.Replace('.', ',');
                if (height.EndsWith(",0"))
                {
                    height = height.Substring(0, height.Length - 2);
                }

                return length + "x" + width + "x" + height + " cm";
            }
            else
            {
                string length = (((double)this.Length) / 10).ToString("0.0");
                length = length.Replace('.', ',');
                if (length.EndsWith(",0"))
                {
                    length = length.Substring(0, length.Length - 2);
                }

                string diameter = (((double)this.Diameter) / 10).ToString("0.0");
                diameter = diameter.Replace('.', ',');
                if (diameter.EndsWith(",0"))
                {
                    diameter = diameter.Substring(0, diameter.Length - 2);
                }

                return length + "x" + diameter + " cm";
            }
        }

        #endregion Methods
    }
}