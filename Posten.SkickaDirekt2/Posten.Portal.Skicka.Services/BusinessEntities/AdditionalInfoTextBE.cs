﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class AdditionalInfoTextBE
    {
        #region Constructors

        public AdditionalInfoTextBE()
        {
        }

        public AdditionalInfoTextBE(AdditionalInfoText additionalInfoText)
        {
            this.Delivery = new ServiceTextBE(additionalInfoText.delivery);
            this.DeliveryTime = new ServiceTextBE(additionalInfoText.deliveryTime);
            this.GoodsValue = new ServiceTextBE(additionalInfoText.goodsValue);
            this.IdRequired = new ServiceTextBE(additionalInfoText.idRequired);
            this.MaxWeight = new ServiceTextBE(additionalInfoText.maxWeight);
            this.Receiver = new ServiceTextBE(additionalInfoText.receiver);
            this.Size = new ServiceTextBE(additionalInfoText.size);
            this.Submission = new ServiceTextBE(additionalInfoText.submission);
            this.Traceable = new ServiceTextBE(additionalInfoText.traceable);
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public ServiceTextBE Delivery
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE DeliveryTime
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE GoodsValue
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE IdRequired
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE MaxWeight
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE Receiver
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE Size
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE Submission
        {
            get;
            set;
        }

        [DataMember]
        public ServiceTextBE Traceable
        {
            get;
            set;
        }

        #endregion Properties
    }
}