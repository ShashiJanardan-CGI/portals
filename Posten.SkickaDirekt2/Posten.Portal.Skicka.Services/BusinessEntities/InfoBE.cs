﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    [Serializable]
    public class InfoBE
    {
        #region Properties

        [DataMember]
        public string Attribute
        {
            get;
            set;
        }

        [DataMember]
        public string Code
        {
            get;
            set;
        }

        [DataMember]
        public string Value
        {
            get;
            set;
        }

        #endregion Properties
    }
}