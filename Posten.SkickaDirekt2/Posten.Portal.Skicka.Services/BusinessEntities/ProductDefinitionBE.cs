﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.Utils;

    [Serializable]
    [DataContract]
    public class ProductDefinitionBE
    {
        #region Constructors

        public ProductDefinitionBE(ProductDefinition dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        public ProductDefinitionBE()
        {
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public AdditionalInfoTextBE AdditionalInfoText
        {
            get;
            set;
        }

        [DataMember]
        public AdditionalServiceBE[] AdditionalServices
        {
            get;
            set;
        }

        [DataMember]
        public string Code
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public ServiceDocumentBE[] Documents
        {
            get;
            set;
        }

        [DataMember]
        public bool International
        {
            get;
            set;
        }

        [DataMember]
        public ParcelSizeBE MaxSize
        {
            get;
            set;
        }

        [DataMember]
        public int MaxWeight
        {
            get;
            set;
        }

        [DataMember]
        public ParcelSizeBE MinSize
        {
            get;
            set;
        }

        [DataMember]
        public int MinWeight
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public PriceBE Price
        {
            get;
            set;
        }

        [DataMember]
        public long ProductId
        {
            get;
            set;
        }

        [DataMember]
        public Boolean IsRoll
        {
            get;
            set;
        }
        //[DataMember]
        //public long DestinationId
        //{
        //    get;
        //    set;
        //}

        [DataMember]
        public ServicePropertiesBE Properties
        {
            get;
            set;
        }

        public int[] WeightIntervals
        {
            get;
            set;
        }

        [DataMember]
        public WeightBE[] Weights
        {
            get
            {
                var weights = from t in this.WeightIntervals
                              select new WeightBE()
                              {
                                  DisplayUnit = t > 1000 ? "kg" : "g",
                                  DisplayValue = t > 1000 ? decimal.Round(decimal.Divide(t, 1000), 2) : t,
                                  WeightInGrams = t
                              };

                return weights.ToArray<WeightBE>();
            }
        }

        #endregion Properties

        #region Methods

        public void DataAccessObjectToBusinessEntity(ProductDefinition dataAccessObject)
        {
            // Map Code to dataAccessObject when coresponding field is added in proxy
            this.ProductId = dataAccessObject.productId;            
            this.Code = dataAccessObject.code;
            this.Name = dataAccessObject.name;
            this.International = dataAccessObject.international;
            this.MinSize = new ParcelSizeBE(dataAccessObject.minSize);
            this.MaxSize = new ParcelSizeBE(dataAccessObject.maxSize);
            this.Description = dataAccessObject.description;
            this.IsRoll = dataAccessObject.properties.tubeAllowed;

            if (dataAccessObject.additionalServices != null)
            {
                this.AdditionalServices = Array.ConvertAll(
                   dataAccessObject.additionalServices,
                   a => this.TranslateAdditionalServiceToAdditionalServiceBE(a));

                this.AdditionalServices = this.AdditionalServices.Where(o => o != null).ToArray();
            }

            this.Properties = new ServicePropertiesBE(dataAccessObject.properties);

            if (dataAccessObject.documents != null)
            {
                this.Documents = Array.ConvertAll(
                    dataAccessObject.documents,
                    d => this.TranslateDocumentToServiceDocumentBE(d));

                this.Documents = this.Documents.Where(o => o != null).ToArray();
            }

            this.WeightIntervals = dataAccessObject.weightIntervals;
            this.Price = dataAccessObject.price != null ? new PriceBE(dataAccessObject.price) : new PriceBE();
            this.AdditionalInfoText = new AdditionalInfoTextBE(dataAccessObject.additionalInfo);
            this.MinWeight = dataAccessObject.minWeight;
            this.MaxWeight = dataAccessObject.maxWeight;
        }

        private AdditionalServiceBE TranslateAdditionalServiceToAdditionalServiceBE(AdditionalService additionalService)
        {
            try
            {
                return new AdditionalServiceBE(additionalService);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        private ServiceDocumentBE TranslateDocumentToServiceDocumentBE(Document document)
        {
            try
            {
                return new ServiceDocumentBE(document);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}