@Echo off
Rem Set the Host variable according to environment
Rem Set Host=f10.service.utv.posten.se
Rem Host=i10.service.utv.posten.se

Rem Path to executable
Set SvcUtilExe="c:\program files (x86)\Microsoft SDKs\Windows\v7.0A\bin\SvcUtil.exe"

Echo *******************************************
Echo Creates proxy for SkickaService/ItemNotesServiceBean
Echo Server: %Host%
Echo *******************************************
Pause

%SvcUtilExe% ItemNotesService_v0200_1.wsdl /out:..\SkickaServiceProxy.cs /config:..\SkickaServiceProxy.config /namespace:*,Posten.Portal.Skicka.SkickaService.Proxy /serializer:XmlSerializer
Pause

Rem NOTE! This file must be saved with encoding CodePage 1252 (not UTF-8!)
