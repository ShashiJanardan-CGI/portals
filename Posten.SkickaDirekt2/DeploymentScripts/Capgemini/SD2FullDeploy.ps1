# Get Start Time
$startDTM = (Get-Date)

.\LoadConfig.ps1
. .\Functions.ps1

$webAppName = $settings["webApplicationName"]
$WebApplicationUrl = $settings["webApplicationUrl"]

Write-Host "*******Cleaning Skicka2 Packages from the farm *****************"
Reset-IISOnServers
RetractSolution "Posten.SkickaDirekt2.wsp"
#RetractSolution "Posten.Portal.Skicka.Services.wsp"
Reset-IISOnServers $True


Write-Host "*******Deploying Skicka2 Packages on the farm *****************"
Reset-IISOnServers
DeploySolution "Posten.SkickaDirekt2.wsp" $webAppName
Reset-IISOnServers $True

Write-Host "*******Activating web.config features on web application:" $webAppName "*****************"

#Write-Host "Please place your target file (named 'templateWeb.config'): templateweb.config in the 14-hive folder: template/layouts/Posten.SkickaDirekt2/XML/Config on all of the WFE servers on the farm before the script continues!" -foregroundcolor "magenta"
#$modifer = [ConsoleModifiers]::Control
#Pause "G" $modifer "Ctrl + G" $true 

Reset-IISOnServers $True

echo "----------------------------------------------------------------------------"
echo "Activating Feature: Skicka2 Web.Config features"
echo "----------------------------------------------------------------------------"
Enable-SPFeature "3a51330d-d26c-4640-8ef6-efffc5957bd4"  -url $WebApplicationUrl

	
Write-Host "*******Setting up Skicka2 Site*****************"
.\SD2SiteSetUp.ps1

