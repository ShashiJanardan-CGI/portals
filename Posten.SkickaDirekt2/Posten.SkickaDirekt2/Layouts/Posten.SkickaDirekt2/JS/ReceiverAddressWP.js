﻿function receiverType(evt) {
    var val = $("input[name=receiverType]:checked").val();
    if (val == "company") {
        if ($("#receiverCompanyNameDisplay").hasClass("noneDisplay")) {
            $("#receiverCompanyNameDisplay").removeClass("noneDisplay");
        }
    } else {
        if (!$("#receiverCompanyNameDisplay").hasClass("noneDisplay")) {
            $("#receiverCompanyNameDisplay").addClass("noneDisplay");
        }
    }
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    try{
        $("input[name=receiverType]:radio").change(receiverType);
        receiverType();

        ko.applyBindings(window.skickaReceiverVM, document.getElementById('ReceiverAddressDisplay'));
    }
    catch (e) {
        console.log("ReceiverAddressWP failed: " + e.message);
    }
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaReceiverVM = (function () {
    var vm = {};

    /* --------------------------------- Observables ------------------------------------------*/
    vm.ReceiverAddress = ko.observable(new AddressBE(false)).publishOn("ReceiverAddress");

    vm.ReceiverAddressValidationErrors = ko.validatedObservable({
        ReceiverName: vm.ReceiverAddress().Name,
        ReceiverCompanyName: vm.ReceiverAddress().CompanyName,
        ReceiverAddress: vm.ReceiverAddress().AddressField1,
        ReceiverAddress2: vm.ReceiverAddress().AddressField2,
        ReceiverZipCode: vm.ReceiverAddress().ZipCode,
        ReceiverCity: vm.ReceiverAddress().City,
        ReceiverEmail: vm.ReceiverAddress().Email,
        ReceiverEmail2: vm.ReceiverAddress().Email2,
        ReceiverMobilePhoneCountryPrefix: vm.ReceiverAddress().MobilePhoneCountryPrefix,
        ReceiverMobilePhoneNumber: vm.ReceiverAddress().MobilePhoneNumber,
        ReceiverTelephoneCountryPrefix: vm.ReceiverAddress().TelephoneCountryPrefix,
        ReceiverTelephoneNumber: vm.ReceiverAddress().TelephoneNumber,
        ReceiverEntryCode: vm.ReceiverAddress().EntryCode
    });

    vm.ValidateAddress = ko.observable(false);
    vm.InvalidZipMsg = ko.observable(false);
    vm.ShowReceiverZipProgress = ko.observable(false);

    vm.ShowMobilePrefix = ko.computed(function () {
        try{
            if (vm.ReceiverAddress().MobilePhoneCountryPrefix() !== undefined && vm.ReceiverAddress().MobilePhoneCountryPrefix() !== "") {
                return (!vm.ReceiverAddress().MobilePhoneCountryPrefix().isValid && vm.ValidateAddress());
            }
            else {
                return false;
            }
            return true;
        }
        catch (e)
        {
            console.log(e.message);
            return false;
        }
    }, vm);

    /* --------------------------------- Subscribers ------------------------------------------*/
    vm.SelectedDestination = ko.observable().subscribeTo("SelectedDestination");
    vm.SelectedProductSuggestion = ko.observable().subscribeTo("SelectedProductSuggestion");
    vm.ReceiverAddressValidationErrors.isValid.publishOn("ReceiverAddressIsValid");
   
    ko.postbox.subscribe("IsShowProposalsClicked", function () {
        vm.ReceiverAddress().Country(vm.SelectedDestination().DestinationID);
        if (vm.SelectedDestination().ISO == "SE") {
            vm.ReceiverAddress().TelephoneCountryPrefix("46");
        }
        else {
            vm.ReceiverAddress().TelephoneCountryPrefix(undefined);
        }
    });

    ko.postbox.subscribe("IsAddToCartClicked", function () {
        EnableValidation();
    });

    ko.postbox.subscribe("ConsignmentToEdit", function (consignment) {
        
        var destinationID = consignment.DestinationID;
        var receiverAddressObject = extractAddress(consignment.ToAddress, destinationID);
        vm.ReceiverAddress().AddressField1(receiverAddressObject.AddressField1);
        vm.ReceiverAddress().AddressField2(receiverAddressObject.AddressField2);
        vm.ReceiverAddress().ZipCode(receiverAddressObject.ZipCode);               
        vm.ReceiverAddress().CompanyName(receiverAddressObject.CompanyName);
        vm.ReceiverAddress().Name(receiverAddressObject.Name);
        vm.ReceiverAddress().Email(receiverAddressObject.Email);
        vm.ReceiverAddress().Email2(receiverAddressObject.Email);
        vm.ReceiverAddress().MobilePhoneCountryPrefix(receiverAddressObject.MobilePhoneCountryPrefix);
        vm.ReceiverAddress().MobilePhoneNumber(receiverAddressObject.MobilePhoneNumber);
        vm.ReceiverAddress().TelephoneCountryPrefix(receiverAddressObject.TelephoneCountryPrefix);
        if (vm.SelectedDestination().ISO == "SE") {
            vm.ReceiverAddress().TelephoneCountryPrefix("46");
        };
        vm.ReceiverAddress().TelephoneNumber(receiverAddressObject.TelephoneNumber);        
        if (receiverAddressObject.EntryCode !== undefined) {
            vm.ReceiverAddress().EntryCode(receiverAddressObject.EntryCode);
        }
        vm.InvalidZipMsg(false);
        vm.ReceiverAddress().City(receiverAddressObject.City);
        vm.ReceiverAddress().Country(vm.SelectedDestination().DestinationID);
    });

    /* ------------------------------- Private methods  --------------------------------------------*/
    var EnableValidation = function () {
        vm.ValidateAddress(true);

        vm.ReceiverAddress().AddressField1.valueHasMutated();
        vm.ReceiverAddress().AddressField2.valueHasMutated();
        vm.ReceiverAddress().City.valueHasMutated();
        vm.ReceiverAddress().CompanyName.valueHasMutated();
        vm.ReceiverAddress().Name.valueHasMutated();
        vm.ReceiverAddress().Email.valueHasMutated();
        vm.ReceiverAddress().Email2.valueHasMutated();
        vm.ReceiverAddress().MobilePhoneCountryPrefix.valueHasMutated();
        vm.ReceiverAddress().MobilePhoneNumber.valueHasMutated();
        vm.ReceiverAddress().TelephoneCountryPrefix.valueHasMutated();
        vm.ReceiverAddress().TelephoneNumber.valueHasMutated();
        vm.ReceiverAddress().ZipCode.valueHasMutated();
        vm.ReceiverAddress().EntryCode.valueHasMutated();
    };

    var SetBasicAddressValidation = function () {
        vm.ReceiverAddress().Name.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && !vm.ReceiverAddress().IsCompany(); } }
        });
        vm.ReceiverAddress().CompanyName.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress() && vm.ReceiverAddress().IsCompany(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.ReceiverAddress().IsCompany(); } }
        });
        vm.ReceiverAddress().AddressField1.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && !vm.ReceiverAddress().IsCompany(); } }
        });
        vm.ReceiverAddress().AddressField2.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 }
        });
        vm.ReceiverAddress().ZipCode.extend({
            required: { onlyIf: function () { return vm.ValidateAddress() } },
            digit: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedDestination() && vm.SelectedDestination().ZipCodeDigitsSpecified() } },
            minLengthFunction: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedDestination() && vm.SelectedDestination().ZipCodeDigitsSpecified() }, params: function () { return vm.SelectedDestination().ZipCodeDigits() } },
            maxLengthFunction: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedDestination() && vm.SelectedDestination().ZipCodeDigitsSpecified() }, params: function () { return vm.SelectedDestination().ZipCodeDigits() } }
        });
        vm.ReceiverAddress().City.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 20 }
        });
        vm.ReceiverAddress().Email.extend({
            email: { onlyIf: function () { return vm.ValidateAddress() } },
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 63 }
        });
        vm.ReceiverAddress().Email2.extend({
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.ReceiverAddress().Email(); } },
            equal: { onlyIf: function () { return vm.ValidateAddress() }, params: vm.ReceiverAddress().Email }
        });
        vm.ReceiverAddress().City.extend({
            required: { onlyIf: function () { return vm.ValidateAddress(); } }
        });
    };

    var ClearAddressFieldsValidation = function () {
        skickaCore.RemoveRules(vm.ReceiverAddress().AddressField1);
        skickaCore.RemoveRules(vm.ReceiverAddress().City);
        skickaCore.RemoveRules(vm.ReceiverAddress().CompanyName);
        skickaCore.RemoveRules(vm.ReceiverAddress().Name);
        skickaCore.RemoveRules(vm.ReceiverAddress().Email);
        skickaCore.RemoveRules(vm.ReceiverAddress().Email2);
        skickaCore.RemoveRules(vm.ReceiverAddress().MobilePhoneCountryPrefix);
        skickaCore.RemoveRules(vm.ReceiverAddress().MobilePhoneNumber);
        skickaCore.RemoveRules(vm.ReceiverAddress().TelephoneCountryPrefix);
        skickaCore.RemoveRules(vm.ReceiverAddress().TelephoneNumber);
        skickaCore.RemoveRules(vm.ReceiverAddress().ZipCode);
        skickaCore.RemoveRules(vm.ReceiverAddress().EntryCode);
    };

    var SetRequiredNotificationFields = function () {
        vm.ReceiverAddress().Email.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireEmail(); } } });
        vm.ReceiverAddress().MobilePhoneNumber.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireMobile(); } } });
        vm.ReceiverAddress().MobilePhoneCountryPrefix.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireMobile(); } } });
        vm.ReceiverAddress().TelephoneCountryPrefix.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireTelephone(); } } });
        vm.ReceiverAddress().TelephoneNumber.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireTelephone(); } } });
        vm.ReceiverAddress().EntryCode.extend({ required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.RequireEntryCode(); } } });
    };

    var SetPhoneValidation = function () {
        // Domestic phone numbers        
        vm.ReceiverAddress().MobilePhoneNumber.extend({ mobileSE: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && !vm.SelectedProductSuggestion().Product.International && vm.SelectedProductSuggestion().Product.ShowMobile(); } } });
        // International phone numbers
        vm.ReceiverAddress().TelephoneCountryPrefix.extend({ phonePrefix: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.International && vm.SelectedProductSuggestion().Product.ShowTelephone() && vm.SelectedDestination().ISO !== "SE"; } } });
        vm.ReceiverAddress().TelephoneNumber.extend({ telSE: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.International && vm.SelectedProductSuggestion().Product.ShowTelephone(); } } });
        vm.ReceiverAddress().MobilePhoneCountryPrefix.extend({ phonePrefix: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.International && vm.SelectedProductSuggestion().Product.ShowMobile(); } } });
        vm.ReceiverAddress().MobilePhoneNumber.extend({ phoneExclPrefix: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.International && vm.SelectedProductSuggestion().Product.ShowMobile(); } } });
    };

    var SetCityValidation = function () {
        vm.ReceiverAddress().City.extend({
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion() && vm.SelectedProductSuggestion().Product.International } }
        });
    };

    var SetAddressValidation = function () {
        SetBasicAddressValidation();
        SetRequiredNotificationFields();
        SetPhoneValidation();
        SetCityValidation();
    };

    /* ------------------------------- Public methods  --------------------------------------------*/
    vm.ResetReceiverAddress = function () {
        vm.ValidateAddress(false);
       
        vm.ReceiverAddress().IsCompany(false);
        vm.ReceiverAddress().Name(undefined);
        vm.ReceiverAddress().CompanyName(undefined);
        vm.ReceiverAddress().OrganizationNumber(undefined);
        vm.ReceiverAddress().AddressField1(undefined);
        vm.ReceiverAddress().AddressField2(undefined);
        vm.ReceiverAddress().ZipCode(undefined);
        vm.ReceiverAddress().City(undefined);
        vm.ReceiverAddress().Email(undefined);
        vm.ReceiverAddress().Email2(undefined);
        vm.ReceiverAddress().MobilePhoneCountryPrefix(undefined);
        vm.ReceiverAddress().MobilePhoneNumber("");
        vm.ReceiverAddress().TelephoneCountryPrefix(undefined);
        if (vm.SelectedDestination().ISO == "SE") {
            vm.ReceiverAddress().TelephoneCountryPrefix("46");
        }
        vm.ReceiverAddress().TelephoneNumber("");
        vm.ReceiverAddress().EntryCode(undefined);
        vm.ReceiverAddress().Mobile = "";
        vm.ReceiverAddress().Phone = "";
        
        vm.InvalidZipMsg(false);

    };

    
    /* ----------------------------Subscriptions -------------------------------------------------*/

    vm.ReceiverAddress().ZipCode.subscribe(function (zipCode) {
        vm.ShowReceiverZipProgress(true);
        if (vm.SelectedProductSuggestion() && !vm.SelectedProductSuggestion().Product.International) {
            if (vm.ReceiverAddress().ZipCode.isValid()) {
                ZipToCity(zipCode, vm.ReceiverAddress().City, vm.InvalidZipMsg, vm.ShowReceiverZipProgress);
            }
            else {
                vm.ReceiverAddress().City("");
                vm.ShowReceiverZipProgress(false);
            }
        } else {
            vm.ShowReceiverZipProgress(false);
        }
    }.bind(this));

    vm.SelectedProductSuggestion.subscribe(function (productSuggestion) {
        vm.ValidateAddress(false);
    });

    var init = (function () {
        SetAddressValidation()  
    })()

    return vm;
})();


/* ----------------------------General Functions -------------------------------------------------*/
