﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    ko.applyBindings(window.skickaCustomsDocumentsVM, document.getElementById('CustomsDocumentsDisplay'));
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaCustomsDocumentsVM = (function () {
    var vm = {};

    /* --------------------------------- Constants and initial values ------------------------------------------*/


    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.SelectedProduct = ko.observable().subscribeTo("SelectedProduct");

    
    /* --------------------------------- Observables ------------------------------------------*/
    

    /* ----------------------------Validation observables -------------------------------------*/
   

    /* ------------------------------- Private methods  --------------------------------------------*/
    

    /* ----------------------------Subscriptions -------------------------------------------------*/
   
    return vm;
})();
