﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Web service calls ---------------------------*/
/* -------------------------------------------------------------------------------------*/

function GetProductSuggestions(countryCode, measurements, weight, filters, callback, showProposalsProgress) {
    // Show progress image
    showProposalsProgress(true);
    // create json string
    var myData = JSON.stringify(
            {
                countryCode: countryCode,
                measurements: measurements,
                weight: weight,
                filters: filters
            });

    // call webservice
    $.ajax({
        type: "POST",
        contentType: "application/json",
        async: true,
        url: serverRelativeUrl + servicePath + "/GetProductSuggestions",
        data: myData,
        cache: false,
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
        success: function (data) {
            var obj = $.parseJSON(data.GetProductSuggestionsResult);
            
            // Hide progress image
            showProposalsProgress(false);
            callback(obj);
        },
        error: function (data) {
            console.log(data);
            showProposalsProgress(false);
        }
    });
}

function GetPreferredProduct(productId, destinationId, measurements, weight, callback, showProposalsProgress) {
    // Show progress image
    showProposalsProgress(true);
    // create json string
    var myData = JSON.stringify(
           {
               productId: productId,
               destinationID: destinationId,
               measurements: measurements,
               weight: weight
           });

    // call webservice  
    $.ajax({
        type: "POST",
        contentType: "application/json",
        async: false,
        url: serverRelativeUrl + servicePath + "/GetPreferredProduct",
        data: myData,
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
        success: function (data) {
            var obj = $.parseJSON(data.GetPreferredProductResult);
            
            // Hide progress image
            showProposalsProgress(false);
            callback(obj);
        },
        error: function (data) {
            console.log(data);
            showProposalsProgress(false);
        }
    });
}

var dd;
function GetDestinations(callback, destinationsProgress) {
    // Show progress image
    console.log(returnSessionValue(logIDKey));
    destinationsProgress(true);
    $.ajax({
        type: "GET",        
        contentType: "application/json; charset=utf-8",
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },        
        url: serverRelativeUrl + servicePath + "/GetDestinations",
        dataType: "json",
        success: function (data) {
            var response = $.parseJSON(data.GetDestinationsResult);
            dd = response;
            // Hide progress image
            destinationsProgress(false);
            callback(response);
        },
        error: function (data) {
            console.log(data);
            console.log("destination error");
        }
    });
}

function GetProducts(countryCode, callback, servicesProgress) {
    // Show progress image
    servicesProgress(true);
    $.ajax({
        type: "GET",
        cache: false,
        contentType: "application/json; charset=utf-8",
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
        async: true,
        url: serverRelativeUrl + servicePath + "/GetProducts?countryCode=" + countryCode,
        dataType: "json",
        success: function (data) {
            var serviceObjs = $.parseJSON(data.GetProductsResult);
            // Hide progress image
            servicesProgress(false);
            callback(serviceObjs);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    try{
        ko.applyBindings(window.skickaSelectServiceVM, document.getElementById('SelectServiceDisplay'));
        registerGAC();
        intializeWeight();
    }
    catch (e) {
        console.log("skickaSelectServiceVM is broken: " + e.message);
    }
});

var intializeWeight = function(){
    $("#" + returnObjectID("gRadio")).prop('checked', true);
    skickaSelectServiceVM.CurrentWeight().Multiplier('1');

    var weightRadios = $('input[name=weightGroup]');
    $.each(weightRadios, function (index, radio) {
        if (radio.value == skickaSelectServiceVM.CurrentWeight().WeightInGrams()) {
            radio.checked = true;
        }
    });
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- MODEL ---------------------------------------*/
/* -------------------------------------------------------------------------------------*/

var AdditionalInfoTextsBE = (function () {
    function additionalInfoTexts(Delivery, DeliveryTime, GoodsValue, IdRequired, MaxWeight, Receiver, Size, Submission, Traceable) {
        this.Delivery = Delivery;
        this.DeliveryTime = DeliveryTime;
        this.GoodsValue = GoodsValue;
        this.IdRequired = IdRequired;
        this.MaxWeight = MaxWeight;
        this.Receiver = Receiver;
        this.Size = Size;
        this.Submission = Submission;
        this.Traceable = Traceable;
    }

    return additionalInfoTexts;
})();

var CountryBE = (function () {
    function destination(DestinationID, EU, Europe, Information, ISO, Name, ZipCodeDigits, ZipCodeDigitsSpecified) {
        this.DestinationID = DestinationID;
        this.EU = EU;
        this.Europe = Europe;
        this.Information = Information;
        this.ISO = ISO;
        this.Name = Name;
        this.ZipCodeDigits = ko.observable(ZipCodeDigits);
        this.ZipCodeDigitsSpecified = ko.observable(ZipCodeDigitsSpecified);
    };

    function informationLink(DestinationID) {
        return "http://services3.posten.se/ptm/ptm_do.jsp?country=" + DestinationID + "&action=searchCountry";
    }
    return destination;
})();

var sp;
var ProductDefinitionBE = (function () {
    function productDefinition(ProductId, DestinationId, Code, Name, WeightIntervals, MinSize, MaxSize, MinWeight, MaxWeight, ShowRoll, AdditionalServices, ServiceDocuments, Price, Description, AdditionalInfoTexts, ServiceProperties, International, CodMaxAmount, IsActive) {
        var self = this;
        sp = ServiceProperties;
        self.ProductId = ProductId;
        self.DestinationId = DestinationId;
        self.Code = Code;
        self.Name = Name;
        self.WeightIntervals = ko.observableArray(WeightIntervals);
        self.MinSize = MinSize;
        self.MaxSize = MaxSize;
        self.MinWeight = MinWeight;
        self.MaxWeight = MaxWeight;
        self.ShowRoll = ShowRoll;
        self.AdditionalServices = ko.observableArray(AdditionalServices);
        self.ServiceDocuments = ServiceDocuments;
        self.Price = ko.observable(Price);
        self.Description = Description;
        self.AdditionalInfoTexts = AdditionalInfoTexts;
        self.ServiceProperties = ServiceProperties;
        self.International = International;
        self.CodMaxAmount = CodMaxAmount;
        self.IsActive = IsActive;

        self.ShowEmail = ko.pureComputed(function () { return self.ServiceProperties.MailNotification == 1 || self.ServiceProperties.MailNotification == 2 }, self);
        self.ShowSenderEmail = ko.pureComputed(function () { return self.ServiceProperties.SenderEmail == 1 || self.ServiceProperties.SenderEmail == 2 }, self);
        self.RequireSenderEmail = ko.pureComputed(function () { return self.ServiceProperties.SenderEmail == 2 }, self);
        self.RequireEmail = ko.pureComputed(function () { return self.ServiceProperties.MailNotification == 2 }, self);
        self.ShowMobile = ko.pureComputed(function () { return self.ServiceProperties.SMSNotification == 1 || self.ServiceProperties.SMSNotification == 2 }, self);
        self.RequireMobile = ko.pureComputed(function () { return self.ServiceProperties.SMSNotification == 2 }, self);
        self.ShowTelephone = ko.pureComputed(function () { return self.ServiceProperties.PhoneNotification == 1 || self.ServiceProperties.PhoneNotification == 2 }, self);
        self.RequireTelephone = ko.pureComputed(function () { return self.ServiceProperties.PhoneNotification == 2 }, self);
        self.ShowEntryCode = ko.pureComputed(function () { return self.ServiceProperties.DoorCode == 1 || self.ServiceProperties.DoorCode == 2 }, self);
        self.RequireEntryCode = ko.pureComputed(function () { return self.ServiceProperties.DoorCode == 2 }, self);
    }

    return productDefinition;
})();

var ServiceDocumentBE = (function () {
    function serviceDocument(DocumentTypeName, Name, Url) {
        this.DocumentTypeName = DocumentTypeName;
        this.Name = Name;
        this.Url = Url;
    }

    return serviceDocument;
})();

var ServicePropertiesBE = (function () {
    function serviceProperties(DoorCode, FilterCOD, FilterDelivery, FilterExpress, FilterTrace, FilterValue, LetterAllowed, MailNotification, PhoneNotification, SMSNotification, Vat, SenderEmail) {
        this.DoorCode = DoorCode,
        this.FilterCOD = FilterCOD,
        this.FilterDelivery = FilterDelivery,
        this.FilterExpress = FilterExpress,
        this.FilterTrace = FilterTrace,
        this.FilterValue = FilterValue,
        this.LetterAllowed = LetterAllowed,
        this.MailNotification = MailNotification,
        this.PhoneNotification = PhoneNotification,
        this.SMSNotification = SMSNotification,
        this.SenderEmail = SenderEmail,
        this.Vat = Vat
    }

    return serviceProperties;
})();

var SizeBE = (function () {
    function size(Height, Length, Width, Diameter, IsRollData, RollLength) {
        this.IsRoll = ko.observable(IsRollData);
        this.Height = ko.observable(Height);
        this.Length = ko.observable(Length);
        this.Width = ko.observable(Width);
        this.Diameter = ko.observable(Diameter);
        this.RollLength = RollLength;
    };

    return size;
})();

var TextBE = (function () {
    function text(LabelKey, LabelName, LabelText) {
        this.LabelKey = LabelKey;
        this.LabelName = LabelName;
        this.LabelText = LabelText;
    }

    return text;
})();

var WeightBE = (function () {
    function weight(value, displayUnit, displayValue, weightInGrams, multiplier) {

        var self = this;        
        self.Value = ko.observable(value);
        self.DisplayUnit = ko.observable(displayUnit);
        self.DisplayValue = ko.observable(displayValue);
        self.WeightInGrams = ko.observable(weightInGrams);
        self.Multiplier = ko.observable(multiplier);

        self.CalculatedWeight = ko.computed(function () {
            var calculatedValue = self.Value();
            if (typeof (calculatedValue) == 'string') {
                calculatedValue = calculatedValue.replace(/\,/g, '.');
            }
            var returnValue;        
            returnValue = calculatedValue * self.Multiplier();           
            return returnValue;
        }, this);

        self.RadioText = ko.computed(function () {
            var returnVal = "";
            if (displayValue >= 1000) {
                displayValue = displayValue / 1000;
                displayUnit = 'kg';
            }            
            returnVal = displayValue + ' ' + displayUnit;            
            return returnVal;
        }, this);
    };

    function CalculateValue(displayUnit, displayValue) {
        if (displayUnit == 'g' || displayUnit == undefined) { //default should be considered as 'g'
            this.Value = displayValue;
            this.WeightInGrams = displayVlaue;
            this.Multiplier = 1;
            this.DisplayUnit = displayUnit;
            this.DisplayValue = displayValue;
        }
        else if (displayUnit == 'kg') {
            this.Value = displayValue;
            this.WeightInGrams = displayVlaue * 1000;
            this.Multiplier = 1000;
            this.DisplayUnit = displayUnit;
            this.DisplayValue = displayValue;
        }
    }

    return weight;
})();

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaSelectServiceVM = (function () {
    var vm = {};

    var consignmentData = undefined;

    /* --------------------------------- Constants and initial values ------------------------------------------*/
    var IsServiceGuideSelectedInitial = false;

    var FilterStates = {
        FAST: 0,
        SAFE: 1,
        TRACEABLE: 2
    };

    var ClearSuggestedProductsStates = {
        CLEARDATA: true,
        DONOTCLEAR: false
    };

    var DoClearSuggestedProducts = ClearSuggestedProductsStates.CLEARDATA;

    /* --------------------------------- Observables ------------------------------------------*/
    var IsShoppingCartEditMode = ko.observable(false).subscribeTo("IsShoppingCartEditMode", true);

    vm.ValidateProduct = ko.observable(false);
    vm.Destinations = ko.observableArray([]);
    vm.CountryInformationLink = ko.observable(countryInfoLink);
    vm.TypeOfSize = ko.observable('own');
    vm.CurrentWeight = ko.observable(new WeightBE(undefined, undefined, undefined, undefined, undefined));
    vm.Filters = [FilterStates.FAST, FilterStates.SAFE, FilterStates.TRACEABLE];
    vm.SelectedFilters = ko.observableArray([]);
    vm.C4 = new SizeBE("0,1", "22,9", "32,4", "0", false);
    vm.C5 = new SizeBE("0,1", "16,2", "22,9", "0", false);
    vm.ClearSize = new SizeBE(undefined, undefined, undefined, undefined, false);

    vm.CurrentSize = ko.observable(new SizeBE(undefined, undefined, undefined, undefined, false)).publishOn("CurrentSize");

    vm.LengthBoundaries = ko.pureComputed(function () {
        if (vm.CurrentSize().IsRoll()) {
            return vm.GetBounderies(vm.SelectedProduct().MinSize.RollLength, vm.SelectedProduct().MaxSize.RollLength, 'cm', true);
        }
        else {
            return vm.GetBounderies(vm.SelectedProduct().MinSize.Length(), vm.SelectedProduct().MaxSize.Length(), 'cm', true);
        }
    });
    vm.WidthBoundaries = ko.pureComputed(function () {
        return vm.GetBounderies(vm.SelectedProduct().MinSize.Width(), vm.SelectedProduct().MaxSize.Width(), 'cm', true);
    });
    vm.HeightBoundaries = ko.pureComputed(function () {
        return vm.GetBounderies(vm.SelectedProduct().MinSize.Height(), vm.SelectedProduct().MaxSize.Height(), 'cm', true);
    });
    vm.DiameterBoundaries = ko.pureComputed(function () {
        return vm.GetBounderies(vm.SelectedProduct().MinSize.Diameter(), vm.SelectedProduct().MaxSize.Diameter(), 'cm', true);
    });
    vm.WeightBoundaries = ko.pureComputed(function () {
        return vm.GetWeightBounderies(vm.SelectedProduct().MinWeight, vm.SelectedProduct().MaxWeight);
    });

    vm.DestinationsProgress = ko.observable(false);
    vm.ServicesProgress = ko.observable(false);

    /* ----------------------------Validation observables -------------------------------------*/
    vm.WeightValidationErrors = ko.validatedObservable({
        Weight: vm.CurrentWeight().Value,
        CalculatedWeight: vm.CurrentWeight().CalculatedWeight
    });

    vm.SizeValidationErrors = ko.validatedObservable({
        Length: vm.CurrentSize().Length,
        Width: vm.CurrentSize().Width,
        Height: vm.CurrentSize().Height,
        Diameter: vm.CurrentSize().Diameter,
    });

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.SelectedProduct = ko.observable().syncWith("SelectedProduct");

    vm.ServiceError = ko.observable(new ErrorBE(true, false, "")).syncWith("ServiceError");

    vm.ShowProposalsProgress = ko.observable().publishOn("ShowProposalsProgress");

    vm.ProductSuggestionsScrollTo = ko.observable().syncWith("ProductsScrollTo");

    vm.IsServiceGuideSelected = ko.observable(IsServiceGuideSelectedInitial).publishOn("IsServiceGuideSelected");
    vm.IsServiceTypeSwitched = ko.observable(false).publishOn("IsServiceTypeSwitched");
    vm.SizeValidationErrors.isValid.publishOn("SizeValidationErrorsIsValid");

    vm.WeightValidationErrors.isValid.publishOn("WeightValidationErrorsIsValid");
    vm.IsWeightValid = ko.pureComputed(
            function () { return true; }, this)
        .publishOn("IsWeightRadioValid");

    vm.Products = ko.observableArray([]).publishOn("Products");

    vm.SelectedDestination = ko.observable().publishOn("SelectedDestination");
        
    vm.IsValidSelectService = ko.pureComputed(
            function () { return vm.SizeValidationErrors.isValid() && vm.WeightValidationErrors.isValid() }, this)
        .publishOn("IsValidSelectService");

    // Do not publish initial value, always publish on change
    vm.ProductSuggestions = ko.observable(undefined).publishOn("ProductSuggestions", true, function() {
        return false;
    });

    ko.postbox.subscribe("IsShowProposalsClicked", function () {
        vm.ProductSuggestions(null);
        CallGetProductSuggestions();                 
    });

    ko.postbox.subscribe("SelectProductSelectionClicked", function (selectedProductSuggestion) {
        vm.SelectedProduct(selectedProductSuggestion.Product);
        vm.IsServiceGuideSelected(false);
        vm.ProductSuggestions(selectedProductSuggestion);
    });

    ko.postbox.subscribe("IsResetClicked", function (newValue) {
        console.log("reset clicked");
        vm.ProductSuggestions(null);
        GetDestinations(HandleGetDestinationsResponse, vm.DestinationsProgress);

        vm.SetCurrentSize(vm.ClearSize);
        vm.TypeOfSize('own');
        vm.CurrentWeight().Value(undefined);
        vm.SelectedProduct(null);
        vm.SelectedFilters([]);
    });

    ko.postbox.subscribe("ConsignmentToEdit", function (consignment) {
        consignmentData = consignment;
        
        dd = vm.Destinations();
        $.each(vm.Destinations(), function (index, value) {
            if (value.ISO == consignment.DestinationID)
            {
                vm.SelectedDestination(value);
            }
        });
    });

    
    /* ------------------------------- Private methods  --------------------------------------------*/
    var CallGetProductSuggestions = function () {
        var validation = true;
        vm.ValidateProduct(true);

        vm.WeightValidationErrors.isValid.publishOn("WeightValidationErrorsIsValid");
        if (!vm.SizeValidationErrors.isValid() || !vm.ServiceError().IsValid) {
            vm.SizeValidationErrors.errors.showAllMessages();
            validation = false;
        }
        if(!vm.WeightValidationErrors.isValid() || !vm.ServiceError().IsValid)
        {
            vm.WeightValidationErrors.isValid.publishOn("WeightValidationErrorsIsValid");
            vm.WeightValidationErrors.errors.showAllMessages();
            validation = false;
        }
        if(validation){
            var size = ko.toJS(vm.CurrentSize);
            size.Length = size.Length ? CmStringToMmString(size.Length) : size.Length;
            if (vm.CurrentSize().IsRoll()) {
                size.Width = undefined;
                size.Height = undefined;
                size.Diameter = size.Diameter ? CmStringToMmString(size.Diameter) : size.Diameter;
            }
            else {
                size.Width = size.Width ? CmStringToMmString(size.Width) : size.Width;
                size.Height = size.Height ? CmStringToMmString(size.Height) : size.Height;
                size.Diameter = undefined;
            }

            if (vm.SelectedProduct()) {
                vm.ShowProposalsProgress(true);
                GetPreferredProduct(
                        vm.SelectedProduct().ProductId,
                        vm.SelectedDestination().DestinationID,
                        size,
                        vm.CurrentWeight().CalculatedWeight(),
                        HandleGetProductResponse,
                        vm.ShowProposalsProgress);
            }
            else {
                GetProductSuggestions(
                    vm.SelectedDestination().DestinationID,
                    size,
                    vm.CurrentWeight().CalculatedWeight(),
                    ko.toJS(vm.SelectedFilters),
                    HandleGetProductResponse,
                    vm.ShowProposalsProgress);
            }
        }
    }

    var HandleGetProductResponse = function (response) {
        if (response.Status == ResponseStatus.SUCCESS) {
            vm.ProductSuggestions(response.ProductSuggestions);
            vm.ProductSuggestionsScrollTo(true);
            vm.ProductSuggestionsScrollTo(false);
            vm.ValidateProduct(false);
        }
        else {
            skickaCore.HandleServiceError(response);
        }
    };

    var HandleGetDestinationsResponse = function (destinationsResponse) {
        if (destinationsResponse.Status == ResponseStatus.SUCCESS) {
            vm.ServiceError().IsValid = true;

            var destinations = [];
            $.each(destinationsResponse.Destinations, function (index, destinationObject) {
                var destination = new CountryBE(destinationObject.DestinationID,
                                       destinationObject.EU,
                                       destinationObject.Europe,
                                       destinationObject.Information,
                                       destinationObject.ISO,
                                       destinationObject.Name,
                                       destinationObject.ZipCodeDigits,
                                       destinationObject.ZipCodeDigitsSpecified);

                destinations.push(destination);
            });

            vm.Destinations(destinations);
        }
        else {
            skickaCore.HandleServiceError(destinationsResponse);
        }
    };

    
    var HandleGetProducts = function (response) {
        if (response.Status == ResponseStatus.SUCCESS) {
            vm.ServiceError().IsValid = true;
            vm.Products([]);
            $.each(response.Products, function (productObjectsIndex, productObject) {
                
                var weightBEs = [];
                $.each(productObject.Weights, function (index, value) {
                    var newWeightBE = new WeightBE(undefined, value.DisplayUnit, value.DisplayValue, value.WeightInGrams, undefined);
                    weightBEs.push(newWeightBE);
                });

                var additionalServiceBEs = [];
                $.each(productObject.AdditionalServices, function (index, value) {
                    var newPriceBE = new PriceBE(value.Price.Amount, value.Price.Vat, value.Price.AmountNoVat, value.Price.Currency, value.Price.VatCode, value.Price.VATAmount);//MVU7734 - S
                    var newAdditionalServiceBE = new AdditionalServiceBE(value.AdditionalServiceCode, value.Name, value.PVTCode, newPriceBE);
                    additionalServiceBEs.push(newAdditionalServiceBE);
                });

                var serviceDocumentsBEs = [];
                if (productObject.Documents) {
                    $.each(productObject.Documents, function (index, value) {
                        var newServiceDocumentBE = new ServiceDocumentBE(value.DocumentTypeName, value.Name, value.Url);
                        serviceDocumentsBEs.push(newServiceDocumentBE);
                    });
                };

                var additionalInfoText = new AdditionalInfoTextsBE(
                    new TextBE(productObject.AdditionalInfoText.Delivery.LabelKey, productObject.AdditionalInfoText.Delivery.LabelName, productObject.AdditionalInfoText.Delivery.Text),
                    new TextBE(productObject.AdditionalInfoText.DeliveryTime.LabelKey, productObject.AdditionalInfoText.DeliveryTime.LabelName, productObject.AdditionalInfoText.DeliveryTime.Text),
                    new TextBE(productObject.AdditionalInfoText.GoodsValue.LabelKey, productObject.AdditionalInfoText.GoodsValue.LabelName, productObject.AdditionalInfoText.GoodsValue.Text),
                    new TextBE(productObject.AdditionalInfoText.IdRequired.LabelKey, productObject.AdditionalInfoText.IdRequired.LabelName, productObject.AdditionalInfoText.IdRequired.Text),
                    new TextBE(productObject.AdditionalInfoText.MaxWeight.LabelKey, productObject.AdditionalInfoText.MaxWeight.LabelName, productObject.AdditionalInfoText.MaxWeight.Text),
                    new TextBE(productObject.AdditionalInfoText.Receiver.LabelKey, productObject.AdditionalInfoText.Receiver.LabelName, productObject.AdditionalInfoText.Receiver.Text),
                    new TextBE(productObject.AdditionalInfoText.Size.LabelKey, productObject.AdditionalInfoText.Size.LabelName, productObject.AdditionalInfoText.Size.Text),
                    new TextBE(productObject.AdditionalInfoText.Submission.LabelKey, productObject.AdditionalInfoText.Submission.LabelName, productObject.AdditionalInfoText.Submission.Text),
                    new TextBE(productObject.AdditionalInfoText.Traceable.LabelKey, productObject.AdditionalInfoText.Traceable.LabelName, productObject.AdditionalInfoText.Traceable.Text));

                var basePrice = new PriceBE(undefined, undefined, undefined, undefined, undefined, undefined);//MVU7734 - S

                var serviceProperties = new ServicePropertiesBE(productObject.Properties.DoorCode, productObject.Properties.FilterCOD, productObject.Properties.FilterDelivery,
                    productObject.Properties.FilterExpress, productObject.Properties.FilterTrace, productObject.Properties.FilterValue, productObject.Properties.LetterAllowed, productObject.Properties.MailNotification,
                    productObject.Properties.PhoneNotification, productObject.Properties.SMSNotification, productObject.Properties.Vat, productObject.Properties.SenderEmail);

                var mmToCmMultiplier = 0.1;
                var minSize = new SizeBE(
                    productObject.MinSize.Height * mmToCmMultiplier,
                    productObject.MinSize.Length * mmToCmMultiplier,
                    productObject.MinSize.Width * mmToCmMultiplier,
                    productObject.MinSize.Diameter * mmToCmMultiplier,
                    productObject.IsRoll,
                    productObject.MinSize.RollLength * mmToCmMultiplier);

                var maxSize = new SizeBE(
                    productObject.MaxSize.Height * mmToCmMultiplier,
                    productObject.MaxSize.Length * mmToCmMultiplier,
                    productObject.MaxSize.Width * mmToCmMultiplier,
                    productObject.MaxSize.Diameter * mmToCmMultiplier,
                    productObject.IsRoll,
                    productObject.MaxSize.RollLength * mmToCmMultiplier);
                              
                var service = new ProductDefinitionBE(productObject.ProductId, productObject.DestinationId, productObject.Code, productObject.Name, weightBEs, minSize, maxSize, productObject.MinWeight, productObject.MaxWeight, productObject.IsRoll, additionalServiceBEs, serviceDocumentsBEs, basePrice, productObject.Description, additionalInfoText, serviceProperties, productObject.International, productObject.CodMaxAmount, productObject.IsActive);
                vm.Products.push(service);
            });

            // if there is consignment data (i.e. editing a consigmnet) set selected product
            if (consignmentData && IsShoppingCartEditMode())
            {
                // TODO: Find better way to set consignmentId without matching DestinationID to a hardcoded value ("Sverige")
                var consignmentId = consignmentData.ProductCode + (consignmentData.DestinationID == "SE" ? 0 : 1)
                var product = GetMatchingProduct(consignmentId);
                vm.SelectedProduct(product);
                
                if (!ConsignmentIsC4orC5(consignmentData)) {
                    var mmToCmMultiplier = 0.1;
                    vm.SetCurrentSize(new SizeBE((consignmentData.Height * mmToCmMultiplier).toString(), (consignmentData.Length * mmToCmMultiplier).toString(), (consignmentData.Width * mmToCmMultiplier).toString(), (consignmentData.Diameter * mmToCmMultiplier).toString(), false));
                }
                       
                vm.CurrentWeight().Value(consignmentData.Weight);
                vm.CurrentWeight().WeightInGrams(consignmentData.Weight);
                intializeWeight();
                
                // Set product suggestion from consignmentData                
                vm.ProductSuggestions(new PriceAndRankBE(
                    product.ProductId,
                    new PriceBE(
                            consignmentData.Amount.Amount, //This amount contains the additional service prices included at this point
                            consignmentData.Amount.VATPercentage,
                            consignmentData.Amount.AmountNoVat,
                            consignmentData.Amount.Currency,
                            consignmentData.Amount.VatCode,//MVU7734 - S
                            consignmentData.Amount.VATAmount//MVU7734 - S
                        ),
                    1,
                    product
                    ));
                // Select additional services
                $.each(consignmentData.AdditionalServices, function (index, value) {
                    $.each(product.AdditionalServices(), function (i, additionalService) {
                        if (additionalService.Code() == value.Code())
                            vm.ProductSuggestions().SelectedAdditionalServices.push(additionalService);
                    });
                });


                //Subtract Additional Service price from base Suggested Product Price
                vm.ProductSuggestions().Price.Amount(vm.ProductSuggestions().Price.Amount() - vm.ProductSuggestions().AdditionalServicesPrice());
                
                consignmentData = undefined;
            }
        }
        else {
            skickaCore.HandleServiceError(response);
        }
    }

    var SelectionsChanged = function () {
        if (!IsShoppingCartEditMode()) {
            vm.ProductSuggestions(null);
        }
    };

    var ReturnRoundedDecimal = function (num) {
        return Math.round(num * 100) / 100;
    }

    var ConsignmentIsC4orC5 = function (consig) {
        var value = false;
        var mmToCmMultiplier = 0.1;
        var consignmentSize = new SizeBE(ReturnRoundedDecimal(consig.Height * mmToCmMultiplier).toLocaleString(), ReturnRoundedDecimal(consig.Length * mmToCmMultiplier).toLocaleString(), ReturnRoundedDecimal(consig.Width * mmToCmMultiplier).toLocaleString(), ReturnRoundedDecimal(consig.Diameter * mmToCmMultiplier).toLocaleString(), false);
        
        if (consignmentSize.Height() == vm.C4.Height() && consignmentSize.Length() == vm.C4.Length() && consignmentSize.Width() == vm.C4.Width() && consignmentSize.Diameter() == vm.C4.Diameter()) {
            vm.SetCurrentSize(vm.ClearSize);
            vm.TypeOfSize('C4');
            vm.SetCurrentSize(vm.C4);
            value = true;
        }
        else if (consignmentSize.Height() == vm.C5.Height() && consignmentSize.Length() == vm.C5.Length() && consignmentSize.Width() == vm.C5.Width() && consignmentSize.Diameter() == vm.C5.Diameter()) {
            vm.SetCurrentSize(vm.ClearSize);
            vm.TypeOfSize('C5');
            vm.SetCurrentSize(vm.C5);
            value = true;
        }
        return value;
    }

    var CmStringToMmString = function (value) {
        return (Math.round(parseFloat(value.replace(',', '.')) * 100) / 10).toString();
    };

    var ResetService = function () {
        vm.SelectedProduct(null);
    };

    var SetSizeAndWeightValidation = function () {
        vm.CurrentSize().Length.extend({
            required: {
                onlyIf: function () {
                    return vm.ValidateProduct()
                }
            },
            sizeNumberSE: { onlyIf: function () { return vm.ValidateProduct() } },
            minCheck: {
                onlyIf: function () {
                    return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MinSize.Length() > 0
                }, params: function () {
                    if (vm.CurrentSize().IsRoll()) {
                        return vm.SelectedProduct().MinSize.RollLength;
                    }
                    else {
                        return vm.SelectedProduct().MinSize.Length();
                    }
                }
            },
            maxCheck: {
                onlyIf: function () {
                    return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MaxSize.Length() > 0
                }, params: function () {
                    if (vm.CurrentSize().IsRoll()) {
                        return vm.SelectedProduct().MaxSize.RollLength;
                    }
                    else {
                        return vm.SelectedProduct().MaxSize.Length();
                    }
                }
            }
        });

        vm.CurrentSize().Width.extend({
            required: { onlyIf: function () { return vm.ValidateProduct() && !vm.CurrentSize().IsRoll() } },
            sizeNumberSE: { onlyIf: function () { return vm.ValidateProduct() && !vm.CurrentSize().IsRoll() } },
            minCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MinSize.Width() > 0 && !vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MinSize.Width() }},
            maxCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MaxSize.Width() > 0 && !vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MaxSize.Width() }}
        });

        vm.CurrentSize().Height.extend({
            required: { onlyIf: function () { return vm.ValidateProduct() && !vm.CurrentSize().IsRoll() } },
            sizeNumberSE: { onlyIf: function () { return vm.ValidateProduct() && !vm.CurrentSize().IsRoll() } },
            minCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MinSize.Height() > 0 && !vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MinSize.Height() }},
            maxCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MaxSize.Height() > 0 && !vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MaxSize.Height() }}
        });

        vm.CurrentSize().Diameter.extend({
            required: { onlyIf: function () { return vm.ValidateProduct() && vm.CurrentSize().IsRoll() } },
            sizeNumberSE: { onlyIf: function () { return vm.ValidateProduct() && vm.CurrentSize().IsRoll() } },
            minCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MinSize.Diameter() > 0 && vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MinSize.Diameter() }},
            maxCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.SelectedProduct().MaxSize.Diameter() > 0 && vm.CurrentSize().IsRoll() }, params: function () { return vm.SelectedProduct().MaxSize.Diameter() }}
        });

        vm.CurrentWeight().CalculatedWeight.extend({
            minCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && (vm.CurrentWeight().Value() !== undefined) && vm.SelectedProduct().MinWeight > 0 }, params: function () { return vm.SelectedProduct().MinWeight } },
            maxCheck: { onlyIf: function () { return vm.ValidateProduct() && vm.SelectedProduct() && vm.CurrentWeight().CalculatedWeight() && vm.SelectedProduct().MaxWeight > 0 }, params: function () { return vm.SelectedProduct().MaxWeight }}
        });

        vm.CurrentWeight().Value.extend({
            required: {
                onlyIf: function () {
                    return vm.ValidateProduct()
                }
            },            
            digit: { onlyIf: function () { return vm.ValidateProduct() && vm.CurrentWeight().Multiplier() == '1' } },
            kgNumberSE: { onlyIf: function () { return vm.ValidateProduct() && vm.CurrentWeight().Multiplier() == '1000' } }
        });

    };

    // find product definition which matches the suggested product
    var GetMatchingProduct = function (serviceCode) {
        var matchingProduct = null;
        var productCode = null;
        for (var i = 0, j = vm.Products().length; i < j; i++) {
            productCode = vm.Products()[i].Code + (vm.Products()[i].International ? '1' : '0')
            if (productCode == serviceCode) {
                matchingProduct = vm.Products()[i];
                break;
            }
        };

        return matchingProduct;
    }

    
    vm.GetBounderies = function (min, max, unit, br) {
        if (min == 0 && max == 0) {
            return null;
        } else {
            if (max == 0) {
                if (br) {
                    return 'Min: ' + min + ' ' + unit + '<br />';
                } else {
                    return 'Min: ' + min + ' ' + unit;
                }
            } else {
                if (br) {
                    return 'Min: ' + min + ' ' + unit + '<br />Max: ' + max + ' ' + unit;
                } else {
                    return 'Min: ' + min + ' ' + unit + ' - Max: ' + max + ' ' + unit;
                }
            }
        }
    };


    vm.GetWeightBounderies = function (min, max) {
        var minUnit = 'g';
        var maxUnit = 'g';
        if (min == 0 && max == 0) {
            return null;
        } else {
            if (min >= 1000) {
                min = min/1000;
                minUnit = 'kg';
            }
            if (max >= 1000) {
                max = max/1000;
                maxUnit = 'kg';
            }
            if (max == 0) {
                return 'Min: ' + min + ' ' + minUnit;
            } else {
                return 'Min: ' + min + ' ' + minUnit + ' - Max: ' + max + ' ' + maxUnit;
            }
        }
    }


    /* ------------------------------- Public methods  --------------------------------------------*/
    vm.SwitchServiceTypeClick = function () {
        vm.IsServiceTypeSwitched(true);
    };

    vm.SetCurrentSize = function (sizeObject) {
        vm.CurrentSize().Height(sizeObject.Height());
        vm.CurrentSize().Length(sizeObject.Length());
        vm.CurrentSize().Width(sizeObject.Width());
        vm.CurrentSize().Diameter(sizeObject.Diameter());
        vm.CurrentSize().IsRoll(sizeObject.IsRoll());
    };

    vm.ShowWeightBox = function () {                   
        return true;       
    };

    vm.SetSelectedWeight = function (displayValue, displayUnit) {
        var multiplierVal = 1; //default should be considered as 'g'
        if (displayUnit == 'kg') {
            var multiplierVal = 1000;
        }
        vm.CurrentWeight().Value(displayValue);
           
    }


    /* ----------------------------Subscriptions -------------------------------------------------*/

    vm.IsServiceGuideSelected.subscribe(function (newValue) {
        if (newValue) {
            ResetService();
        }
        console.log("Guide selected");
        SelectionsChanged();

        //Reset error
        vm.ServiceError(new ErrorBE(true, false, ""));
    });

    vm.SelectedProduct.subscribe(function () {
        if (vm.SwitchServiceTypeClick() == false) {
            console.log("SelectedProduct Subscription");
            vm.SetSelectedWeight(undefined, undefined);
            if (!vm.ShowWeightBox() && vm.CurrentWeight().Value()) {
                var isWeightSet = false;
                for (var i = 0; i < vm.SelectedProduct().WeightIntervals().length; i++) {
                    if (vm.CurrentWeight().Value() <= vm.SelectedProduct().WeightIntervals()[i].WeightInGrams()) {                      
                        vm.CurrentWeight().Value(vm.SelectedProduct().WeightIntervals()[i].WeightInGrams());
                        isWeightSet = true;
                        break;
                    }
                };

                if (!isWeightSet) {
                    vm.CurrentWeight().Value(undefined);
                }
            }         

            if (!vm.IsServiceGuideSelected()) {
                if (vm.SwitchServiceTypeClick()== false) {
                    console.log("about to reset");
                    SelectionsChanged();
                    vm.TypeOfSize('own');
                    vm.SetCurrentSize(vm.ClearSize);
                    vm.CurrentWeight().Value(undefined);
                }
            }
        }
    });

    vm.CurrentSize().Height.subscribe(function () {
            SelectionsChanged();
    });

    vm.CurrentSize().Width.subscribe(function () {
            SelectionsChanged();
    });

    vm.CurrentSize().Length.subscribe(function () {
            SelectionsChanged();
    });

    vm.CurrentSize().Diameter.subscribe(function () {
            SelectionsChanged();
    });

    vm.CurrentSize().IsRoll.subscribe(function () {
        if (!vm.CurrentSize().IsRoll()) { 
            vm.CurrentSize().Diameter(undefined);
        }

        SelectionsChanged();
    });

    vm.SelectedFilters.subscribe(function () {
            SelectionsChanged();
    });

    vm.SelectedDestination.subscribe(function (newDestination) {
        vm.IsServiceGuideSelected(IsServiceGuideSelectedInitial);
        vm.CountryInformationLink(countryInfoLink.replace("*", vm.SelectedDestination().DestinationID));
        GetProducts(newDestination.DestinationID, HandleGetProducts, vm.ServicesProgress);
       
        SelectionsChanged();
    });

    var init = (function () {
        this.GetDestinations(HandleGetDestinationsResponse, vm.DestinationsProgress);
        SetSizeAndWeightValidation();
    })()

    return vm;
})();


/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Google Tracker js ---------------------------*/
/* -------------------------------------------------------------------------------------*/
var registerGAC = (function () {
    try {
        //Select Service Dropdown
        var dimensionValue = 'SelectService';
        var gac = new gaCookies();
        var VID = gac.getUniqueId();
        ga('send', 'pageview', {
            'dimension1': dimensionValue,
            'dimension2': '1',
            'dimension3': null,
            'dimension4': 'Service',
            'dimension5': 'Completed',
            'dimension6': null,
            'dimension7': null,
            'dimension8': null,
            'dimension9': null,
            'dimension10': VID
        });
        var inFormOrLink = false;
        $('a').live('click', function () { inFormOrLink = true; });
        $('form').bind('submit', function () { inFormOrLink = true; });

        $(window).bind('beforeunload', function (eventObject) {
            if (!inFormOrLink) {
                ga('send', 'pageview', {
                    'dimension1': dimensionValue,
                    'dimension2': '1',
                    'dimension3': null,
                    'dimension4': 'Service',
                    'dimension5': 'Aborted',
                    'dimension6': null,
                    'dimension7': null,
                    'dimension8': null,
                    'dimension9': null,
                    'dimension10': VID
                });
            }
            window.setTimeout(function () {
                // this will execute 1 second later
            }, 1000)
        });

        //Additional Services
        var dimensionValue = 'SelectAddService';
        var gac = new gaCookies();
        var VID = gac.getUniqueId();
        ga('send', 'pageview', {
            'dimension1': dimensionValue,
            'dimension2': '2',
            'dimension3': '1',
            'dimension4': 'Service',
            'dimension5': 'Completed',
            'dimension6': null,
            'dimension7': null,
            'dimension8': null,
            'dimension9': null,
            'dimension10': VID
        });
    }
    catch (e) {
        console.log(e.message);
    }
});