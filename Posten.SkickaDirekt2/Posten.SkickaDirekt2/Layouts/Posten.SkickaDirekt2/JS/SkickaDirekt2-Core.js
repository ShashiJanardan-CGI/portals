﻿var logIDKey = "transactionLogID";
var logIDHeader = "X-TransactionLogID";
var senderAddressKey = "senderAddress";
var receiverAddressKey = "receiverAddress";
var accountKey = "account";
var sizeKey = "size";
var weightKey = "weight";
var selectedProductKey = "selectedProduct";
var selectedDestinationKey = "selectedDestination";
var nonSupportedBrowsers = "NonSupportedBrowser.aspx"; 
var servicePath = "/_vti_bin/Posten.SkickaDirekt2/transaction.svc";

var serverRelativeUrl;
$(document).ready(function () {
    if (navigator.appName.indexOf("Internet Explorer") != -1) { // Checking for versions of Internet Explorer
        /* Redirect for older browsers (IE8) */
        if ((navigator.appVersion.indexOf("MSIE 9") == -1) && (navigator.appVersion.indexOf("MSIE 1") == -1)) {
            if (location.href.toLowerCase().indexOf(nonSupportedBrowsers.toLowerCase()) == -1) {
                location.href = nonSupportedBrowsers;
            }
        }
    }
    else {
        if (serverRelativeUrl === undefined) {
            serverRelativeUrl = window.location.protocol + "//" + window.location.host;
        }

        setLogID();
    }

});


/* ------------------------------------------------------------------------------------ */
/* ------------------------------- Custom knockout bindings ---------------------------  */
/* -------------------------------------------------------------------------------------*/

ko.bindingHandlers.scrollToView = {
    update: function (element, valueAccessor) {
        var _value = valueAccessor();
        if (ko.unwrap(_value)) {
            element.scrollIntoView();
        }
    }
};

/* -------------------------------------------------------------------------------------*/
/* ------------------------------- Custom validation rules------------------------------*/
/* -------------------------------------------------------------------------------------*/

ko.validation.rules['amountRange'] = {
    validator: function (val) { 
        var limit = skickaCashOnDeliveryVM.SelectedProductSuggestion().Product.CodMaxAmount;
        console.log(1);
        console.log("value:" + val);
                console.log("limit:" + limit);
            if (limit === 0) {
                console.log(4);
                return true;
            }
            else {
                console.log(5);
                console.log("value:" + val);
                console.log("limit:" + limit);
                console.log("validation result:" + (parseFloat(StripSpace(val).replace(',', '.')) <= limit) && (/^(\d+(?:[\.\,]\d{1,2})?)$/.test(StripSpace(val))));
                return (parseFloat(StripSpace(val).replace(',', '.')) <= limit) && (/^(\d+(?:[\.\,]\d{1,2})?)$/.test(StripSpace(val)));
            }        
    },
    message: 'Ogiltigt belopp - Max {0} kr'
};

ko.validation.rules['referenceNumber'] = {
    validator: function (val, validationResult) {
        var boolValue = true;
        if (validationResult !== "") {
            boolValue = false;
        }
        return boolValue;
    },
    message: 'Felaktigt OCR-nummer'
};

// Native 'number' rule only accepts '.', not ','
// Start with at least one digit then optional ',' with one or two digits
ko.validation.rules['numberSE'] = {
    validator: function (val) {
        return (/^\d+(,\d{1,2})?$/.test(val));
    },
    message: 'Endast siffror'
};

// Size fileds should only accept one decimal
ko.validation.rules['sizeNumberSE'] = {
    validator: function (val) {
        val = parseFloat(val.toString().replace(".", ","));
        return (/^\d+(,\d)?$/.test(val));
    },
    message: 'Endast siffror'
};

// Weight in kg should accept three decimals
ko.validation.rules['kgNumberSE'] = {
    validator: function (val) {
        return (/^\d+(,\d{1,3})?$/.test(val));
    },
    message: 'Endast siffror (max tre decimaler)'
};

ko.validation.rules['minCheck'] = {
    validator: function (val, minFunction) {
        if (typeof (val) == 'string') {
            val = val.replace(/\,/g, '.');
        }
        return ko.validation.utils.isEmptyVal(val) || val >= minFunction();
    },
    message: 'Minst {0}'
};

ko.validation.rules['maxCheck'] = {
    validator: function (val, maxFunction) {
        if (typeof (val) == 'string') {
            val = val.replace(/\,/g, '.');
        }
        return ko.validation.utils.isEmptyVal(val) || val <= maxFunction();
    },
    message: 'Max {0}'
};

ko.validation.rules['maxLengthFunction'] = {
    validator: function (val, maxFunction) {
        return ko.validation.utils.isEmptyVal(val) || val.length <= maxFunction();
    },
    message: 'Max {0} tecken'
};
ko.validation.rules['minLengthFunction'] = {
    validator: function (val, minFunction) {
        return ko.validation.utils.isEmptyVal(val) || val.length >= minFunction();
    },
    message: 'Minst {0} tecken'
};

// Start with 0, 5-12 digits/spaces, ends with digit
ko.validation.rules['phoneSE'] = {
    validator: function (val) {
        if (val)
            return (/^0[\d]{1,3}(-)?[\d ]{4,9}\d$/.test(val));
        else
            return true;
    },
    message: 'Kontrollera format'
};

//Tel numbers should be 8+ digits
ko.validation.rules['telSE'] = {
    validator: function (val) {
        if (val) {                      
            return (/^\d{8,}$/.test(val))
        }
        else {            
            return true;
        }
    },
    message: 'Kontrollera format'
};

//Mobile numbers should be 10 digits
ko.validation.rules['mobileSE'] = {
    validator: function (val) {
        if (val)            
            return (/^0[\d]{1,9}$/.test(StripDashSpace(val)));
        else
            return true;
    },
    message: 'Kontrollera format - mobilnummer måste vara 10 siffror'
};

ko.validation.rules['phoneExclPrefix'] = {
    validator: function (val) {
        if (val)
            return (/\d[\d ]{4,18}\d$/.test(val));
        else
            return true;
    },
    message: 'Kontrollera format'
};

ko.validation.rules['phonePrefix'] = {
    validator: function (val) {
        if (val) {
            
            return (/^[\d]{1,3}$/.test(val));
        }
        else {
            return true;
        }
    },
    message: 'Kontrollera format'
};

ko.validation.rules['bankgiro'] = {
    validator: function (val) {
        return (/^\d{3,4}-\d{4}$/.test(val));
    },
    message: 'Ogiltigt kontonummer'
};

ko.validation.rules['plusgiro'] = {
    validator: function (val) {
        return (/^\d{1,7}-\d{1}$/.test(val));
    },
    message: 'Ogiltigt kontonummer'
};

ko.validation.rules['organizationNumber'] = {
    validator: function (val) {
        if (val)
            return (/^\d{6}(-)?\d{4}$/.test(val));
        else
            return true;
    },
    message: 'Kontrollera format'
};


ko.validation.registerExtenders();

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- MODEL ---------------------------------------*/
/* -------------------------------------------------------------------------------------*/

var AdditionalServiceBE = (function () {
    function additionalService(Code, Name, PVTCode, Price) {
        this.Code = ko.observable(Code);
        this.Name = ko.observable(Name);
        this.PVTCode = ko.observable(PVTCode);
        this.Price = Price;
    }

    return additionalService;
})();

var AddressBE = (function () {
    function addressbe(IsCompany, Name, CompanyName, OrganizationNumber, AddressField1, AddressField2, ZipCode, City, Country, EmailData, Email2, MobilePhoneCountryPrefix, MobilePhoneNumber, TelephoneCountryPrefix, TelephoneNumber, EntryCode) {
        var self = this;
        self.IsCompany = ko.observable(IsCompany);
        self.Name = ko.observable(Name);
        self.CompanyName = ko.observable(CompanyName);
        self.OrganizationNumber = ko.observable(OrganizationNumber);
        self.AddressField1 = ko.observable(AddressField1);
        self.AddressField2 = ko.observable(AddressField2);
        self.ZipCode = ko.observable(ZipCode);
        self.City = ko.observable(City);
        self.Country = ko.observable(Country);
        self.Email = ko.observable(EmailData);
        self.Email2 = ko.observable(Email2);
        self.MobilePhoneCountryPrefix = ko.observable(MobilePhoneCountryPrefix);
        self.MobilePhoneNumber = ko.observable(MobilePhoneNumber);
        self.TelephoneCountryPrefix = ko.observable(TelephoneCountryPrefix);
        self.TelephoneNumber = ko.observable(TelephoneNumber);
        self.EntryCode = ko.observable(EntryCode);

        self.Mobile = "";
        self.Phone = "";
    }

    return addressbe;
})();

var ErrorBE = (function () {
    function error(IsValid, GeneralError, message) {
        this.IsValid = IsValid;
        this.GeneralError = GeneralError;
        this.Message = message;
    }

    return error;
})();

var PriceAndRankBE = (function () {
    function priceandrankbe(ProductId, Price, Rank, ProductData) {
        var self = this;

        self.ProductId = ProductId;
        self.Price = Price;
        self.Rank = Rank;
        self.Product = ProductData;
        self.SelectedAdditionalServices = ko.observableArray([]);
        self.AdditionalServicesPrice = ko.observable(0);
        self.ShowCashOnDelivery = ko.observable(false);
        //MVU7734 - S 
        self.AdditionalServicesPriceVat = ko.observable(0);
        //MVU7734 - E
        //var showCOD = (ProductData.CodMaxAmount > 0);        
        //self.ShowCashOnDelivery(showCOD);

        // subscriptions
        self.SelectedAdditionalServices.subscribe(function () {
            var total = 0;
            var vatTotal = 0;
            self.ShowCashOnDelivery(false);
            $.each(self.SelectedAdditionalServices(), function (index, value) {
                // Show Cash On Delivery web part if that service is selected
                //CGI CR C34026 START
                var updateTotalPrice = true;
                if (value.Code() == '01') {                    
                    if (self.Product.Name == 'Brevpostförskott' || self.Product.Name == 'Postförskott') {
                        self.ShowCashOnDelivery(true);
                        updateTotalPrice = false;
                    }                    
                }

                if (updateTotalPrice === true) {
                    total += value.Price.Amount();
                }
                //CGI CR C34026 END

                //MVU7734 - S 
                if (value.Price.VatAmount() != undefined || value.Price.VatAmount() != 0) {
                    vatTotal += value.Price.VatAmount();
                }
                //MVU7734 - E 
            });

            self.AdditionalServicesPrice(total);
            //MVU7734 - S 
            self.AdditionalServicesPriceVat(vatTotal);
            //MVU7734 - E
        });
    }

    return priceandrankbe;
})();

var PriceBE = (function () {
    function pricebe(Amount, Vat, AmountNoVat, Currency,VatCode,VatAmount) {
        this.Amount = ko.observable(Amount);
        this.Vat = ko.observable(Vat);
        this.AmountNoVat = ko.observable(AmountNoVat);
        this.Currency = ko.observable(Currency);
        //MVU7734 - S 
        this.VatCode = ko.observable(VatCode);
        this.VatAmount = ko.observable(VatAmount);
        //MVU7734 - E
    }

    return pricebe;
})();

/* -------------------------------------------------------------------------------------*/
/* ------------------------------------- VIEWMODEL -------------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaCore = (function () {
    var vm = {};

    /* --------------------------------- pub/sub ---------------------------------------------*/
    vm.ServiceError = ko.observable().publishOn("ServiceError");    
    vm.IsValidSelectService = ko.observable(); //.publishOn("IsValidSelectService");
    

    /* ---------------------------------Observables ------------------------------------------*/
    vm.ValidateAddresses = ko.observable(false);


    /* --------------------------------- Public methods --------------------------------------*/
    vm.RemoveRules = function (obj) {
        if (obj.rules) {
            obj.rules.removeAll();
        }
    };

    vm.HandleServiceError = function (response) {
        var resultString = " ";
        if (response.StatusMsg !== null && response.StatusMsg != "") {
            resultString = response.StatusMsg;
        }

        if (response.ResponseInfos !== null) {
            $.each(response.ResponseInfos, function (index, responseInfo) {
                resultString = "<br/>" + resultString + "<br/>" + responseInfo.Description;

            });
        }
        vm.ServiceError(new ErrorBE(false, true, resultString));
    };

/* ------------------------------- Private methods  --------------------------------------------*/


    return vm;
})();

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialize ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

ko.validation.init({
    decorateInputElement: true,
    errorElementClass: 'invalidElement',
    messageTemplate: 'skickaDirektMessageTemplate',
    messagesOnModified: false
});

$(document).ready(function () {
    
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Global functions ----------------------------*/
/* -------------------------------------------------------------------------------------*/
var returnString = function (obj) {
    var value = "";
    if (obj !== null & obj !== undefined) {
        value = obj.toString();
    }

    return value;
}

var formatAddress = function (addressObj) {
    var telNumber = returnString(addressObj.TelephoneNumber());
    var mobileNumber = returnString(addressObj.MobilePhoneNumber());
   

    var formattedObj = {
        Name: addressObj.Name(),
        AddressField1: addressObj.AddressField1(),
        AddressField2: addressObj.AddressField2(),
        City: addressObj.City(),
        CompanyName: addressObj.CompanyName(),
        Country: addressObj.Country(),
        Email: addressObj.Email(),
        DoorCode: addressObj.EntryCode(),
        Mobile: mobileNumber,
        Phone: (telNumber == '' || telNumber == '+') ? '' : returnString(addressObj.TelephoneCountryPrefix()) + "-" + returnString(addressObj.TelephoneNumber()),
        ZipCode: addressObj.ZipCode()
    };

    return formattedObj;
}

var StripDashSpace = function (inString) {
    var outString = "";
    if (inString !== null && inString !== undefined && inString.length > 0) {
        outString = inString.replace(/-|\s/g, "");
    }
    return outString;
}

var StripSpace = function (inData) {
    var outString = "";
    
    if (inData !== null && inData !== undefined) {
        var inString = inData.toString();
        if(inString.length > 0){
            outString = inString.replace(/\s/g, "");
        }
        return outString;
    } else {
        return outString;
    }
}



var exObj;
var preExObj;
var extractAddress = function (addressObj, destinationID) {
    preExObj = addressObj;
    var mobileArray = returnPhoneNumberArray(addressObj.Mobile);
    var phoneArray = returnPhoneNumberArray(addressObj.Phone);
    var isDestinationSweden = (destinationID == "SE");
   
    var extractedObj = {
        Name: addressObj.Name,
        AddressField1: addressObj.AddressField1,
        AddressField2: addressObj.AddressField2,
        City: addressObj.City,
        CompanyName: addressObj.CompanyName,
        Country: addressObj.Country,
        Email: addressObj.Email,
        EntryCode: addressObj.DoorCode,
        MobilePhoneCountryPrefix: (isDestinationSweden== false ? mobileArray[0] : ""),
        MobilePhoneNumber: (isDestinationSweden== false ? mobileArray[1] : addressObj.Mobile),
        TelephoneCountryPrefix: (isDestinationSweden == false ? phoneArray[0] : ""),
        TelephoneNumber: (isDestinationSweden == false ? phoneArray[1] : addressObj.Phone),
        ZipCode: addressObj.ZipCode,
    };
    exObj = extractedObj;
    return extractedObj;
}

var returnPhoneNumberArray = function (numberString)
{
    var numberArray = [];
    if (numberString == "") {
        numberArray.push("");
        numberArray.push("");
    }
    else {
        var bufferArray = numberString.split("-");
        if (bufferArray.length > 1) {
            numberArray.push(bufferArray[0]);
            numberArray.push(bufferArray[1]);
        }
        else {
            numberArray.push("");
            numberArray.push(bufferArray[0]);
        }
    }
    return numberArray;
}

var ResponseStatus = (function () {
    return {
        SUCCESS: 0,
        INVALID_INPUT: 1,
        FUNCTIONAL_ERROR_FROM_EXTERNAL_SYSTEM: 2,
        INTERNAL_ERROR: 3,
        GENERAL_FUNCTIONAL_ERROR: 4,
        DB_ERROR: 5
    };
})();

function setLogID() {    
    var returnValue = false;    
    if (returnSessionValue(logIDKey) === "" || returnSessionValue(logIDKey) === undefined) {//Generate new Log ID        
        returnValue = getNewLogID();
    }
        
   if (returnValue !== false) {    
    $.ajaxSetup({
        beforeSend: function (xhr) {//Add the log ID as a header to all ajax requests
            xhr.setRequestHeader("X-TransactionLogID", returnSessionValue(logIDKey));
        }        
        });
   }    
};


function returnSessionValue(key) {
    var value = "";
    if (sessionStorage.getItem(key)) {
        value = sessionStorage.getItem(key);
    }

    return value;
};

function parseThousandSeperator(value, seperator, decimalPoint) {
    var endResult = value;
    var valueToParse = value.split(decimalPoint)[0];
    if (valueToParse.length > 3) {
        var firstSeperator = valueToParse.length % 3;
        var numberOfFollowingSeperators = (valueToParse.length - firstSeperator) / 3;
        var offsetLength = seperator.length;
        var offset = 0;
        if (firstSeperator > 0) {
            endResult = value.slice(0, firstSeperator) + seperator + value.slice(firstSeperator);
            offset = offsetLength;
        }
        if (numberOfFollowingSeperators > 1) {

            for (var i = 1; i < numberOfFollowingSeperators; i++) {
                var insertAt = firstSeperator + offset + (3 * i);
                endResult = endResult.slice(0, insertAt) + seperator + endResult.slice(insertAt);
                offset = offset + offsetLength;
            }
        }
    }
    return endResult;
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Web service calls ---------------------------*/
/* -------------------------------------------------------------------------------------*/

function getNewLogID() {
    try {
       
        $.ajax({
            type: "GET",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + servicePath + "/GetTransactionLogID",
            dataType: "json",
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                var newLogID = data.GetTransactionLogIDResult;                
                if (newLogID !== undefined) {                    
                    sessionStorage.setItem(logIDKey, newLogID);
                    return newLogID;
                }
            },
            error: function (data) {
                //Generate Error Msg
                console.log(data);
                return false;
            }
        });
    }
    catch (e) {
        console.log(e.message);
    }
};

function ZipToCity(zip, viewModelObject, showZipError, showProgress) {
    try {
        showZipError(false);
        viewModelObject('');
        ///
        //viewModelObject("Malmö");
        $.ajax({
            type: "GET",
            cache: false,
            contentType: "application/json; charset=utf-8",
            async: false,
            url: serverRelativeUrl + servicePath + "/ZipToCity?zip=" + zip,
            dataType: "json",
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                var returnedCity = data.ZipToCityResult;
                if (returnedCity == "" || returnedCity == null || returnedCity == "fail" || returnedCity == "false" || returnedCity == "Invalid") {
                    showZipError(true);
                }
                else {
                    viewModelObject(returnedCity);
                }
                showProgress(false);
            },
            error: function (data) {
                console.log("invalid zip " + data);                
                showZipError(true);
                showProgress(false);
            }
        });
    }
    catch (e) {
        console.log("invalid zip " + e.Message);
        showZipError(true);
        showProgress(false);
    }
}

function returnObjectID(id) {
    return $("[id*=" + id + "]").attr('id');
}

/* -------------------------------------------------------------------------------------*/
/* ------------------------------- Cookies / SessionStorage / LocalStorage --------------*/
/* -------------------------------------------------------------------------------------*/

function setLocalValue(key, value) {
    localStorage.setItem(key, value);
}

function returnLocalValue(key) {
    var value = localStorage.getItem(key);
    if (value === null) {
        value = "";
    }
    return value;
}

function setSessionValue(key, value) {
    sessionStorage.setItem(key, value);
}

function returnSessionValue(key) {
    var value = sessionStorage.getItem(key);
    if (value === null) {
        value = "";
    }
    return value;
}

/* -------------------------------------------------------------------------------------*/
/* ------------------------------- Session Objects --------------*/
/* -------------------------------------------------------------------------------------*/

function AddressSession(addressBE) {
    this.IsCompany = addressBE.IsCompany();
    this.Name = addressBE.Name();
    this.CompanyName = addressBE.CompanyName();
    this.OrganizationNumber = addressBE.OrganizationNumber();
    this.AddressField1 = addressBE.AddressField1();
    this.AddressField2 = addressBE.AddressField2();
    this.ZipCode = addressBE.ZipCode();
    this.City = addressBE.City();
    this.Country = addressBE.Country();
    this.Email = addressBE.Email();
    this.Email2 = addressBE.Email2();
    this.MobilePhoneCountryPrefix = addressBE.MobilePhoneCountryPrefix();
    this.MobilePhoneNumber = addressBE.MobilePhoneNumber();
    this.TelephoneCountryPrefix = addressBE.TelephoneCountryPrefix();
    this.TelephoneNumber = addressBE.TelephoneNumber();
    this.EntryCode = addressBE.EntryCode();
    this.Mobile = addressBE.Mobile;
    this.Phone = addressBE.Phone;
}

function AccountSession(accountBE) {
    this.AccountType = accountBE.AccountType();
    this.AccountNumber = accountBE.AccountNumber();
    this.Reference = accountBE.Reference();
    this.Amount = accountBE.Amount();
}

function SizeSession(sizeBE) {
    this.IsRoll = sizeBE.IsRoll();
    this.Height = sizeBE.Height();
    this.Length = sizeBE.Length();
    this.Width = sizeBE.Width();
    this.Diameter = sizeBE.Diameter();
    this.RollLength = sizeBE.RollLength;
}

function WeightSession(weightBE) {    
    this.Value = weightBE.Value();
    this.DisplayUnit = weightBE.DisplayUnit();
    this.DisplayValue = weightBE.DisplayValue();
    this.WeightInGrams = weightBE.WeightInGrams();
    this.Multiplier = weightBE.Multiplier();
    this.CalculatedWeight = weightBE.CalculatedWeight();
    this.RadioText = weightBE.RadioText();
}

function SelectedProductSession(productBE) {
    this.Name = productBE.Name ;
    this.ID = productBE.ProductId;
    this.Code = productBE.Code;
}

function SelectedDestinationSession(destinationBE) {
    this.ID = destinationBE.DestinationID;
    this.Name = destinationBE.Name;    
}