﻿/// <reference path="../Src/knockout.validation.js" />

/************************************************
* This is an example localization page. All of these
* messages are the default messages for ko.validation
*
* Currently ko.validation only does a single parameter replacement
* on your message (indicated by the {0}).
*
* The parameter that you provide in your validation extender
* is what is passed to your message to do the {0} replacement.
*
* eg: myProperty.extend({ minLength: 5 });
* ... will provide a message of "Please enter at least 5 characters"
* when validated
*
* This message replacement obviously only works with primitives
* such as numbers and strings. We do not stringify complex objects
* or anything like that currently.
*/

ko.validation.localize({
    required: 'Obligatoriskt',
    min: 'Minst {0}',
    max: 'Max {0}',
    minLength: 'Minst {0} tecken',
    maxLength: 'Max {0} tecken',
    pattern: 'Kontrollera värdet',
    step: 'Öka med {0}',
    email: 'Felaktig e-postadress',
    date: 'Felaktigt datum',
    dateISO: 'Felaktigt datum',
    number: 'Endast siffror',
    digit: 'Endast siffror',
    phoneUS: 'Felaktigt telefonnummer',
    equal: 'Upprepa',
    notEqual: 'Ange annat värde',
    unique: 'Ange unikt värde'
});