namespace Posten.Portal.SkickaDirekt2.Features.Configuration
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security.Permissions;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Administration;

    using Posten.Portal.Platform.Common.Utilities;
    using Posten.Portal.Skicka.Services.SkickaService;
    using Posten.Portal.SkickaDirekt2.Utilities;
    using Posten.SkickaDirekt2.Resources;

    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid("32bfb97b-f095-4f9c-90f8-6df392c2b1d2")]
    public class ConfigurationEventReceiver : WebConfigEngine
    {
        #region Properties

        protected override string OwnerModif
        {
            get { return "Posten.Portal.SkickaDirekt2"; }
        }

        #endregion Properties

        #region Methods

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWebApplication webApplication = properties.Feature.Parent as SPWebApplication;
            string configurationMode = properties.Feature.Properties[Constants.FeatureProperty_WebConfigMode].Value;
            string templateFilePath = properties.Feature.Properties[Constants.FeatureProperty_WebConfigTemplateFile].Value;

            var configHelper = new ConfigHelper();
            configHelper.RemoveOwnerCustomisations(webApplication, Constants.ConfigOwner);

            var Modifications = configHelper.GetModifications(Constants.ConfigOwner, templateFilePath, configurationMode);
            foreach (var modification in Modifications)
            {
                webApplication.WebConfigModifications.Add(modification);
            }

            webApplication.Update();
            webApplication.Farm.Services.GetValue<SPWebService>(webApplication.WebService.Id).ApplyWebConfigModifications();

            // TODO: Add unity mapping for Skicka Service
            // TODO: Add add webservice mapping
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            var webApp = properties.Feature.Parent as SPWebApplication;
            var configHelper = new ConfigHelper();
            configHelper.RemoveOwnerCustomisations(webApp, Constants.ConfigOwner);
        }

        #endregion Methods
    }
}