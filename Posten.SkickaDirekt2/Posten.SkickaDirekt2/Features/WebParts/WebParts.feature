﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="458a54ca-73d1-4566-86d5-6024fb1d4012" description="$Resources:SkickaDirekt2Resources, Feature_WebParts_Description;" featureId="458a54ca-73d1-4566-86d5-6024fb1d4012" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:SkickaDirekt2Resources, Feature_WebParts_Title;" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="8bff91b1-00df-440c-ad60-0d77681ea00b" />
    <projectItemReference itemId="6a0a3f48-d24a-46f4-b4aa-ffcd0f9f8579" />
    <projectItemReference itemId="0d80ca04-5cb1-4ba5-becb-4afb7d78553c" />
    <projectItemReference itemId="a9637108-e0dc-4030-9061-16245c35e27f" />
    <projectItemReference itemId="787eb334-5077-437d-9e94-852e43ace6d9" />
    <projectItemReference itemId="7005b935-d4ac-4c8f-a0a6-efd70badb339" />
    <projectItemReference itemId="96e85188-056a-493b-acb1-8b87d56138cc" />
    <projectItemReference itemId="b223f756-2855-4231-a9a2-874fc291ac19" />
    <projectItemReference itemId="a4434c19-083f-413f-8a23-4e0a0616fe35" />
    <projectItemReference itemId="1b11b7dc-d4b4-498c-a652-80f1f880427e" />
    <projectItemReference itemId="0f636f30-7c2e-468a-8f6b-e399177153b6" />
    <projectItemReference itemId="0009d210-40f6-4f38-901e-3c10f2678b60" />
  </projectItems>
</feature>