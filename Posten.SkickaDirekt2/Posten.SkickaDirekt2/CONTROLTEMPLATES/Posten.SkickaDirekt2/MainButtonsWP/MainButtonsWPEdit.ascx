﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainButtonsWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.MainButtonsWPEdit" %>

<div id="MainButtonsEdit">
    <div id="MainErrorMessage" class="errorMessage rounded paddings bottomMargin">
        <div class="leftPartEdit">Felmeddelande storlek:</div>
        <div class="rightPartEdit">
            <textarea id="sizeValidationPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
        <div class="leftPartEdit">Felmeddelande vikt:</div>
        <div class="rightPartEdit">
            <textarea id="weightValidationPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
        <div class="leftPartEdit">Generellt felmeddelande:</div>
        <div class="rightPartEdit">
            <textarea id="generalServerErrorPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
    </div>
    <div class="buttonContainer leftFloat bottomMargin largeButton mainButton centerText rounded buttonGradient">
        <input class="inputEdit" id="resetButtonPopertyEdit" runat="server" type="text" enableviewstate="true" />
    </div>
    <div class="buttonContainer rightFloat bottomMargin largeButton mainButton centerText rounded buttonGradient">
        <input class="inputEdit" id="showProposalsButtonPropertyEdit" runat="server" type="text" enableviewstate="true" />
    </div>
    <div class="bothClear"></div>
</div>