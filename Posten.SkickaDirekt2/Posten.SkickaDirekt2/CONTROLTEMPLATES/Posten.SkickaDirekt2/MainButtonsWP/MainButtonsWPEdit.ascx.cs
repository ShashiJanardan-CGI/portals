﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class MainButtonsWPEdit : UserControl
    {
        #region Fields

        private string generalServerErrorProperty;
        private string resetButtonProperty;
        private string showProposalsButtonProperty;
        private string sizeValidationProperty;
        private string weightValidationProperty;

        #endregion Fields

        #region Properties

        public string GeneralServerErrorProperty
        {
            get
            {
                return this.generalServerErrorPropertyEdit.Value;
            }
            set
            {
                generalServerErrorProperty = value;
            }
        }

        public string ResetButtonProperty
        {
            get
            {
                return resetButtonPopertyEdit.Value;
            }
            set
            {
                resetButtonProperty = value;
            }
        }

        public string ShowProposalsButtonProperty
        {
            get
            {
                return showProposalsButtonPropertyEdit.Value;
            }
            set
            {
                showProposalsButtonProperty = value;
            }
        }

        public string SizeValidationProperty
        {
            get
            {
                return sizeValidationPropertyEdit.Value;
            }
            set
            {
                sizeValidationProperty = value;
            }
        }

        public string WeightValidationlProperty
        {
            get
            {
                return weightValidationPropertyEdit.Value;
            }
            set
            {
                weightValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(resetButtonPopertyEdit.Value))
            {
                resetButtonPopertyEdit.Value = resetButtonProperty;
            }
            if (string.IsNullOrEmpty(showProposalsButtonPropertyEdit.Value))
            {
                showProposalsButtonPropertyEdit.Value = showProposalsButtonProperty;
            }
            if (string.IsNullOrEmpty(sizeValidationPropertyEdit.Value))
            {
                sizeValidationPropertyEdit.Value = sizeValidationProperty;
            }
            if (string.IsNullOrEmpty(weightValidationPropertyEdit.Value))
            {
                weightValidationPropertyEdit.Value = weightValidationProperty;
            }
            if (string.IsNullOrEmpty(generalServerErrorPropertyEdit.Value))
            {
                generalServerErrorPropertyEdit.Value = generalServerErrorProperty;
            }
        }

        #endregion Methods
    }
}