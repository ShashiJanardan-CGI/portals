﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class SenderAddressWPEdit : UserControl
    {
        #region Fields

        private string address2LabelProperty;
        private string address2TooltipProperty;
        private string addressLabelProperty;
        private string cityLabelProperty;
        private string cityPlaceholderProperty;
        private string companyLabelProperty;
        private string companyNameLabelProperty;
        private string email2LabelProperty;
        private string emailLabelProperty;
        private string emailTooltipProperty;
        private string headerProperty;
        private string nameLabelProperty;
        private string organizationNumberLabelProperty;
        private string privateLabelProperty;
        private string senderTypeLabelProperty;
        private string zipLabelProperty;

        #endregion Fields

        #region Properties

        public string Address2LabelProperty
        {
            get
            {
                return address2LabelPropertyEdit.Value;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipPropertyEdit.Value;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        public string AddressLabelProperty
        {
            get
            {
                return addressLabelPropertyEdit.Value;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        public string CityLabelProperty
        {
            get
            {
                return cityLabelPropertyEdit.Value; ;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderPropertyEdit.Value;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelPropertyEdit.Value;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelPropertyEdit.Value;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        public string Email2LabelProperty
        {
            get
            {
                return email2LabelPropertyEdit.Value;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        public string EmailLabelProperty
        {
            get
            {
                return emailLabelPropertyEdit.Value;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        public string EmailTooltipProperty
        {
            get
            {
                return emailTooltipPropertyEdit.Value;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string NameLabelProperty
        {
            get
            {
                return nameLabelPropertyEdit.Value;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        public string OrganizationNumberLabelProperty
        {
            get
            {
                return organizationNumberLabelPropertyEdit.Value;
            }
            set
            {
                organizationNumberLabelProperty = value;
            }
        }

        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelPropertyEdit.Value;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        public string SenderTypeLabelProperty
        {
            get
            {
                return senderTypeLabelPropertyEdit.Value;
            }
            set
            {
                senderTypeLabelProperty = value;
            }
        }

        public string ZipLabelProperty
        {
            get
            {
                return zipLabelPropertyEdit.Value;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(companyNameLabelPropertyEdit.Value))
            {
                companyNameLabelPropertyEdit.Value = companyNameLabelProperty;
            }
            if (string.IsNullOrEmpty(organizationNumberLabelPropertyEdit.Value))
            {
                organizationNumberLabelPropertyEdit.Value = organizationNumberLabelProperty;
            }
            if (string.IsNullOrEmpty(nameLabelPropertyEdit.Value))
            {
                nameLabelPropertyEdit.Value = nameLabelProperty;
            }
            if (string.IsNullOrEmpty(addressLabelPropertyEdit.Value))
            {
                addressLabelPropertyEdit.Value = addressLabelProperty;
            }
            if (string.IsNullOrEmpty(address2LabelPropertyEdit.Value))
            {
                address2LabelPropertyEdit.Value = address2LabelProperty;
            }
            if (string.IsNullOrEmpty(zipLabelPropertyEdit.Value))
            {
                zipLabelPropertyEdit.Value = zipLabelProperty;
            }
            if (string.IsNullOrEmpty(emailLabelPropertyEdit.Value))
            {
                emailLabelPropertyEdit.Value = emailLabelProperty;
            }
            if (string.IsNullOrEmpty(email2LabelPropertyEdit.Value))
            {
                email2LabelPropertyEdit.Value = email2LabelProperty;
            }
            if (string.IsNullOrEmpty(senderTypeLabelPropertyEdit.Value))
            {
                senderTypeLabelPropertyEdit.Value = senderTypeLabelProperty;
            }
            if (string.IsNullOrEmpty(privateLabelPropertyEdit.Value))
            {
                privateLabelPropertyEdit.Value = privateLabelProperty;
            }
            if (string.IsNullOrEmpty(companyLabelPropertyEdit.Value))
            {
                companyLabelPropertyEdit.Value = companyLabelProperty;
            }
            if (string.IsNullOrEmpty(cityPlaceholderPropertyEdit.Value))
            {
                cityPlaceholderPropertyEdit.Value = cityPlaceholderProperty;
            }
            if (string.IsNullOrEmpty(cityPlaceholderPropertyEdit.Value))
            {
                cityPlaceholderPropertyEdit.Value = cityPlaceholderProperty;
            }
            if (string.IsNullOrEmpty(address2TooltipPropertyEdit.Value))
            {
                address2TooltipPropertyEdit.Value = address2TooltipProperty;
            }
            if (string.IsNullOrEmpty(emailTooltipPropertyEdit.Value))
            {
                emailTooltipPropertyEdit.Value = emailTooltipProperty;
            }
        }

        #endregion Methods
    }
}