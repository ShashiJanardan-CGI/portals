﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SenderAddressWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.SenderAddressWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/SenderAddressWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div style="display: none" id="SenderAddressDisplay" data-bind="visible: true, if: SelectedProductSuggestion()">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <span runat="server" id="headerPropertyDisplay"></span>
        </div>
        <div class="paddings" data-bind="with: SenderAddress">
            <div class="leftPart"><span runat="server" id="senderTypeLabelPropertyDisplay"></span></div>
            <div class="rightPart rightPartText">
                <input type="radio" name="senderType" data-bind="checked: IsCompany, checkedValue: false" /><label for="senderTypePrivate" class="rightMargin"><span runat="server" id="privateLabelPropertyDisplay"></span></label>
                <input type="radio" name="senderType" data-bind="checked: IsCompany, checkedValue: true" class="leftMargin" /><label for="senderTypeCompany"><span runat="server" id="companyLabelPropertyDisplay"></span></label>
            </div>
            <div class="bothClear bottomMargin"></div>

            <div class="bottomMargin" data-bind="visible: IsCompany()">
                <div class="leftPart">
                    <span runat="server" id="companyNameLabelPropertyDisplay"></span>
                    <span data-bind="if: IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" maxlength="30" data-bind="value: CompanyName" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div class="bottomMargin" data-bind="visible: IsCompany()">
                <div class="leftPart">
                    <span runat="server" id="organizationNumberLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" data-bind="value: OrganizationNumber" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderNameDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="nameLabelPropertyDisplay"></span>
                    <span data-bind="if: !IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="senderName" maxlength="30" data-bind="value: Name" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderAddressDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="addressLabelPropertyDisplay"></span>
                    <span data-bind="if: !IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="SenderAddress" maxlength="30" data-bind="value: AddressField1" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderAddress2Display" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="address2LabelPropertyDisplay"></span>
                    <span class="circle tooltip">
                        <div class="buttonGradient insideCircle bold">?</div>
                        <span runat="server" id="address2TooltipPropertyDisplay"></span>
                    </span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" maxlength="30" data-bind="value: AddressField2" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderZipCodeDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="zipLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <div class="leftFloat">
                        <input type="text" class="field smallField" id="senderZipCode" data-bind="value: ZipCode" />
                    </div>
                    <div class="leftFloat relativePosition">
                        <span class="ajaxSmall ajaxLeft" id="senderCityProgress" style="display: none;" data-bind="visible: $root.ShowSenderZipProgress"></span>
                    </div>
                </div>
                <div class="bothClear">
                    <span>
                      <br />
                      <span class="errorText" data-bind="visible: $root.InvalidZipMsg">Ogiltigt postnummer</span>
                    </span>
                </div>
            </div>

            <div id="senderCityDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="cityLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <div class="ajaxSmallRightContainer">
                        <div class="leftFloat">
                            <input type="text" class="field mediumField" id="senderCity" runat="server" data-bind="if:!($root.InvalidZipMsg), value: City" />
                        </div>
                    </div>
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderEmailDisplay" class="bottomMargin" data-bind="visible: $root.SelectedProductSuggestion().Product.ServiceProperties.SenderEmail == 1 || $root.SelectedProductSuggestion().Product.ServiceProperties.SenderEmail == 2 ">
                <div class="leftPart">
                    <span runat="server" id="emailLabelPropertyDisplay"></span>
                    <span class="circle tooltip">
                        <div class="buttonGradient insideCircle bold">?</div>
                        <span runat="server" id="emailTooltipPropertyDisplay"></span>
                    </span>
                </div>
                <div class="rightPart" >
                    <input type="text" class="field largestField" id="senderEmail" maxlength="63" data-bind="value: Email" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="senderEmailConfirmDisplay" class="bottomMargin" data-bind="visible: $root.SelectedProductSuggestion().Product.ServiceProperties.SenderEmail == 1 || $root.SelectedProductSuggestion().Product.ServiceProperties.SenderEmail == 2 ">
                <div class="leftPart">
                    <span runat="server" id="email2LabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="senderEmailConfirm" maxlength="63" data-bind="value: Email2" />
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
    </div>
</div>
