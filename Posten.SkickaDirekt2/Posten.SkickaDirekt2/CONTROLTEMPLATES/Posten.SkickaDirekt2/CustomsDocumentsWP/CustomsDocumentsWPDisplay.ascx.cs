﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class CustomsDocumentsWPDisplay : UserControl
    {
        #region Fields

        private string aboutLabelProperty;
        private string headerProperty;
        private string customsDocumentsInfoLinkProperty;

        #endregion Fields

        #region Properties

        public string AboutLabelProperty
        {
            get
            {
                return aboutLabelProperty;
            }
            set
            {
                aboutLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CustomsDocumentsInfoLinkProperty
        {
            get
            {
                return customsDocumentsInfoLinkProperty;
            }
            set
            {
                customsDocumentsInfoLinkProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.headerPropertyDisplay.InnerText = headerProperty;
            this.aboutLabelPropertyDisplay.InnerText = aboutLabelProperty;
            this.customsDocumentsInfoLink.HRef = customsDocumentsInfoLinkProperty;
        }

        #endregion Methods
    }
}