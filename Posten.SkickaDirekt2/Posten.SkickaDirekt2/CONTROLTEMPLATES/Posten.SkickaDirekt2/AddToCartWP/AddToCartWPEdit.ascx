﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddToCartWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.AddToCartWPEdit" %>

<div id="AddToCartWPEdit">
    <div id="MainErrorMessage" class="errorMessage rounded paddings bottomMargin">
        <div class="leftPartEdit">Postförskott:</div>
        <div class="rightPartEdit">
            <textarea id="accountValidationPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
        <div class="leftPartEdit">Avsändare:</div>
        <div class="rightPartEdit">
            <textarea id="senderValidationPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
        <div class="leftPartEdit">Mottagare:</div>
        <div class="rightPartEdit">
            <textarea id="receiverValidationPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
        </div>
    </div>
    <div class="leftFloat bottomMargin"></div>
    <div class="buttonContainer rightFloat bottomMargin largeButton addToCartButton centerText rounded buttonGradient">
        <input class="textareaEdit" id="addToCartButtonPropertyEdit" runat="server" type="text" enableviewstate="true" />
    </div>
    <div class="bothClear"></div>
</div>