﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddToCartWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.AddToCartWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink2" Name="/Posten.SkickaDirekt2/JS/AddToCartWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="AddToCartWPDisplay" style="display: none" data-bind="visible: true, if: SelectedProductSuggestion()">
    
    <div id="CartErrorMessage" style="display: none" class="errorMessage rounded paddings bottomMargin" data-bind="visible: !IsValid()">
        <div data-bind="visible: !AccountIsValid()">
            <span runat="server" id="accountValidationPropertyDisplay"></span>
        </div>
        <div data-bind="visible: !SenderAddressIsValid()">
            <span runat="server" id="senderValidationPropertyDisplay"></span>
        </div>
        <div data-bind="visible: !ReceiverAddressIsValid()">
            <span runat="server" id="recevicerValidationPropertyDisplay"></span>
        </div>
        <div data-bind="visible: ReceiverAddressIsValid()&& SenderAddressIsValid() && AccountIsValid()">
            <span runat="server" id="generalErrorDisplay">Något gick tyvärr fel. Vänligen kontrollera dina indata och försök igen.</span>
        </div>
        <div data-bind="visible: true, if: ISPValidationError !==''">
            <span runat="server" id="ispValidationError" data-bind="text: ISPValidationError"></span>
        </div>
    </div>
    <div class="leftFloat bottomMargin"></div>
    <div class="rightFloat ajaxLargeLeftContainer">
        <span class="ajaxLarge ajaxLeft" id="addToCartProgress" style="display: none;" data-bind="visible: ShowProgress()"></span>
        <div class="buttonContainer rightFloat bottomMargin">
            <input type="button" id="addToCartButton" class="largeButton addToCartButton" runat="server" data-bind="click: AddToCartClick, value: IsShoppingCartEditMode() ? 'Uppdatera' : 'Lägg i varukorgen', disable: ShowProgress()" />
        </div>
    </div>
    <div class="bothClear"></div>
    <div class="bottomMargin">
        När du beställt och betalat börjar vi producera frakthandlingen enligt dina anvisningar. Köpet omfattas därför inte av ångerrätt och du har ingen möjlighet att ångra ditt köp. Köpt frakthandling är giltig i 60 dagar.
    </div>
    <div class="bothClear"></div>
</div>

<script type="text/javascript">
    var ConfirmationDownloadUrl = "<%= ConfirmationUrlProperty%>";
    var ConfirmationPageUrl = "<%= ConfirmationPageUrlProperty%>";
    var ProduceUrl = "<%= ProduceUrlProperty%>";
    var ApplicationUrl = "<%= ApplicationUrlProperty%>";
    var EditUrl = "<%= EditUrlProperty%>";
    var WaybillUrl = "<%= WaybillUrlProperty%>";
</script>