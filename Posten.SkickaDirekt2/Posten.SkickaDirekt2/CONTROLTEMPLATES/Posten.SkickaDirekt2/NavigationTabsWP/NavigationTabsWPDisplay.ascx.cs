﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    public partial class NavigationTabsWPDisplay : UserControl
    {
        #region Fields

        private string tab1TitleProperty;
        private string tab1UrlProperty;
        private string tab2TitleProperty;
        private string tab2UrlProperty;
        private string tab3TitleProperty;
        private string tab3UrlProperty;

        #endregion Fields

        #region Properties

        public string Tab1TitleProperty
        {
            get
            {
                return tab1TitleProperty;
            }
            set
            {
                tab1TitleProperty = value;
            }
        }

        public string Tab1UrlProperty
        {
            get
            {
                return tab1UrlProperty;
            }
            set
            {
                tab1UrlProperty = value;
            }
        }

        public string Tab2TitleProperty
        {
            get
            {
                return tab2TitleProperty;
            }
            set
            {
                tab2TitleProperty = value;
            }
        }

        public string Tab2UrlProperty
        {
            get
            {
                return tab2UrlProperty;
            }
            set
            {
                tab2UrlProperty = value;
            }
        }

        public string Tab3TitleProperty
        {
            get
            {
                return tab3TitleProperty;
            }
            set
            {
                tab3TitleProperty = value;
            }
        }

        public string Tab3UrlProperty
        {
            get
            {
                return tab3UrlProperty;
            }
            set
            {
                tab3UrlProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            List<HtmlAnchor> links = new List<HtmlAnchor>();

            this.tab1TitlePropertyDisplay.InnerText = this.tab1TitleProperty;
            this.tab1Url.HRef = this.tab1UrlProperty;
            links.Add(this.tab1Url);
            this.tab2TitlePropertyDisplay.InnerText = this.tab2TitleProperty;
            //this.tab2Url.HRef = this.tab2UrlProperty;
            links.Add(this.tab2Url);
            this.tab3TitlePropertyDisplay.InnerText = this.tab3TitleProperty;
            //this.tab3Url.HRef = this.tab3UrlProperty;
            links.Add(this.tab3Url);

            // Match tab urls with current page url to highlight correct tab.
            foreach (HtmlAnchor link in links)
            {
                if (!string.IsNullOrEmpty(link.HRef) && HttpContext.Current.Request.Url.ToString().EndsWith(link.HRef))
                {
                    link.Attributes.Add("class", "active");
                }
            }
        }

        #endregion Methods
    }
}