﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class NavigationTabsWPEdit : UserControl
    {
        #region Fields

        private string tab1TitleProperty;
        private string tab1UrlProperty;
        private string tab2TitleProperty;
        private string tab2UrlProperty;
        private string tab3TitleProperty;
        private string tab3UrlProperty;

        #endregion Fields

        #region Properties

        public string Tab1TitleProperty
        {
            get
            {
                return tab1TitlePropertyEdit.Value;
            }
            set
            {
                tab1TitleProperty = value;
            }
        }

        public string Tab1UrlProperty
        {
            get
            {
                return tab1UrlPropertyEdit.Value;
            }
            set
            {
                tab1UrlProperty = value;
            }
        }

        public string Tab2TitleProperty
        {
            get
            {
                return tab2TitlePropertyEdit.Value;
            }
            set
            {
                tab2TitleProperty = value;
            }
        }

        public string Tab2UrlProperty
        {
            get
            {
                return tab2UrlPropertyEdit.Value;
            }
            set
            {
                tab2UrlProperty = value;
            }
        }

        public string Tab3TitleProperty
        {
            get
            {
                return tab3TitlePropertyEdit.Value;
            }
            set
            {
                tab3TitleProperty = value;
            }
        }

        public string Tab3UrlProperty
        {
            get
            {
                return tab3UrlPropertyEdit.Value;
            }
            set
            {
                tab3UrlProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tab1TitlePropertyEdit.Value))
            {
                tab1TitlePropertyEdit.Value = tab1TitleProperty;
            }
            if (string.IsNullOrEmpty(tab2TitlePropertyEdit.Value))
            {
                tab2TitlePropertyEdit.Value = tab2TitleProperty;
            }
            if (string.IsNullOrEmpty(tab3TitlePropertyEdit.Value))
            {
                tab3TitlePropertyEdit.Value = tab3TitleProperty;
            }
            if (string.IsNullOrEmpty(tab1UrlPropertyEdit.Value))
            {
                tab1UrlPropertyEdit.Value = tab1UrlProperty;
            }
            if (string.IsNullOrEmpty(tab2UrlPropertyEdit.Value))
            {
                tab2UrlPropertyEdit.Value = tab2UrlProperty;
            }
            if (string.IsNullOrEmpty(tab3UrlPropertyEdit.Value))
            {
                tab3UrlPropertyEdit.Value = tab3UrlProperty;
            }

            tab1TitleLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_TitleLable") as string;
            tab2TitleLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_TitleLable") as string;
            tab3TitleLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_TitleLable") as string;
            tab1UrlLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_UrlLable") as string;
            tab2UrlLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_UrlLable") as string;
            tab3UrlLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_UrlLable") as string;
        }

        #endregion Methods
    }
}