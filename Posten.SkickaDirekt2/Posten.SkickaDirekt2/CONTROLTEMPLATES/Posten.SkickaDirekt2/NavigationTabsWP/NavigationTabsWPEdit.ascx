﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationTabsWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.NavigationTabsWPEdit" %>

<div id="NavigationTabsEdit">
    <ul class="navigationTabListEdit">
        <li class="navigationTabEdit">
            <a>
                <span id="tab1TitleLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab1TitlePropertyEdit" runat="server" enableviewstate="true" class="inputEdit" /><br />
                <span id="tab1UrlLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab1UrlPropertyEdit" runat="server" enableviewstate="true" class="inputEdit" />
            </a>
        </li>
        <li class="navigationTabEdit">
            <a>
                <span id="tab2TitleLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab2TitlePropertyEdit" runat="server" enableviewstate="true" class="inputEdit" /><br />
                <span id="tab2UrlLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab2UrlPropertyEdit" runat="server" enableviewstate="true" class="inputEdit" />
            </a>
        </li>
        <li class="navigationTabEdit">
            <a>
                <span id="tab3TitleLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab3TitlePropertyEdit" runat="server" enableviewstate="true" class="inputEdit" /><br />
                <span id="tab3UrlLabel" runat="server" class="labelFixedWidth"></span>
                <input type="text" id="tab3UrlPropertyEdit" runat="server" enableviewstate="true" class="inputEdit" />
            </a>
        </li>
    </ul>
    <div class="bothClear"></div>
</div>