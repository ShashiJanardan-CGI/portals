﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Utilities;

    public partial class JavaScriptReferences : UserControl
    {
        #region Methods

        public static string StyleLibraryScriptPath(string scriptName)
        {
            return SPUtility.GetServerRelativeUrlFromPrefixedUrl(string.Format(SPContext.Current.Site.Url + "/_layouts/Posten.SkickaDirekt2/JS/{0}? v={1}", scriptName, (Guid.NewGuid()).ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion Methods
    }
}