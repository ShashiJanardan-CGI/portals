﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JavaScriptReferences.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.JavaScriptReferences" %>

<SharePoint:ScriptLink runat="server" ID="slSkicka2Core" Name="/_layouts/Posten.SkickaDirekt2/JS/SkickaDirekt2-Core.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="SD2Jquery" Name="/_layouts/Posten.SkickaDirekt2/JS/jquery-1.11.0.min.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="SD2Knockout" Name="/_layouts/Posten.SkickaDirekt2/JS/knockout-3.1.0.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="SD2KnockoutValidation" Name="/_layouts/Posten.SkickaDirekt2/JS/knockout.validation.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="SD2KnockoutLocalization" Name="/_layouts/Posten.SkickaDirekt2/JS/sv-se.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/_layouts/Posten.SkickaDirekt2/JS/knockout-postbox.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>


<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("SkickaDirekt2-Core.js")%>'></script>
<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("jquery-1.11.0.min.js")%>'></script>
<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("knockout-3.1.0.js")%>'></script>
<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("knockout.validation.js")%>'></script>
<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("sv-se.js")%>'></script>
<script type="text/javascript" language="javascript" src='<%= StyleLibraryScriptPath("knockout-postbox.js")%>'></script>


<span></span>