﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ShoppingCartWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/ShoppingCartWP.js" Localizable="false" />

<div id="ShoppingCartDisplay" class="relativePosition shoppingCart" data-bind="scrollToView: CartScrollTo()">
    <div class="shoppingCartIcon">
        <div class="shoppingCartIconContent buttonGradient">
            <span></span>
        </div>
    </div>
    <div class="rounded visibleOverflow">
        <div class="buttonGradient paddings topRounded header whiteText"><span runat="server" id="headerPropertyDisplay"></span></div>
        <div class="bottomBorders bottomRounded greyBackground" data-bind="ifnot: ShoppingCartList() && ShoppingCartList().length > 0">
            <div class="paddings">
                <span runat="server" id="cartIsEmptyPropertyDisplay"></span>
            </div>
        </div>

        <!-- ko if: ShoppingCartList() && ShoppingCartList().length > 0 -->
        <div class="bottomBorders bottomRounded greyBackground visibleOverflow">
            <div class="paddings bottomBorder bold">Artikel</div>
        <!-- ko foreach: ShoppingCartList -->
            <div class="paddings bottomBorder" id="itemPanel">
                <div class="rightCart">
                    <button class="shoppingCartButton rightMargin" type="button" data-bind="click: $parent.editItem">
                        <span class="editButton"></span>
                    </button><button class="shoppingCartButton" type="button" data-bind="click: $parent.confirmRemoveItem">
                        <span class="deleteButton"></span>
                    </button>
                </div>
                <div class="leftCart carttip">
                    <div class="bold" data-bind="text: ProductName"></div>
                    <div data-bind="text: ToAddress.Name"></div>
                    <div data-bind="text: GenerateAdditionalServiceString(AdditionalServices)"></div>
                    <div data-bind="text: Price + ' kr'"></div>
                    <div data-bind="visible: (AdditionalInformation !== null) && (skickaAddToCartVM.SelectedProductSuggestion() !== null) && (skickaAddToCartVM.SelectedProductSuggestion() !== undefined) && (parseFloat(skickaAddToCartVM.SelectedProductSuggestion().Price.Amount()).toFixed(2).toLocaleString().replace('.', ',') !== Price.replace(' ','')) && (Index == 1)">(Ytterligare kostnader ingår)</div>
                    <span data-bind="html: GenerateTooltipString(FreeText1, FreeText2, AdditionalInformation)"></span>
                </div>
                <div class="bothClear"></div>
            </div>

            <div class="dialogContainer" data-bind="visible: $parent.ShoppingCartRemoveConfirm() === OrderNumber" style="display: none;">
                <div class="rounded dialogContent">
                    <div class="buttonGradient paddings whiteText header topRounded">
                        <span id="confirmHeaderPropertyDisplay" runat="server"></span>
                    </div>
                    <div class="bottomRounded bottomBorders lightGradient paddings">
                        <div class="bottomMargin"><span id="confirmQuestionStartPropertyDisplay" runat="server"></span> <span data-bind="text: ProductName"></span> <span id="confirmQuestionEndPropertyDisplay" runat="server"></span></div>
                        <div class="bottomMargin">
                            <div data-bind="text: ToAddress.Name"></div>
                            <div data-bind="text: ToAddress.AddressField1"></div>
                            <div><span data-bind="text: ToAddress.ZipCode"></span> <span data-bind="text: ToAddress.City"></span></div>
                            <div data-bind="text: GenerateAdditionalServiceString(AdditionalServices)"></div>
                            <div data-bind="text: Price + ' kr'"></div>
                        </div>
                        <div>
                            <input type="button" class="smallButton leftMargin rightFloat" data-bind="click: $parent.noRemoveItem" value="Nej" /><input type="button" class="smallButton rightFloat" data-bind="click: $parent.yesRemoveItem" value="Ja" />
                        </div>
                    </div>
                </div>
            </div>

        <!-- /ko -->
            <div class="paddings bottomBorder">
                <div class="leftFloat header">
                    Totalt
                </div>
                <div class="rightFloat header" data-bind="text: Total()">
                </div>
                <div class="bothClear"></div>
            </div>
            <div class="paddings">
                <div class="rightFloat ajaxLargeLeftContainer">
                    <span class="ajaxLarge ajaxLeft" id="checkoutProgress" style="display: none;" data-bind="visible: ShowCheckoutProgress()"></span>
                    <div class="buttonContainer rightFloat">
                        <input type="button" class="largeButton mainButton" value="Till kassan" id="checkoutButton" runat="server" onserverclick="checkoutButton_ServerClick" data-bind="disable: ShowCheckoutProgress(), click: checkoutClick" />
                    </div>
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
        <!-- /ko -->
    </div>
    <input type="hidden" id="basketIDHolder" runat="server"/>
    <div class="ajaxCover" id="shoppingCartProgress" style="display: none;" data-bind="visible: ShoppingCartProgress"></div>
</div>
