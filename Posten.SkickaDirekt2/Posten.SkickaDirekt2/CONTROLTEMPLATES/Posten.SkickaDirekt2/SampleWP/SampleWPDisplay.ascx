﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SampleWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.SampleWPDisplay" ClassName="SampleWPDisplay" %>

<div id="SampleWebPart" class="borders rounded paddings">
    <span>This is a test webpart - display mode!</span>
    <div class="topMargin">
        <select class="field mediumField" data-bind="options: destinations,
    optionsText: 'Name',
    optionsCaption: 'Välj land',
    value: SelectedDestination">
        </select>
        <span data-bind="with: SelectedDestination">Information: <span data-bind="    text: Information"></span></span>
    </div>

    <div class="topMargin">
        <span id="propertyDisplay" runat="server"></span>
    </div>
</div>