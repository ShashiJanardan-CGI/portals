﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectServiceWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.SelectServiceWPDisplay" ClassName="SelectServiceWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/SelectServiceWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="SelectServiceDisplay">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header topRounded">
            <span runat="server" id="headerPropertyDisplay"></span>
        </div>
        <div class="paddings topBorder">
            <div class="leftPart">
                <span runat="server" id="countryLabelPropertyDisplay"></span>
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                    <span runat="server" id="countryTooltipPropertyDisplay"></span>
                </span>
            </div>
            <div class="rightPart">
                <div class="ajaxSmallRightContainer">
                    <select class="field largestField" data-bind="options: Destinations,
        optionsText: 'Name',
        value: SelectedDestination">
                    </select>
                    <span class="ajaxSmall ajaxRight" id="selectDestinationProgress" style="display: none;" data-bind="visible: DestinationsProgress"></span>
                    <div class="topPadding">
                        <a data-bind="attr: { href: CountryInformationLink }" target="_blank" >Information om valt land</a>
                    </div>
                </div>
            </div>
            <div class="bothClear"></div>
        </div>
        <div class="paddings topBorder">
            <div class="leftPart">
                <span runat="server" id="sendServiceLabelPropertyDisplay"></span>
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                    <span runat="server" id="sendServiceTooltipPropertyDisplay"></span>
                </span>
            </div>
            <div class="rightPart">
                <div class="ajaxSmallRightContainer">
                    <input type="radio" name="guide" data-bind="checked: IsServiceGuideSelected, checkedValue: false" onclick="skickaSelectServiceVM.SwitchServiceTypeClick()" />
                    <select class="field largeField" data-bind="options: Products,
        optionsCaption: 'Välj tjänst',
        optionsText: 'Name',
        value: $root.SelectedProduct,
        disable: $root.IsServiceGuideSelected()">
                    </select>
                    <span class="ajaxSmall ajaxRight" id="selectServiceProgress" style="display: none;" data-bind="visible: ServicesProgress"></span>
                    <br />
                    <label>
                        <input type="radio" name="guide" data-bind="checked: IsServiceGuideSelected, checkedValue: true" onclick="skickaSelectServiceVM.SwitchServiceTypeClick()" />
                        <span runat="server" id="chooseServiceLabel"></span>
                    </label>
                </div>
            </div>
            <div class="bothClear"></div>
        </div>
        <div class="paddings topBorder">
            <div class="leftPartSize">
                <span runat="server" id="sizeLabelPropertyDisplay"></span>
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                    <span runat="server" id="sizeTooltipPropertyDisplay"></span>
                </span>
            </div>
            <div class="rightPartSize" data-bind="if: !SelectedProduct() || (SelectedProduct() && SelectedProduct().ServiceProperties.LetterAllowed)">
                <div class="leftFloat bottomPadding" data-bind="if: !SelectedProduct() || (SelectedProduct() && SelectedProduct().ServiceProperties.LetterAllowed)">
                    <img id="selectServiceLetterImage" runat="server" />
                </div>
                <div class="radioButtonContainer rounded transparentBorder" data-bind="css: { invalidElement: (!CurrentSize().Length.isValid() || !CurrentSize().Width.isValid() || !CurrentSize().Height.isValid() || !CurrentSize().Diameter.isValid()) && TypeOfSize() != 'own' }">
                    <label>
                        <input type="radio" name="standard" data-bind="checkedValue: 'C4', checked: TypeOfSize, click: function () {
    SetCurrentSize(skickaSelectServiceVM.C4);
    return true;
}" />
                        <span runat="server" id="envelopeC4"></span>
                    </label>
                    <label>
                        <input type="radio" name="standard" data-bind="checkedValue: 'C5', checked: TypeOfSize, click: function () {
    SetCurrentSize(skickaSelectServiceVM.C5);
    return true;
}" />
                        <span runat="server" id="envelopeC5"></span>
                    </label>
                    <label>
                        <input type="radio" name="standard" data-bind="checkedValue: 'own', checked: TypeOfSize, click: function () {
    SetCurrentSize(skickaSelectServiceVM.ClearSize);
    return true;
}" />
                        <span runat="server" id="measureMyself"></span>
                    </label>
                </div>
            </div>
            <div class="bothClear"></div>
            <div data-bind="visible: TypeOfSize() == 'own' || (SelectedProduct() && (!SelectedProduct().ServiceProperties.LetterAllowed && TypeOfSize() != 'own'))">
                <div class="leftPartSize">
                </div>
                <div class="rightPartSize" >
                    <div class="leftFloat">
                        <img id="selectServiceParcelImage" runat="server" />
                    </div>
                    <div class="leftFloat">
                        <div class="leftMargin sizeBottomMargin" runat="server" id="lengthLabel"></div>
                        <div class="leftFloat sizeInput">
                            <input type="text" class="field smallestField" data-bind="value: CurrentSize().Length" />
                        </div>
                        <div class="rightFloat topPadding">
                            <span runat="server" id="lengthUnit"></span>
                        </div>
                        <div class="bothClear"></div>
                        <div class="leftFloat">
                            <span data-bind="if: SelectedProduct()">
                                <span data-bind="html: LengthBoundaries()" class="italic"></span>
                            </span>
                        </div>
                    </div>
                    <div class="leftFloat leftMargin" data-bind="if: !CurrentSize().IsRoll(), visible:true">
                        <div class="leftMargin sizeBottomMargin" runat="server" id="widthLabel"></div>
                        <div class="leftFloat sizeInput">
                            <input type="text" class="field smallestField" data-bind="value: CurrentSize().Width" />
                        </div>
                        <div class="rightFloat topPadding smallWidth"><span runat="server" id="widthUnit"></span></div>
                        <div class="bothClear"></div>
                        <div class="leftFloat">
                            <span data-bind="if: SelectedProduct()">
                                <span data-bind="html: WidthBoundaries()" class="italic"></span>
                            </span>
                        </div>
                    </div>
                    <div class="leftFloat leftMargin" data-bind="if: !CurrentSize().IsRoll(), visible: true">
                        <div class="leftMargin sizeBottomMargin" runat="server" id="heightLabel"></div>
                        <div class="leftFloat sizeInput">
                            <input type="text" class="field smallestField" data-bind="value: CurrentSize().Height" />
                        </div>
                        <div class="rightFloat topPadding smallWidth"><span runat="server" id="heightUnit"></span></div>
                        <div class="bothClear"></div>
                        <div class="leftFloat">
                            <span data-bind="if: SelectedProduct()">
                                <span data-bind="html: HeightBoundaries()" class="italic"></span>
                            </span>
                        </div>
                    </div>
                    <div class="leftFloat leftMargin" style="display: none" data-bind="if: CurrentSize().IsRoll(), visible: true">
                        <div class="leftMargin sizeBottomMargin" runat="server" id="diameterLabel"></div>
                        <div class="leftFloat sizeInput">
                            <input type="text" class="field smallestField" data-bind="value: CurrentSize().Diameter" />
                        </div>
                        <div class="rightFloat topPadding smallWidth"><span runat="server" id="diameterUnit"></span></div>
                        <div class="bothClear"></div>
                        <div class="leftFloat">
                            <div data-bind="if: SelectedProduct()">
                                <span data-bind="html: DiameterBoundaries()" class="italic"></span>
                            </div>
                        </div>
                    </div>
                    
                        <div class="leftFloat leftMargin selectServiceRoll" data-bind="visible: ((SelectedProduct() == undefined)||(SelectedProduct().ShowRoll)== true)">
                            <input id="tube" type="checkbox" value="true" data-bind="checked: CurrentSize().IsRoll" />
                            <label for="tube">
                                <span runat="server" id="sendTubeLabel"></span>
                            </label>
                        </div>
                    
                   
                </div>
            </div>
            <div style="display: none" data-bind="visible: (!CurrentSize().Length.isValid() || !CurrentSize().Width.isValid() || !CurrentSize().Height.isValid() || !CurrentSize().Diameter.isValid()) && TypeOfSize() != 'own'">
                <div class="leftPart">
                    <div class="rightFloat rightPartText">
                    </div>
                </div>
                <div class="rightPart">
                    <div class="leftFloat errorText">
                        <span id="sizeErrorPropertyDisplay" runat="server"></span>
                    </div>
                </div>
            </div>
            <div class="bothClear"></div>
            
        </div>
        <div class="paddings topBorder">
            <div class="leftPart">
                <span runat="server" id="weightLabelPropertyDisplay"></span>
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                    <span runat="server" id="weightTooltipPropertyDisplay"></span>
                </span>
            </div>
                        
            <div class="rightPart rightPartText">                
                <div class="section" data-bind="visible: !(!ShowWeightBox())"> 
                    <div class="leftFloat" data-bind="validationOptions: { insertMessages: false, decorateInputElement: false }">
                        <input class="field mediumField" type="text" data-bind="value: CurrentWeight().Value, css: { invalidElement: ((CurrentWeight().CalculatedWeight() && !CurrentWeight().CalculatedWeight.isValid()) || (!CurrentWeight().Value.isValid())) }" />
                        <div data-bind="if: SelectedProduct()">
                            <span data-bind="html: WeightBoundaries()"></span>
                        </div>
                    </div>
                    <div class="topPadding">
                        <span>
                            <label>
                                <input type="radio" name="measure" id="gRadio" value="1" data-bind="checked: (CurrentWeight().Multiplier('1') !== undefined)"  >
                                g
                            </label>
                            <label>
                                <input type="radio" name="measure" id="kgRadio" value="1000"  data-bind="checked: (CurrentWeight().Multiplier('1000') !== undefined)">
                                kg
                            </label>
                        </span>
                    </div>
                </div>
                <div class="section" style="display:none"  data-bind="visible: SelectedProduct() && !ShowWeightBox()"> 
                    <div class="radioButtonContainer rounded transparentBorder" data-bind="css: { invalidElement: ((CurrentWeight().CalculatedWeight() && !CurrentWeight().CalculatedWeight.isValid()) || (CurrentWeight().Value() && !CurrentWeight().Value.isValid())) }">
                        <div data-bind="validationOptions: { insertMessages: false, decorateInputElement: false }">
                            <div data-bind="if: SelectedProduct()">
                            <span data-bind="foreach: SelectedProduct().WeightIntervals()">
                                <label>
                                    <input type="radio" name="weightGroup" data-bind="value: $data.WeightInGrams" onclick="skickaSelectServiceVM.SetSelectedWeight(this.value, 'g')" />
                                    <span data-bind="text: RadioText()"></span>
                                </label>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="errorText" data-bind="visible: !WeightValidationErrors.isValid()">
                    <span data-bind="text: CurrentWeight().CalculatedWeight.error, visible: !(CurrentWeight().CalculatedWeight.isValid())"></span>
                    <span data-bind="text: CurrentWeight().Value.error, visible: !(CurrentWeight().Value.isValid())"></span>                    
                </div>
            </div>
            <div class="bothClear"></div>
        </div>
        <div style="display: none" data-bind="if: IsServiceGuideSelected, visible: true">
            <div class="paddings topBorder">
                <div class="leftPart">
                    <span runat="server" id="serviceFilterLabelPropertyDisplay"></span>
                    <span class="circle tooltip">
                        <div class="buttonGradient insideCircle bold">?</div>
                        <span runat="server" id="serviceFilterTooltipPropertyDisplay"></span>
                    </span>
                </div>
                <div class="rightPart">
                    <input type="checkbox" id="fast" class="selectServiceImportant" data-bind="checked: SelectedFilters, checkedValue: Filters[0]" />
                    <label for="fast"><span runat="server" id="fastButtonPropertyDisplay"></span></label>

                    <input type="checkbox" id="secure" class="selectServiceImportant" data-bind="checked: SelectedFilters, checkedValue: Filters[1]" />
                    <label for="secure"><span runat="server" id="secureButtonPropertyDisplay"></span></label>

                    <input type="checkbox" id="traceable" class="selectServiceImportant" data-bind="checked: SelectedFilters, checkedValue: Filters[2]" />
                    <label for="traceable"><span runat="server" id="traceableButtonPropertyDisplay"></span></label>
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var countryInfoLink = "<%= this.CountryInformationProperty %>";
</script>