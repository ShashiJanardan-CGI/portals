﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class SelectServiceWPEdit : UserControl
    {
        #region Fields

        private string countryInformationLinkProperty;
        private string countryLabelProperty;
        private string countryTooltipProperty;
        private string fastButtonProperty;
        private string headerProperty;
        private string secureButtonProperty;
        private string sendServiceLabelProperty;
        private string sendServiceTooltipProperty;
        private string serviceFilterLabelProperty;
        private string serviceFilterTooltipProperty;
        private string sizeErrorProperty;
        private string sizeLabelProperty;
        private string sizeTooltipProperty;
        private string traceableButtonProperty;
        private string weightLabelProperty;
        private string weightTooltipProperty;
        

        #endregion Fields

        #region Properties

        public string CountryInformationLinkProperty
        {
            get
            {
                return countryInformationLinkPropertyEdit.Value;
            }
            set
            {
                countryInformationLinkProperty = value;
            }
        }

        public string CountryLabelProperty
        {
            get
            {
                return countryLabelPropertyEdit.Value;
            }
            set
            {
                countryLabelProperty = value;
            }
        }

        public string CountryTooltipProperty
        {
            get
            {
                return countryTooltipPropertyEdit.Value;
            }
            set
            {
                countryTooltipProperty = value;
            }
        }

        public string FastButtonProperty
        {
            get
            {
                return fastButtonPropertyEdit.Value;
            }
            set
            {
                fastButtonProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string SecureButtonProperty
        {
            get
            {
                return secureButtonPropertyEdit.Value;
            }
            set
            {
                secureButtonProperty = value;
            }
        }

        public string SendServiceLabelProperty
        {
            get
            {
                return sendServiceLabelPropertyEdit.Value;
            }
            set
            {
                sendServiceLabelProperty = value;
            }
        }

        public string SendServiceTooltipProperty
        {
            get
            {
                return sendServiceTooltipPropertyEdit.Value;
            }
            set
            {
                sendServiceTooltipProperty = value;
            }
        }

        public string ServiceFilterLabelProperty
        {
            get
            {
                return serviceFilterLabelPropertyEdit.Value;
            }
            set
            {
                serviceFilterLabelProperty = value;
            }
        }

        public string ServiceFilterTooltipProperty
        {
            get
            {
                return serviceFilterTooltipPropertyEdit.Value;
            }
            set
            {
                serviceFilterTooltipProperty = value;
            }
        }

        public string SizeErrorProperty
        {
            get
            {
                return sizeErrorPropertyEdit.Value;
            }
            set
            {
                sizeErrorProperty = value;
            }
        }

        public string SizeLabelProperty
        {
            get
            {
                return sizeLabelPropertyEdit.Value;
            }
            set
            {
                sizeLabelProperty = value;
            }
        }

        public string SizeTooltipProperty
        {
            get
            {
                return sizeTooltipPropertyEdit.Value;
            }
            set
            {
                sizeTooltipProperty = value;
            }
        }

        public string TraceableButtonProperty
        {
            get
            {
                return traceableButtonPropertyEdit.Value;
            }
            set
            {
                traceableButtonProperty = value;
            }
        }

        public string WeightLabelProperty
        {
            get
            {
                return weightLabelPropertyEdit.Value;
            }
            set
            {
                weightLabelProperty = value;
            }
        }

        public string WeightTooltipProperty
        {
            get
            {
                return weightTooltipPropertyEdit.Value;
            }
            set
            {
                weightTooltipProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(countryInformationLinkPropertyEdit.Value))
            {
                countryInformationLinkPropertyEdit.Value = countryInformationLinkProperty;
            }
            if (string.IsNullOrEmpty(countryLabelPropertyEdit.Value))
            {
                countryLabelPropertyEdit.Value = countryLabelProperty;
            }
            if (string.IsNullOrEmpty(sendServiceLabelPropertyEdit.Value))
            {
                sendServiceLabelPropertyEdit.Value = sendServiceLabelProperty;
            }
            if (string.IsNullOrEmpty(sizeLabelPropertyEdit.Value))
            {
                sizeLabelPropertyEdit.Value = sizeLabelProperty;
            }
            if (string.IsNullOrEmpty(weightLabelPropertyEdit.Value))
            {
                weightLabelPropertyEdit.Value = weightLabelProperty;
            }
            if (string.IsNullOrEmpty(serviceFilterLabelPropertyEdit.Value))
            {
                serviceFilterLabelPropertyEdit.Value = serviceFilterLabelProperty;
            }
            if (string.IsNullOrEmpty(countryTooltipPropertyEdit.Value))
            {
                countryTooltipPropertyEdit.Value = countryTooltipProperty;
            }
            if (string.IsNullOrEmpty(sendServiceTooltipPropertyEdit.Value))
            {
                sendServiceTooltipPropertyEdit.Value = sendServiceTooltipProperty;
            }
            if (string.IsNullOrEmpty(sizeTooltipPropertyEdit.Value))
            {
                sizeTooltipPropertyEdit.Value = sizeTooltipProperty;
            }
            if (string.IsNullOrEmpty(weightTooltipPropertyEdit.Value))
            {
                weightTooltipPropertyEdit.Value = weightTooltipProperty;
            }
            if (string.IsNullOrEmpty(serviceFilterTooltipPropertyEdit.Value))
            {
                serviceFilterTooltipPropertyEdit.Value = serviceFilterTooltipProperty;
            }
            if (string.IsNullOrEmpty(fastButtonPropertyEdit.Value))
            {
                fastButtonPropertyEdit.Value = fastButtonProperty;
            }
            if (string.IsNullOrEmpty(secureButtonPropertyEdit.Value))
            {
                secureButtonPropertyEdit.Value = secureButtonProperty;
            }
            if (string.IsNullOrEmpty(traceableButtonPropertyEdit.Value))
            {
                traceableButtonPropertyEdit.Value = traceableButtonProperty;
            }
            if (string.IsNullOrEmpty(sizeErrorPropertyEdit.Value))
            {
                sizeErrorPropertyEdit.Value = sizeErrorProperty;
            }

            this.chooseServiceLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ChooseSercviceLabel") as string;
            this.lengthLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_LengthLabel") as string;
            this.widthLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WidthLabel") as string;
            this.heightLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_HeightLabel") as string;
            this.diameterLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_DiameterLabel") as string;
            this.envelopeC4.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_EnvelopeC4") as string;
            this.envelopeC5.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_EnvelopeC5") as string;
            this.measureMyself.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_MeasureMyself") as string;
            this.heightUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.widthUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.lengthUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.diameterUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.selectServiceLetterImage.Src = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_LetterlImage") as string;
            this.selectServiceParcelImage.Src = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ParcelImage") as string;
            this.weightUnitHigh.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WeightUnitHigh") as string;
            this.weightUnitLow.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WeightUnitLow") as string;
        }

        #endregion Methods
    }
}