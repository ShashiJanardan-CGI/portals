﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class ServicesResultItem : UserControl
    {
        #region Fields

        private string additionsHeaderProperty;
        private string additionsTooltipProperty;
        private string buyOnlineProperty;
        private string morePropertiesLinkProperty;
        private string wayBillProperty;
        private string vatProperty; 
        private string vatFreeProperty; 

        #endregion Fields

        #region Properties
        public string VatFreeProperty
        {
            get
            {
                return vatFreeProperty;
            }
            set
            {
                vatFreeProperty = value;
            }
        }

        public string VatProperty
        {
            get
            {
                return vatProperty;
            }
            set
            {
                vatProperty = value;
            }
        }

        public string AdditionsHeaderProperty
        {
            get
            {
                return additionsHeaderProperty;
            }
            set
            {
                additionsHeaderProperty = value;
            }
        }

        public string AdditionsTooltipProperty
        {
            get
            {
                return additionsTooltipProperty;
            }
            set
            {
                additionsTooltipProperty = value;
            }
        }

        public string BuyOnlineProperty
        {
            get
            {
                return buyOnlineProperty;
            }
            set
            {
                buyOnlineProperty = value;
            }
        }

        public string MorePropertiesLinkProperty
        {
            get
            {
                return morePropertiesLinkProperty;
            }
            set
            {
                morePropertiesLinkProperty = value;
            }
        }

        public string WayBillProperty
        {
            get
            {
                return wayBillProperty;
            }
            set
            {
                wayBillProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.buyOnlinePropertyDisplay.InnerText = buyOnlineProperty;
            this.wayBillButton.Value = wayBillProperty;
            this.morePropertiesLinkPropertyDisplay.InnerText = morePropertiesLinkProperty;
            this.additionsHeaderPropertyDisplay.InnerText = additionsHeaderProperty;
            this.additionsTooltipPropertyDisplay.InnerHtml = additionsTooltipProperty;

            this.VATLabel.InnerText = vatProperty;
            this.NoVATLabel.InnerText = vatFreeProperty;
        }

        #endregion Methods
    }
}