﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class ServicesResultItemWPDisplay : UserControl
    {
        #region Fields

        private string additionsHeaderProperty;
        private string additionsTooltipProperty;
        private string buyOnlineProperty;
        private string headerProperty;
        private string morePropertiesLinkProperty;
        private string moreProposalsLinkProperty;
        private string wayBillProperty;
        private string vatProperty;
        private string vatFreeProperty;

        #endregion Fields

        #region Properties
        public string VatFreeProperty
        {
            get
            {
                return vatFreeProperty;
            }
            set
            {
                vatFreeProperty = value;
            }
        }

        public string VatProperty
        {
            get
            {
                return vatProperty;
            }
            set
            {
                vatProperty = value;
            }
        }

        public string AdditionsHeaderProperty
        {
            get
            {
                return additionsHeaderProperty;
            }
            set
            {
                additionsHeaderProperty = value;
            }
        }

        public string AdditionsTooltipProperty
        {
            get
            {
                return additionsTooltipProperty;
            }
            set
            {
                additionsTooltipProperty = value;
            }
        }

        public string BuyOnlineProperty
        {
            get
            {
                return buyOnlineProperty;
            }
            set
            {
                buyOnlineProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MorePropertiesLinkProperty
        {
            get
            {
                return morePropertiesLinkProperty;
            }
            set
            {
                morePropertiesLinkProperty = value;
            }
        }

        public string MoreProposalsLinkProperty
        {
            get
            {
                return moreProposalsLinkProperty;
            }
            set
            {
                moreProposalsLinkProperty = value;
            }
        }

        public string WayBillProperty
        {
            get
            {
                return wayBillProperty;
            }
            set
            {
                wayBillProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.headerPropertyDisplay.InnerText = headerProperty;
                this.moreProposalsLinkPropertyDisplay.InnerText = moreProposalsLinkProperty;

                ServicesResultItem servicesResultItem = (ServicesResultItem)this.ServicesResultItem;
                servicesResultItem.BuyOnlineProperty = buyOnlineProperty;
                servicesResultItem.WayBillProperty = wayBillProperty;
                servicesResultItem.AdditionsHeaderProperty = additionsHeaderProperty;
                servicesResultItem.AdditionsTooltipProperty = additionsTooltipProperty;
                servicesResultItem.MorePropertiesLinkProperty = morePropertiesLinkProperty;
                servicesResultItem.VatFreeProperty = vatFreeProperty;
                servicesResultItem.VatProperty = vatProperty;
            }
        }

        #endregion Methods
    }
}