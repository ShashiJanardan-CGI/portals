﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesResultItem.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ServicesResultItem" %>

<div data-bind="if: $data !== null && $data.Product!== undefined">
<div class="paddings resultItemHeader topBorder" data-bind="text: $data.Product.Name"></div>

<div data-bind="if: ($data.Product.AdditionalServices().length > 0) && $root.ShowAdditionalServicesRow($data.Product.ProductId)">
    <div class="paddings resultItemOptions topBorder bottomBorder">
        <div class="resultItemOptionsLeft leftFloat">
            <b><span id="additionsHeaderPropertyDisplay" runat="server"></span></b>
            <a href="#" onclick="javascript: return false;" class="circle tooltip">
                <div class="buttonGradient insideCircle bold">?</div>
                <span runat="server" id="additionsTooltipPropertyDisplay"></span>
            </a>
        </div>
        <div class="resultItemOptionsRight leftFloat" data-bind="if: $data.Product !== undefined">
            <div data-bind="foreach: $data.Product.AdditionalServices()">
			<!-- ko if: Code() !== '0000010'  -->
                <div class="leftFloat rightPadding resultItemOption">
                    <label>
                        <input type="checkbox" data-bind="checkedValue: $data, checked: $parent.SelectedAdditionalServices, visible: ($parent.Product !== undefined) && $root.ShowAdditionalService($parent.Product.ProductId, $root.SelectedDestination().EU, $data.Code())" />
                        <span data-bind="visible: ($parent.Product !== undefined) && $root.ShowAdditionalService($parent.Product.ProductId, $root.SelectedDestination().EU, $data.Code()), text: Name() + ' (' + parseFloat(Price.Amount()).toFixed(2).toLocaleString().replace('.', ',') + ' kr' + (Price.VatAmount() == 0 ? '&nbsp; momsfri' : '&nbsp;inkl. moms') + ')'"></span>
                    </label>
                </div>
				<!--/ko -->
            </div>
        </div>
        <div class="bothClear"></div>
    </div>
</div>

<div class="paddings relativePosition">
    <div class="resultItemLeft leftFloat">
        <div class="resultItemLeftMargin" data-bind="html: $data.Product.Description"></div>
    </div>
    <div class="resultItemRight rightFloat leftPadding">
        <div class="resultItemRightContent rightText">
            <div>
                <span class="resultItemPrice" data-bind="text: parseFloat($data.Price.Amount() + $data.AdditionalServicesPrice()).toFixed(2).toLocaleString().replace('.', ',') + ' kr'"></span>
                <br />
                <!-- ko if: !$root.IsShoppingCartEditMode() -->
                <span id="VatAmount" runat="server" data-bind="text: 'varav moms &nbsp;' + parseFloat($data.Price.VatAmount() + $data.AdditionalServicesPriceVat()).toFixed(2).toLocaleString().replace('.', ',') + ' Kr'"></span>
				<!--/ko -->
                <!-- ko if: $root.IsShoppingCartEditMode() -->
                <span id="VatAmountEdit" runat="server" data-bind="text: 'varav moms &nbsp;' + parseFloat($data.Price.VatAmount()).toFixed(2).toLocaleString().replace('.', ',') + ' Kr'"></span>
				<!--/ko -->
                <span id="VATLabel" runat="server" visible="false" data-bind="if: $root.SelectedDestination().EU"></span>
                <span id="NoVATLabel" runat="server" visible="false" data-bind="if: !$root.SelectedDestination().EU"></span>
            </div>
            <div class="buttonContainer topMargin bottomMargin" data-bind="if: !$root.SelectedProductSuggestion()">
                <input type="button" id="wayBillButton" runat="server" class="largeButton resultItemButton" data-bind="click: $root.SelectProductSelectionClick.bind($data)" />
            </div>
            <span id="buyOnlinePropertyDisplay" runat="server"></span>
        </div>
    </div>
    <span class="resultShowMore blueText bold" data-bind="click: function(item, event) { resultItemToggleProperties(event.target, $data.Product.ProductId); }">
        <span class="resultShowMoreClosed" id="morePropertiesLinkPropertyDisplay" runat="server"></span>
    </span>
    <div class="bothClear"></div>
</div>

<div class="leftPadding bottomPadding rightPadding noneDisplay" data-bind="attr: { id: 'resultMoreProperties_' + $data.Product.ProductId  }">
    <div class="borders smallPaddings">
    <div class="resultMoreColumn leftFloat rightPadding">
        <div data-bind="with: $data.Product.AdditionalInfoTexts.Delivery" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" style="display: inline-block; word-wrap: break-word;" data-bind="html: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.DeliveryTime" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" style="word-wrap: break-word;" data-bind="html: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.GoodsValue" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" style="word-wrap: break-word;" data-bind="html: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.IdRequired" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" style="word-wrap: break-word;" data-bind="html: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.Receiver" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" style="word-wrap: break-word;" data-bind="html: LabelText"></div>
            <div class="bothClear"></div>
        </div>
    </div>

    <div class="resultMoreColumn leftFloat">
        <div data-bind="with: $data.Product.AdditionalInfoTexts.Size" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" data-bind="text: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.MaxWeight" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" data-bind="text: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.Submission" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" data-bind="text: LabelText"></div>
            <div class="bothClear"></div>
        </div>
        <div data-bind="with: $data.Product.AdditionalInfoTexts.Traceable" class="smallBottomMargin">
            <div class="title leftFloat" data-bind="text: LabelName"></div>
            <div class="text leftFloat" data-bind="text: LabelText"></div>
            <div class="bothClear"></div>
        </div>
    </div>

    <div class="bothClear"></div>
    </div>
</div>

</div>