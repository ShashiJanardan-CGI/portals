﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class ServicesResultItemWPEdit : UserControl
    {
        #region Fields

        private string additionsHeaderProperty;
        private string additionsTooltipProperty;
        private string buyOnlineProperty; // = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_BuyOnline") as string;
        private string headerProperty;
        private string morePropertiesLinkProperty;
        private string moreProposalsLinkProperty;
        private string wayBillProperty;
        private string vatProperty ; //= HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_inclVAT") as string;
        private string vatFreeProperty; // = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_noVAT") as string;

        #endregion Fields

        #region Properties
        public string VatFreeProperty
        {
            get
            {
                return noVatTextValueEdit.Value;
            }
            set
            {
                vatFreeProperty = value;
            }
        }

        public string VatProperty
        {
            get
            {
                return vatTextValueEdit.Value;
            }
            set
            {
                vatProperty = value;
            }
        }

        public string AdditionsHeaderProperty
        {
            get
            {
                return additionsHeaderPropertyEdit.Value;
            }
            set
            {
                additionsHeaderProperty = value;
            }
        }

        public string AdditionsTooltipProperty
        {
            get
            {
                return additionsTooltipPropertyEdit.Value;
            }
            set
            {
                additionsTooltipProperty = value;
            }
        }

        public string BuyOnlineProperty
        {
            get
            {
                return buyOnlinePropertyEdit.Value;
            }
            set
            {
                buyOnlineProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MorePropertiesLinkProperty
        {
            get
            {
                return morePropertiesLinkPropertyEdit.Value;
            }
            set
            {
                morePropertiesLinkProperty = value;
            }
        }

        public string MoreProposalsLinkProperty
        {
            get
            {
                return moreProposalsLinkPropertyEdit.Value;
            }
            set
            {
                moreProposalsLinkProperty = value;
            }
        }

        public string WayBillProperty
        {
            get
            {
                return wayBillPropertyEdit.Value;
            }
            set
            {
                wayBillProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(moreProposalsLinkPropertyEdit.Value))
            {
                moreProposalsLinkPropertyEdit.Value = moreProposalsLinkProperty;
            }
            if (string.IsNullOrEmpty(buyOnlinePropertyEdit.Value))
            {
                buyOnlinePropertyEdit.Value = buyOnlineProperty;
            }
            if (string.IsNullOrEmpty(wayBillPropertyEdit.Value))
            {
                wayBillPropertyEdit.Value = wayBillProperty;
            }
            if (string.IsNullOrEmpty(morePropertiesLinkPropertyEdit.Value))
            {
                morePropertiesLinkPropertyEdit.Value = morePropertiesLinkProperty;
            }
            if (string.IsNullOrEmpty(additionsHeaderPropertyEdit.Value))
            {
                additionsHeaderPropertyEdit.Value = additionsHeaderProperty;
            }
            if (string.IsNullOrEmpty(additionsTooltipPropertyEdit.Value))
            {
                additionsTooltipPropertyEdit.Value = additionsTooltipProperty;
            }
            if (string.IsNullOrEmpty(vatTextValueEdit.Value))
            {
                vatTextValueEdit.Value = vatProperty;
            }
            if (string.IsNullOrEmpty(noVatTextValueEdit.Value))
            {
                noVatTextValueEdit.Value = vatFreeProperty;
            }

            this.EUPriceVat.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_EUVATText") as string;
            this.nonEUPriceVat.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_NonEUVATText") as string;
        }

        #endregion Methods
    }
}