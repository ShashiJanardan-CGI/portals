﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class AdditionalServiceBE
    {
        #region Fields

        private bool availableForCompanyReceiver;

        #endregion Fields

        #region Constructors

        public AdditionalServiceBE()
        {
        }

        public AdditionalServiceBE(AdditionalService dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        public AdditionalServiceBE(Skicka.SkickaService.Proxy.additionalService1 dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Unique id for this additional service.
        /// </summary>
        [DataMember]
        public string AdditionalServiceCode
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyCode
        {
            get;
            set;
        }

        /// <summary>
        /// Description of this additional service.
        /// </summary>
        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool Enforced
        {
            get;
            set;
        }

        /// <summary>
        /// True if the additional service is available when the receiver is a company, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsAvailableForCompanyReceiver
        {
            get
            {
                return this.availableForCompanyReceiver;
            }

            set
            {
                this.availableForCompanyReceiver = value;
            }
        }

        /// <summary>
        /// Long description of this additional service.
        /// </summary>
        [DataMember]
        public string LongDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Name of this additional service.
        /// </summary>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Price information for this additional service.
        /// </summary>
        [DataMember]
        public PriceBE Price
        {
            get;
            set;
        }

        [DataMember]
        public string PvtId
        {
            get;
            set;
        }

        [DataMember]
        public PriceBE RetailPrice
        {
            get;
            set;
        }

        [DataMember]
        public string SapId
        {
            get;
            set;
        }

        /// <summary>
        /// True if selected by system or user.
        /// </summary>
        [DataMember]
        public bool Selected
        {
            get;
            set;
        }

        /// <summary>
        /// Unique id for the base/parent service for this additional service.
        /// </summary>
        [DataMember]
        public string ServiceKey
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public void DataAccessObjectToBusinessEntity(AdditionalService dataAccessObject)
        {
            this.PvtId = dataAccessObject.pvtCode;
            this.AdditionalServiceCode = dataAccessObject.code;
            this.Name = dataAccessObject.name;
            this.Price = new PriceBE(dataAccessObject.price);
        }

        private void DataAccessObjectToBusinessEntity(Skicka.SkickaService.Proxy.additionalService1 dataAccessObject)
        {
            this.PvtId = dataAccessObject.pvtId;
            this.AdditionalServiceCode = dataAccessObject.serviceId;
            this.Name = dataAccessObject.name;
            this.Price = new PriceBE(dataAccessObject.price);
        }

        #endregion Methods
    }
}