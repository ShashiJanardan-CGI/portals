﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class ValidationResponseBE
    {
        #region Properties

        [DataMember]
        public List<InfoBE> AdditionalInfo
        {
            get;
            set;
        }

        [DataMember]
        public List<AdditionalServiceBE> AdditionalServices
        {
            get;
            set;
        }

        [DataMember]
        public AdditionalServiceBE BaseService
        {
            get;
            set;
        }

        [DataMember]
        public PriceBE BaseServicePrice
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyCode
        {
            get;
            set;
        }

        [DataMember]
        public DeliveryInfoBE DeliveryInfo
        {
            get;
            set;
        }

        // external marketplace
        [DataMember]
        public string Marketplace
        {
            get;
            set;
        }

        [DataMember]
        public string MarketplaceBookingId
        {
            get;
            set;
        }

        [DataMember]
        public PriceBE Price
        {
            get;
            set;
        }

        [DataMember]
        public string ProductCode
        {
            get;
            set;
        }

        [DataMember]
        public int ReferenceNumberSerialType
        {
            get;
            set;
        }

        [DataMember]
        public string SapId
        {
            get;
            set;
        }

        [DataMember]
        public string StatusMsg
        {
            get;
            set;
        }

        [DataMember]
        public bool Valid
        {
            get;
            set;
        }

        #endregion Properties
    }
}