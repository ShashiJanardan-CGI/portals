﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.Utils;

    [Serializable]
    [DataContract]
    public class ProductsBE : BaseBE
    {
        #region Constructors

        public ProductsBE()
        {
        }

        public ProductsBE(ServicesResponse serviceResponse)
            : base(serviceResponse.status, serviceResponse.statusMsg, serviceResponse.responseInfos)
        {
            if (serviceResponse.status == responseStatus.SUCCESS && serviceResponse.service != null)
            {
                this.Products = Array.ConvertAll(
                       serviceResponse.service,
                       p => this.TranslateProductDefinitionToProductDefinitionBE(p));

                this.Products = this.Products.Where(o => o != null).ToArray();
            }
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public ProductDefinitionBE[] Products
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private ProductDefinitionBE TranslateProductDefinitionToProductDefinitionBE(ProductDefinition productDefinition)
        {
            try
            {
                return new ProductDefinitionBE(productDefinition);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}