﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class AccountBE
    {
        #region Constructors

        public AccountBE()
        {
        }

        public AccountBE(CodInformation codInformation)
        {
            this.DataAccessObjectToBusinessEntity(codInformation);
        }

        #endregion Constructors

        #region Enumerations

        public enum GiroTypeEnum
        {
            BANKGIRO = 0,
            PLUSGIRO = 1
        }

        #endregion Enumerations

        #region Properties

        [DataMember]
        public string AccountNumber
        {
            get;
            set;
        }

        [DataMember]
        public GiroTypeEnum AccountType
        {
            get;
            set;
        }

        [DataMember]
        public string Amount
        {
            get;
            set;
        }

        [DataMember]
        public string Reference
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public i2GiroType ReturnAccountGiroType()
        {
            if(this.AccountType.Equals(GiroTypeEnum.BANKGIRO)){
            return i2GiroType.BANKGIRO;
            }
            else if(this.AccountType.Equals(GiroTypeEnum.PLUSGIRO))
            {
            return i2GiroType.PLUSGIRO;
            }
            else{
            return i2GiroType.BANKGIRO;
            }
        }

        private void DataAccessObjectToBusinessEntity(CodInformation dataAccessObject)
        {
            this.AccountType = (GiroTypeEnum)Enum.Parse(typeof(GiroTypeEnum), Enum.GetName(typeof(i2GiroType), dataAccessObject.giroType));
            this.AccountNumber = dataAccessObject.accountNo;
            this.Reference = dataAccessObject.reference;
            this.Amount = dataAccessObject.amount.ToString();
        }

        #endregion Methods
    }
}