﻿namespace Posten.Portal.Skicka.Services.BusinessEntities.CartEntities
{
    using System;

    using Posten.Portal.Skicka.PVT.Proxy;

    public class ServiceOrderLineItem
    {
        #region Constructors

        public ServiceOrderLineItem()
        {
        }

        public ServiceOrderLineItem(LineItem lineItem)
        {
            this.DataAccessObjectToBusinessEntity(lineItem);
        }

        #endregion Constructors

        #region Properties

        public string ArticleId
        {
            get;
            set;
        }

        public string ArticleName
        {
            get;
            set;
        }

        public decimal ArticlePrice
        {
            get;
            set;
        }

        public string CompanyCode
        {
            get;
            set;
        }

        public string CustomerId
        {
            get;
            set;
        }

        public string FootnoteText
        {
            get;
            set;
        }

        public int OrderLineNumber
        {
            get;
            set;
        }

        public decimal Quantity
        {
            get;
            set;
        }

        public string TaxCountry
        {
            get;
            set;
        }

        public decimal Total
        {
            get;
            set;
        }

        public decimal Vat
        {
            get;
            set;
        }

        public decimal VatPercentage
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public void DataAccessObjectToBusinessEntity(LineItem dataAccessObject)
        {
            this.ArticleId = dataAccessObject.sapProductId;
            this.ArticleName = dataAccessObject.name;
            this.ArticlePrice = Convert.ToDecimal(dataAccessObject.price.amountNoVat);
            this.Vat = Convert.ToDecimal(dataAccessObject.price.vat);

            this.CompanyCode = dataAccessObject.companyCode;
            this.CustomerId = dataAccessObject.customerId;
            this.OrderLineNumber = dataAccessObject.orderRowNumber;
            this.Quantity = dataAccessObject.quantity;
            //this.TaxCountry = dataAccessObject.t;
        }

        #endregion Methods
    }
}