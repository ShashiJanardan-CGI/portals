﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.BusinessEntities.CartEntities;
    using Posten.Portal.Skicka.Services.Utils;

    [Serializable]
    [DataContract]
    public class ItemBE
    {
        #region Constructors

        public ItemBE()
        {
        }

        public ItemBE(Item dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        #endregion Constructors

        #region Properties

        public string ApplicationId
        {
            get;
            set;
        }

        public string ApplicationName
        {
            get;
            set;
        }

        public string ConfirmationUrl
        {
            get;
            set;
        }

        public string CurrencyCode
        {
            get;
            set;
        }

        public string Detials
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public PriceBE Price
        {
            get;
            set;
        }

        public string ProducteUrl
        {
            get;
            set;
        }

        public ServiceOrderLineItem[] ServiceOrderLines
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private void DataAccessObjectToBusinessEntity(Item dataAccessObject)
        {
            //this.ApplicationId = dataAccessObject.applicationId;
            //this.ApplicationName = dataAccessObject.applicationName;
            //this.ConfirmationUrl = dataAccessObject.confirmationUrl;
            this.CurrencyCode = dataAccessObject.currencyCode;
            //this.Detials = dataAccessObject.detials;
            this.Name = dataAccessObject.name;
            this.Price = new PriceBE(dataAccessObject.totalPrice);
            //this.ProducteUrl = dataAccessObject.producteUrl;

            if (dataAccessObject.additionalServices != null)
            {
                this.ServiceOrderLines = Array.ConvertAll(
                    dataAccessObject.additionalServices,
                    s => TranslateToCartServiceOrderLineItem(s));

                this.ServiceOrderLines = this.ServiceOrderLines.Where(o => o != null).ToArray();
            }
        }

        private ServiceOrderLineItem TranslateToCartServiceOrderLineItem(LineItem lineItem)
        {
            try
            {
                return new ServiceOrderLineItem(lineItem);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}