﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class PriceBE
    {
        #region Constructors

        public PriceBE()
        {
        }

        public PriceBE(Price price)
        {
            this.Currency = price.currency;
            this.Amount = price.amount;
            this.AmountNoVat = price.amountNoVat;
            //MVU7734 - S
            this.VATAmount = price.vatAmount;
            this.VatCode = price.vatCode;
            this.VATPercentage = Convert.ToDecimal(price.vat);
            //MVU7734 - E
        }

        public PriceBE(Skicka.SkickaService.Proxy.price1 price)
        {
            this.Currency = price.currency;
            this.Amount = (price.amount)/100;
            this.AmountNoVat = (price.amountNoVat)/100;
            this.VATAmount = (price.vatAmount)/100;            
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public double Amount
        {
            get;
            set;
        }

        [DataMember]
        public double AmountNoVat
        {
            get;
            set;
        }

        [DataMember]
        public string Currency
        {
            get;
            set;
        }

        [DataMember]
        public double VATAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal VATPercentage
        {
            get;
            set;
        }

        //MVU7734 - S
        [DataMember]
        public string VatCode
        {
            get;
            set;
        }
        //MVU7734 - E
        #endregion Properties
    }
}