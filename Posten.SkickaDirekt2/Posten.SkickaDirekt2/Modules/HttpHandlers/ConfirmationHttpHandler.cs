﻿using Posten.Portal.Skicka.Services.Logging;
using Posten.Portal.Skicka.Services.Utils;
using Posten.SkickaDirekt2.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Posten.Portal.SkickaDirekt2.Modules.HttpHandlers
{
    public class ConfirmationHttpHandler : IHttpHandler
    {
        #region Members
        private static LogController logger;
        private string transactionLogID;
        #endregion Members

        #region Constructors

        public ConfirmationHttpHandler()
        {
            try
            {
                logger = new LogController();
            }
            catch (Exception e)
            {
                var message = e.Message + e.StackTrace;
            }
        }


        #endregion Constructors

        #region Properties

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public string TransactionLogID
        {
            get { return transactionLogID; }
            set { transactionLogID = value; }
        }

        #endregion Properties

        #region Methods

        public void ProcessRequest(HttpContext context)
        {
            StringBuilder confirmationLogInfo = new StringBuilder();
            
            try
            {
                LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
                logger.LogMessage(this.transactionLogID, "Start:ProcessRequestFor Confirmation_HTTP_Handler");
               
                string htmlOutput = "No output!";                
                string bookingNr = string.Empty;
                bookingNr = context.Request[Constants.Request_bookingId_key];
                Page pageHolder = new Page();

                logger.LogMessage("about to load user control");               
                UserControl viewControl = (UserControl)pageHolder.LoadControl(@"/_controltemplates/Posten.SkickaDirekt2/ConfirmationControl/ConfirmationResults.ascx");
                Type viewControlType = viewControl.GetType();
                logger.LogMessage("booking ID:" + bookingNr);
               
                // Set BookingId property
                System.Reflection.PropertyInfo property = viewControlType.GetProperty("BookingId");
                if (property != null)
                {
                    property.SetValue(viewControl, bookingNr, null);
                    logger.LogMessage("Set booking Nr property");               
                }
                else
                {
                    logger.LogMessage("User control does not have a public property");               
                    throw new NullReferenceException(string.Format("UserControl: does not have a public property."));
                }

                // Load control
                logger.LogMessage("About to load control");
               
                pageHolder.Controls.Add(viewControl);
                StringWriter output = new StringWriter();
                HttpContext.Current.Server.Execute(pageHolder, output, false);
                htmlOutput = output.ToString();
                context.Response.Write(htmlOutput);
                confirmationLogInfo.AppendLine("The bookingId that comes with the request is: " + bookingNr);

            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);                                
                string errmess = "<div style='background-color:#feeeee;margin-top:20px;margin-bottom:10px;color:#cd0104;border-top:#ce0004 5px solid;font-weight:bold;padding:5px 15px 10px 15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:bold;font-size:14px;color:#031e50;'>Något har blivit fel. Var god kontakta Kundtjänst på telefon 020 - 23 22 21</div>";
                string errmess2 = "<div style='background-color:#feeeee;margin-top:20px;margin-bottom:10px;color:#cd0104;border-top:#ce0004 5px solid;font-weight:bold;padding:5px 15px 10px 15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:bold;font-size:14px;color:#031e50;'>" + ex.Message + "</div>";
                context.Response.Write(errmess);
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:ProcessRequestFor Confirmation_HTTP_Handler");
            }
        }

        #endregion Methods
    }
}
