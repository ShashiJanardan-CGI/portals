﻿namespace Posten.SkickaDirekt2.WebParts.Presenter
{
    using Posten.SkickaDirekt2.WebParts.Model;
    using Posten.SkickaDirekt2.WebParts.Presenter.Interface;
    using Posten.SkickaDirekt2.WebParts.View.Interfaces;

    public class SampleWPPresenter : ISampleWPPresenter
    {
        #region Fields

        private SampleWPDataContract model;
        private ISampleWP view;

        #endregion Fields

        #region Constructors

        public SampleWPPresenter(ISampleWP view)
        {
            this.view = view;
            this.model = new SampleWPDataContract();
        }

        #endregion Constructors
    }
}