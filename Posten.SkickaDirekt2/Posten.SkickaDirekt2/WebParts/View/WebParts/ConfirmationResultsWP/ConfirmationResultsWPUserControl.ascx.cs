﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class ConfirmationResultsWPUserControl : UserControl
    {
        #region Fields

       

        #endregion Fields

        #region Properties

        public ConfirmationResultsWP ParentWebPart
        {
            get;
            set;
        }

       

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            //Register webpart css
            UIHelper.RegisterCSSInPageHeader(this.Page, "ConfirmationResultsWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

       
        #endregion Methods
    }
}