﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class AddToCartWPUserControl : UserControl
    {
        #region Fields

        private string accountValidationProperty;
        private string addToCartButtonProperty;
        private string receiverValidationProperty;
        private string senderValidationProperty;

        private string confirmationUrlProperty;
        private string waybillUrlProperty;
        private string confirmationPageUrlProperty;
        private string applicationUrlProperty;
        private string editUrlProperty;
        private string produceUrlProperty;

        #endregion Fields

        #region Properties

        public string ConfirmationUrlProperty
        {
            get
            {
                return confirmationUrlProperty;
            }
            set
            {
                confirmationUrlProperty = value;
            }
        }


        public string WaybillUrlProperty
        {
            get
            {
                return waybillUrlProperty;
            }
            set
            {
                waybillUrlProperty = value;
            }
        }


        public string ConfirmationPageUrlProperty
        {
            get
            {
                return confirmationPageUrlProperty;
            }
            set
            {
                confirmationPageUrlProperty = value;
            }
        }


        public string ApplicationUrlProperty
        {
            get
            {
                return applicationUrlProperty;
            }
            set
            {
                applicationUrlProperty = value;
            }
        }


        public string EditUrlProperty
        {
            get
            {
                return editUrlProperty;
            }
            set
            {
                editUrlProperty = value;
            }
        }


        public string ProduceUrlProperty
        {
            get
            {
                return produceUrlProperty;
            }
            set
            {
                produceUrlProperty = value;
            }
        }

        public string AccountValidationProperty
        {
            get
            {
                return accountValidationProperty;
            }
            set
            {
                accountValidationProperty = value;
            }
        }

        public string AddToCartButtonProperty
        {
            get
            {
                return addToCartButtonProperty;
            }
            set
            {
                addToCartButtonProperty = value;
            }
        }

        public AddToCartWP ParentWebPart
        {
            get;
            set;
        }

        public string ReceiverValidationProperty
        {
            get
            {
                return receiverValidationProperty;
            }
            set
            {
                receiverValidationProperty = value;
            }
        }

        public string SenderValidationProperty
        {
            get
            {
                return senderValidationProperty;
            }
            set
            {
                senderValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "AddToCartWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((AddToCartWPDisplay)this.displayControl).AddToCartButtonProperty = this.ParentWebPart.AddToCartButtonProperty;
                ((AddToCartWPDisplay)this.displayControl).AccountValidationProperty = this.ParentWebPart.AccountValidationProperty;
                ((AddToCartWPDisplay)this.displayControl).SenderValidationProperty = this.ParentWebPart.SenderValidationProperty;
                ((AddToCartWPDisplay)this.displayControl).ReceiverValidationProperty = this.ParentWebPart.ReceiverValidationProperty;

                ((AddToCartWPDisplay)this.displayControl).ConfirmationPageUrlProperty = this.ParentWebPart.ConfirmationPageUrlProperty;
                ((AddToCartWPDisplay)this.displayControl).ConfirmationUrlProperty = this.ParentWebPart.ConfirmationUrlProperty;
                ((AddToCartWPDisplay)this.displayControl).WaybillUrlProperty = this.ParentWebPart.WaybillUrlProperty;
                ((AddToCartWPDisplay)this.displayControl).ApplicationUrlProperty = this.ParentWebPart.ApplicationUrlProperty;
                ((AddToCartWPDisplay)this.displayControl).ProduceUrlProperty = this.ParentWebPart.ProduceUrlProperty;
                ((AddToCartWPDisplay)this.displayControl).EditUrlProperty = this.ParentWebPart.EditUrlProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((AddToCartWPEdit)this.editControl).AddToCartButtonProperty = this.ParentWebPart.AddToCartButtonProperty;
                ((AddToCartWPEdit)this.editControl).AccountValidationProperty = this.ParentWebPart.AccountValidationProperty;
                ((AddToCartWPEdit)this.editControl).SenderValidationProperty = this.ParentWebPart.SenderValidationProperty;
                ((AddToCartWPEdit)this.editControl).ReceiverValidationProperty = this.ParentWebPart.ReceiverValidationProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.AddToCartButtonProperty = ((AddToCartWPEdit)this.editControl).AddToCartButtonProperty;
            this.ParentWebPart.AccountValidationProperty = ((AddToCartWPEdit)this.editControl).AccountValidationProperty;
            this.ParentWebPart.SenderValidationProperty = ((AddToCartWPEdit)this.editControl).SenderValidationProperty;
            this.ParentWebPart.ReceiverValidationProperty = ((AddToCartWPEdit)this.editControl).ReceiverValidationProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}