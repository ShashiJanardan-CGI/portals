﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class NavigationTabsWPUserControl : UserControl
    {
        #region Fields

        private string tab1TitleProperty;
        private string tab1UrlProperty;
        private string tab2TitleProperty;
        private string tab2UrlProperty;
        private string tab3TitleProperty;
        private string tab3UrlProperty;

        #endregion Fields

        #region Properties

        public NavigationTabsWP ParentWebPart
        {
            get;
            set;
        }

        public string Tab1TitleProperty
        {
            get
            {
                return tab1TitleProperty;
            }
            set
            {
                tab1TitleProperty = value;
            }
        }

        public string Tab1UrlProperty
        {
            get
            {
                return tab1UrlProperty;
            }
            set
            {
                tab1UrlProperty = value;
            }
        }

        public string Tab2TitleProperty
        {
            get
            {
                return tab2TitleProperty;
            }
            set
            {
                tab2TitleProperty = value;
            }
        }

        public string Tab2UrlProperty
        {
            get
            {
                return tab2UrlProperty;
            }
            set
            {
                tab2UrlProperty = value;
            }
        }

        public string Tab3TitleProperty
        {
            get
            {
                return tab3TitleProperty;
            }
            set
            {
                tab3TitleProperty = value;
            }
        }

        public string Tab3UrlProperty
        {
            get
            {
                return tab3UrlProperty;
            }
            set
            {
                tab3UrlProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "NavigationTabsWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((NavigationTabsWPDisplay)this.displayControl).Tab1TitleProperty = this.ParentWebPart.Tab1TitleProperty;
                ((NavigationTabsWPDisplay)this.displayControl).Tab2TitleProperty = this.ParentWebPart.Tab2TitleProperty;
                ((NavigationTabsWPDisplay)this.displayControl).Tab3TitleProperty = this.ParentWebPart.Tab3TitleProperty;
                ((NavigationTabsWPDisplay)this.displayControl).Tab1UrlProperty = this.ParentWebPart.Tab1UrlProperty;
                ((NavigationTabsWPDisplay)this.displayControl).Tab2UrlProperty = this.ParentWebPart.Tab2UrlProperty;
                ((NavigationTabsWPDisplay)this.displayControl).Tab3UrlProperty = this.ParentWebPart.Tab3UrlProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((NavigationTabsWPEdit)this.editControl).Tab1TitleProperty = this.ParentWebPart.Tab1TitleProperty;
                ((NavigationTabsWPEdit)this.editControl).Tab2TitleProperty = this.ParentWebPart.Tab2TitleProperty;
                ((NavigationTabsWPEdit)this.editControl).Tab3TitleProperty = this.ParentWebPart.Tab3TitleProperty;
                ((NavigationTabsWPEdit)this.editControl).Tab1UrlProperty = this.ParentWebPart.Tab1UrlProperty;
                ((NavigationTabsWPEdit)this.editControl).Tab2UrlProperty = this.ParentWebPart.Tab2UrlProperty;
                ((NavigationTabsWPEdit)this.editControl).Tab3UrlProperty = this.ParentWebPart.Tab3UrlProperty;

                // Show correct control
                this.displayControl.Visible = false;
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.Tab1TitleProperty = ((NavigationTabsWPEdit)this.editControl).Tab1TitleProperty;
            this.ParentWebPart.Tab2TitleProperty = ((NavigationTabsWPEdit)this.editControl).Tab2TitleProperty;
            this.ParentWebPart.Tab3TitleProperty = ((NavigationTabsWPEdit)this.editControl).Tab3TitleProperty;
            this.ParentWebPart.Tab1UrlProperty = ((NavigationTabsWPEdit)this.editControl).Tab1UrlProperty;
            this.ParentWebPart.Tab2UrlProperty = ((NavigationTabsWPEdit)this.editControl).Tab2UrlProperty;
            this.ParentWebPart.Tab3UrlProperty = ((NavigationTabsWPEdit)this.editControl).Tab3UrlProperty;
            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}