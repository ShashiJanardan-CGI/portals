﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="NavigationTabsWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/NavigationTabsWP/NavigationTabsWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="NavigationTabsWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/NavigationTabsWP/NavigationTabsWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationTabsWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.NavigationTabsWPUserControl" %>

<%--<SharePoint:ScriptLink runat="server" ID="NavigationTabsJSLink" Name="/Posten.SkickaDirekt2/JS/NavigationTabsWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>--%>

<div id="NavigationTabsWPDisplayPanel" runat="server">
    <CustomControls:NavigationTabsWPDisplay ID="displayControl" runat="server"></CustomControls:NavigationTabsWPDisplay>
</div>

<div id="NavigationTabsWPEditPanel" runat="server">
    <CustomControls:NavigationTabsWPEdit ID="editControl" runat="server"></CustomControls:NavigationTabsWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>