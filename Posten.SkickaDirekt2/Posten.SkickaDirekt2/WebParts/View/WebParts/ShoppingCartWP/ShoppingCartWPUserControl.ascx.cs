﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class ShoppingCartWPUserControl : UserControl
    {
        #region Fields

        private string headerProperty;
        private string cartIsEmptyProperty;
        private string confirmHeaderProperty;
        private string confirmQuestionStartProperty;
        private string confirmQuestionEndProperty;

        #endregion Fields

        #region Properties

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CartIsEmptyProperty
        {
            get
            {
                return cartIsEmptyProperty;
            }
            set
            {
                cartIsEmptyProperty = value;
            }
        }

        public string ConfirmHeaderProperty
        {
            get
            {
                return confirmHeaderProperty;
            }
            set
            {
               confirmHeaderProperty = value;
            }
        }

        public string ConfirmQuestionStartProperty
        {
            get
            {
                return confirmQuestionStartProperty;
            }
            set
            {
                confirmQuestionStartProperty = value;
            }
        }

        public string ConfirmQuestionEndProperty
        {
            get
            {
                return confirmQuestionEndProperty;
            }
            set
            {
                confirmQuestionEndProperty = value;
            }
        }

        public ShoppingCartWP ParentWebPart
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "ShoppingCartWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((ShoppingCartWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ShoppingCartWPDisplay)this.displayControl).CartIsEmptyProperty = this.ParentWebPart.CartIsEmptyProperty;
                ((ShoppingCartWPDisplay)this.displayControl).ConfirmHeaderProperty = this.ParentWebPart.ConfirmHeaderProperty;
                ((ShoppingCartWPDisplay)this.displayControl).ConfirmQuestionStartProperty = this.ParentWebPart.ConfirmQuestionStartProperty;
                ((ShoppingCartWPDisplay)this.displayControl).ConfirmQuestionEndProperty = this.ParentWebPart.ConfirmQuestionEndProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((ShoppingCartWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ShoppingCartWPEdit)this.editControl).CartIsEmptyProperty = this.ParentWebPart.CartIsEmptyProperty;
                ((ShoppingCartWPEdit)this.editControl).ConfirmHeaderProperty = this.ParentWebPart.ConfirmHeaderProperty;
                ((ShoppingCartWPEdit)this.editControl).ConfirmQuestionStartProperty = this.ParentWebPart.ConfirmQuestionStartProperty;
                ((ShoppingCartWPEdit)this.editControl).ConfirmQuestionEndProperty = this.ParentWebPart.ConfirmQuestionEndProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }

        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((ShoppingCartWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.CartIsEmptyProperty = ((ShoppingCartWPEdit)this.editControl).CartIsEmptyProperty;
            this.ParentWebPart.ConfirmHeaderProperty = ((ShoppingCartWPEdit)this.editControl).ConfirmHeaderProperty;
            this.ParentWebPart.ConfirmQuestionStartProperty = ((ShoppingCartWPEdit)this.editControl).ConfirmQuestionStartProperty;
            this.ParentWebPart.ConfirmQuestionEndProperty = ((ShoppingCartWPEdit)this.editControl).ConfirmQuestionEndProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}