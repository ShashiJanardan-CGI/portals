﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="ShoppingCartWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ShoppingCartWP/ShoppingCartWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="ShoppingCartWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ShoppingCartWP/ShoppingCartWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.ShoppingCartWPUserControl" %>

<div id="ShoppingCartWPDisplayPanel" runat="server">
    <CustomControls:ShoppingCartWPDisplay ID="displayControl" runat="server"></CustomControls:ShoppingCartWPDisplay>
</div>

<div id="ShoppingCartWPEditPanel" runat="server">
    <CustomControls:ShoppingCartWPEdit ID="editControl" runat="server"></CustomControls:ShoppingCartWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>