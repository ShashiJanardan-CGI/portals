﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class ReceiverAddressWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/ReceiverAddressWP/ReceiverAddressWPUserControl.ascx";

        private string address2LabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_Address2Label") as string;
        private string address2TooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_Address2Tooltip") as string;
        private string addressLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_AddressLabel") as string;
        private string cityLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_CityLabel") as string;
        private string cityPlaceholderProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_CityPlaceholder") as string;
        private string companyLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_CompanyLabel") as string;
        private string companyNameLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_CompanyNameLabel") as string;
        private string email2LabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_Email2Label") as string;
        private string emailLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_EmailLabel") as string;
        private string emailTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_EmailTooltip") as string;
        private string entryCodeLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_EntryCodeLabel") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_Header") as string;
        private string mobilePhoneLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_MobilePhoneLabel") as string;
        private string mobilePhoneTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_MobilePhoneTooltip") as string;
        private string nameLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_NameLabel") as string;
        private string privateLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_PrivateLabel") as string;
        private string receiverTypeLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_ReceiverTypeLabel") as string;
        private string telephoneLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_TelephoneLabel") as string;
        private string telephoneTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_TelephoneTooltip") as string;
        private string zipLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ReceiverAddress_ZipLabel") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Address field 2 Label:"),
        WebDescription("Label for Address field 2"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Address2LabelProperty
        {
            get
            {
                return address2LabelProperty;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Address field 2 Tooltip:"),
        WebDescription("Tooltip for Address field 2"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipProperty;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Address Label:"),
        WebDescription("Label for Address field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AddressLabelProperty
        {
            get
            {
                return addressLabelProperty;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("City Label:"),
        WebDescription("Label for City field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CityLabelProperty
        {
            get
            {
                return cityLabelProperty;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("City placeholder:"),
        WebDescription("Text to be shown in City field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderProperty;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Company  Label:"),
        WebDescription("Label for Company radio button "),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelProperty;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Company Name Label:"),
        WebDescription("Label for Company Name field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelProperty;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email verification Label:"),
        WebDescription("Label for Email verification field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Email2LabelProperty
        {
            get
            {
                return email2LabelProperty;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email Label:"),
        WebDescription("Label for Email field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EmailLabelProperty
        {
            get
            {
                return emailLabelProperty;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email Tooltip:"),
        WebDescription("Tooltip for Email field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EmailTooltipProperty
        {
            get
            {
                return emailTooltipProperty;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Entry code Label:"),
        WebDescription("Label for Entry code field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EntryCodeLabelProperty
        {
            get
            {
                return entryCodeLabelProperty;
            }
            set
            {
                entryCodeLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Receiver Address Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Mobile Phone Label:"),
        WebDescription("Label for Mobile Phone field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string MobilePhoneLabelProperty
        {
            get
            {
                return mobilePhoneLabelProperty;
            }
            set
            {
                mobilePhoneLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Mobile Phone Tooltip:"),
        WebDescription("Tooltip for Mobile Phone field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string MobilePhoneTooltipProperty
        {
            get
            {
                return mobilePhoneTooltipProperty;
            }
            set
            {
                mobilePhoneTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Name Label:"),
        WebDescription("Label for Name field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string NameLabelProperty
        {
            get
            {
                return nameLabelProperty;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Private Label:"),
        WebDescription("Label for Private radio button"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelProperty;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Private/Company Label:"),
        WebDescription("Label for selection of Private/Company"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ReceiverTypeLabelProperty
        {
            get
            {
                return receiverTypeLabelProperty;
            }
            set
            {
                receiverTypeLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Telephone Label:"),
        WebDescription("Label for Telephone field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string TelephoneLabelProperty
        {
            get
            {
                return telephoneLabelProperty;
            }
            set
            {
                telephoneLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Telephone Tooltip:"),
        WebDescription("Tooltip for Telephone field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string TelephoneTooltipProperty
        {
            get
            {
                return telephoneTooltipProperty;
            }
            set
            {
                telephoneTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Zip Label:"),
        WebDescription("Label for Zip field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ZipLabelProperty
        {
            get
            {
                return zipLabelProperty;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                ReceiverAddressWP webPart = (ReceiverAddressWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.CompanyNameLabelProperty = this.companyNameLabelProperty;
                webPart.NameLabelProperty = this.nameLabelProperty;
                webPart.AddressLabelProperty = this.addressLabelProperty;
                webPart.Address2LabelProperty = this.address2LabelProperty;
                webPart.ZipLabelProperty = this.zipLabelProperty;
                webPart.EmailLabelProperty = this.emailLabelProperty;
                webPart.Email2LabelProperty = this.email2LabelProperty;
                webPart.MobilePhoneLabelProperty = this.mobilePhoneLabelProperty;
                webPart.TelephoneLabelProperty = this.telephoneLabelProperty;
                webPart.ReceiverTypeLabelProperty = this.receiverTypeLabelProperty;
                webPart.PrivateLabelProperty = this.privateLabelProperty;
                webPart.CompanyLabelProperty = this.companyLabelProperty;
                webPart.CityPlaceholderProperty = this.cityPlaceholderProperty;
                webPart.CityLabelProperty = this.cityLabelProperty;
                webPart.Address2TooltipProperty = this.address2TooltipProperty;
                webPart.EmailTooltipProperty = this.emailTooltipProperty;
                webPart.MobilePhoneTooltipProperty = this.mobilePhoneTooltipProperty;
                webPart.TelephoneTooltipProperty = this.telephoneTooltipProperty;
                webPart.EntryCodeLabelProperty = this.entryCodeLabelProperty;
                webPart.TelephoneTooltipProperty = this.telephoneTooltipProperty;
                webPart.TelephoneLabelProperty = this.telephoneLabelProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            ReceiverAddressWPUserControl control = (ReceiverAddressWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}