﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class MainButtonsWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/MainButtonsWP/MainButtonsWPUserControl.ascx";

        private string generalServerErrorProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_MainButtons_GeneralServerError") as string;
        private string resetButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_MainButtons_ResetButton") as string;
        private string showProposalsButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_MainButtons_showProposalsButton") as string;
        private string sizeValidationProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_MainButtons_SizeValidation") as string;
        private string weightValidationProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_MainButtons_WeightValidation") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("General server error message:"),
        WebDescription("Default message for all uncaught errors"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string GeneralServerErrorProperty
        {
            get
            {
                return generalServerErrorProperty;
            }
            set
            {
                generalServerErrorProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Reset button:"),
        WebDescription("Text to be shown on reset button"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ResetButtonProperty
        {
            get
            {
                return resetButtonProperty;
            }
            set
            {
                resetButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Show proposals button:"),
        WebDescription("Text to be shown on button to show proposals"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ShowProposalsButtonProperty
        {
            get
            {
                return showProposalsButtonProperty;
            }
            set
            {
                showProposalsButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Size validation message:"),
        WebDescription("Validation message for size section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SizeValidationProperty
        {
            get
            {
                return sizeValidationProperty;
            }
            set
            {
                sizeValidationProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Weight validation message:"),
        WebDescription("Validation message for weight section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string WeightValidationlProperty
        {
            get
            {
                return weightValidationProperty;
            }
            set
            {
                weightValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                MainButtonsWP webPart = (MainButtonsWP)manager.WebParts[this.ID];
                webPart.ResetButtonProperty = this.resetButtonProperty;
                webPart.ShowProposalsButtonProperty = this.showProposalsButtonProperty;
                webPart.SizeValidationProperty = this.sizeValidationProperty;
                webPart.WeightValidationlProperty = this.weightValidationProperty;
                webPart.GeneralServerErrorProperty = this.generalServerErrorProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            MainButtonsWPUserControl control = (MainButtonsWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}