﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class MainButtonsWPUserControl : UserControl
    {
        #region Fields

        private string resetButtonProperty;
        private string showProposalsButtonProperty;
        private string sizeValidationProperty;
        private string weightValidationProperty;

        #endregion Fields

        #region Properties

        public MainButtonsWP ParentWebPart
        {
            get;
            set;
        }

        public string ResetButtonProperty
        {
            get
            {
                return resetButtonProperty;
            }
            set
            {
                resetButtonProperty = value;
            }
        }

        public string ShowProposalsButtonProperty
        {
            get
            {
                return showProposalsButtonProperty;
            }
            set
            {
                showProposalsButtonProperty = value;
            }
        }

        public string SizeValidationProperty
        {
            get
            {
                return sizeValidationProperty;
            }
            set
            {
                sizeValidationProperty = value;
            }
        }

        public string WeightValidationlProperty
        {
            get
            {
                return weightValidationProperty;
            }
            set
            {
                weightValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "MainButtonsWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((MainButtonsWPDisplay)this.displayControl).ResetButtonProperty = this.ParentWebPart.ResetButtonProperty;
                ((MainButtonsWPDisplay)this.displayControl).ShowProposalsButtonProperty = this.ParentWebPart.ShowProposalsButtonProperty;
                ((MainButtonsWPDisplay)this.displayControl).SizeValidationProperty = this.ParentWebPart.SizeValidationProperty;
                ((MainButtonsWPDisplay)this.displayControl).WeightValidationlProperty = this.ParentWebPart.WeightValidationlProperty;
                ((MainButtonsWPDisplay)this.displayControl).GeneralServerErrorProperty = this.ParentWebPart.GeneralServerErrorProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((MainButtonsWPEdit)this.editControl).ResetButtonProperty = this.ParentWebPart.ResetButtonProperty;
                ((MainButtonsWPEdit)this.editControl).ShowProposalsButtonProperty = this.ParentWebPart.ShowProposalsButtonProperty;
                ((MainButtonsWPEdit)this.editControl).SizeValidationProperty = this.ParentWebPart.SizeValidationProperty;
                ((MainButtonsWPEdit)this.editControl).WeightValidationlProperty = this.ParentWebPart.WeightValidationlProperty;
                ((MainButtonsWPEdit)this.editControl).GeneralServerErrorProperty = this.ParentWebPart.GeneralServerErrorProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.ResetButtonProperty = ((MainButtonsWPEdit)this.editControl).ResetButtonProperty;
            this.ParentWebPart.ShowProposalsButtonProperty = ((MainButtonsWPEdit)this.editControl).ShowProposalsButtonProperty;
            this.ParentWebPart.SizeValidationProperty = ((MainButtonsWPEdit)this.editControl).SizeValidationProperty;
            this.ParentWebPart.WeightValidationlProperty = ((MainButtonsWPEdit)this.editControl).WeightValidationlProperty;
            this.ParentWebPart.GeneralServerErrorProperty = ((MainButtonsWPEdit)this.editControl).GeneralServerErrorProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}