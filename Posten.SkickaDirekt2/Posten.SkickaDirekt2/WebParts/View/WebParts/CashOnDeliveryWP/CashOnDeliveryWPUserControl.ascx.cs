﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;

    public partial class CashOnDeliveryWPUserControl : UserControl
    {
        #region Fields

        private string accountNumberLabelProperty;
        private string accountTypeLabelProperty;
        private string amountLabelProperty;
        private string bankGiroLabelProperty;
        private string headerProperty;
        private string postalGiroLabelProperty;
        private string referenceLabelProperty;
        private string referencePlaceholderProperty;

        #endregion Fields

        #region Properties

        public string AccountNumberLabelProperty
        {
            get
            {
                return accountNumberLabelProperty;
            }
            set
            {
                accountNumberLabelProperty = value;
            }
        }

        public string AccountTypeLabelProperty
        {
            get
            {
                return accountTypeLabelProperty;
            }
            set
            {
                accountTypeLabelProperty = value;
            }
        }

        public string AmountLabelProperty
        {
            get
            {
                return amountLabelProperty;
            }
            set
            {
                amountLabelProperty = value;
            }
        }

        public string BankGiroLabelProperty
        {
            get
            {
                return bankGiroLabelProperty;
            }
            set
            {
                bankGiroLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public CashOnDeliveryWP ParentWebPart
        {
            get;
            set;
        }

        public string PostalGiroLabelProperty
        {
            get
            {
                return postalGiroLabelProperty;
            }
            set
            {
                postalGiroLabelProperty = value;
            }
        }

        public string ReferenceLabelProperty
        {
            get
            {
                return referenceLabelProperty;
            }
            set
            {
                referenceLabelProperty = value;
            }
        }

        public string ReferencePlaceholderProperty
        {
            get
            {
                return referencePlaceholderProperty;
            }
            set
            {
                referencePlaceholderProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            // UIHelper.RegisterCSSInPageHeader(this.Page, "CashOnDeliveryWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((CashOnDeliveryWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).AccountTypeLabelProperty = this.ParentWebPart.AccountTypeLabelProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).BankGiroLabelProperty = this.ParentWebPart.BankGiroLabelProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).PostalGiroLabelProperty = this.ParentWebPart.PostalGiroLabelProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).AccountNumberLabelProperty = this.ParentWebPart.AccountNumberLabelProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).ReferenceLabelProperty = this.ParentWebPart.ReferenceLabelProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).ReferencePlaceholderProperty = this.ParentWebPart.ReferencePlaceholderProperty;
                ((CashOnDeliveryWPDisplay)this.displayControl).AmountLabelProperty = this.ParentWebPart.AmountLabelProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((CashOnDeliveryWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((CashOnDeliveryWPEdit)this.editControl).AccountTypeLabelProperty = this.ParentWebPart.AccountTypeLabelProperty;
                ((CashOnDeliveryWPEdit)this.editControl).BankGiroLabelProperty = this.ParentWebPart.BankGiroLabelProperty;
                ((CashOnDeliveryWPEdit)this.editControl).PostalGiroLabelProperty = this.ParentWebPart.PostalGiroLabelProperty;
                ((CashOnDeliveryWPEdit)this.editControl).AccountNumberLabelProperty = this.ParentWebPart.AccountNumberLabelProperty;
                ((CashOnDeliveryWPEdit)this.editControl).ReferenceLabelProperty = this.ParentWebPart.ReferenceLabelProperty;
                ((CashOnDeliveryWPEdit)this.editControl).ReferencePlaceholderProperty = this.ParentWebPart.ReferencePlaceholderProperty;
                ((CashOnDeliveryWPEdit)this.editControl).AmountLabelProperty = this.ParentWebPart.AmountLabelProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((CashOnDeliveryWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.AccountTypeLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).AccountTypeLabelProperty;
            this.ParentWebPart.BankGiroLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).BankGiroLabelProperty;
            this.ParentWebPart.PostalGiroLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).PostalGiroLabelProperty;
            this.ParentWebPart.AccountNumberLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).AccountNumberLabelProperty;
            this.ParentWebPart.ReferenceLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).ReferenceLabelProperty;
            this.ParentWebPart.ReferencePlaceholderProperty = ((CashOnDeliveryWPEdit)this.editControl).ReferencePlaceholderProperty;
            this.ParentWebPart.AmountLabelProperty = ((CashOnDeliveryWPEdit)this.editControl).AmountLabelProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}