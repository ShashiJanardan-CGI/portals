﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class ServicesResultItemWPUserControl : UserControl
    {
        #region Fields

        private string additionsHeaderProperty;
        private string additionsTooltipProperty;
        private string buyOnlineProperty;
        private string headerProperty;
        private string morePropertiesLinkProperty;
        private string moreProposalsLinkProperty;
        private string wayBillProperty;
        private string vatProperty;
        private string vatFreeProperty;
        #endregion Fields

        #region Properties
        public string VatFreeProperty
        {
            get
            {
                return vatFreeProperty;
            }
            set
            {
                vatFreeProperty = value;
            }
        }

        public string VatProperty
        {
            get
            {
                return vatProperty;
            }
            set
            {
                vatProperty = value;
            }
        }

        public string AdditionsHeaderProperty
        {
            get
            {
                return additionsHeaderProperty;
            }
            set
            {
                additionsHeaderProperty = value;
            }
        }

        public string AdditionsTooltipProperty
        {
            get
            {
                return additionsTooltipProperty;
            }
            set
            {
                additionsTooltipProperty = value;
            }
        }

        public string BuyOnlineProperty
        {
            get
            {
                return buyOnlineProperty;
            }
            set
            {
                buyOnlineProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MorePropertiesLinkProperty
        {
            get
            {
                return morePropertiesLinkProperty;
            }
            set
            {
                morePropertiesLinkProperty = value;
            }
        }

        public string MoreProposalsLinkProperty
        {
            get
            {
                return moreProposalsLinkProperty;
            }
            set
            {
                moreProposalsLinkProperty = value;
            }
        }

        public ServicesResultItemWP ParentWebPart
        {
            get;
            set;
        }

        public string WayBillProperty
        {
            get
            {
                return wayBillProperty;
            }
            set
            {
                wayBillProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "ServicesResultItemWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((ServicesResultItemWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).MoreProposalsLinkProperty = this.ParentWebPart.MoreProposalsLinkProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).BuyOnlineProperty = this.ParentWebPart.BuyOnlineProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).WayBillProperty = this.ParentWebPart.WayBillProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).MorePropertiesLinkProperty = this.ParentWebPart.MorePropertiesLinkProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).AdditionsHeaderProperty = this.ParentWebPart.AdditionsHeaderProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).AdditionsTooltipProperty = this.ParentWebPart.AdditionsTooltipProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).VatProperty = this.ParentWebPart.VatProperty;
                ((ServicesResultItemWPDisplay)this.displayControl).VatFreeProperty = this.ParentWebPart.VatFreeProperty;


                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((ServicesResultItemWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ServicesResultItemWPEdit)this.editControl).MoreProposalsLinkProperty = this.ParentWebPart.MoreProposalsLinkProperty;
                ((ServicesResultItemWPEdit)this.editControl).BuyOnlineProperty = this.ParentWebPart.BuyOnlineProperty;
                ((ServicesResultItemWPEdit)this.editControl).WayBillProperty = this.ParentWebPart.WayBillProperty;
                ((ServicesResultItemWPEdit)this.editControl).MorePropertiesLinkProperty = this.ParentWebPart.MorePropertiesLinkProperty;
                ((ServicesResultItemWPEdit)this.editControl).AdditionsHeaderProperty = this.ParentWebPart.AdditionsHeaderProperty;
                ((ServicesResultItemWPEdit)this.editControl).AdditionsTooltipProperty = this.ParentWebPart.AdditionsTooltipProperty;
                ((ServicesResultItemWPEdit)this.editControl).VatProperty = this.ParentWebPart.VatProperty;
                ((ServicesResultItemWPEdit)this.editControl).VatFreeProperty = this.ParentWebPart.VatFreeProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((ServicesResultItemWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.MoreProposalsLinkProperty = ((ServicesResultItemWPEdit)this.editControl).MoreProposalsLinkProperty;
            this.ParentWebPart.BuyOnlineProperty = ((ServicesResultItemWPEdit)this.editControl).BuyOnlineProperty;
            this.ParentWebPart.WayBillProperty = ((ServicesResultItemWPEdit)this.editControl).WayBillProperty;
            this.ParentWebPart.MorePropertiesLinkProperty = ((ServicesResultItemWPEdit)this.editControl).MorePropertiesLinkProperty;
            this.ParentWebPart.AdditionsHeaderProperty = ((ServicesResultItemWPEdit)this.editControl).AdditionsHeaderProperty;
            this.ParentWebPart.AdditionsTooltipProperty = ((ServicesResultItemWPEdit)this.editControl).AdditionsTooltipProperty;
            this.ParentWebPart.VatProperty = ((ServicesResultItemWPEdit)this.editControl).VatProperty;
            this.ParentWebPart.VatFreeProperty = ((ServicesResultItemWPEdit)this.editControl).VatFreeProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}