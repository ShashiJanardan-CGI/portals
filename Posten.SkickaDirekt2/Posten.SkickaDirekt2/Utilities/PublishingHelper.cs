﻿namespace Posten.SkickaDirekt2.Utilities
{
    using System.Linq;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Publishing;
    using Microsoft.SharePoint.WebPartPages;

    public static class PublishingHelper
    {
        #region Methods

        /// <summary>
        /// Adds given webpart to page
        /// </summary>
        /// <param name="web"></param>
        /// <param name="pageUrl"></param>
        /// <param name="webPartName"></param>
        /// <param name="zoneID"></param>
        /// <param name="zoneIndex"></param>
        /// <param name="webPart"></param>
        /// <returns></returns>
        public static string AddWebPartToPage(SPWeb web, string pageUrl, string webPartName, string zoneID, int zoneIndex, System.Web.UI.WebControls.WebParts.WebPart webPart)
        {
            using (SPLimitedWebPartManager webPartManager = web.GetLimitedWebPartManager(pageUrl, PersonalizationScope.Shared))
            {
                webPartManager.AddWebPart(webPart, zoneID, zoneIndex);
                webPartManager.Web.Dispose();
                return webPart.ID;
            }
        }

        /// <summary>
        /// Sets up generic webpart settings to close a webpart on the page
        /// </summary>
        /// <param name="webPart"></param>
        public static void CloseWebPart(System.Web.UI.WebControls.WebParts.WebPart webPart)
        {
            webPart.AllowClose = true;
        }

        /// <summary>
        /// /// Creates a new publishing page based on the given page layout name
        /// </summary>
        /// <param name="page"></param>
        /// <param name="publishingWeb"></param>
        /// <param name="pageLayoutName"></param>
        /// <param name="pageName"></param>
        /// <param name="pageLayoutCTypeID"></param>
        /// <returns></returns>
        public static PublishingPage CreatePageFromPageLayout(PublishingPage page, PublishingWeb publishingWeb, string pageLayoutName, string pageName, string pageLayoutCTypeID)
        {
            page = null;
            PageLayout pagelayout = publishingWeb.GetAvailablePageLayouts(new SPContentTypeId(pageLayoutCTypeID)).ToList().Where(x => x.Name.Equals(pageLayoutName)).FirstOrDefault();
            if (pagelayout != null)
            {
                var newPage = publishingWeb.GetPublishingPages().ToList().Where(x => x.Name.Equals(pageName));
                if (newPage.Count() == 0)
                {
                    page = publishingWeb.GetPublishingPages().Add(pageName, pagelayout);
                    page.Update();
                    PublishingHelper.PageCheckIn(page, string.Empty);
                }
                else
                {
                    page = newPage.First();
                }
            }
            return page;
        }

        /// <summary>
        /// Returns the target publishing web from the page library
        /// </summary>
        /// <param name="publishingWeb"></param>
        /// <param name="pageName"></param>
        /// <returns></returns>
        public static PublishingPage GetPublishingPage(PublishingWeb publishingWeb, string pageName)
        {
            PublishingPage page = null;
            var newPage = publishingWeb.GetPublishingPages().ToList().Where(x => x.Name.Equals(pageName));
            if (newPage.Count() > 0)
            {
                page = newPage.First();
            }

            return page;
        }

        /// <summary>
        /// Checks in the given publishing page, publishes and approves
        /// </summary>
        /// <param name="page"></param>
        /// <param name="comment"></param>
        public static void PageCheckIn(PublishingPage page, string comment)
        {
            page.ListItem.File.CheckIn(comment);
            page.ListItem.File.Publish(comment);
            page.ListItem.File.Approve(comment);
        }

        /// <summary>
        /// Checks out the given publishing page
        /// </summary>
        /// <param name="page"></param>
        public static void PageCheckOut(PublishingPage page)
        {
            if (page.ListItem.File.Level == SPFileLevel.Published) page.ListItem.File.CheckOut();
        }

        /// <summary>
        /// Sets the given publishing page as the default page of the portal
        /// </summary>
        /// <param name="page"></param>
        /// <param name="publishingWeb"></param>
        public static void SetPageAsDefault(PublishingPage page, PublishingWeb publishingWeb)
        {
            if (page != null)
            {
                publishingWeb.DefaultPage = page.ListItem.File;
                publishingWeb.Update();
            }
        }

        /// <summary>
        /// Sets up generic webpart settings
        /// </summary>
        /// <param name="webPart"></param>
        public static void SetUpWebPart(System.Web.UI.WebControls.WebParts.WebPart webPart, string webPartTitle)
        {
            webPart.ChromeType = System.Web.UI.WebControls.WebParts.PartChromeType.None;
            webPart.Title = webPartTitle;
        }

        #endregion Methods
    }
}