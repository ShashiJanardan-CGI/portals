namespace Posten.Portal.Skicka.SkickaService.Proxy
{
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "informPutInCartResponse", WrapperNamespace = "http://model.itemnotes.app.cisp.posten.se/", IsWrapped = true)]
    public partial class informPutInCartResponse
    {
        #region Fields

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://model.itemnotes.app.cisp.posten.se/", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Posten.Portal.Skicka.SkickaService.Proxy.PutInCartResponse @return;

        #endregion Fields

        #region Constructors

        public informPutInCartResponse()
        {
        }

        public informPutInCartResponse(Posten.Portal.Skicka.SkickaService.Proxy.PutInCartResponse @return)
        {
            this.@return = @return;
        }

        #endregion Constructors
    }
}