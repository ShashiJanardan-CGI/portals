﻿namespace Posten.Portal.Skicka.Services.SkickaService
{
    using Posten.Portal.Platform.Common.Services;
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Logging;
    using Posten.Portal.Skicka.Services.Utils;
    using Posten.Portal.Skicka.SkickaService.Proxy;
    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.Web;

    public class SkickaServiceAgent : ServiceAgentBase, ISkickaService
    {
        #region Fields

        private static LogController logger;

        private ItemNotesServiceBeanClient skickaServiceClient;
        private string transactionLogID;

        #endregion Fields

        #region Constructors

        public SkickaServiceAgent()
        {
            try
            {
                this.skickaServiceClient = new ItemNotesServiceBeanClient(); 
                this.skickaServiceClient.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTUserName];
                this.skickaServiceClient.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTPassword];
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }

        public SkickaServiceAgent(HttpContext context)
        {
            try
            {
                this.skickaServiceClient = new ItemNotesServiceBeanClient(); 
                this.skickaServiceClient.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTUserName];
                this.skickaServiceClient.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTPassword];

                LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }
        

        #endregion Constructors

        #region Properties

        public string TransactionLogID
        {
            get { return transactionLogID; }
            set { transactionLogID = value; }
        }

        #endregion Properties

        #region Methods

        public ReceivingPlaceBE FindReceivingPlace(string bookingId)
        {
            ReceivingPlaceBE rpbe = new ReceivingPlaceBE();
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:FindReceivingPlace:bookingId=" + bookingId);  
                findReceivingPlaceRequest findrpRequest = new findReceivingPlaceRequest() { bookingId = bookingId };
                receivingPlaceInfo recPlace = new receivingPlaceInfo();
                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                                     
                    FindReceivingPlaceResponse findrpResponse = this.skickaServiceClient.findReceivingPlace(findrpRequest);                   

                    recPlace = findrpResponse.receivingPlaceInfo;
                }
                rpbe = new ReceivingPlaceBE() { Name = recPlace.receivePlace.namn, Address = recPlace.receivePlace.adress };

                openTimeData[] b = recPlace.receivePlace.listOpenTimeData;
                int openTimeListSize = b.Count();
                List<string> openTimes = new List<string>();
                foreach (openTimeData otd in b)
                {
                    string theDay = string.Empty;
                    string theTime = string.Empty;
                    string theDayAndTime = string.Empty;

                    if (otd.sistaVeckodag == otd.veckodag && otd.starttid1 != null)
                    {
                        theDay = this.GetWeekDay(otd.veckodag);
                    }
                    else if (otd.starttid1 != null)
                    {
                        theDay = this.GetWeekDay(otd.veckodag) + "-" + this.GetWeekDay(otd.sistaVeckodag);
                    }

                    if (otd.starttid2 != null)
                    {
                        theTime = otd.starttid1 + " " + otd.stopptid1 + ", " + otd.starttid2 + " " + otd.stopptid2;
                    }
                    else
                    {
                        theTime = otd.starttid1 + " " + otd.stopptid1;
                    }

                    if (theDay != string.Empty)
                    {
                        theDayAndTime = theDay + ": " + theTime;
                        openTimes.Add(theDayAndTime);
                    }
                }

                rpbe.OpenTimes = openTimes;
                StopptidData[] std = recPlace.receivePlace.stopptider;
                List<string> stopTimes = new List<string>();

                foreach (StopptidData st in std)
                {
                    string theTime = string.Empty;
                    string theTypAndTime = string.Empty;

                    if (st.klartextTyp.ToLower() == "brev")
                    {
                        theTime = st.stopptid;
                        theTypAndTime = "Brev<br />Vardagar innan " + this.ReturnWithColon(theTime);
                        stopTimes.Add(theTypAndTime);
                    }

                    if (st.klartextTyp.ToLower() == "paket")
                    {
                        theTime = st.stopptid;
                        theTypAndTime = "Paket<br />Vardagar innan " + this.ReturnWithColon(theTime);
                        stopTimes.Add(theTypAndTime);
                    }

                    if (st.klartextTyp.ToLower().Contains("express"))
                    {
                        theTime = st.stopptid;
                        theTypAndTime = "Express<br />Vardagar innan " + this.ReturnWithColon(theTime);
                        stopTimes.Add(theTypAndTime);
                    }
                }

                rpbe.StoppingTimes = stopTimes;
                rpbe.Postnummer = recPlace.receivePlace.postnr;
                rpbe.Postort = recPlace.receivePlace.postort;
                rpbe.ProductionPointId = recPlace.receivePlace.productionPointId;

            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:FindReceivingPlace:bookingId=" + bookingId);
            }

            return rpbe;
        }

        public List<OrderLabelBE> GetAvailableOrderLabels()
        {
            StringBuilder orderLabelLogInfo = new StringBuilder();
            List<OrderLabelBE> ordLabe = new List<OrderLabelBE>();
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetAvailableOrderLabels"); 
 
                getAvailableOrderLabelsRequest ordRequest = new getAvailableOrderLabelsRequest();
                GetAvailableOrderLabelsResponse ordResponse;

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    ordResponse = this.skickaServiceClient.getAvailableOrderLabels(ordRequest);
                }

                List<label> ord = new List<label>();
                for (int i = 0; i < ordResponse.labels.Length; i++)
                {
                    ord.Add(ordResponse.labels.ElementAt(i));
                }
                                
                foreach (label o in ord)
                {
                    OrderLabelBE or = new OrderLabelBE();
                    or.ProdId = o.prodId;
                    or.Name = o.name;
                    or.Quantity = o.quantity.ToString();
                    or.CustomerNumber = o.customerNumber;
                    or.LabelType = o.labelType.ToString();
                    ordLabe.Add(or);
                }
                               
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                ordLabe = null;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetAvailableOrderLabels");               
            }
            return ordLabe;
        }

        public ConfigurationItemBE GetConfigurationItem()
        {
            StringBuilder confItemLogInfo = new StringBuilder();
            ConfigurationItemBE item = new ConfigurationItemBE();
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetConfigurationItem");
                getConfigurationItemsRequest request = new getConfigurationItemsRequest();

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    GetConfigurationItemsResponse respons = this.skickaServiceClient.getConfigurationItems(request);

                    item.CustomerId = respons.customerID;
                    item.TaxCountry = respons.taxCountry;
                    item.ProduceAddress = new System.Uri(respons.produceAddress);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                item = null;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetConfigurationItem"); 
            }
            return item;
        }

        
        public ConsignmentBE GetConsignment(string orderNumber)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetConsignment:orderNumber=" + orderNumber);  

                findConsignmentsRequest consignmentRequest = new findConsignmentsRequest() { orderNumber = orderNumber };
                FindConsignmentsResponse consigmentResponse = new FindConsignmentsResponse();

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    consigmentResponse = this.skickaServiceClient.findConsignments(consignmentRequest);
                }

                if (consigmentResponse.consignmentInfos == null)
                {
                    return null;
                }
                else if (consigmentResponse.consignmentInfos.Length > 1)
                {
                    throw new Exception("Multiple consignemtns with the same order number: " + orderNumber);
                }
                else
                {
                    return new ConsignmentBE(consigmentResponse.consignmentInfos[0].consignment);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetConsignment");
            }
        }

        public List<ConsignmentBE> GetConsignments(string bookingId)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetConsignments:bookingId=" + bookingId);

                List<ConsignmentBE> consignmentList = new List<ConsignmentBE>();
                StringBuilder consmentsLogInfo = new StringBuilder();
                Array consignments;
                findConsignmentsRequest consignmentRequest = new findConsignmentsRequest() { bookingId = bookingId };

                // Logging the bookingId in request
                consmentsLogInfo.AppendLine(string.Format("Requested bookingId: {0}", bookingId));

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    FindConsignmentsResponse consigmentResponse = this.skickaServiceClient.findConsignments(consignmentRequest);
                    consignments = consigmentResponse.consignmentInfos;
                }
                                
                foreach (consignmentInfo conInfo in consignments)
                {
                    consignmentList.Add(new ConsignmentBE(conInfo.consignment));
                }

                return consignmentList;
            }
            catch (Exception ex)
            {                
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetConsignments:bookingId=" + bookingId);
            }
        }

        public byte[] GetItemNotesPDF(string bookingId)
        {
            byte[] returnValue = null;
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetItemNotesPDF:bookingId=" + bookingId);
                getItemNotesPDFRequest request = new getItemNotesPDFRequest();
                request.bookingId = bookingId;

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    GetItemNotesPDFResponse response = this.skickaServiceClient.getItemNotesPDF(request);
                    if (response.status == responseStatus.SUCCESS)
                    {
                        returnValue = Convert.FromBase64String(response.base64EncodedAttachmentContent);
                    }  
                }     
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally {
                logger.LogMessage(this.transactionLogID, "Start:GetItemNotesPDF:bookingId=" + bookingId);
            }

            return returnValue;
        }

        public List<ReceivingPlaceBE> GetValidServicePoints(string bookingId, string[] productionPointIds)
        {
            List<ReceivingPlaceBE> list = new List<ReceivingPlaceBE>();
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetValidServicePoints:bookingId=" + bookingId);
                
                getInfoAboutPossibleReceivingPlacesRequest req = new getInfoAboutPossibleReceivingPlacesRequest();
                req.bookingId = bookingId;
                req.productionPointIds = productionPointIds;

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    GetInfoAboutPossibleReceivingPlacesResponse response = this.skickaServiceClient.getInfoAboutPossibleReceivingPlaces(req);

                    if (response.status == responseStatus.SUCCESS)
                    {
                        foreach (receivingPlaceInfo info in response.receivingPlaceInfos)
                        {
                            ReceivingPlaceBE place = new ReceivingPlaceBE();
                            place.Name = info.receivePlace.namn;
                            place.Address = info.receivePlace.adress;
                            place.Postnummer = info.receivePlace.postnr;
                            place.Postort = info.receivePlace.postort;
                            place.ProductionPointId = info.receivePlace.productionPointId;
                            list.Add(place);
                        }
                    }                    
                }
            }
            catch (Exception e)
            {
                logger.LogError(this.transactionLogID, e);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetValidServicePoints:bookingId=" + bookingId);
            }

            return list;
        }

        public string GetVersion()
        {
            string version = "Version not set";
            try
            {
                version = this.skickaServiceClient.getVersion();
            }
            catch (Exception ex)
            {
                Posten.Portal.Skicka.Services.Utils.LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return version;
        }

        public InformPutInCartResponseBE InformPutInCart(
            string orderNumber,
            ValidationResponseBE validatedService,
            string  serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId)
        {
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:InformPutInCart:orderNumber=" + orderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode + 
                    ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl + 
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId);  //TODO: add (more) parameters

                string baseServiceOrderRowNumber = "0";
                informPutInCartRequest putRequest = new informPutInCartRequest();
                this.InitiatePutRequest(
                    orderNumber,
                    validatedService,
                    serviceName,
                    serviceCode,
                    size,
                    weight,
                    fromAddress,
                    toAddress,
                    codAmount,
                    plusgiro,
                    postforskottKontoNr,
                    postforskottPaymentReference,
                    orderconfirmationUrl,
                    urlText,
                    url,
                    details ?? Constants.TrackAndTraceLink,
                    baseServiceOrderRowNumber,
                    receiverDestinationId,
                    out putRequest);

                InformPutInCartResponseBE responseBE = new InformPutInCartResponseBE();
                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    PutInCartResponse response = this.skickaServiceClient.informPutInCart(putRequest);

                    responseBE = InitiateInformPutInCartResponse(this.transactionLogID, response);
                }

                return responseBE;
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:InformPutInCart:orderNumber=" + orderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode +
                    ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId);  //TODO: add (more) parameters

            }
        }
                
        public InformPutInCartResponseBE InformUpdateInCart(            
            string newOrderNumber,
            string oldOrderNumber,
            string baseServiceOrderRowNumber,
             string serviceName,
            string serviceCode,
            ValidationResponseBE validatedService,
            ServicePropertiesBE serviceCombination,           
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId)
        {
            InformPutInCartResponseBE responseBE = null;
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:InformPutInCart:newOrderNumber=" + newOrderNumber + ",oldOrderNumber=" + oldOrderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode +
                    ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId);

                informChangedInCartRequest changeRequest = new informChangedInCartRequest();
                InitializeChangeRequest(this.transactionLogID, oldOrderNumber, newOrderNumber, baseServiceOrderRowNumber, serviceCode, serviceName, receiverDestinationId, validatedService, size, weight, fromAddress, toAddress, codAmount, plusgiro, postforskottKontoNr, postforskottPaymentReference, orderconfirmationUrl, urlText, url, details, out changeRequest);

                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    ChangedInCartResponse response = this.skickaServiceClient.informChangedInCart(changeRequest);
                    responseBE = InitializeChangeResponse(this.transactionLogID, response);
                }
                
                if (responseBE.Status != "SUCCESS")
                {
                    throw new Exception();
                }
                               
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;              
            }
            finally {
                logger.LogMessage(this.transactionLogID, "End:InformPutInCart:newOrderNumber=" + newOrderNumber + ",oldOrderNumber=" + oldOrderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode +
                        ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                        ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId); 
            }
            return responseBE;
        }

        public void Initialize(HttpContext context)
        {
            LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
            logger = new LogController();
        }

        public string OrderLabels(string[] prodIds, string customerName, string contactPerson, string email, string streetAddress, string zip, string city, string phone, string addressCo)
        {
            string htmlOutput = string.Empty;
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:OrderLabels: customerName=" + customerName + ",email=" + email + ",streetAddress=" + streetAddress + ",zip=" + zip +
                    ",city=" + city + ",phone=" + phone + ",addressCo=" + addressCo );

                string result = string.Empty;
                orderLabelsRequest request = new orderLabelsRequest();
                request.prodIds = prodIds;
                request.customerName = customerName;
                request.contactPerson = contactPerson;
                request.eMail = email;
                request.streetAddress = streetAddress;
                request.zip = zip;
                request.city = city;
                request.city = city;
                request.phone = phone;
                request.co = addressCo;

                OrderLabelsResponse response;
                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);
                    response = this.skickaServiceClient.orderLabels(request);
                }

                if (response.status == responseStatus.SUCCESS)
                {
                    result = "SUCCESS";
                }

                if (response.status == responseStatus.DB_ERROR)
                {
                    result = "DB ERROR";
                }

                if (response.status == responseStatus.GENERAL_FUNCTIONAL_ERROR)
                {
                    result = "GENERAL FUNCTIONAL ERROR";
                }

                if (response.status == responseStatus.INVALID_INPUT)
                {
                    result = "INVALID INPUT";
                }

                //Set returning Html input                
                if (result == "SUCCESS")
                {
                    htmlOutput = "<div class='mailConfirmWrapper'><div class='mailConfirmText'><h4 style='margin-top:10px;padding-bottom:0px'>Din beställning är mottagen</h4>Du kommer att ha dina varor inom några dagar.</div></div>";
                }
                else if (result != "SUCCESS")
                {
                    throw new Exception(string.Format("Något har blivit fel: {0}", result));
                }
                else
                {
                    throw new Exception(string.Format("Något har blivit fel."));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                htmlOutput = "<div class='infoBoxError'><b>Vi beklagar! </b>" + ex.Message.ToString() + "</div>";                
                throw;

            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:OrderLabels: customerName=" + customerName + ",email=" + email + ",streetAddress=" + streetAddress + ",zip=" + zip +
                    ",city=" + city + ",phone=" + phone + ",addressCo=" + addressCo);
            }
            return htmlOutput;
        }

        public string ReturnWithColon(string intime)
        {
            string outtime = intime.Insert(2, ":");
            return outtime;
        }

        protected string GetWeekDay(int day)
        {
            try
            {
                string weekDay = string.Empty;
                switch (day)
                {
                    case 1:
                        weekDay = "Må";
                        break;

                    case 2:
                        weekDay = "Ti";
                        break;

                    case 3:
                        weekDay = "Ons";
                        break;

                    case 4:
                        weekDay = "To";
                        break;

                    case 5:
                        weekDay = "Fre";
                        break;

                    case 6:
                        weekDay = "Lö";
                        break;

                    case 7:
                        weekDay = "Sö";
                        break;

                    default:
                        weekDay = string.Empty;
                        break;
                }

                return weekDay;
            }
            catch (Exception e)
            {
                logger.LogError(e);
                throw;    
            }
            
        }

        private static void InitializeChangeRequest(string transactionLogID, string oldOrderNumber, string newOrderNumber, string baseServiceOrderRowNumber, string serviceCode, string serviceName, string receiverDestinationId, ValidationResponseBE validatedService, SizeBE size, int weight, AddressBE fromAddress, AddressBE toAddress, string codAmount, bool plusgiro, string postforskottKontoNr, string postforskottPaymentReference, string orderconfirmationUrl, string urlText, string url, string details, out informChangedInCartRequest changeRequest)
        {
            try
            {
                var correlationId = logger.LogMessage(transactionLogID, "Start:InitializeChangeRequest:newOrderNumber=" + newOrderNumber + ",oldOrderNumber=" + oldOrderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode +
                    ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId);

                changeRequest = new informChangedInCartRequest();
                changeRequest.externalMarketPlace = validatedService.Marketplace;
                changeRequest.externalMarketPlaceBookingId = validatedService.MarketplaceBookingId;
                changeRequest.orderConfirmationUrl = orderconfirmationUrl;
                changeRequest.url = url;
                changeRequest.urlText = urlText;
                changeRequest.details =  details ?? Constants.TrackAndTraceLink;

                changeRequest.companyCode = validatedService.CompanyCode;
                changeRequest.pabloProductCode = validatedService.ProductCode;
                changeRequest.kolliNrSeriesType = validatedService.ReferenceNumberSerialType;
                changeRequest.orderNumber = newOrderNumber;
                changeRequest.oldOrderNumber = oldOrderNumber;

                changeRequest.service = new service();
                changeRequest.service.baseServiceId = validatedService.BaseService.ServiceKey;//serviceCombination.BaseServiceId;
                changeRequest.service.baseServiceName = serviceName;//serviceCombination.Name;
                changeRequest.productCode = serviceCode;//serviceCombination.ServiceKey;
                changeRequest.service.baseServiceOrderRowNumber = baseServiceOrderRowNumber;
                changeRequest.service.baseServiceSapProductId = validatedService.SapId;

                // FromAddress
                changeRequest.fromAddress = ParseAddress(transactionLogID, fromAddress);
                changeRequest.fromAddress.country = "SE";

                // ToAddress
                changeRequest.toAddress = ParseAddress(transactionLogID, toAddress);
                changeRequest.toAddress.doorCode = toAddress.DoorCode;
                changeRequest.toAddress.country = receiverDestinationId;

                // Size
                changeRequest.diameter = size.Diameter;
                changeRequest.diameterSpecified = true;
                changeRequest.height = size.Height;
                changeRequest.heightSpecified = true;
                changeRequest.length = size.Length;
                changeRequest.width = size.Width;
                changeRequest.widthSpecified = true;
                changeRequest.weight = weight;

                // COD
                double amount = UIHelper.ReturnDouble(codAmount);
                if (amount > 0)
                {
                    changeRequest.plusgiro = plusgiro;
                    changeRequest.plusgiroSpecified = true;
                    changeRequest.postforskottKontoNr = postforskottKontoNr;
                    changeRequest.postforskottPaymentReference = postforskottPaymentReference;
                    changeRequest.postforskottAmount = ParseDecimalValueToLong(amount);
                    changeRequest.postforskottAmountSpecified = true;
                }

                //TODO: Investigate into insurance amount (this related to additional services)
                //if (insuranceAmount > 0)
                //{
                changeRequest.insuranceAmount = ParseDecimalValueToLong(0.0);//ParseDecimalValueToLong(insuranceAmount);
                changeRequest.insuranceAmountSpecified = true;
                //}

                // Price
                changeRequest.service.baseServicePrice = new price();
                changeRequest.service.baseServicePrice.amount = ParseDecimalValueToLong(validatedService.BaseServicePrice.Amount);
                changeRequest.service.baseServicePrice.amountNoVat = ParseDecimalValueToLong(validatedService.BaseServicePrice.AmountNoVat);
                changeRequest.service.baseServicePrice.vatAmount = ParseDecimalValueToLong(validatedService.BaseServicePrice.VATAmount);//MVU7734 - S
                changeRequest.service.baseServicePrice.vatCode = validatedService.BaseServicePrice.VatCode;//MVU7734 - S
                changeRequest.service.baseServicePrice.currency = validatedService.BaseServicePrice.Currency;

                double decimalValidatedServiceBaseServicePriceVatPercentage;
                if (double.TryParse(validatedService.BaseServicePrice.VATPercentage.ToString(), out decimalValidatedServiceBaseServicePriceVatPercentage))
                {
                    changeRequest.service.baseServicePrice.vat = decimalValidatedServiceBaseServicePriceVatPercentage;
                }
                else
                {
                    changeRequest.service.baseServicePrice.vat = 0;
                }

                changeRequest.totalPrice = new price();
                changeRequest.totalPrice.amount = ParseDecimalValueToLong(validatedService.Price.Amount);
                changeRequest.totalPrice.amountNoVat = ParseDecimalValueToLong(validatedService.Price.AmountNoVat);//MVU7734 - S
                changeRequest.totalPrice.vatAmount = ParseDecimalValueToLong(validatedService.Price.VATAmount); //MVU7734 - S
                changeRequest.totalPrice.vatCode = validatedService.Price.VatCode; //MVU7734 - S
                changeRequest.totalPrice.currency = validatedService.Price.Currency;

                double decimalValidatedServicePriceVatPercentage;
                if (double.TryParse(validatedService.Price.VATPercentage.ToString(), out decimalValidatedServicePriceVatPercentage))
                {
                    changeRequest.totalPrice.vat = decimalValidatedServicePriceVatPercentage;
                }
                else
                {
                    changeRequest.totalPrice.vat = 0;
                }

                changeRequest.service.baseServiceOrderRowNumber = "0";


                // Additional Services
                int index = 1;
                List<additionalService> services = new List<additionalService>();
                if (validatedService.AdditionalServices != null)
                {
                    foreach (AdditionalServiceBE additionalServiceBE in validatedService.AdditionalServices)
                    {
                        additionalService additionalService = new additionalService();
                        additionalService.name = additionalServiceBE.Name;
                        if (additionalService.name == string.Empty)
                        {
                            additionalService.name = "Unknown";
                        }

                        additionalService.orderRowNumber = index.ToString();
                        additionalService.price = new price();
                        additionalService.price.amount = ParseDecimalValueToLong(additionalServiceBE.Price.Amount);
                        additionalService.price.amountNoVat = ParseDecimalValueToLong(additionalServiceBE.Price.AmountNoVat);
                        additionalService.price.vatAmount = ParseDecimalValueToLong(additionalServiceBE.Price.VATAmount);//MVU7734 - S
                        additionalService.price.vatCode = additionalServiceBE.Price.VatCode;//MVU7734 - S
                        additionalService.price.currency = additionalServiceBE.Price.Currency;

                        double decimalAdditionalServicePriceVatPercentage;
                        if (double.TryParse(additionalServiceBE.Price.VATPercentage.ToString(), out decimalAdditionalServicePriceVatPercentage))
                        {
                            additionalService.price.vat = decimalAdditionalServicePriceVatPercentage;
                        }
                        else
                        {
                            additionalService.price.vat = 0;
                        }

                        additionalService.sapProductId = additionalServiceBE.SapId;
                        additionalService.pvtId = additionalServiceBE.PvtId; // additionalServiceBE.AdditionalServiceCode;
                        additionalService.serviceId = additionalServiceBE.AdditionalServiceCode;//additionalServiceBE.ProductCode; RR
                        services.Add(additionalService);
                        index++;
                    }
                }
                changeRequest.service.additionalServices = services.ToArray();

                //Additional costs
                List<additionalInfoEntity> additionalCostList = new List<additionalInfoEntity>();
                if (validatedService.AdditionalInfo != null)
                {
                    foreach (InfoBE informationItem in validatedService.AdditionalInfo)
                    {
                        additionalInfoEntity additionalCost = new additionalInfoEntity();
                        additionalCost.textId = UIHelper.ReturnLong(informationItem.Code);
                        additionalCost.textIdSpecified = true;
                        additionalCost.text = informationItem.Value;
                        additionalCostList.Add(additionalCost);
                    }
                }
                changeRequest.additionalInfos = additionalCostList.ToArray();
            }
            catch (Exception ex)
            {
                logger.LogError(transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:InitializeChangeRequest:newOrderNumber=" + newOrderNumber + ",oldOrderNumber=" + oldOrderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode +
                     ",weight=" + weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                     ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",receiverDestinationId=" + receiverDestinationId);
            }
        }


        private static InformPutInCartResponseBE InitializeChangeResponse(string transactionLogID, ChangedInCartResponse response)
        {
            StringBuilder informUpdateInCartResponseLogInfo = new StringBuilder();
            InformPutInCartResponseBE responseBE = new InformPutInCartResponseBE();

            try
            {
                logger.LogMessage(transactionLogID, "Start:InitializeChangeResponse");
                responseBE.Status = response.status.ToString();
                responseBE.AdditionalInfo = new List<InfoBE>();
                if (response.compositeFault != null)
                {
                    foreach (fault cartFault in response.compositeFault)
                    {
                        foreach (parameter param in cartFault.parameters)
                        {
                            InfoBE error = new InfoBE();
                            error.Code = cartFault.faultCode.ToString();
                            error.Attribute = param.name;
                            error.Value = param.value;
                            responseBE.AdditionalInfo.Add(error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:InitializeChangeResponse");
            }
            return responseBE;
        }

        private static InformPutInCartResponseBE InitiateInformPutInCartResponse(string transactionLogID, PutInCartResponse response)
        {
            InformPutInCartResponseBE responseBE = new InformPutInCartResponseBE();
            try
            {
                logger.LogMessage(transactionLogID, "Start:InitiateInformPutInCartResponse");
                responseBE.Status = response.status.ToString();
                responseBE.AdditionalInfo = new List<InfoBE>();
                
                if (response.compositeFault != null)
                {
                    foreach (fault cartFault in response.compositeFault)
                    {
                        foreach (parameter param in cartFault.parameters)
                        {
                            InfoBE error = new InfoBE();
                            error.Code = cartFault.faultCode.ToString();
                            error.Attribute = param.name;
                            error.Value = param.value;
                            responseBE.AdditionalInfo.Add(error);
                        }
                    }
                }

                // If the response status is not a success and the response statusMsg is not empty or null
                // then the response might be caused by an itemnotesinternalexception or itemnotestechnicalexception in the ISP layer
                if (response.status != responseStatus.SUCCESS && !string.IsNullOrEmpty(response.statusMsg))
                {
                    InfoBE error = new InfoBE() { Code = response.statusMsg, Value = response.statusMsg };
                    responseBE.AdditionalInfo.Add(error);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:InitiateInformPutInCartResponse");
            }
            return responseBE;
        }

        private static address ParseAddress(string transactionLogID, AddressBE toAddress)
        {
            address address = new address();
            try
            {
                logger.LogMessage(transactionLogID, "Start:ParseAddress");
                address.fullName = toAddress.Name;
                address.city = toAddress.City;
                address.company = toAddress.CompanyName;
                address.country = toAddress.Country;
                address.eMail = toAddress.Email;
                address.mobile = toAddress.Mobile;
                address.phone = toAddress.Phone;
                address.street = toAddress.AddressField1;
                address.street2 = toAddress.AddressField2;
                address.zip = toAddress.ZipCode;
            }
            catch (Exception e)
            {
                logger.LogError(transactionLogID, e);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:ParseAddress");
            }
            return address;
        }

        private static AddressBE ParseAddressBE(string transactionLogID, address1 address)
        {
            AddressBE addressBE = new AddressBE();
            try
            {
                logger.LogMessage(transactionLogID, "Start:ParseAddressBE");
                addressBE.City = address.city;
                addressBE.CompanyName = address.company;
                addressBE.Country = address.country;
                addressBE.Email = address.eMail;
                addressBE.Name = address.fullName;
                addressBE.Mobile = address.mobile;
                addressBE.Mobile = address.phone;
                addressBE.AddressField1 = address.street;
                addressBE.AddressField2 = address.street2;
                addressBE.ZipCode = address.zip;
            }
            catch (Exception e)
            {
                logger.LogError(transactionLogID, e);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:ParseAddressBE");
            }
            return addressBE;
        }

        private static long ParseDecimalValueToLong(double amount)
        {                 
            decimal decimalAmount;
            try
            {
                if (decimal.TryParse(amount.ToString(), out decimalAmount))
                {
                    return (long)(decimalAmount * 100);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                logger.LogError(e);
                throw;
            }        
        }

        private void InitiatePutRequest(
            string orderNumber, 
            ValidationResponseBE validatedService, 
            string serviceName, 
            string serviceCode, 
            SizeBE size, 
            int weight, 
            AddressBE fromAddress, 
            AddressBE toAddress, 
            string codAmount, 
            bool plusgiro, 
            string postforskottKontoNr, 
            string postforskottPaymentReference, 
            string orderconfirmationUrl, 
            string urlText, 
            string url, 
            string details, 
            string baseServiceOrderRowNumber, 
            string receiverDestinationId, 
            out informPutInCartRequest putRequest)
        {
            try
            {
                logger.LogMessage(this.transactionLogID, "Start:InitiatePutRequest:orderNumber=" + orderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode + ",weight=" + 
                    weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl + 
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",baseServiceOrderRowNumber=" + baseServiceOrderRowNumber + ",receiverDestinationId=" + receiverDestinationId);  

                putRequest = new informPutInCartRequest();
                putRequest.externalMarketPlace = validatedService.Marketplace;
                putRequest.externalMarketPlaceBookingId = validatedService.MarketplaceBookingId;
                putRequest.orderConfirmationUrl = orderconfirmationUrl;
                putRequest.url = url;
                putRequest.urlText = urlText;
                putRequest.details = details;
                putRequest.companyCode = validatedService.CompanyCode;
                putRequest.pabloProductCode = serviceCode;
                putRequest.kolliNrSeriesType = validatedService.ReferenceNumberSerialType;
                putRequest.orderNumber = orderNumber;

                putRequest.service = new service();
                putRequest.service.baseServiceId = validatedService.BaseService.ServiceKey;//serviceCombination.BaseServiceId;
                putRequest.service.baseServiceName = serviceName;//serviceCombination.Name;
                putRequest.productCode = validatedService.ProductCode;//serviceCode;//serviceCombination.ServiceKey;

                putRequest.service.baseServiceOrderRowNumber = baseServiceOrderRowNumber;
                putRequest.service.baseServiceSapProductId = validatedService.SapId;

                // FromAddress
                putRequest.fromAddress = ParseAddress(this.transactionLogID, fromAddress);
                putRequest.fromAddress.country = "SE"; // TODO: move somewhere else, preferably to ISP layer

                // ToAddress
                putRequest.toAddress = ParseAddress(this.transactionLogID, toAddress);
                putRequest.toAddress.doorCode = toAddress.DoorCode;
                putRequest.toAddress.country = receiverDestinationId;

                // Size
                putRequest.diameter = size.Diameter;
                putRequest.diameterSpecified = true;
                putRequest.height = size.Height;
                putRequest.heightSpecified = true;
                putRequest.length = size.Length;
                putRequest.width = size.Width;
                putRequest.widthSpecified = true;
                putRequest.weight = weight;

                // COD
                double amount = UIHelper.ReturnDouble(codAmount);
                if (amount > 0)
                {
                    putRequest.plusgiro = plusgiro;
                    putRequest.plusgiroSpecified = true;
                    putRequest.postforskottKontoNr = postforskottKontoNr;
                    putRequest.postforskottPaymentReference = postforskottPaymentReference;
                    putRequest.postforskottAmount = ParseDecimalValueToLong(amount);
                    putRequest.postforskottAmountSpecified = true;
                }

                //TODO: Investigate into insurance amount (this related to additional services)
                //if (insuranceAmount > 0)
                //{
                putRequest.insuranceAmount = ParseDecimalValueToLong(0.0);//ParseDecimalValueToLong(insuranceAmount);
                putRequest.insuranceAmountSpecified = true;
                //}

                // Price
                putRequest.service.baseServicePrice = new price();
                putRequest.service.baseServicePrice.amount = ParseDecimalValueToLong(validatedService.BaseServicePrice.Amount);
                putRequest.service.baseServicePrice.amountNoVat = ParseDecimalValueToLong(validatedService.BaseServicePrice.AmountNoVat);
                putRequest.service.baseServicePrice.vatAmount = ParseDecimalValueToLong(validatedService.BaseServicePrice.VATAmount);//MVU7734 - S
                putRequest.service.baseServicePrice.vatCode = validatedService.BaseServicePrice.VatCode;//MVU7734 - S
                putRequest.service.baseServicePrice.currency = validatedService.BaseServicePrice.Currency;

                double doubleValidatedServiceBaseServicePriceVatPercentage;
                if (double.TryParse(validatedService.BaseServicePrice.VATPercentage.ToString(), out doubleValidatedServiceBaseServicePriceVatPercentage))
                {
                    putRequest.service.baseServicePrice.vat = doubleValidatedServiceBaseServicePriceVatPercentage;
                }
                else
                {
                    putRequest.service.baseServicePrice.vat = 0;
                }

                putRequest.totalPrice = new price();
                putRequest.totalPrice.amount = ParseDecimalValueToLong(validatedService.Price.Amount);
                putRequest.totalPrice.currency = validatedService.Price.Currency;
                putRequest.totalPrice.vatAmount = ParseDecimalValueToLong(validatedService.Price.VATAmount);//MVU7734 - S
                putRequest.totalPrice.vatCode = validatedService.Price.VatCode;//MVU7734 - S

                double decimalValidatedServicePriceVatPercentage;
                if (double.TryParse(validatedService.Price.VATPercentage.ToString(), out decimalValidatedServicePriceVatPercentage))
                {
                    putRequest.totalPrice.vat = decimalValidatedServicePriceVatPercentage;
                }
                else
                {
                    putRequest.totalPrice.vat = 0;
                }

                putRequest.service.baseServiceOrderRowNumber = "0";

                // Additional Services
                int index = 1;
                List<additionalService> services = new List<additionalService>();
                if (validatedService.AdditionalServices != null)
                {
                    foreach (AdditionalServiceBE additionalServiceBE in validatedService.AdditionalServices)
                    {
                        additionalService additionalService = new additionalService();
                        additionalService.name = additionalServiceBE.Name;
                        if (additionalService.name == string.Empty)
                        {
                            additionalService.name = "Unknown";
                        }

                        additionalService.orderRowNumber = index.ToString();
                        additionalService.price = new price();
                        additionalService.price.amount = ParseDecimalValueToLong(additionalServiceBE.Price.Amount);
                        additionalService.price.amountNoVat = ParseDecimalValueToLong(additionalServiceBE.Price.AmountNoVat);
                        additionalService.price.vatAmount = ParseDecimalValueToLong(additionalServiceBE.Price.VATAmount);//MVU7734 - S
                        additionalService.price.vatCode = additionalServiceBE.Price.VatCode; //MVU7734 -S
                        additionalService.price.currency = additionalServiceBE.Price.Currency;

                        double decimalAdditionalServicePriceVatPercentage;
                        if (double.TryParse(additionalServiceBE.Price.VATPercentage.ToString(), out decimalAdditionalServicePriceVatPercentage))
                        {
                            additionalService.price.vat = decimalAdditionalServicePriceVatPercentage;
                        }
                        else
                        {
                            additionalService.price.vat = 0;
                        }

                        additionalService.sapProductId = additionalServiceBE.SapId;
                        additionalService.pvtId = additionalServiceBE.PvtId; // additionalServiceBE.AdditionalServiceCode;
                        additionalService.serviceId = additionalServiceBE.AdditionalServiceCode;//additionalServiceBE.ProductCode; RR
                        services.Add(additionalService);
                        index++;
                    }
                }
                putRequest.service.additionalServices = services.ToArray();

                //Additional costs
                List<additionalInfoEntity> additionalCostList = new List<additionalInfoEntity>();
                if (validatedService.AdditionalInfo != null)
                {
                    foreach (InfoBE informationItem in validatedService.AdditionalInfo)
                    {
                        additionalInfoEntity additionalCost = new additionalInfoEntity();
                        additionalCost.textId = UIHelper.ReturnLong(informationItem.Code);
                        additionalCost.textIdSpecified = true;
                        additionalCost.text = informationItem.Value;
                        additionalCostList.Add(additionalCost);
                    }
                }
                putRequest.additionalInfos = additionalCostList.ToArray();
                
            }
            catch (Exception ex)
            {
                logger.LogError(transactionLogID, ex);

                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "Start:InitiatePutRequest:orderNumber=" + orderNumber + ",serviceName=" + serviceName + ",serviceCode=" + serviceCode + ",weight=" +
                    weight + ",postforskottKontoNr=" + postforskottKontoNr + ",postforskottPaymentReference=" + postforskottPaymentReference + ",orderconfirmationUrl=" + orderconfirmationUrl +
                    ",urlText=" + urlText + ",url=" + url + ",details=" + details + ",baseServiceOrderRowNumber=" + baseServiceOrderRowNumber + ",receiverDestinationId=" + receiverDestinationId);  

            }
        }

        public bool RemoveConsignment(string orderNumber)
        {
            var removedSuccessfully = false;
            try
            {
                var correlationId = logger.LogMessage(transactionLogID, "Start:RemoveConsignment:orderNumber=" + orderNumber);
                using (new OperationContextScope(this.skickaServiceClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    informDeletedInCartRequest request = new informDeletedInCartRequest();
                    request.orderNumber = orderNumber;
                    InformDeletedInCartResponse response = this.skickaServiceClient.informDeletedInCart(request);
                    if (response.status == responseStatus.SUCCESS)
                    {
                        removedSuccessfully = true;
                    }
                    else {
                        throw new Exception(response.statusMsg);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.LogError(transactionLogID, exception);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End:RemoveConsignment: OrderNumber=" + orderNumber);               
            }
            return removedSuccessfully;
        }

        #endregion Methods
    }
}