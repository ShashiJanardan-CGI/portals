﻿namespace Posten.Portal.eShop.Presentation
{
    using System.CodeDom;
    using System.Web.Compilation;
    using System.Web.UI;

    [ExpressionPrefix("Code")]
    public class eShopCodeExpressionBuilder : ExpressionBuilder
    {
        #region Methods

        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        {
            return new CodeSnippetExpression(entry.Expression);
        }

        #endregion Methods
    }
}