﻿namespace Posten.Portal.eShop.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Publishing;
    using Microsoft.SharePoint.Publishing.Navigation;

    using Posten.Portal.eShop.BusinessEntities;
    using Posten.Portal.eShop.Services;
    using Posten.Portal.Platform.Common.Container;

    public class EshopSiteMapProvider : PortalSiteMapProvider
    {
        #region Fields

        private const string CategoryIdKey = "PostenEshopCategory";

        #endregion Fields

        #region Constructors

        public EshopSiteMapProvider()
        {
            // do not limit the number of children a particular node may store
            this.DynamicChildLimit = 0;

            // do not include pages in results
            ////base.IncludePages = IncludeOption.Never;

            // do not include authored links in results
            ////base.IncludeAuthoredLinks = false;

            // do not include headings but return nodes beneath headings
            ////base.FlattenHeadings = true;
        }

        #endregion Constructors

        #region Methods

        public override SiteMapNodeCollection GetChildNodes(SiteMapNode node)
        {
            var portalNode = node as PortalSiteMapNode;
            if (portalNode != null)
            {
                if (portalNode.Type == NodeTypes.Area)
                {
                    var areaNodes = base.GetChildNodes(portalNode);

                    for (int i = 0; i < areaNodes.Count; i++)
                    {
                        // TODO: check for correct content type of page
                        var childNode = areaNodes[i] as PortalListItemSiteMapNode;
                        if (childNode != null && childNode.Type == NodeTypes.Page && childNode.IsVisible)
                        {
                            var categoryId = childNode[CategoryIdKey] as string;
                            if (!string.IsNullOrEmpty(categoryId))
                            {
                                PortalSiteMapNode newNode = new PortalSiteMapNode(portalNode.WebNode, childNode.Key, NodeTypes.Custom, childNode.Url, childNode.Title, childNode.Description);
                                newNode.ParentNode = portalNode;
                                newNode[CategoryIdKey] = categoryId;

                                areaNodes.Insert(i, newNode);
                                areaNodes.RemoveAt(i + 1);
                            }
                        }
                    }

                    return areaNodes;
                }

                if (portalNode.Type == NodeTypes.Custom)
                {
                    var categoryId = portalNode[CategoryIdKey] as string;
                    if (!string.IsNullOrEmpty(categoryId))
                    {
                        try
                        {
                            ICatalogService catalogService = IoC.Resolve<ICatalogService>();
                            ICollection<CategoryModel> categories = catalogService.GetCategory(categoryId);

                            var childNodes = new SiteMapNodeCollection();

                            foreach (var category in categories)
                            {
                                string itemUrl = string.Format("{0}?categoryId={1}", portalNode.Url, category.Id);
                                PortalSiteMapNode newNode = new PortalSiteMapNode(portalNode.WebNode, portalNode.Url + "/" + category.Id, NodeTypes.Custom, itemUrl, category.Name, null);
                                newNode.ParentNode = portalNode;
                                childNodes.Add(newNode);
                            }

                            return childNodes;
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry(ex.Source, ex.Message, EventLogEntryType.Error);
                            throw;
                        }
                    }

                    return PortalSiteMapProvider.EmptyCollection;
                }
            }

            return base.GetChildNodes(node);
        }

        #endregion Methods
    }
}