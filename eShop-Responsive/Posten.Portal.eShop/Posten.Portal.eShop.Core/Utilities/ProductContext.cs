﻿namespace Posten.Portal.eShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.eShop.BusinessEntities;
    using Posten.Portal.eShop.Services;
    using Posten.Portal.Platform.Common.Container;

    public partial class ProductContext
    {
        #region Fields

        private static ICatalogService productBasketService;

        #endregion Fields

        #region Properties

        private static ICatalogService ProductService
        {
            get
            {
                if (productBasketService == null)
                {
                    productBasketService = IoC.Resolve<ICatalogService>();
                }

                return productBasketService;
            }
        }

        #endregion Properties

        #region Methods

        public static ProductModel GetProduct(string productId)
        {
            return ProductService.GetProduct(productId);
        }

        #endregion Methods
    }
}