﻿namespace Posten.Portal.eShop.BusinessEntities
{
    using System;

    #region Enumerations

    public enum DisplaySize : int
    {
        OneColumn = 1,
        TwoColumns = 2,
        ThreeColumns = 3,
        FourColumns = 4
    }

    public enum DisplayType : int
    {
        Image = 1,
        Flash = 2,
        Silverlight = 3
    }

    #endregion Enumerations
}