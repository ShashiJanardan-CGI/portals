﻿namespace Posten.Portal.eShop.BusinessEntities
{
    using System;

    public class BannerItemModel
    {
        #region Properties

        public string ActionUrl
        {
            get; set;
        }

        public string AdUrl
        {
            get; set;
        }

        public DisplaySize DisplaySize
        {
            get; set;
        }

        public DisplayType DisplayType
        {
            get; set;
        }

        #endregion Properties
    }
}