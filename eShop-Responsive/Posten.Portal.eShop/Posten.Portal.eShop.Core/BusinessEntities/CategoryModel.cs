﻿namespace Posten.Portal.eShop.BusinessEntities
{
    using System;

    public class CategoryModel
    {
        #region Properties

        public string Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        #endregion Properties
    }
}