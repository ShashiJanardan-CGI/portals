using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Posten.Portal.Platform.Common.Utilities;
using Posten.Portal.eShop;

namespace Posten.Portal.eShop.EventReceivers
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("98853489-630d-4977-bff3-563f6540307f")]
    public class WebConfigEventReceiver : WebConfigEngine
    {
        /// <summary>
        /// Gets the owner modif.
        /// </summary>
        /// <value>A owner modif.</value>
        protected override string OwnerModif
        {
            get { return "Posten.Portal.eShop"; }
        }

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            // Unity mapping for the Category Service
            AddUnityMapping(typeof(Services.ICatalogService), typeof(Services.CatalogServiceAgent), typeof(Services.CatalogServiceStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.CatalogService.ICatalogService), "http://localhost:8777/CatalogService.svc");

            // Unity mapping for the Banner Service
            AddUnityMapping(typeof(Services.IBannerService), typeof(Services.BannerServiceAgent), typeof(Services.BannerServiceStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.BannerService.IBannerService), "http://localhost:8777/BannerService.svc");

            // Register site map provider
            AddSiteMapProvider(typeof(Utilities.EshopSiteMapProvider), Microsoft.SharePoint.Publishing.Navigation.PortalNavigationType.Current, false);

            AddExpressionBuilder("eShopCode", typeof(Presentation.eShopCodeExpressionBuilder));

            // Apply changes
            ApplyChangesInWebConfig(properties);
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            // Roll-back web.config changes
             RemoveChangesInWebConfig(properties);
        }

        public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        {
        }
    }
}
