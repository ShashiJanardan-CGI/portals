﻿using System;
using System.Collections.Generic;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Services.Proxy.CatalogService;
using Posten.Portal.eShop.Translators;
using Posten.Portal.eShop.Utilities;
using Posten.Portal.Platform.Common.Services;
using System.Security.Principal;
using System.Web;
using Microsoft.SharePoint;

namespace Posten.Portal.eShop.Services
{
    public class CatalogServiceAgent : ServiceAgentBase, ICatalogService
    {
        private CatalogServiceClient serviceClientInstance;

        private CatalogServiceClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<CatalogServiceClient, Proxy.CatalogService.ICatalogService>() as CatalogServiceClient;
                }

                return this.serviceClientInstance;
            }
        }

        public ICollection<CategoryModel> GetCategory(string categoryId)
        {
            // Call web-service
            CategoryItem[] response = new CategoryItem[] { };

            var currentUser = ImpersonateUser();
            var currentculture = Utils.SetToSharepointCulture(SPContext.Current.RegionalSettings.LocaleId);            
            response = this.ServiceClient.GetCategory(categoryId);
            RevertImpersonation(currentUser);
            Utils.RevertToCurrentCulture(currentculture);
            // TODO: do we need to rebuild a node tree for the category?

            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }

        public ICollection<CategoryModel> GetCategoryList()
        {
            CategoryItem[] response = new CategoryItem[] { };
            // Call web-service
            var currentUser = ImpersonateUser();
            var currentculture = Utils.SetToSharepointCulture(SPContext.Current.RegionalSettings.LocaleId);
            response = this.ServiceClient.GetCategoryList();
            RevertImpersonation(currentUser);
            Utils.RevertToCurrentCulture(currentculture);
            // TODO: do we need to rebuild a node tree for the category?

            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }

        public ICollection<ProductModel> GetProductList()
        {
            // Call web-service
            ProductItem[] response = new ProductItem[] { };
            var currentUser = ImpersonateUser();
            var currentculture = Utils.SetToSharepointCulture(SPContext.Current.RegionalSettings.LocaleId);
            response = this.ServiceClient.GetProductList();
            RevertImpersonation(currentUser);
            Utils.RevertToCurrentCulture(currentculture);
            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }

        public ProductModel GetProduct(string productId)
        {
            // Call web-service
            ProductItem response = new ProductItem();
            var currentUser = ImpersonateUser();
            var currentculture = Utils.SetToSharepointCulture(SPContext.Current.RegionalSettings.LocaleId);
            response = this.ServiceClient.GetProduct(productId);
            RevertImpersonation(currentUser);
            Utils.RevertToCurrentCulture(currentculture);
            // Translate to a list with extention method
            return response.ToBusinessEntity();
        }


        private static IPrincipal ImpersonateUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated) return HttpContext.Current.User;
            var tempUser = HttpContext.Current.User;
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity("guest"), null);
            return tempUser;
        }

        private static void RevertImpersonation(IPrincipal user)
        {
            if (!HttpContext.Current.User.Equals(user)) HttpContext.Current.User = user;
        }

    }
}
