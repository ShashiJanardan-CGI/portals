﻿using System;
using System.Collections.Generic;
using Posten.Portal.eShop.BusinessEntities;

namespace Posten.Portal.eShop.Services
{
    public interface ICatalogService
    {
        ICollection<CategoryModel> GetCategory(string categoryId);

        ICollection<CategoryModel> GetCategoryList();

        ProductModel GetProduct(string productId);

        ICollection<ProductModel> GetProductList();
    }
}
