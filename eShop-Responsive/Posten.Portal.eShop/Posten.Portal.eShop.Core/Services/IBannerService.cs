﻿using System;
using System.Collections.Generic;
using Posten.Portal.eShop.BusinessEntities;

namespace Posten.Portal.eShop.Services
{
    public interface IBannerService
    {
        //IBannerItem GetBannersByColumnSize(DisplaySize displaySize);
        ICollection<BannerItemModel> GetBannersByColumnSize(DisplaySize displaySize, int itemRequest);
    }
}
