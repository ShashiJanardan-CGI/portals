﻿using System;
using System.Collections.Generic;
using System.Linq;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Services.Proxy.CatalogService;

namespace Posten.Portal.eShop.Translators
{
    public static class CatalogTranslator
    {
        internal static CategoryModel ToBusinessEntity(this CategoryItem item)
        {
            return new CategoryModel
            {
                Id = item.Id,
                Name = !string.IsNullOrEmpty(item.CategoryName) ? item.CategoryName : item.Id
            };
        }

        internal static ProductModel ToBusinessEntity(this ProductItem item)
        {
            return new ProductModel
            {
                ApplicationId = item.ApplicationId,
                Colours = item.Colours,
                DefinitionName = item.DefinitionName,
                EditImageURL = item.EditImageURL,
                EditURL = item.EditURL,
                ESPNumber = item.ESPNumber,
                FreeText1 = item.FreeText1,
                FreeText2 = item.FreeText2,
                IsSellable = item.IsSellable,
                ItemsInPackage = item.ItemsInPackage,
                LongArticleName = item.LongArticleName,
                LongDescription = item.LongDescription,
                ParentIds = item.ParentIds,
                Popularity = item.Popularity,
                PriceWithoutVAT = item.PriceWithoutVAT,
                ProductType = (ProductType)item.ProductType,
                Quantity = item.Quantity,
                QuantityValues = item.QuantityValues,
                RelatedProducts = item.RelatedProducts.ToBusinessEntityCollection(),
                ReleaseYear = item.ReleaseYear,
                SAPId = item.SAPId,
                SellStandalone = item.SellStandalone,
                ServiceImageURL = item.ServiceImageURL,
                ShortArticleName = item.ShortArticleName,
                ShortDescription = item.ShortDescription,
                StampPicture = item.StampPicture,
                StockStatus = item.StockStatus,
                Weight = item.Weight,

                Id = item.Id,
                Name = !string.IsNullOrEmpty(item.LongArticleName) ? item.LongArticleName : item.ShortArticleName,
                Price = item.PriceWithoutVAT,
                Vat = item.PriceWithoutVAT * .25m, // stub data
                Total = item.PriceWithoutVAT * 1.25m, // stub data
                CurrencyCode = "SEK", // stub data
                VatPercentage = .25m, // stub data
                Currency = "kr", // stub data
                Value = item.Value,
                ProductPackages = item.ProductPackages.ToBusinessEntityCollection(),
                IsAvailable = true, // stub data
                ImagesUri = Services.CatalogServiceStub.GetImagesUri(item.Id), // stub data
                Details = item.ShortDescription,
                DeliveryDetails = string.Empty, // stub data
                Description = item.LongDescription,
                Facts = item.ShortDescription
            };
        }




        internal static RelatedProductModel ToBusinessEntity(this RelatedProductItem item)
        {
            return new RelatedProductModel
            {
                RelationshipName = (RelationshipName)item.RelationshipName,
                TargetId = item.TargetId,
                TargetItemType = (CatalogItemType)item.TargetItemType
            };
        }

        internal static ICollection<RelatedProductModel> ToBusinessEntityCollection(this IEnumerable<RelatedProductItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }


        internal static ICollection<ProductPackageModel> ToBusinessEntityCollection(this IEnumerable<ProductPackageItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ProductPackageModel ToBusinessEntity(this ProductPackageItem item)
        {
            return new ProductPackageModel
            {
                Id = item.Id,
                ProductPackageInterval = item.ProductPackageInterval.ToBusinessEntity(),
                Name = item.Name,
                Quantity = item.Quantity
            };
        }

        internal static ProductPackageIntervalModel ToBusinessEntity(this ProductPackageIntervalItem item)
        {
            return new ProductPackageIntervalModel
            {
                Id = item.Id,
                MaxQuantity = item.MaxQuantity,
                MinQuantity = item.MinQuantity,
                Name = item.Name
            };
        }

        internal static ICollection<CategoryModel> ToBusinessEntityCollection(this IEnumerable<CategoryItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICollection<ProductModel> ToBusinessEntityCollection(this IEnumerable<ProductItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }
    }
}
