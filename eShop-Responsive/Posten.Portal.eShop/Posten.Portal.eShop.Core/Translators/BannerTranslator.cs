﻿using System;
using System.Collections.Generic;
using System.Linq;
using Posten.Portal.eShop.BusinessEntities;


namespace Posten.Portal.eShop.Translators
{
    public static class BannerTranslator
    {
        internal static BannerItemModel ToBusinessEntity(this Services.Proxy.BannerService.BannerItem item)
        {
            return new BannerItemModel
            {
                ActionUrl = item.ActionUrl,
                AdUrl = item.AdUrl,
                DisplaySize = (DisplaySize)item.Size,
                DisplayType = (DisplayType)item.Type,
                
            };
        }

        internal static ICollection<BannerItemModel> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.BannerService.BannerItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }
    }
}
