﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class UpdateProductOrderResult
    {
        public string OrderNumber { get; set; }

        public string Total { get; set; }

        public string TotalString { get; set; }
        
        public string TotalTaxString { get; set; }

        public string TotalExcludingTaxString { get; set; }
    }
}
