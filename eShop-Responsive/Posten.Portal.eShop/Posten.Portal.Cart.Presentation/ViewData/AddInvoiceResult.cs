﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class AddInvoiceResult
    {
        public string InvoiceFee { get; set; }
        public string BasketTotal { get; set; }
        public string BasketTotalExcludingTax { get; set; }
        public string VatTotal { get; set; }
    }
}
