﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class BasketDataResult
    {
        public string TotalString { get; set; }

        public string TotalArticles { get; set; }

        public string TotalTaxString { get; set; }

        public string TotalExcludingTaxString { get; set; }

        public List<ProductItems> Products { get; set; }

        public List<ServiceItems> Services { get; set; }
    }

    public class ProductItems 
    {
        public string ArticleName { get; set; }

        public string ImageUrl { get; set; }

        public string OrderNumber { get; set; }

        public int Quantity { get; set; }

        public string Total { get; set; }
    }

    public class ServiceItems
    {
        public string ImageUrl { get; set; }

        public string ServiceName { get; set; }

        public string OrderNumber { get; set; }

        public string FreeTextField1 { get; set; }

        public string FreeTextField2 { get; set; }

        public string Total { get; set; }
    }
}
