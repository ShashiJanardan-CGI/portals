﻿using System;
using System.Linq;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;
using System.Collections;
using Posten.Portal.Cart;
using Posten.Portal.Cart.BusinessEntities;
using System.Text;
using System.Web.Script.Serialization;
using Posten.Portal.Cart.Utilities;



namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class CartOperation : LayoutsPageBase
    {
        public string CurrentBasketId
        {
            get
            {
                return BasketContext.Current.BasketItem.BasketId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string id = Request.QueryString["id"];
                string operation = Request.QueryString["operation"];
                string quantity = Request.QueryString["quantity"];

                var json = this.Operate(id, operation, quantity);
                Response.Write(json);
                Response.End();
                return;
            }
        }

        private string Operate(string id, string operation, string quantity)
        {
            string result = string.Empty;

            //var _basket = IoC.Resolve<IBasketService>();
            //var _basketItem = _basket.GetBaskets();
            //string basketId = _basketItem.FirstOrDefault().BasketId;

            if (operation.ToLower() == "removeproduct" && !string.IsNullOrEmpty(id)) result = RemoveProduct(id, this.CurrentBasketId);
            else if (operation.ToLower() == "removeservice" && !string.IsNullOrEmpty(id)) result = RemoveService(id, this.CurrentBasketId);
            else if (operation.ToLower() == "updateproduct" && !string.IsNullOrEmpty(quantity)) result = UpdateProduct(id, quantity, this.CurrentBasketId);
            else if (operation.ToLower() == "getminicart") result = getMiniCartHTML();
            else if (operation.ToLower() == "getcarttotal") result = getCartTotal();
            else if (operation.ToLower() == "getbask") result = MapToSerializer(this.CurrentBasketId);
            else if (operation.ToLower() == "getcartsummary") result = getCartSummary();


            return result;
        }

        private string MapToSerializer(string currentbasketId)
        {
            try
            {
                ////IBasketService basket = IoC.Resolve<IBasketService>();
                ////var item = basket.GetBasketItem(currentbasketId);
                var item = BasketContext.Current.BasketItem;

                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                string jsonBasket = oSerializer.Serialize(item);

                return jsonBasket;
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }

        private string UpdateProduct(string productOrderNum, string quantity, string currentbasketId)
        {
            try
            {
                ////IProductBasketService prodBasket = IoC.Resolve<IProductBasketService>();
                ////IProductOrderItem initialproditem = prodBasket.GetProductOrderItem(currentbasketId, productOrderNum);
                IProductOrderItem initialproditem = BasketContext.Current.GetProductOrder(productOrderNum);

                initialproditem.Quantity = Convert.ToInt16(quantity);
                initialproditem.Vat = initialproditem.Total*initialproditem.VatPercentage/100m;

                ////string message = prodBasket.UpdateProductOrder(currentbasketId, initialproditem);
                string message = BasketContext.Current.UpdateProductOrder(initialproditem.OrderNumber, initialproditem);

                if (!string.IsNullOrEmpty(message))
                {
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    initialproditem.OrderNumber = message;
                    string jsonProduct = oSerializer.Serialize(initialproditem);

                    return jsonProduct;
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
                return ex.ToString();
            }
        }

        private string getCartSummary()
        {
            string result = "<span id=\"cartsummary\">0 artiklar | 0</span>";
            try
            {
                ////IBasketService basketService = IoC.Resolve<IBasketService>();
                ////IBasketItem basketItem = basketService.GetBasketItem(this.CurrentBasketId);
                IBasketItem basketItem = BasketContext.Current.BasketItem;
                result = string.Format("<span id=\"cartsummary\">{0} artiklar | {1}</span>", basketItem.OrderSummaryItems.Count.ToString(), basketItem.TotalString);
            }
            catch (Exception)
            {
                //TODO: Add logging mechanism here.
            }
            return result;
        }

        private string getMiniCartHTML()
        {
            string result = "<div id='loadminicart' style='text-align:center'>Cart is empty.</div>";
            try
            {
                StringBuilder html = new StringBuilder();

                ////IBasketService basketService = IoC.Resolve<IBasketService>();
                ////IBasketItem basketItem = basketService.GetBasketItem(this.CurrentBasketId);

                ////IProductBasketService prodBasket = IoC.Resolve<IProductBasketService>();
                ////IServiceBasketService servBasket = IoC.Resolve<IServiceBasketService>();

                //All Items in the basket
                ////ICollection<IOrderSummaryItem> orderSummaryItems = basketItem.OrderSummaryItems;


                //Get Product Items
                ////ICollection<IProductOrderItem> productItems = prodBasket.GetProductOrders(this.CurrentBasketId);
                ICollection<IProductOrderItem> productItems = BasketContext.Current.ProductOrderItems;
                foreach (IProductOrderItem productItem in productItems)
                {
                    IOrderSummaryItem item = getBasketItem(productItem.OrderNumber);
                    html.Append("<li>");
                    html.Append("<div class=\"article\"><img alt=\"C5 kuvert\" src=\"{0}\"></div>");
                    html.AppendFormat("<div class=\"description\">{0}</span></div>", productItem.ArticleName);
                    html.AppendFormat("<div class=\"quantity\"><input type=\"text\" onchange=\"updateitem('{0}',this.value)\" value=\"{1}\" name=\"\"></div>", productItem.OrderNumber, productItem.Quantity);
                    html.AppendFormat("<div class=\"price\"><span class=\"theme\">{0}</span></div>", item.LinePriceString);
                    html.AppendFormat("<div class=\"delete\"><a href=\"#\" onclick=\"removeitem('{0}','removeproduct');\"></a></div>", productItem.OrderNumber);
                    html.Append("</li>");
                }
                //Get Service Items
                ////ICollection<IServiceOrderItem> serviceItems = servBasket.GetServiceOrders(this.CurrentBasketId);
                ICollection<IServiceOrderItem> serviceItems = BasketContext.Current.ServiceOrderItems;
                foreach (IServiceOrderItem serviceItem in serviceItems)
                {
                    IOrderSummaryItem item = getBasketItem(serviceItem.OrderNumber);
                    html.Append("<li>");
                    html.Append("<div class=\"article\"><img alt=\"C5 kuvert\" src=\"{0}\"></div>");
                    html.AppendFormat("<div class=\"description\">{0}</span></div>", serviceItem.Name);
                    html.Append("<div class=\"quantity\">1</div>");
                    html.AppendFormat("<div class=\"price\"><span class=\"theme\">{0}</span></div>", item.LinePriceString);
                    html.AppendFormat("<div class=\"delete\"><a href=\"#\" onclick=\"removeitem('{0}','removeservice');\"></a></div>", serviceItem.OrderNumber);
                    html.Append("</li>");
                }
                result = html.ToString();
            }
            catch (Exception)
            {
                //TODO: Add logging mechanism here.
            }
            return result;

        }

        private IOrderSummaryItem getBasketItem(string orderNo)
        {
            ////IBasketService basketService = IoC.Resolve<IBasketService>();
            ////IBasketItem basketItem = basketService.GetBasketItem(this.CurrentBasketId);
            ////ICollection<IOrderSummaryItem> orderSummaryItems = basketItem.OrderSummaryItems;
            ////IOrderSummaryItem item = orderSummaryItems.Where(o => o.OrderNumber == orderNo).FirstOrDefault();
            IOrderSummaryItem item = BasketContext.Current.BasketItem.OrderSummaryItems.Where(o => o.OrderNumber == orderNo).FirstOrDefault();
            return item;
        }

        private string getCartTotal()
        {
            string result = "0";
            try
            {
                ////IBasketService basketService = IoC.Resolve<IBasketService>();
                ////IBasketItem basketItem = basketService.GetBasketItem(this.CurrentBasketId);
                ////result = basketItem.TotalString;
                result = BasketContext.Current.BasketItem.TotalString;
            }
            catch (Exception)
            {

            }
            return result;
        }

        private string RemoveService(string soNum, string currentbasketId)
        {
            try
            {
                ////IServiceBasketService servBasket = IoC.Resolve<IServiceBasketService>();
                ////bool result = servBasket.RemoveServiceOrder(currentbasketId, soNum);
                bool result = BasketContext.Current.RemoveServiceOrder(soNum);
                if (result)
                {
                    return MapToSerializer(currentbasketId);

                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        private string RemoveProduct(string productOrderNumber, string currentbasketId)
        {
            try
            {
                ////IProductBasketService prodBasket = IoC.Resolve<IProductBasketService>();
                ////bool result = prodBasket.RemoveProductOrder(currentbasketId, productOrderNumber);
                bool result = BasketContext.Current.RemoveProductOrder(productOrderNumber);
                if (result)
                {
                    return MapToSerializer(currentbasketId);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
                return ex.ToString();
            }
        }
    }
}
