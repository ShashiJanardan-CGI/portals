﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartConfigurationPage.aspx.cs"
    Inherits="Posten.Portal.Cart.Presentation.Layouts.Posten.Portal.Cart.Presentation.CartConfigurationPage"
    DynamicMasterPageFile="~masterurl/default.master" %>

<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
    Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style type="text/css">
        .header-title
        {
            float: left;
            width: 100%;
            margin: 5px 5px 10px 10px;
            font-size: 16px;
            font-weight: bold;
        }
        .cart-configuration
        {
            margin: 10px;
        }
        .cart-configuration .payment-type, .cart-configuration .invoice-option
        {
            float: left;
            width: 100%;
            margin: 10px 0;
        }
        .cart-configuration .payment-type .label, .cart-configuration .invoice-option .label
        {
            float: left;
            width: 170px;
            font-weight: bold;
        }
        .cart-configuration .payment-type .text-box, .cart-configuration .invoice-option .text-box
        {
            float: left;
            width: 130px;
        }
        
        .cart-configuration .invoice-option .text-box-label
        {
            float: left;
            width: 50px;
            margin:0 0 0  20px;
        }
        
        .cart-configuration .payment-type .text-box select, .cart-configuration .invoice-option .text-box select
        {
            width: 130px;
        }
        .cart-configuration .payment-type .payment-description, .cart-configuration .invoice-option .invoice-description
        {
            width: 100%;
            float: left;
            font-style: italic;
        }
        .cart-configuration .save-type, .cart-configuration .save-message
        {
            float: left;
            width: 100%;
            margin-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div class="cart-configuration">
        <div class="payment-type">
            <div class="label">
                <asp:Label runat="server" ID="DefaultPaymentLabel" Text="Default Payment Type:" /></div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="DefaultPaymentList">
                    <asp:ListItem Text="Card" Value="CreditCard"></asp:ListItem>
                    <asp:ListItem Text="Invoice" Value="Invoice"></asp:ListItem>
                    <asp:ListItem Text="Bank" Value="OnlineBanking"></asp:ListItem>
                </asp:DropDownList></div>                      
            <div class="payment-description">
                <asp:Literal ID="Literal3" runat="server" Text="Set the default selected payment type on the checkout page."></asp:Literal></div>
        </div>
        <div class="invoice-option">            
            <div class="label">
                <asp:Label ID="InvoiceOptionLabel" runat="server" Text="Login required for invoice option:"/></div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="InvoiceOptionList">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                </asp:DropDownList></div>
            <div class="invoice-description">
                <asp:Literal ID="Literal1" runat="server" Text="Enable/Disable Invoice Option for anonymous users." /></div>
        </div>
        <div class="payment-type">
            <div class="label">Private Max Invoice Amount:</div>
            <div class="text-box"><asp:TextBox ID="PrivMaxAmountTextBox" runat="server" Width="50px" Text="0"></asp:TextBox></div>
            <div class="invoice-description"></div>
        </div>
        <div class="payment-type">
            <div class="label">Company Max Invoice Amount:</div>
            <div class="text-box"><asp:TextBox ID="CompMaxAmountTextBox" runat="server"  Width="50px" Text="0"></asp:TextBox></div>
            <div class="invoice-description"></div>
        </div>
        <div class="invoice-option">            
            <div class="label">Enable Credit Card Payment:</div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="EnableCreditCardOption">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                </asp:DropDownList></div>
            <div class="invoice-description">Enable/Disable Credit Card Payment Method</div>
        </div>
        <div class="invoice-option">            
            <div class="label">Enable Invoice Payment:</div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="EnableInvoiceOption">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                </asp:DropDownList></div>
            <div class="invoice-description">Enable/Disable Invoice Payment Method</div>
        </div>
        <div class="invoice-option">            
            <div class="label">Enable Internet Banking Payment:</div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="EnableIBankingOption">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                </asp:DropDownList></div>
            <div class="invoice-description">Enable/Disable Internet Banking Payment Method</div>
        </div>
        
        <div class="payment-type">
            <div class="label">
                Number of services to display on checkout:</div>
            <div class="text-box">
                <asp:TextBox ID="txtMaxServicesToDisplay" runat="server"></asp:TextBox>
            <div class="payment-description">
                The maximum number of services to display on the checkout page. if there are more services than the maximum a scrollbar will appear.
            </div>
        </div>

        <div class="save-type"><PostenWebControls:PostenButton ID="SaveDefaultPayment" OnClick="Save_Settings" runat="server" Text="Save" /></div>  
        <div class="save-message"><asp:Literal ID="SaveMessageText" runat="server" Text="Updates have been saved" Visible="false" /></div>


        <div class="invoice-option">            
            <div class="label">Add Blocked Service:</div>
            <div class="text-box-label">ID:</div>
            <div class="text-box"><asp:TextBox ID="IDTextBox" runat="server"></asp:TextBox></div>
            <div class="text-box-label">Name:</div>
            <div class="text-box"><asp:TextBox ID="ServiceNameTextBox" runat="server"></asp:TextBox></div>
            <div class="text-box-label"><asp:Button ID="AddButton" runat="server" Text="Add" OnClick="AddService" /></div>
            <div class="invoice-description">Add Block Services for Invoice Payment</div>
            
        </div>

        
         
        
        <div class="invoice-option">
            <div class="label">Block Services:</div>
            <div class="text-box">
                <asp:GridView ID="BlockedServicesGridView" runat="server" AutoGenerateColumns="false" OnRowCommand="GridView1_RowCommand" 
       OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="EmptyHandler">
                    <Columns>
                        <asp:BoundField DataField="ApplicationId" HeaderText="ID" />
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" 
                                                CommandArgument='<%# Eval("ApplicationId") %>' 
                                                CommandName="Delete" runat="server">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="invoice-description"></div>
        </div>   
        
        <div class="invoice-option">            
            <div class="label">Add Blocked SSN:</div>
            <div class="text-box-label">SSN:</div>
            <div class="text-box"><asp:TextBox ID="txtSSN" runat="server"></asp:TextBox></div>
            <div class="text-box-label"><asp:Button ID="btnAddSSN" runat="server" Text="Add SSN/Company No"  OnClick="btnAddSSN_Click"/></div>
            <div class="invoice-description">Add Blocked SSN(for private) or Company Nos so they will not be able to use Invoice  as a Payment alternative</div>
            
        </div>   
        
        <div class="invoice-option">
            <div class="label">Block SSNs:</div>
            <div class="text-box">
                <asp:GridView ID="gvBlockedSSN" runat="server" AutoGenerateColumns="false" OnRowCommand="gvBlockedSSN_RowCommand" OnRowDataBound="gvBlockedSSN_RowDataBound"
                OnRowDeleting="EmptyHandler">
                    <Columns>
                         <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <%# Container.DataItem %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDeleteSSN" 
                                                CommandArgument='<%# Container.DataItem %>' 
                                                CommandName="Delete" runat="server">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="invoice-description"></div>
        </div>       


        
</div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Cart Configuration Page
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Cart Configuration Page
</asp:Content>
