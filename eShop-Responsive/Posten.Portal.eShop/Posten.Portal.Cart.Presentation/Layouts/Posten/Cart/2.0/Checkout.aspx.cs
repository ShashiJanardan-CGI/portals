﻿using Microsoft.SharePoint.WebControls;
using System;
using System.Web;
using Posten.Portal.Cart.Utilities;

namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class CheckoutPage : UnsecuredLayoutsPageBase
    {
        protected string PaymentErrorTitle
        {
            get
            {
                return Utils.GetResourceString("Payment_Cannot_Be_Processed_Error_Title");
            }
        }


        protected string Close_Button_Name
        {
            get
            {
                return Utils.GetResourceString("Close_PopUp_Button_Name");
            }
        }

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //set the continue shopping link to the last url visited. this is stored in a cookie.
            
            if (Request.Cookies["eshopUrl"] != null)
                lnkContinueShop.NavigateUrl = Request.Cookies["eshopUrl"].Value ;
            else
                lnkContinueShop.NavigateUrl = "javascript:history.go(-1)";


        }
       
    }
}
