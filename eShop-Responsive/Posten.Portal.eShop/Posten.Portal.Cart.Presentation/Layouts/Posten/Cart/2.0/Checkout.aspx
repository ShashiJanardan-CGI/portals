﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
    Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register TagPrefix="PostenCartControls" TagName="NewCheckout" Src="~/_controltemplates/Posten/Cart/2.0/CheckOut.ascx" %>


<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.CheckoutPage"
    EnableSessionState="True" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ContentPlaceHolderID="PlaceHolderMiniBasket" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMainMenu" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderLeftNavBar" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link href="/_layouts/Posten/Cart/Presentation/styles/Cart.css" type="text/css" rel="stylesheet" />
    <script src="/_layouts/Posten/Cart/Presentation/js/common/utils.js" type="text/javascript"></script>
    <script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var message = '<%= Request.QueryString["Message"] %>';
            var title = '<%= PaymentErrorTitle %>';
            if (message != "") {
                var button_close_name = '<%= Close_Button_Name %>'; 
                showPopUp(title, message, "popup", false, 800, button_close_name);
            }
            
        });
    </script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-24276704-1']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <asp:Literal runat="server" Text="<% $Resources: PostenCart, CheckOut_Page_Title %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <asp:Literal runat="server" Text="<% $Resources: PostenCart, CheckOut_Page_Title %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div class="checkOut-title">
        <asp:Literal runat="server" Text="<% $Resources: PostenCart, CheckOut_Page_Title %>" /></div>
    <div class="checkOut-subtitle">
        <asp:HyperLink ID="lnkContinueShop" CssClass="continue-shop" runat="server">
            <asp:Literal ID="Literal1" runat="server" Text="<% $Resources: PostenCart, Continue_Shopping_Text %>" /></asp:HyperLink>
        <div class="help">
            <strong>
                <asp:Literal runat="server" Text="<% $Resources: PostenCart, Need_Help_Label %>" /></strong>&nbsp;<asp:Literal
                    runat="server" Text="<% $Resources: PostenCart, Customer_Service_Label %>" /><strong>&nbsp;
                        <asp:Literal runat="server" Text="<% $Resources: PostenCart, Customer_Service_Number_Text %>" /></strong>
        </div>
    </div>
    <div class="middle-zone">
        <PostenCartControls:NewCheckout ID="ucCheckOut" runat="server" MinValue="1" MaxValue="10" />
    </div>
</asp:Content>
