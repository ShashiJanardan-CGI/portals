﻿using System;
using System.Web ;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Posten.Portal.Cart.Services;
using Posten.Portal.Platform.Common.Container;
using Posten.Portal.Cart.Utilities;
using System.Collections.Generic;
using Posten.Portal.Cart.BusinessEntities;
using System.Collections;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Security.Principal;


namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class MiniCartPage : UnsecuredLayoutsPageBase
    {

      

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Impersonation impersonation = new Impersonation();
                //impersonation.ImpersonateValidUser("install", "int", "p@ssword1");
                //SPSecurity.RunWithElevatedPrivileges(delegate()
                //{

               
               showMiniBasket();
               


                //});


                //impersonation.UndoImpersonation();
            }
        }

        








        private void showMiniBasket()
        {
            string result = string.Empty;

            try
            {
                result += string.Format("Basket Id Created:{0}<br/>", BasketContext.Current.BasketId);

                IProductOrderItem productOrder1 = new ProductOrderItem
                {
                    ArticleName = "Välgörenhetsfrimärke",
                    ArticlePrice = 70m,
                    ArticleId = "123-ABC",
                    Vat = 17.5m,
                    Quantity = 1,
                    CurrencyCode = "SEK",
                    VatPercentage = .25m
                };

                BasketContext.Current.AddProductOrder(productOrder1);

                result += string.Format("Välgörenhetsfrimärke Added to Basket:{0}<br/>", BasketContext.Current.BasketId);

                IServiceOrderLineItem serviceOrderLineItem1 = new ServiceOrderLineItem
                {
                    OrderLineNumber = 1,
                    ArticleId = "1111",
                    ArticleName = "1:a klassbrev",
                    ArticlePrice = 20.5m,
                    Quantity = 2,
                    Total = 41m,
                    VatPercentage = 0.25m,
                    Vat = 10.25m,
                    CompanyCode = "FO91",
                    CustomerId = "??",
                    TaxCountry = "SE",
                    FootnoteText = "1"
                };

                IServiceOrderLineItem serviceOrderLineItem2 = new ServiceOrderLineItem
                {
                    OrderLineNumber = 2,
                    ArticleId = "2222",
                    ArticleName = "Rek inrikes",
                    ArticlePrice = 10,
                    Quantity = 1,
                    Total = 10m,
                    VatPercentage = 0.25m,
                    Vat = 2.5m,
                    CompanyCode = "FO91",
                    CustomerId = "??",
                    TaxCountry = "SE",
                    FootnoteText = "1"
                };

                IServiceOrderLineItem[] serviceOrderLineItemList1 = { serviceOrderLineItem1, serviceOrderLineItem2 };

                IServiceOrderItem serviceOrder1 = new ServiceOrderItem
                {
                    ApplicationId = "Reklam",
                    ApplicationName = "E-brev web",
                    ServiceOrderLines = serviceOrderLineItemList1,
                    Name = "Rekommenderat utrikes",
                    FreeTextField1 = "Mottagare: Andersson Sven ",
                    FreeTextField2 = "Personlig utlämning, postförskott",
                    Details = string.Empty,
                    SubTotal = 51m,
                    Vat = 12.75m,
                    CurrencyCode = "SEK",
                    EditUrl = "/_layouts/Posten/Kundkorg/1/TestPages/EditPage.aspx?OrderNo={0}&url={1}",
                    ServiceImageUrl = null,
                    ProduceUrl = new Uri((ConfigurationManager.AppSettings["ProduceServiceUrl"] == null) ? string.Empty : ConfigurationManager.AppSettings["ProduceServiceUrl"].ToString())
                };

                BasketContext.Current.AddServiceOrder(serviceOrder1);

                result += string.Format("Rekommenderat Service utrikes Added to Basket:{0}<br/>", BasketContext.Current.BasketId);
            }
            catch (Exception ex)
            {
                result += string.Format("Error Encountered:{0}<br/>", ex.ToString());
            }

            ltrlCart.Text = result;
        }
       
    }
}
