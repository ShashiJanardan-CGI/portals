﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint;
using Posten.Portal.Cart.Services;
using Posten.Portal.Platform.Common.Container;
using Posten.Portal.Cart.BusinessEntities;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Posten.Portal.Cart.Presentation.Layouts.Posten.Portal.Cart.Presentation
{
    public partial class CartConfigurationPage : UnsecuredLayoutsPageBase
    {

        protected void Save_Settings(object sender, EventArgs e)
        {
            ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
            var item = new CartSettingItems();
            decimal pmaxamount = 0;
            decimal cmaxamount = 0;
            int servicemaxnumber = 0;


            item.defaultpayment = this.DefaultPaymentList.SelectedValue;
            item.invoiceoption = this.InvoiceOptionList.SelectedValue;
            
            decimal.TryParse(PrivMaxAmountTextBox.Text, out pmaxamount);
            item.PrivateInvoiceMaxAmount = pmaxamount;

            decimal.TryParse(CompMaxAmountTextBox.Text, out cmaxamount);
            item.CompanyInvoiceMaxAmount = cmaxamount;

            Int32.TryParse(txtMaxServicesToDisplay.Text, out servicemaxnumber);
            item.DisplayableCartItems = servicemaxnumber;

            item.EnableCreditCardPayment = EnableCreditCardOption.SelectedValue.ToLower().Equals("yes") ? true : false;
            item.EnableInvoicePayment = EnableInvoiceOption.SelectedValue.ToLower().Equals("yes") ? true : false;
            item.EnableInternetBankingPayment = EnableIBankingOption.SelectedValue.ToLower().Equals("yes") ? true : false;

                        //save default payment type
            var result = service.SaveItems(item);

            if (result)
            { this.SaveMessageText.Visible = true; }
        }


        protected void AddService(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(IDTextBox.Text)) || (!string.IsNullOrEmpty(ServiceNameTextBox.Text)))
            {
                ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
                service.SaveBlockService(IDTextBox.Text, ServiceNameTextBox.Text);
                loadDefaultValues();
                IDTextBox.Text = string.Empty;
                ServiceNameTextBox.Text = string.Empty;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton l = (LinkButton)e.Row.FindControl("LinkButton1");
                l.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "ApplicationId") + "')");
            }
        }

        protected void GridView1_RowCommand(object sender,GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                string ApplicationID = Convert.ToString(e.CommandArgument);
                ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
                service.RemoveBlockService(ApplicationID);
                loadDefaultValues();

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
                loadDefaultValues();
        }


        private void loadDefaultValues()
        {
            ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
            var listItem = service.GetItems();

            this.DefaultPaymentList.SelectedValue = listItem.defaultpayment;
            this.InvoiceOptionList.SelectedValue = listItem.invoiceoption;

            this.CompMaxAmountTextBox.Text = listItem.CompanyInvoiceMaxAmount.ToString();
            this.PrivMaxAmountTextBox.Text = listItem.PrivateInvoiceMaxAmount.ToString();

            EnableCreditCardOption.SelectedValue = listItem.EnableCreditCardPayment ? "Yes" : "No";
            EnableIBankingOption.SelectedValue = listItem.EnableInternetBankingPayment ? "Yes" : "No";
            EnableInvoiceOption.SelectedValue = listItem.EnableInvoicePayment ? "Yes" : "No";


            BlockedServicesGridView.DataSource = listItem.BlockServices;
            BlockedServicesGridView.DataBind();

            gvBlockedSSN.DataSource = listItem.BlockSSNs;
            gvBlockedSSN.DataBind();

            txtMaxServicesToDisplay.Text = listItem.DisplayableCartItems.ToString();
        }

        protected void gvBlockedSSN_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                string ssn = Convert.ToString(e.CommandArgument);
                ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
                service.RemoveBlockedSSn(ssn);
                loadDefaultValues();

            }
        }

        protected void gvBlockedSSN_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton l = (LinkButton)e.Row.FindControl("lnkDeleteSSN");
                l.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this record " + e.Row.DataItem.ToString() + "')");
            }
        }

        protected void EmptyHandler(object sender, EventArgs e)
        {
        }

        protected void btnAddSSN_Click(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(txtSSN.Text)))
            {
                ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
                service.AddBlockedSSn(txtSSN.Text);
                loadDefaultValues();
                txtSSN.Text = "";
            }
        }
    }
}
