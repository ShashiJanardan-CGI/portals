﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.PaymentPage" EnableEventValidation="false" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Posten Payment System</title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
html, body { font-family: Calibri, Arial, sans-serif; font-size: 14px; text-align: center; background-color: #f5f5f5; }
h1 { font-size: 1.5em; margin: 1em 0; padding: 0; }
span.error { font-size: 1em; color: #ff0000; margin: 0 0 2em; display: inline-block; }
form { visibility: hidden; }
div.box { position:absolute; top:36%; left:50%; height:10em; padding:1em 0; margin-top:-6em; width:36em; margin-left:-18em; border:1px solid #999; background-color: #fff;
          border-radius: 7px; -moz-border-radius: 7px; -webkit-border-radius: 7px;
          box-shadow: 0 0 7px #999; -moz-box-shadow: 0 0 7px #999; -webkit-box-shadow: 0 0 7px #999;
}
</style>
<script type="text/javascript">
    function doPostBack() {
        var vs = document.getElementsByTagName('input');
        var f = document.getElementById('PaymentForm');
        if (vs != null && f != null && f.attributes["action"].value.indexOf("Payment.aspx") != 0) {
            for (var i = 0; i < vs.length; i++) {
                if (vs[i] != null && vs[i].attributes["name"].value == "__VIEWSTATE") {
                    vs[i].parentNode.removeChild(vs[i]);
                    break;
                }
            }
        }

        document.getElementById('PaymentForm').submit();
        window.setTimeout('document.getElementById("LoadingImage").src = "/_layouts/images/posten/cart/spinner.gif";', 50);
    }
function autoPostBack() { window.setTimeout('doPostBack();', postBackDelay); }
</script>
</head>
<body>
<div class="box" style="display:none;" id="allcontents">
<h1><asp:Literal runat="server" Text='<% $Resources: PostenCart, Payment_Processing_Label %>' /></h1>
<asp:Image ID="LoadingImage" runat="server" ImageUrl="/_layouts/images/posten/cart/spinner.gif" />
<asp:Label ID="ErrorMessage" runat="server" CssClass="error" />
<form ID="PaymentForm" runat="server" method="post">
<asp:HiddenField ID="PostBackCounter" runat="server" Value="0" />
<asp:Repeater ID="PaymentInputs" runat="server">
<ItemTemplate>
<input name="<%# Eval("Key") %>" value="<%# HttpUtility.HtmlAttributeEncode(Eval("Value") as string) %>" type="hidden" /></ItemTemplate>
</asp:Repeater>
<input type="submit" value="Continue" />
</form>
</div>
<script type="text/javascript">
    var regex = new RegExp(/.*Action=Finalize|Action=Complete.*/);
    if (document.location.href.match(regex)) {
        document.getElementById('allcontents').removeAttribute('style');
    }
</script>

<noscript>
<style type="text/css">
form { visibility: visible; }
</style>
</noscript>
</body>
</html>
