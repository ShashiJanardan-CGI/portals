﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

    using Posten.Portal.Cart.Services;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Container;

    public class ReceiptHandler : IHttpHandler
    {
        #region Properties

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        public void ProcessRequest(HttpContext context)
        {
            var bookingId = context.Request.QueryString["BookingId"];
            var basketService = IoC.Resolve<IBasketService>();
            var paymentService = IoC.Resolve<IPaymentService>();

            var transid = basketService.GetBasketItemByBookingId(bookingId).TransactionId;

            try
            {
                var result = paymentService.GetPaymentResult(transid, true, false);
                context.Response.AppendHeader("content-disposition", "attachment; filename=" + string.Format("receipt-{0}.pdf", bookingId));
                context.Response.ContentType = "application/pdf";

                var buffer = (byte[])Convert.FromBase64String(result.Receipt);

                context.Response.BinaryWrite(buffer);
                context.Response.End();
            }
            catch (Exception ex)
            {
                ex.RecursiveLog();
                throw;
            }
        }

        #endregion Methods
    }
}