﻿"use strict";

/*globals jQuery*/

// create Posten namespace
var Posten; if (!Posten) { Posten = {}; }

// create Posten.MiniBasket namespace
Posten.MiniBasket = (function ($) {

    // used to render the basket, overide it by calling init with a render function as the first parameter
    var render = function (basket) { },

    emptyCallback = function (basket) { },
    callback = emptyCallback,

    // updates the basket on a successfull ajax call
    ajaxSuccess = function (data) {
        render(data.d);
        callback(data.d);
        callback = emptyCallback;
    },

    // default ajax settings
    ajaxSettings = {
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        cache: false
    },

    // calls the web-service
    ajax = function (url, data, callback) {
        $.ajax($.extend({ url: '/_layouts/Posten/Cart/Common/CartService.aspx/' + url, data: data, success: callback }, ajaxSettings));
    },

    // calls the web-service and trigger an update of the basket
    call = function (url, data) {
        ajax(url, data, ajaxSuccess);
    };

    var methods = {
        init: function (func) {
            render = func;
        },
        update: function (func) {
            call('GetBasket');
            if (typeof func === 'function') {
                callback = func;
            }
        },
        updateOrderItem: function (orderNumber, quantity) {
            call('UpdateOrderItem', '{"orderNumber":"' + orderNumber + '","quantity":"' + quantity + '"}');
        },
        removeOrderItem: function (orderNumber) {
            call('RemoveOrderItem', '{"orderNumber":"' + orderNumber + '"}');
        },
        emptyBasket: function () {
            call('EmptyBasket');
        },
        getPostalCodeCity: function (country, postalCode, callback) {
            ajax('GetPostalCodeCity', '{"country":"' + country + '","postalCode":"' + postalCode + '"}', callback);
        }
    };

    // return the public methods and constants
    return {
        init: methods.init,
        update: methods.update,
        updateOrderItem: methods.updateOrderItem,
        removeOrderItem: methods.removeOrderItem,
        emptyBasket: methods.emptyBasket,
        getPostalCodeCity: methods.getPostalCodeCity,
        OrderItemType: { Service: 1, Product: 2 }
    };

})(jQuery);

