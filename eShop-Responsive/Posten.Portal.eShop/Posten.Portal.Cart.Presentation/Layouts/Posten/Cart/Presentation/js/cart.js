﻿window.checkout = function () { };

checkout.prototype._ziplength = "";
checkout.prototype._timer = "";
checkout.prototype._Online_Label = "";
checkout.prototype._Invoice_Label = "";
checkout.prototype._Internet_Label = "";
checkout.prototype._Online_Message = "";
checkout.prototype._Invoice_Message = "";
checkout.prototype._Internet_Message = "";
checkout.prototype._Email_id = "";
checkout.prototype._Validate_id = "";
checkout.prototype._Firstname_id = "";
checkout.prototype._Lastname_id = "";
checkout.prototype._Address_id = "";
checkout.prototype._Postalcode_id = "";
checkout.prototype._City_id = "";
checkout.prototype._Mobile_id = "";
checkout.prototype._Checkout_id = "";
checkout.prototype._Delivery_Firstname_id = "";
checkout.prototype._Delivery_Lastname_id = "";
checkout.prototype._Delivery_Address_id = "";
checkout.prototype._Delivery_Postalcode_id = "";
checkout.prototype._Delivery_City_id = "";
checkout.prototype._Delivery_Mobile_id = "";
checkout.prototype._Cart_Modal_Error = "";
checkout.prototype._PostalAndCity_Error_Msg = "";
checkout.prototype._Recalculate_Vat_Error_Msg = "";
checkout.prototype._Postalcode_name = "";
checkout.prototype._legaladdresses = "";
checkout.prototype._SameAsBillingAddressLabel = "";
checkout.prototype._SameAsInvoiceAddressLabel = "";
checkout.prototype._GenericErrorTitle = "";
checkout.prototype._GenericErrorMessage = "";
checkout.prototype._ModalErrorMessage = "";
checkout.prototype._InvalidSSNoErrorMessage = "";
checkout.prototype._InvalidCompanyNoErrorMessage = "";
checkout.prototype._InvalidQuantityErrorMessage = "";
checkout.prototype._BillingAddressLabel = "";
checkout.prototype._SelectLabel = "";
checkout.prototype._phone_regex_msg = "";
checkout.prototype._ValidCity = true;
checkout.prototype._personal_regex_msg = "";
checkout.prototype._mobile_unique_id = "";
checkout.prototype._delivery_unique_mobile_id = "";
checkout.prototype._accept_unique_id = "";
checkout.prototype._accept_error_msg = "";
checkout.prototype._general_error_msg = "";
checkout.prototype._accept_id = "";
checkout.prototype._remove_product_error_msg = "";
checkout.prototype._remove_service_error_msg = "";
checkout.prototype._update_product_error_msg = "";
checkout.prototype._civic_lbl = "";
checkout.prototype._organisation_lbl = "";
checkout.prototype._Cancel_PopUp_Button_Name = "";
checkout.prototype._Close_PopUp_Button_Name = "";
checkout.prototype._OK_PopUp_Button_Name = "";
checkout.prototype._Remove_Service_Confirmation_Message = "";
checkout.prototype._Remove_Service_Confirmation_Title = "";
checkout.prototype._Invoice_Organization_Label = "";
checkout.prototype._Current_Url = "";
checkout.prototype._Enable_Invoice_Payment = true;
checkout.prototype._SSN_Instruction_Label = "";
checkout.prototype._Organization_Instruction_Label = "";
checkout.prototype._ssnotextbox_id = "";
checkout.prototype._invoicefirstname_id = "";
checkout.prototype._invoicelastname_id = "";
checkout.prototype._invoicemobilenbr_id = "";
checkout.prototype._is_get_address_button_clicked = false;
checkout.prototype._getaddress_button_error_message = "";
checkout.prototype._cartmaxitemsbeforescroll = 1;

checkout.prototype.init = function (_online_btn, _invoice_btn, _internet_btn, _online_msg, _invoice_msg, _internet_msg, _civic_lbl, _organisation_lbl, _general_error_msg, _email_id, _firstname_id, _lastname_id, _address_id, _postalcode_id, _city_id, _mobile_id, _checkout_id, _phone_regex_msg, _personal_regex_msg, _accept_error_msg, _accept_id, _mobile_unique_id, _accept_unique_id, _remove_product_error_msg, _remove_service_error_msg, _update_product_error_msg, _check_email_error_msg, _delivery_firstname_id, _delivery_lastname_id, _delivery_address_id, _delivery_postalcode_id, _delivery_city_id, _delivery_mobile_id, _delivery_unique_mobile_id, _cart_modal_error, _postalandcity_error_msg, _recalculate_vat_error_msg, _button_save_id, _Postalcode_name, _sameasbillingaddress_label, _sameasinvoiceaddress_label, _modalerrormessage, _invalidssnoerrormessage, _invalidcompanynoerrormessage, _invalidquantityerrormessage, _billingaddresslabel, _selectlabel, _cancelpopup_buttonname, _closepopup_buttonname, _okpopup_buttonname, _removeservice_confirmationmessage, _removeservice_confirmationtitle, _invoice_organization_label, _current_url, _ssn_instruction, _organization_instruction, _getaddress_errormessage, _cartmaxitemsbeforescroll) {
    var self = this;
    self._ziplength = 5;

    self._civic_lbl = _civic_lbl;
    self._organisation_lbl = _organisation_lbl;
    self._remove_product_error_msg = _remove_product_error_msg;
    self._update_product_error_msg = _update_product_error_msg;
    self._remove_service_error_msg = _remove_service_error_msg;
    self._accept_id = _accept_id;
    self._general_error_msg = _general_error_msg;
    self._delivery_unique_mobile_id = _delivery_unique_mobile_id;
    self._accept_unique_id = _accept_unique_id;
    self._accept_error_msg = _accept_error_msg;
    self._phone_regex_msg = _phone_regex_msg;
    self._personal_regex_msg = _personal_regex_msg;
    self._mobile_unique_id = _mobile_unique_id;
    self._Online_Label = _online_btn;
    self._Invoice_Label = _invoice_btn;
    self._Internet_Label = _internet_btn;
    self._Online_Message = _online_msg;
    self._Invoice_Message = _invoice_msg;
    self._Internet_Message = _internet_msg;
    self._Email_id = _email_id;
    self._Firstname_id = _firstname_id;
    self._Lastname_id = _lastname_id;
    self._Address_id = _address_id;
    self._Postalcode_id = _postalcode_id;
    self._City_id = _city_id;
    self._Mobile_id = _mobile_id;
    self._Checkout_id = _checkout_id;
    self._Delivery_Firstname_id = _delivery_firstname_id;
    self._Delivery_Lastname_id = _delivery_lastname_id;
    self._Delivery_Address_id = _delivery_address_id;
    self._Delivery_Postalcode_id = _delivery_postalcode_id;
    self._Delivery_City_id = _delivery_city_id;
    self._Delivery_Mobile_id = _delivery_mobile_id;
    self._Cart_Modal_Error = _cart_modal_error;
    self._PostalAndCity_Error_Msg = _postalandcity_error_msg;
    self._Recalculate_Vat_Error_Msg = _recalculate_vat_error_msg;
    self._Postalcode_name = _Postalcode_name;
    self._SameAsBillingAddressLabel = _sameasbillingaddress_label;
    self._SameAsInvoiceAddressLabel = _sameasinvoiceaddress_label;
    self._ModalErrorMessage = _modalerrormessage;
    self._InvalidSSNoErrorMessage = _invalidssnoerrormessage;
    self._InvalidCompanyNoErrorMessage = _invalidcompanynoerrormessage;
    self._InvalidQuantityErrorMessage = _invalidquantityerrormessage;
    self._BillingAddressLabel = _billingaddresslabel;
    self._SelectLabel = _selectlabel;
    self._Cancel_PopUp_Button_Name = _cancelpopup_buttonname;
    self._Close_PopUp_Button_Name = _closepopup_buttonname;
    self._OK_PopUp_Button_Name = _okpopup_buttonname;
    self._Remove_Service_Confirmation_Message = _removeservice_confirmationmessage;
    self._Remove_Service_Confirmation_Title = _removeservice_confirmationtitle;
    self._Invoice_Organization_Label = _invoice_organization_label;
    self._Current_Url = _current_url;
    self._SSN_Instruction_Label = _ssn_instruction;
    self._Organization_Instruction_Label = _organization_instruction;
    self._cartmaxitemsbeforescroll = _cartmaxitemsbeforescroll;

    self._ssnotextbox_id = $(".SocialSecNoTextBox").attr("id");
    self._invoicefirstname_id = $(".invoicefname_tbox").attr("id");
    self._invoicelastname_id = $(".invoicelname_tbox").attr("id");
    self._invoicemobilenbr_id = $(".invoicemobile_tbox").attr("id");

    self._getaddress_button_error_message = _getaddress_errormessage;


    //Remove Global Navigation, minicart and Search
    $("#GlobalMenu, #Search, #GlobalCart, #GlobalUserInfo, #Footer").html("");
    $(".SubMenu").parent().html("");

    //set url of the logo posten.se
    $("#Logo a.logo").attr("href", "http://www.posten.se");

    //make service scrollable
    self.UpdateServiceScrollableDisplay();

    //set cookie to false - private and company checkbox is not yet click
    $.cookie("private_comp_change", false, { path: "/", expires: 7 })
    $.cookie("setprivateorcompany", false, { path: "/", expires: 7 })


    //set values from cart configuration settings
    self.getCartSettings();

    //enable Handlers for Payment Methods.
    self.setPaymentMethodFromCookie();




    if ($("input[id*='radioButtonInvoicePayment']").is(":checked")) {
        self.InvoiceSelected(this);
    }
    else if ($("input[id*='radioButtonOnlinePayment']").is(":checked")) {
        self.CreditCardSelected(this);
    }
    else if ($("input[id*='radioButtonInternetPayment']").is(":checked")) {
        self.InternetBankingSelected(this);
    }

    $("input[id*='radioButtonOnlinePayment']").click(function () {
        self.CreditCardSelected(this);
    });

    $("input[id*='radioButtonInvoicePayment']").click(function () {
        self.InvoiceSelected(this);
    });

    $("input[id*='radioButtonInternetPayment']").click(function () {
        self.InternetBankingSelected(this);
    });

    $("#deliveryhelp-target-1").ezpz_tooltip({
        contentPosition: 'belowRightFollow',
        stayOnContent: true,
        offset: 20,
        overrideTop: $("#deliveryhelp-target-1").offset().top - 70,
        overrideLeft: $("#deliveryhelp-target-1").offset().left + 30
    });

    $("#HelpImageLink").ezpz_tooltip({
        contentId: 'promo-content',
        contentPosition: 'belowRightFollow',
        stayOnContent: true,
        offset: 20,
        overrideTop: $("#HelpImageLink").offset().top - 70,
        overrideLeft: $("#HelpImageLink").offset().left + 30
    });


    //Remember all items when a user inputs.
    $(".enablestate").focusout(function () {
        if (this.value != null) $.cookie(this.id, this.value, { path: "/", expires: 7 });
        else $.cookie(this.id, null);
    });

    //Handle Changes on Private or Organization
    if ($(".private input").is(":checked")) {
        self.PrivateSelected(true);
    }
    if ($(".company input").is(":checked")) {
        self.PrivateSelected(false);
    }

    //Change text depending on private person or an organization
    $('.private input').change(function () {
        $.cookie("private_comp_change", true, { path: "/", expires: 7 })
        $.cookie("isprivate", true, { path: "/", expires: 7 });
        self.PrivateSelected(true);
    });

    $('.company input').change(function () {
        $.cookie("private_comp_change", true, { path: "/", expires: 7 })
        $.cookie("isprivate", false, { path: "/", expires: 7 });
        self.PrivateSelected(false);
    });

    //Check if cookies has values
    self.setformvaluesfromcookie();

    self.setcountryvaluefromcookie();

    self.SetPrivateOrCompany();

    self.ShowAddressFromCookie();

    self.InitializeValidators();


    //This section adds the delivery details section.
    $(".newadd-rbutton input").change(function () {
        self.DifferentDeliveryAddressSelected(this);
    })
    .click(function () {
        self.DifferentDeliveryAddressSelected(this);
    });

    $(".sameadd-rbutton input").change(function () {
        self.DifferentDeliveryAddressSelected($(".newadd-rbutton input")[0]);
    })
    .click(function () {
        self.DifferentDeliveryAddressSelected($(".newadd-rbutton input")[0]);
    });

    //Handles Remove Product
    $('.remove-prod').click(function () {
        self.RemoveProduct(this);
    });

    //Handle Service Removal
    $('.remove-svc').click(function () {
        var me = this;
        showConfirmPopUp(self._Remove_Service_Confirmation_Title, self._Remove_Service_Confirmation_Message, "popup", self._OK_PopUp_Button_Name, self._Cancel_PopUp_Button_Name, function (ok) {
            if (ok) {
                self.RemoveService(me);
            }
        });
    });

    //displays the details of the services as a tooltip
    $(".servicesCont .items").each(function (index, element) {

        var orderid = $(element).attr("id");
        var helpdiv = $("#content_" + orderid);

        //compute location of help divs
        //move divs to body to avoid overflow hidden problems.
        helpdiv.prependTo("body");

        $(element).find(".svc-name > a:first-child").ezpz_tooltip({ contentId: "content_" + orderid,  contentPosition: "rightFollow" });
        /*.mouseenter(function () {
        helpdiv.show();
        }).mouseleave(function () {
        helpdiv.hide();
        });*/
    })


    //Handle update service when quantity is updated
    $('.tbox-quantity').keyup(function () {
        self.UpdateProductQuantity(this);
    });

    $('.firstname').keyup(function () {
        var txt = $(this).val();
        $('.invoicefname_tbox').val(txt);
    });

    $('.lastname').keyup(function () {
        var txt = $(this).val();
        $('.invoicelname_tbox').val(txt);
    });

    $('.mobileidno').keyup(function () {
        var txt = $(this).val();
        $('.invoicemobile_tbox').val(txt);
    });

    $(".postalcode").change(function () {
        self.getLocationInformation();
    });

    $(".country").change(function () {
        //3098 - [Posten] Validation appears after selecting another country on the Land dropdown list
        //self.getLocationInformation();
        $.cookie('selectedcountry', $('.country option:selected').val(), { path: "/", expires: 7 });
    });

    $(".new-postal").change(function () {
        self.getLocationInformationforDelivery();
    });

    $(".new-country").change(function () {
        self.getLocationInformationforDelivery();
    });

    // We will comment out the body since we will remove integration with Access. Remove comments once integration with Access will start.
    $(".email-tbox input").focusout(function () {
        /*
        var email = $('.email-tbox .email').val();
        $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOPeration.svc/DoesEmailExist",
        data: '{"email":"' + email + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
        if (msg == true) {
        $('.save-section').children().hide();
        $("#divPassword").show();
        $('.password-tbox input').focus();
        }
        else { $("#divPassword").hide(); }
        },
        error: function (xhr, desc, err) {
        showPopUp(self._Cart_Modal_Error, _check_email_error_msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
        }
        });*/
    });


    //Get Legal address
    $('.getLegalAddressButton').click(function () {
        return self.GetLegalAddress();
    });


    //this block is commented out because of the removal of the access project. Uncomment once integration will resume.
    /*
    $('.btnlogin a').click(function () {
    var email = $('.email-tbox .email').val();
    var pass = $('.password-tbox input').val();
    $.ajax({
    type: "POST",
    url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOPeration.svc/GetAccountInformation",
    data: '{"email":"' + email + '","password":"' + pass + '"}',
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (msg) {
    if (msg != null) {
    $('.firstname').val(msg.FirstName);
    $('.lastname').val(msg.LastName);
    $('.house-address').val(msg.Address.HouseNumber);
    $('.address').val(msg.Address.StreetAddress);
    $('.postalcode').val(msg.Address.PostalCode);
    $('.city').val(msg.Address.City);
    $('.country').val(msg.Address.Country);
    $('.mobile').val(msg.MobileNumber);
    self.getLocationInformation();
    }
    else {
    showPopUp(self._Cart_Modal_Error, 'Account is Invalid', 'popup', false, 800, self._Close_PopUp_Button_Name);
    }
    },
    error: function (xhr, desc, err) {
    showPopUp(self._Cart_Modal_Error, 'There was an error retrieving your account', 'popup', false, 800, self._Close_PopUp_Button_Name);
    }
    });

    });
    */


    //Body is commented out because we will be removing access integration.
    /*
    $('#' + _button_save_id).click(function () {
    var code;
    var isValid = true;

    isValid = self.userdetails_validateresult();
    if (!isValid) {
    return;
    }
    else {
    var email = $('.email').val();
    var fname = $('.firstname').val();
    var lname = $('.lastname').val();
    var codevalue = $('.civic-tbox input').val();
    var address = $('.address').val();
    var postal = $('.postalcode').val();
    var city = $('.city').val();
    var country = $('.country').val();
    var countryvalue = $(".country option:selected").text();
    var mobile = $('.mobile').val();
    var password = $('.pword-tbox input').val();

    if ($(".private input").is(":checked")) {
    code = '0';
    }
    else if ($(".company input").is(":checked")) {
    code = '1';
    }

    $.ajax({
    type: "POST",
    url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/SaveAccount",
    data: '{"email":"' + email + '","firstName":"' + fname + '","lastName":"' + lname + '","codeValue":"' + codevalue + '","code":"' + code + '","postal":"' + postal + '","city":"' + city + '","streetAddress":"' + address + '","country":"' + country + '","mobile":"' + mobile + '","password":"' + password + '"}',
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (msg) {
    (msg[0].code == '0') ? showPopUp("Account has been created", "Your account has been created. Check your email to activate your account.", 'popup', false, 800, self._Close_PopUp_Button_Name) : self.DisplayErrorResults(msg);
    },
    error: function errorFunction() {
    showPopUp(self._Cart_Modal_Error, "Cannot Create your account", 'popup', false, 800, self._Close_PopUp_Button_Name);
    }
    });

    return false;
    }
    }); */
};

checkout.prototype.setPaymentMethodFromCookie = function () {
    var self = this;
    var selectedmop = $.cookie("selectedmop");
    if (selectedmop != null) {
        if (selectedmop == "invoice") {
            $("input[id*='radioButtonInvoicePayment']").attr("checked", true).click();
        }
        else if (selectedmop == "card") {
            $("input[id*='radioButtonOnlinePayment']").attr("checked", true).click();
        }
        else if (selectedmop == "internet") {
            $("input[id*='radioButtonInternetPayment']").attr("checked", true).click();
        }
    }
}

checkout.prototype.enableInvoiceRequiredFields = function (enable) {
    //Select same address then set delivery address and disable delivery address radio button.
    if (enable) {

        //Phone number validation
        $.validator.addMethod(
        "phone",
        function (value, element) {
            var check = false;
            if (value == '')
                return true;
            var re = new RegExp(eval(self._phone_regex_msg));
            return this.optional(element) || re.test(value);
        },
        "");
        //Personal number validation
        $.validator.addMethod(
        "personalno",
        function (value, element) {
            var check = false;
            if (value == '')
                return true;
            var re = new RegExp(eval(self._personal_regex_msg));
            return this.optional(element) || re.test(value);
        },
        "");



        $(".SocialSecNoTextBox").addClass('required personalno');
        $(".invoicefname_tbox").addClass('required');
        $(".invoicelname_tbox").addClass('required');
        $(".invoicemobile_tbox").addClass('required phone');
    }
    else {

        //Phone number validation
        $.validator.addMethod("phone", function (value, element) { return true; }, "");
        //Personal number validation
        $.validator.addMethod("personalno", function (value, element) { return true; }, "");

        $("div.secondcolumnerrorsummary").hide();
        $(".SocialSecNoTextBox").val('');
        $(".invoicemobile_tbox").val('');
        $(".SocialSecNoTextBox").removeClass('required personalno error');
        $("label[for=" + $(".SocialSecNoTextBox").attr("id") + "]").removeClass('error');
        $(".invoicefname_tbox").removeClass('required error');
        $("label[for=" + $(".invoicefname_tbox").attr("id") + "]").removeClass('error');
        $(".invoicelname_tbox").removeClass('required error');
        $("label[for=" + $(".invoicelname_tbox").attr("id") + "]").removeClass('error');
        $(".invoicemobile_tbox").removeClass('required phone error');
        $("label[for=" + $(".invoicemobile_tbox").attr("id") + "]").removeClass('error');
    }


}

checkout.prototype.DisableSetDeliveryAddress = function (disable) {
    //Select same address then set delivery address and disable delivery address radio button.
    var id = $("input[id*='chkNewDeliveryAddress']").attr("id");
    if (disable) {
        $("label[for=" + id + "]").addClass("login-label");
        $("input[id*='chkNewDeliveryAddress']").attr('disabled', disable);
        $("input[id*='chkSameBillingAddress']").attr('checked', true).click();
    }
    else {
        $("input[id*='chkNewDeliveryAddress']").removeAttr('disabled');
        $("label[for=" + id + "]").removeClass("login-label");
    }
}

checkout.prototype.SelectedLegalAddress = function (msg) {
    var self = this;

    //Disable the delivery address button. 
    //set the delivery address fields.
    if (msg.FirstName != null) {
        $('.new-firstname').val(msg.FirstName);
        $('.new-lastname').val(msg.LastName);
        $('.invoicefname_tbox').val(msg.FirstName);
        $('.invoicelname_tbox').val(msg.LastName);
        $('.new-company').val('');
    }
    else {
        $('.new-firstname').val('');
        $('.new-lastname').val('');
        $('.new-company').val(msg.CompanyName);
    }

    $('.new-address').val(msg.StreetAddress);
    $('.new-postal').val(msg.PostalCode);
    $('.new-city').val(msg.City);
    $('.new-country').val(msg.Country);

}



checkout.prototype.DisplayErrorResults = function (msg) {
    var self = this;
    var results = "";
    for (i = 0; i < msg.length; i++) {
        results += "<div>" + msg[i].message + "</div>";
    }
    showPopUp(self._Cart_Modal_Error, results, 'popup', false, 800, self._Close_PopUp_Button_Name);
}

checkout.prototype.HideLabelsIfEmpty = function (item) {
    var self = this;

    var name = $(item);
    var itemlength = name.children(".items").length;
    var productcount = $('.productsCont div.items').length;
    var servicescount = $('.servicesCont div.items').length;

    if (servicescount == 0 && productcount == 0) {
        $('.labels').fadeOut('fast', function () {
            //hide services and products
            $('.emptyCartControl').show();
            
            return;
        });
    }

    if (servicescount == 0) {
        $('.labels.serv').fadeOut('fast');
    }
    if (productcount == 0) {
        $('.labels.prod').fadeOut('fast');
    }

}

//Stores selected country state to cookie
checkout.prototype.setcountryvaluefromcookie = function () {
    if ($.cookie('selectedcountry') != null) {

        $('.country option[value="' + $.cookie('selectedcountry') + '"]').attr('selected', 'selected');
    }
}

checkout.prototype.setformvaluesfromcookie = function () {
    var fields = $(".enablestate");
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        if ($.cookie(field.id) != null) field.value = $.cookie(field.id);
    }

    //    if ($.cookie("isprivate") != null) {
    //        var isprivate = $.cookie("isprivate");
    //        if (isprivate != "false") {
    //            $(".private input").attr("checked", "checked");
    //            $(".company input").removeAttr("checked");
    //        }
    //        else {
    //            $(".company input").attr("checked", "checked");
    //            $(".private input").removeAttr("checked");
    //        }
    //    }
    //Validate the city
    this.getLocationInformation();


}

//Commented Out because of Access Integration
/*
checkout.prototype.userdetails_validateresult = function () {
var self = this;
var isvalid = true;
if (!$("#" + self._Email_id).valid()) isvalid = false;
if (!$("#" + self._Firstname_id).valid()) isvalid = false;
if (!$("#" + self._Lastname_id).valid()) isvalid = false;
if (!$("#" + self._Address_id).valid()) isvalid = false;
if (!$("#" + self._Postalcode_id).valid()) isvalid = false;
if (!$("#" + self._City_id).valid()) isvalid = false;
//if (!$("#<%= txtNewPassword.ClientID %>").valid()) isvalid=false;
if (!$("#" + self._Mobile_id).valid()) isvalid = false;

//Invoice fields
if ($("input[id*='radioButtonInvoicePayment']").is(":checked")) {
if (!$(".SocialSecNoTextBox").valid()) isvalid = false;
if (!$(".invoicefname_tbox").valid()) isvalid = false;
if (!$(".invoicelname_tbox").valid()) isvalid = false;
if (!$(".invoicemobile_tbox").valid()) isvalid = false;
}
    

if (!isvalid) $("div.errorsummary").hide();
return isvalid;
}
*/

checkout.prototype.getLocationInformation = function () {
    var self = this;
    var postalcode = $(".postalcode").val();
    var country = $(".country").val();
    postalcode = postalcode.replace(" ", "");
    country = "SE"; //disable validation for country
    //do not validate if postal code is empty, just set the validator indicator to false 
    if (postalcode == "") {
        self._ValidCity = false;
        return;
    }
    
    if (postalcode != "" && country != "" && $(".country").val() == "SE") {
        $.ajax({
            type: "POST",
            url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/GetLocation",
            data: '{"postalcode":"' + postalcode + '","countrycode":"' + country + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg != null) {
                    $(".city").val(msg.city);
                    $(".postalcode").val(msg.zip);
                    //if ($(".country").val() == country) {
                    //    $(".country").val(msg.country);
                    //}

                    self._ValidCity = true;
                    if (!$(".newadd-rbutton input").is(":checked")) {
                        //self.recalculateVatInformation(postalcode, $(".new-country").val());
                    }
                    //Workaround for issue#2796
                    $(".postalcode").valid();
                }
                else {
                    self._ValidCity = false;
                    showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
                }
            },
            error: function errorFunction() {
                showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
            }
        });

    }
    else {
        //showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
        //just do nothing.
    }
    self._ValidCity = true;
}

checkout.prototype.getLocationInformationforDelivery = function () {
    var self = this;
    var postalcode = $(".new-postal").val();
    postalcode = postalcode.replace(" ", "");

    if (postalcode.length <= self._ziplength && postalcode != "" && $(".new-country").val() != "") {
        $.ajax({
            type: "POST",
            url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/GetLocation",
            data: '{"postalcode":"' + postalcode + '","countrycode":"' + $(".new-country").val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg != null) {
                    $(".new-city").val(msg.city);
                    $(".new-country").val(msg.country);
                    $(".new-postal").val(msg.zip);
                    //self.recalculateVatInformation(postalcode, $(".new-country").val());
                }
                else {
                    showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800);
                }
            },
            error: function errorFunction() {
                showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
            }
        });
    }
}



checkout.prototype.checkMaximumInvoiceAmount = function () {
    var self = this;
    var type = "private";
    if ($(".company input").is(":checked")) { type = "company"; }
    var id = $("input[id*='radioButtonInvoicePayment']").attr("id");
    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/IsMaximumInvoiceAmount",
        data: '{"type":"' + type + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg.isMaximum) {
                $("input[id*='radioButtonInvoicePayment']").removeAttr("checked");
                $("input[id*='radioButtonInvoicePayment']").attr("disabled", "disabled");
                $("#divFaktura").hide();
                $("#invoicepaymentmessage").show();
                $("#invoicepaymentmessage").html(msg.Message);
                $("label[for=" + id + "]").addClass("login-label");
                self.setDefaultPaymentMethod();
            }
            else {
                if (self._Enable_Invoice_Payment) {
                    if ($("input[id*='radioButtonInvoicePayment']").is(":disabled")) {
                        $("input[id*='radioButtonInvoicePayment']").removeAttr("disabled");
                        $("label[for=" + id + "]").removeClass("login-label");
                    }
                }
                $("#invoicepaymentmessage").hide();
                $("#invoicepaymentmessage").html("");
            }
        },
        error: function errorFunction() {

        }
    });
}

checkout.prototype.addInvoice = function (willadd, currentelement) {
    var self = this;
    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/AddInvoice",
        data: '{"willadd":"' + willadd + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $('.InvoiceFee-value').text(msg.InvoiceFee);
            $('.lblBasketTotal').text(msg.BasketTotal);
            $('.lblToPay').text(msg.BasketTotal);
            $('.lblVatValue').text(msg.VatTotal);
            //$('.lblTotalExcludingVat').text(msg.BasketTotalExcludingTax);
        },
        error: function errorFunction(jqXHR, textStatus, errorThrown) {
            //Retry again for the user

        }
    });

}


checkout.prototype.InvoiceSelected = function (currentelement) {
    var self = this;
    //Show 
    $("#divFaktura").show();
    $("#Fakturaavgift").show();
    $(".invoice_section").show();
    //Rename Text for invoice
    $(".lnkCheckOut").text(self._Invoice_Label);
    $("#txtNextPage").text(self._Invoice_Message);

    //Cannot Set Delivery Address
    self.DisableSetDeliveryAddress(true);

    //Rename the text for setting the delivery address
    var id = $("input[id*='chkSameBillingAddress']").attr("id");
    $("label[for=" + id + "]").text(self._SameAsInvoiceAddressLabel);

    //Set firstname and lastname  if company is selected
    if ($(".company input").is(":checked")) {
        $('.invoicefname_tbox').val($('.firstname').val());
        $('.invoicelname_tbox').val($('.lastname').val());
        
          
    }
    //set invoice phone from customer information
    $('.invoicemobile_tbox').val($('.mobileidno').val());

    //make civic number required
    self.enableInvoiceRequiredFields(true);

    //Add Invoice
    self.addInvoice(true, currentelement);

    // add select payment to cookie
    $.cookie("selectedmop", "invoice", { path: "/", expires: 7 });
}

checkout.prototype.CreditCardSelected = function (currentelement) {
    var self = this;
    //Hide invoice area
    $("#divFaktura").hide();
    $("#Fakturaavgift").hide();
    $(".invoice_section").hide();
    //Rename Text
    $(".lnkCheckOut").text(self._Online_Label);
    $("#txtNextPage").text(self._Online_Message);
    var id = $("input[id*='chkSameBillingAddress']").attr("id");
    $("label[for=" + id + "]").text(self._SameAsBillingAddressLabel);

    // add select payment to cookie
    $.cookie("selectedmop", "card", { path: "/", expires: 7 });

    //Cannot Set Delivery Address
    self.DisableSetDeliveryAddress(false);

    //Remove Required Validation for invoice.
    self.enableInvoiceRequiredFields(false);

    //Remove Invoice
    self.addInvoice(false, currentelement);

}

checkout.prototype.InternetBankingSelected = function (currentelement) {
    var self = this;
    //Update Text of the page
    $("#divFaktura").hide();
    $(".invoice_section").hide();
    $("#Fakturaavgift").show();
    $(".lnkCheckOut").text(self._Internet_Label);
    $("#txtNextPage").text(self._Internet_Message);
    var id = $("input[id*='chkSameBillingAddress']").attr("id");
    $("label[for=" + id + "]").text(self._SameAsBillingAddressLabel);

    //Enable Custom Delivery Address
    self.DisableSetDeliveryAddress(false);

    // add select payment to cookie
    $.cookie("selectedmop", "internet", { path: "/", expires: 7 });

    //Remove required Validation for Invoice.
    self.enableInvoiceRequiredFields(false);
    //Remove Invoice
    self.addInvoice(false, currentelement);


}



checkout.prototype.setDefaultPaymentMethod = function () {
    var self = this;
    //Check default payment method from cookie
    var selectedmop = $.cookie("selectedmop");

    if (selectedmop == "invoice" && !$("input[id*='radioButtonInvoicePayment']").is(":disabled")) {
        $("input[id*='radioButtonInvoicePayment']").attr("checked", true);
        self.InvoiceSelected($("input[id*='radioButtonInvoicePayment']")[0]);
    }
    else if (selectedmop == "card" && !$("input[id*='radioButtonOnlinePayment']").is(":disabled")) {
        $("input[id*='radioButtonOnlinePayment']").attr("checked", true);
        self.CreditCardSelected($("input[id*='radioButtonOnlinePayment']")[0]);
    }
    else if (selectedmop == "internet" && !$("input[id*='radioButtonInternetPayment']").is(":disabled")) {
        $("input[id*='radioButtonInternetPayment']").attr("checked", true);
        self.InternetBankingSelected($("input[id*='radioButtonInternetPayment']")[0]);
    }

}

checkout.prototype.PrivateSelected = function (isprivate) {
    var self = this;
    var is_change = $.cookie("private_comp_change");
    var is_from_method_change = $.cookie("setprivateorcompany")
    if (isprivate) {
        $('#civiclabelid').text(self._civic_lbl + ": *");

        $(".invoicefname_tbox").removeClass("required");
        $(".invoicelname_tbox").removeClass("required");

        //Show Organization on the Customer Details and Set label.
        $("#ssno-tbox").hide();
        $("#ssno-lbl").hide();
        $('#ssno-lbl').html(self._civic_lbl + ":&nbsp;");
        $('#inv-ssn-instruction').text(self._SSN_Instruction_Label);
        //self._Invoice_Organization_Label

        //set the invoice phonenumber
        $('.invoicemobile_tbox').val($('.mobileidno').val());
    }
    else {
        //Handle for organization
        //rename label to Organization
        $('#civiclabelid').text(self._Invoice_Organization_Label + ": *");
        //Enable Editing of Firstname and lastname for Invoice
        //Only make required if invoice is checked.
        if ($("input[id*='radioButtonInvoicePayment']").is(":checked")) {
            $(".invoicefname_tbox").addClass("required");
            $(".invoicelname_tbox").addClass("required");
        }

        //Show Organization on the Customer Details and Set label.
        $("#ssno-tbox").show();
        $("#ssno-lbl").show();
        $('#ssno-lbl').html(self._organisation_lbl + ":&nbsp;");

        $('#inv-ssn-instruction').text(self._Organization_Instruction_Label);

        

        //Set firstname and lastname of the invoice if company is selected
        $('.invoicefname_tbox').val($('.firstname').val());
        $('.invoicelname_tbox').val($('.lastname').val());
        $('.invoicemobile_tbox').val($('.mobileidno').val());

    }

    //Reset invoice fields and cookies if private / company checkbox is click
    if (is_change == "true" && is_from_method_change == "false") {
        $.cookie("getaddressbuttonclicked", false, { path: "/", expires: 7 });
        $(".SocialSecNoTextBox").attr("value", "").focusout();
        if (isprivate) {
            //we copied the names from the customer information, so no need to delete if it company.
            $(".invoicefname_tbox").attr("value", "").focusout();
            $(".invoicelname_tbox").attr("value", "").focusout();
            
        }
        

        //remove addresses and adjust viewpoirt height
        $(".addressSection .addresses").remove();
        $(".addressSection .viewport").height("100%");
        $(".addressSection .scrollbar").hide();
        $(".addressSection .viewport .overview").css("position", "relative");
    }
    self.checkMaximumInvoiceAmount();
}

checkout.prototype.SetPrivateOrCompany = function () {
    var is_private = $.cookie("isprivate");
    var is_getaddressbutton_clicked = $.cookie("getaddressbuttonclicked");
    if (is_private != null) {
        $.cookie("setprivateorcompany", true, { path: "/", expires: 7 })
        if (is_private == "true") {
            $('.private input').click().change();

        }
        else {
            $('.company input').click().change();
        }
        $.cookie("setprivateorcompany", false, { path: "/", expires: 7 })
    }
}

checkout.prototype.ShowAddressFromCookie = function () {
    var self = this;
    var is_getaddressbutton_clicked = $.cookie("getaddressbuttonclicked");
    var return_address = $.cookie("return_address");
    var selected_legaladdress = $.cookie("legaladdressindex");

    if (is_getaddressbutton_clicked != null) {
        if (is_getaddressbutton_clicked == "true") {
            if (return_address != null) {
                self.ShowLegalAddress(JSON.parse(return_address));

                if (selected_legaladdress != null) {
                    $(".addresses .selectaddress input[value=" + selected_legaladdress + "]").click().change();
                }
            }
        }
    }
}



checkout.prototype.InitializeValidators = function () {
    var self = this;

    //Phone number validation
    $.validator.addMethod(
        "phone",
        function (value, element) {
            var check = false;
            if (value == '')
                return true;
            var re = new RegExp(eval(self._phone_regex_msg));
            return this.optional(element) || re.test(value);
        },
        "");
    //Personal number validation
    $.validator.addMethod(
        "personalno",
        function (value, element) {
            var check = false;
            if (value == '')
                return true;
            var re = new RegExp(eval(self._personal_regex_msg));
            return this.optional(element) || re.test(value);
        },
        "");

    $.validator.addMethod(
        "City",
        function (value, element) {
            return self._ValidCity;
        },
        "");


    var validationrules = {};
    validationrules[self._mobile_unique_id] = { phone: "" };
    validationrules[self._delivery_unique_mobile_id] = { phone: "" };
    validationrules[self._accept_unique_id] = { required: true };
    //validationrules[self._Postalcode_name] = { City: true, required: true };
    //validationrules[self._Postalcode_name] = { City: true};

    var validationmessages = {};
    validationmessages[self._accept_unique_id] = { required: self._accept_error_msg };

    $("#aspnetForm").validate({
        invalidHandler: function (e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("div.errorsummary span").html(self._general_error_msg);
                $("div.errorsummary").show();
            }
            else {
                $("div.errorsummary").hide();
            }
        },
        onkeyup: false,
        onsubmit: false,
        rules: validationrules,
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            //Handle the checkboxes highlighting in non ie-browsers
            if ($(element).attr("type") == 'checkbox') {
                $(".accept_error_message").show();
                if ($(element).parent(".checkboxerror").length == 0 && !$.browser.msie) {
                    $(element).wrap($('<span class="checkboxerror" />'));
                }
                else {
                    $(element).addClass(errorClass).removeClass(validClass);
                }
                return;
            }


            $(element).addClass(errorClass).removeClass(validClass);
            $(element.form).find("label[for=" + element.id + "]").addClass(errorClass).removeClass(validClass);

            if (element.id == self._ssnotextbox_id || element.id == self._invoicefirstname_id || element.id == self._invoicelastname_id || element.id == self._invoicemobilenbr_id) {
                $("div.secondcolumnerrorsummary").show();
            }
            else {
                $("div.errorsummary span").html(self._general_error_msg);
                $("div.errorsummary").show();
            }
        },
        unhighlight: function (element, errorClass, validClass) {

            //Handle the checkboxes highlighting in non ie-browsers
            if ($(element).attr("type") == 'checkbox') {
                $(".accept_error_message").hide();
                if ($(element).parent(".checkboxerror").length == 1 && !$.browser.msie) {
                    $(element).unwrap();
                }
                else {
                    $(element).removeClass(errorClass).addClass(validClass);
                }
                return;
            }


            $(element).removeClass(errorClass).addClass(validClass);
            $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass).addClass(validClass);

            if (element.id == self._ssnotextbox_id || element.id == self._invoicefirstname_id || element.id == self._invoicelastname_id || element.id == self._invoicemobilenbr_id) {
                var firstname_valid = true;
                var lastname_valid = true;

                var ssno_valid = !$("#" + self._ssnotextbox_id).hasClass('error');
                var firstname_visible = !($("#" + self._invoicefirstname_id).parent().parent().css('display') == "none");
                if (firstname_visible) {
                    firstname_valid = !$("#" + self._invoicefirstname_id).hasClass('error');
                }
                var lastname_visible = !($("#" + self._invoicelastname_id).parent().parent().css('display') == "none");
                if (lastname_visible) {
                    lastname_valid = !$("#" + self._invoicelastname_id).hasClass('error');
                }
                var mobileno_valid = !$("#" + self._invoicemobilenbr_id).hasClass('error');
                if (ssno_valid && mobileno_valid && firstname_valid && lastname_valid) {
                    $("div.secondcolumnerrorsummary").hide();
                }
            }
            else {
                var emailadd_valid = !$(".email").hasClass('error');
                if (emailadd_valid) {
                    $("div.errorsummary").hide();
                }
            }
        },
        messages: validationmessages,
        errorPlacement: function (error, element) {
            //if this is the acceptance checkbox place the error after the anchor tag.
            if ($(element).attr('id') == self._accept_id) {
                error.insertAfter($(element).parent().nextAll("a + br"));
                return;
            }

            error.insertAfter($(element));
        },
        submitHandler: function () {
            $("div.errorsummary").hide();
        }
    });

    //Commented Out because of Removal of Access Integration
    /*
    $(".validatesavedetails").click(function (evt) {
    var isValid = true;
    isValid = self.userdetails_validateresult();
    if (!isValid) {
    var message = self._general_error_msg;
    $("div.errorsummary span").html(message);
    $("div.errorsummary").show();
    evt.preventDefault();
    }
    else {
    $("div.errorsummary").hide();
    }
    });
    */


    //override default messages
    $.extend($.validator.messages, {
        required: "",
        email: ""//"<%=Utils.GetResourceString("EMAIL_ERRORMESSAGE")%>",
    });

    $("#" + self._Checkout_id).click(function (evt) {

        if ($("input[id*='radioButtonInvoicePayment']").attr("checked")) {
            var ssno = $(".SocialSecNoTextBox").val();
            var emobilenbr = $(".invoicemobile_tbox").val();
            if (ssno != "" && emobilenbr != "") {
                var is_getaddressbutton_clicked = $.cookie("getaddressbuttonclicked");
                if (is_getaddressbutton_clicked != null) {
                    if (is_getaddressbutton_clicked != "true") {
                        showPopUp(self._Cart_Modal_Error, self._getaddress_button_error_message, 'popup', false, 800, self._Close_PopUp_Button_Name);
                        return false;
                    }
                }
                else {

                    showPopUp(self._Cart_Modal_Error, self._getaddress_button_error_message, 'popup', false, 800, self._Close_PopUp_Button_Name);
                    return false;

                }
            }
        }

        if (!($("#aspnetForm").valid())) {
            //check if user info is valid
            var email = $("#" + self._Email_id).valid();
            //            var fname = $("#" + self._Firstname_id).valid();
            //            var lname = $("#" + self._Lastname_id).valid();
            //            var add = $("#" + self._Address_id).valid();
            //            var postal = $("#" + self._Postalcode_id).valid();
            //            var city = $("#" + self._City_id).valid();
            //            var mobile = $("#" + self._Mobile_id).valid();

            var isvalid = false;
            //Check if delivery address is checked. 
            if ($(".newadd-rbutton input").is(":checked")) {
                var newfname = $("#" + self._Delivery_Firstname_id).valid();
                var newlname = $("#" + self._Delivery_Lastname_id).valid();
                var newadd = $("#" + self._Delivery_Address_id).valid();
                var newpostal = $("#" + self._Delivery_Postalcode_id).valid();
                var newcity = $("#" + self._Delivery_City_id).valid();
                var newmobile = $("#" + self._Delivery_Mobile_id).valid();

                if (email && newfname && newlname && newadd && newpostal && newcity && newmobile) {//(email && fname && lname && add && postal && city && mobile && newfname && newlname && newadd && newpostal && newcity && newmobile) {
                    $("div.errorsummary").hide();
                }
                else
                    isvalid = false;
            }
            else {
                if (email) {// && fname && lname && add && postal && city && mobile) {
                    $("div.errorsummary").hide();
                }
                else
                    isvalid = false;
            }



            //Check if invoice fields have been populated. 
            var ssnotextbox = $(".SocialSecNoTextBox").valid();
            var invoicemobilenbr = $(".invoicemobile_tbox").valid();
            var invoicefirstname = $(".invoicefname_tbox").valid();
            var invoicelastname = $(".invoicelname_tbox").valid();
            var isInvoiceChecked = $("input[id*='radioButtonInvoicePayment']").attr("checked");
            if (ssnotextbox && invoicefirstname && invoicelastname && invoicemobilenbr && isInvoiceChecked != "checked") {
                $("div.secondcolumnerrorsummary").hide();

            }
            else {
                $("div.secondcolumnerrorsummary").show();
                return false;
            }



            if (!isvalid) return false;

            if (!self._ValidCity) {
                showPopUp(self._Cart_Modal_Error, self._PostalAndCity_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
                return false;
            }
        }
        else {
            __doPostBack('ctl00$PlaceHolderMain$ucCheckOut$lnkCheckOut', '')
            //disable all checkboxes so Invoice cannot be clicked to add. 
            $("input").attr("disabled", "disabled");
        }

    })
    //Set the href to empty.
    .attr("href", "#");
}

checkout.prototype.DifferentDeliveryAddressSelected = function (radiobuttonnewdeliveryaddress) {

    if ($(radiobuttonnewdeliveryaddress).is(":checked")) {
        $(".new-firstname").addClass("required");
        $(".new-lastname").addClass("required");
        $(".new-house-address").addClass("required");
        $(".new-address").addClass("required");
        $(".new-postal").addClass("required");
        $(".new-city").addClass("required");
        $(".new-country").addClass("required");
        $("#divNewAddress").show();
    }
    else if (!$(radiobuttonnewdeliveryaddress).is(":checked")) {
        $(".new-firstname").removeClass("required").val("").focus().focusout();
        $(".new-lastname").removeClass("required").val("").focus().focusout();
        $(".new-house-address").removeClass("required").val("").focus().focusout();
        $(".new-address").removeClass("required").val("").focus().focusout();
        $(".new-postal").removeClass("required").val("").focus().focusout();
        $(".new-city").removeClass("required").val("").focus().focusout();
        $(".new-country").removeClass("required").val("").focus().focusout();
        $(".new-mobile").focus().val("").focusout();
        $("#divNewAddress").hide();
    }
}

checkout.prototype.RemoveProduct = function (currentelement) {
    var self = this;
    var order = $(currentelement).parent().parent();
    var parent = order.parent();
    var orderNum = order.attr("id");
    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveProduct",
        data: '{"productOrderNumber":"' + orderNum + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            if (msg != null) {
                order.fadeOut('fast', function () {
                    $(order).remove();
                    self.HideLabelsIfEmpty(parent);
                    $('.lblBasketTotal').text(msg.TotalString);
                    $('.lblToPay').text(msg.TotalString);
                    $('.lblVatValue').text(msg.TotalTaxString);
                    //$('.lblTotalExcludingVat').text(msg.TotalExcludingTaxString);
                });
                
                self.checkMaximumInvoiceAmount();
            }
        },
        error: function (xhr, desc, err) {
            showPopUp(self._Cart_Modal_Error, self._remove_product_error_msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
        }
    });
}

checkout.prototype.RemoveService = function (currentelement) {

    var self = this;
    var order = $(currentelement).parent().parent();
    var parent = order.parent();
    var orderNum = order.attr("id");
    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveService",
        data: '{"serviceOrderNumber":"' + orderNum + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            if (msg != null) {
                order.fadeOut('fast', function () {
                    $(order).remove();
                    self.HideLabelsIfEmpty(parent);
                    $('.lblBasketTotal').text(msg.TotalString);
                    $('.lblToPay').text(msg.TotalString);
                    $('.lblVatValue').text(msg.TotalTaxString);
                    $('.InvoiceFee-value').text(msg.InvoiceFeeString);
                    self.UpdateServiceScrollableDisplay();
                    //$('.lblTotalExcludingVat').text(msg.TotalExcludingTaxString);
                });
                self.checkMaximumInvoiceAmount();

            }
        },
        error: function (xhr, desc, err) {
            showPopUp(self._Cart_Modal_Error, self._remove_service_error_msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
        }
    });

}


checkout.prototype.UpdateProductQuantity = function (currentelement) {
    var self = this;

    var order = $(currentelement).parent().parent();
    var parent = order.parent();
    var orderNum = order.attr("id");
    var quantity = $(currentelement).val();

    clearTimeout(self._timer);
    self._timer = setTimeout(function validate() {
        if (quantity != "" && parseInt(quantity) > 0) {
            $.ajax({
                type: "POST",
                url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/UpdateProduct",
                data: '{"productOrderNum":"' + orderNum + '","quantity":"' + quantity + '"}',
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    if (msg != null) {
                        order.attr("id", msg.OrderNumber);
                        order.find('.price-amount').text(msg.Total + " kr");
                        $('.lblBasketTotal').text(msg.TotalString);
                        $('.lblToPay').text(msg.TotalString);
                        $('.lblVatValue').text(msg.TotalTaxString);
                        //$('.lblTotalExcludingVat').text(msg.TotalExcludingTaxString);
                        self.checkMaximumInvoiceAmount();
                    }
                },
                error: function (xhr, desc, err) {
                    showPopUp(self._Cart_Modal_Error, self._update_product_error_msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
                }
            });
        }
        else {
            showPopUp(self._ModalErrorMessage, self._InvalidQuantityErrorMessage, 'popup', false, 800, self._Close_PopUp_Button_Name);
        }
    }, 400);

}


checkout.prototype.GetLegalAddress = function () {
    var self = this;
    var id = $('.SocialSecNoTextBox').val();
    var isValid = true;
    if (!$(".SocialSecNoTextBox").valid()) {
        isValid = false;
    }
    if (!isValid) {
        $("div.secondcolumnerrorsummary").show();
        return false;
    }

    $("div.errorsummary").hide();

    var errormsg = self._InvalidSSNoErrorMessage;
    var code = 0;
    if ($(".company input").is(":checked")) {
        code = 1;
        errormsg = self._InvalidCompanyNoErrorMessage;
    }

    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/GetLegalAddresses",
        data: '{"idno":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //set to true
            $.cookie("getaddressbuttonclicked", true, { path: "/", expires: 7 })
            $.cookie("return_address", JSON.stringify(msg), { path: "/", expires: 7 })

            self.ShowLegalAddress(msg);
            

            
        },
        error: function errorFunction() {
            showPopUp(self._ModalErrorMessage, errormsg, 'popup', false, 800, self._Close_PopUp_Button_Name);
        }
    });

    return false;
}


checkout.prototype.ShowLegalAddress = function (msg) {
    var self = this;
    var code = 0;
    var isprivate = true;
    if ($(".company input").is(":checked")) { code = 1; }

    //handle invalid items.
    if (msg.length > 0) {

        if (code == 1) $("[id$=_hidCompanyName]").val(msg[0].CompanyName);

        $("#shippingLabelHidden").hide();
        $(".intsruction-desc").empty();
        $("#shippingLabelShow").show();
        $(".addressSection").show();

    }

    if (msg.length == 1) {



        if (msg[0]["FirstName"] != null)
            isprivate = true;
        else
            $("[id$=_hidCompanyName]").val(msg[0].CompanyName);

        //first check if company or private since a company can have just 1 address.
        var privatemarkup = "<div class='addresses'><div class='addressHead addressItem'><div class='addressname'><div style='width:230px'>${FirstName} ${LastName}<br/>${StreetAddress}<br/>${PostalCode} ${City},${Country}</div></div><div class='headselectaddress selectaddress'></div><div class='addressend'></div></div></div>";
        var companymarkup = "<div class='addresses'><div class='addressHead addressItem'><div class='addressname'><div style='width:230px'>${CompanyName}<br/>${StreetAddress}<br/>${PostalCode} ${City},${Country} </div></div><div class='headselectaddress selectaddress'></div><div class='addressend'></div></div></div>";

        if (isprivate) {
            $.template("addressTemplate", privatemarkup);
        }
        else {
            $.template("addressTemplate", companymarkup);
        }
        $(".addressSection .addresses").remove();

        $.tmpl("addressTemplate", msg[0]).appendTo(".addressSection .overview");

        //set the hidden fields for delivery address.
        self.SelectedLegalAddress(msg[0]);

        $('.invoicemobile_tbox').val($('.mobileidno').val());

        //hide scrollbars
        $(".addressSection .viewport").height("100%");
        $(".addressSection .scrollbar").hide();
        $(".addressSection .viewport .overview").css("position", "relative");

    }
    else {

        //assumes that more than 1 is always a company
        var markup_selected = "<div class='addresses'><div class='addressHead addressItem'><div class='addressname'><div style='width:230px'>${CompanyName}<br/>${StreetAddress}<br/>${PostalCode} ${City},${Country} </div></div><div class='headselectaddress selectaddress'><input name='team' type='radio' value='${index}' checked/></div><div class='addressend'></div></div></div>";
        var markup = "<div class='addresses'><div class='addressHead addressItem'><div class='addressname'><div style='width:230px'>${CompanyName}<br/>${StreetAddress}<br/>${PostalCode} ${City},${Country}</div></div><div class='headselectaddress selectaddress'><input name='team' type='radio' value='${index}'/></div><div class='addressend'></div></div></div>";
        $.template("addressTemplate_selected", markup_selected);
        $.template("addressTemplate", markup);
        $(".addressSection .addresses").remove();

        //add index to message before adding
        msg[0].index = 0;
        $.tmpl("addressTemplate_selected", msg[0]).appendTo(".addressSection .overview");

        //select the first by default
        self.SelectedLegalAddress(msg[0]);

        for (i = 1; i < msg.length; i++) {
            msg[i].index = i;
            $.tmpl("addressTemplate", msg[i]).appendTo(".addressSection .overview");
        }

        self._legaladdresses = msg;

        //Handle Clicks
        $("input[name='team']").change(function () {
            self.SelectedLegalAddress(msg[$(this).attr("value")]);
            $.cookie("legaladdressindex", $(this).attr("value"), { path: "/", expires: 7 });
        });

        //compute total height
        var addresshtml = $("div.addresses")
        var counter = 0;
        var totaladdressheight = 0;


        //enable scrollbars when more then 3 addresses
        if (msg.length > 3) {

            //only compute height of 3 addresses
            while (counter < 3) {
                totaladdressheight += $(addresshtml[counter]).outerHeight();
                counter++;
            }
            $(".addressSection .viewport").height(totaladdressheight);
            $(".addressSection .viewport .overview").css("position", "absolute");
            $(".addressSection .scrollbar").show();
            $(".addressSection").tinyscrollbar();
        }
        else {
            //hide scrollbars
            $(".addressSection .viewport").height("100%");
            $(".addressSection .scrollbar").hide();
            $(".addressSection .viewport .overview").css("position", "relative");
        }

        //copy firstname, lastname from customer information. 
        $('.invoicefname_tbox').attr('value', $(".firstname").val());
        $('.invoicelname_tbox').attr('value', $(".lastname").val());
        $('.invoicemobile_tbox').val($('.mobileidno').val());
    }
    $("<div class='addresses'><div class='addressHead'><div class='addressname'><div><b>" + self._BillingAddressLabel + "</b></div></div><div class='headselectaddress'><b>" + self._SelectLabel + "</b></div><div class='addressend'></div></div></div>").prependTo(".addressSection");
}




checkout.prototype.getCartSettings = function () {
    var self = this;
    $.ajax({
        type: "POST",
        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/GetCartSettings",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            self.setCartSettings(msg);
        },
        error: function errorFunction(jqXHR, textStatus, errorThrown) {
            //Retry again for the user

        }
    });
}


checkout.prototype.setCartSettings = function (msg) {
    var self = this;
    var s = msg;

    self._Enable_Invoice_Payment = s.EnableInvoicePayment;

    if (!s.EnableCreditCardPayment) {
        $("input[id*='radioButtonOnlinePayment']").removeAttr("checked");
        $("input[id*='radioButtonOnlinePayment']").attr("disabled", "disabled");
        $("label[for=" + $("input[id*='radioButtonOnlinePayment']").attr("id") + "]").addClass("login-label");
    }
    if (!s.EnableInternetBankingPayment) {
        $("input[id*='radioButtonInternetPayment']").removeAttr("checked");
        $("input[id*='radioButtonInternetPayment']").attr("disabled", "disabled");
        $("label[for=" + $("input[id*='radioButtonInternetPayment']").attr("id") + "]").addClass("login-label");
    }
    if (!s.EnableInvoicePayment) {

        $("input[id*='radioButtonInvoicePayment']").removeAttr("checked");
        $("input[id*='radioButtonInvoicePayment']").attr("disabled", "disabled");
        $("label[for=" + $("input[id*='radioButtonInvoicePayment']").attr("id") + "]").addClass("login-label");
    }
}

checkout.prototype.UpdateServiceScrollableDisplay = function () {
    //Enable scrolling on the services. there is a maximum number then scrolling will be enabled.
    //Compute the height based on the number of services. 
    var self = this;
    var counter = 0;
    var totalserviceheight = 0;
    var serviceitems = $(".servicesCont div.items")

    if (serviceitems.length > self._cartmaxitemsbeforescroll) {
        while (counter < self._cartmaxitemsbeforescroll) {
            totalserviceheight += $(serviceitems[counter]).outerHeight();
            counter++;
        }
        $(".servicesCont .viewport").height(totalserviceheight);
        $(".servicesCont").tinyscrollbar();
    }
    else {
        $(".servicesCont .viewport").height("100%");
        $(".servicesCont .scrollbar").hide();
        $(".servicesCont .viewport .overview").css("position", "relative");

    }
}




//checkout.prototype.recalculateVatInformation = function (postalcode, countrycode) {
//    var self = this;
//    $.ajax({
//        type: "POST",
//        url: self._Current_Url + "/_vti_bin/Posten/Cart/UIOperation.svc/RecalculateVat",
//        data: '{"postalcode":"' + postalcode + '","countrycode":"' + countrycode + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (msg) {
//            if (msg != null) {
//                var order;
//                for (i = 0; i < msg.Products.length; i++) {
//                    order = '#' + msg.Products[i].OrderNumber;
//                    $(order).find('.price-amount').html(msg.Products[i].Total + ' kr');
//                }
//                for (i = 0; i < msg.Services.length; i++) {
//                    order = '#' + msg.Services[i].OrderNumber;
//                    $(order).find('.price-amount').html(msg.Services[i].SubTotal + ' kr');
//                }
//                $('.lblBasketTotal').text(msg.TotalString);
//                $('.lblToPay').text(msg.BasketTotal);
//                $('.lblVatValue').text(msg.TotalTaxString);
//            }
//            else {
//                showPopUp(self._Cart_Modal_Error, self._Recalculate_Vat_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
//            }
//        },
//        error: function errorFunction() {
//            showPopUp(self._Cart_Modal_Error, self._Recalculate_Vat_Error_Msg, 'popup', false, 800, self._Close_PopUp_Button_Name);
//        }
//    });
//}
