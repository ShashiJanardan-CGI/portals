﻿var timer;
var process;

window.minicart = function(){};

minicart.prototype._timer;
minicart.prototype._process;
minicart.prototype._Empty_Cart_Text;
minicart.prototype._Remove_product_error_msg;
minicart.prototype._Remove_service_error_msg;
minicart.prototype._Cart_Error_Title;
minicart.prototype._Cart_Article_Label;
minicart.prototype._Close_PopUp_Button_Name = "";

minicart.prototype.init = function (Empty_Cart_Text, _remove_product_error_msg, _remove_service_error_msg, _update_product_error_msg, _showcart_msg, _hide_cart_msg, _cart_error_title, _cart_article_label, _closepopup_buttonname) {
    var self = this;
    self._Empty_Cart_Text = Empty_Cart_Text;
    self._Remove_product_error_msg = _remove_product_error_msg;
    self._Remove_service_error_msg = _remove_service_error_msg;
    self._Cart_Error_Title = _cart_error_title;
    self._Cart_Article_Label = _cart_article_label;
    self._Close_PopUp_Button_Name = _closepopup_buttonname;

    $("div.freight span a").ezpz_tooltip({
        contentId: 'minicarthelp',
        contentPosition: 'rightFollow',
        stayOnContent: true
    });

    if ($.cookie('BasketItemAdded')) {
        //Remove the cookie
        $.cookie('BasketItemAdded', null, { expires: -1, path: '/' });
        $("div.minicartnotification").show().fadeOut(4000);
    }

    //Make sure the dialog is attached to the body of the page and not inside any div.
    $('#dialog').insertAfter("#Header");

    //Load contents of minicart
    self.loadminicart();
    $('#dialog').jqm({ onShow: function (obj) {
        $('li.basket a').html('<span />' + _hide_cart_msg);
        obj.w.show();
    },
        onHide: function (obj) {
            $('li.basket a').html('<span />' + _showcart_msg);
            obj.w.hide();
            obj.o.remove();
        }
    });

    $('#GlobalCart li.basket a').click(function (e) {
        e.preventDefault();
        $('#dialog').jqmShow({ overlay: 0 });
    });


    //Attach behavior to continue shopping to close the dialog
    $('a.basketUpdate').click(function () {
        $('#dialog').jqmHide();
    });

    $('.rem-item').live("click", function () {
        var order = $(this).parent().parent();
        var parent = order.parent();
        var orderNum = order.attr("id");
        var itemtype = $(this).attr("id");

        if (itemtype == 0) {
            self.RemoveProduct(order, parent, orderNum);
        }
        else if (itemtype == 1) {
            self.RemoveService(order, parent, orderNum);
        }
    });

    $('.quantity input').live("change", function () {
        if ($(this).val() == "" || $(this).val() == "0") {
            $('.quantity input').focus();
            //show error message.
        }
    });

    $('.quantity input').live("keyup", function () {
        var order = $(this).parent().parent();
        var parent = order.parent();
        var orderNum = order.attr("id");
        var quantity = $(this).val();

        clearTimeout(timer);
        timer = setTimeout(function validate() {
            if (quantity != "" && parseInt(quantity) != 0) {
                $.ajax({
                    type: "POST",
                    url: "/_vti_bin/Posten/Cart/UIOperation.svc/UpdateProduct",
                    data: '{"productOrderNum":"' + orderNum + '","quantity":"' + quantity + '"}',
                    contentType: 'application/json; charset=utf-8',
                    success: function (msg) {
                        if (msg != null) {
                            order.attr("id", msg.OrderNumber);
                            order.find('.price span').text(msg.Total + " kr");
                            $('#carttotal').text(msg.TotalString);
                            self.showCartSummary();
                        }
                    },
                    error: function (xhr, desc, err) {
                        showPopUp(self._Cart_Error_Title, _update_product_error_msg, "popup", false, 800, self._Close_PopUp_Button_Name);
                    }
                });
            }
            else {
                //show error message.
            }
        }, 500);

    });
};

minicart.prototype.ClearMinicart = function () {
    $("#product-items").html('');
    $("#service-items").html('<li class="serv-label" style="display:none;">Services</li>');
    $("#carttotal").html("<span id='carttotal'></span>");
}

minicart.prototype.loadminicart = function () {
    var self = this;
    self.ClearMinicart();
    $.ajax({
        type: "POST",
        url: "/_vti_bin/Posten/Cart/UIOperation.svc/GetMiniCarItems",
        data: '',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {

            var prod = msg.Products;
            var serv = msg.Services;
            if (prod.length != 0) {
                $(".prod-label").show();
                $('.gridHeader').show()
                var prodtemp = '<li id="${OrderNumber}"><div class="article"><img src="${ImageUrl}"></img></div>' +
                          '<div class="description">${ArticleName}</div><div class="quantity">' +
                          '<input id="0" type="text" value="${Quantity}"></div><div class="price">' +
                          '<span>${Total} kr</span></div><div class="delete"><a id="0"href="#" class="rem-item" ></a>' +
                          '</div></li>';
                $.template("cartprodtemplate", prodtemp);
                $.tmpl("cartprodtemplate", prod).appendTo("#product-items");                
            }
            if (serv.length != 0) {
                $(".serv-label").show();
                var servtemp = '<li id="${OrderNumber}"><div class="article"><img src="${ImageUrl}"></img></div>' +
                           '<div class="description"><div class="svc-name">${ServiceName}</div>${FreeTextField1}<br />${FreeTextField2}</div><div class="quantity">' +
                           '<span>1</span></div><div class="price">' +
                           '<span>${Total}</span></div><div class="delete"><a id="1" href="#" class="rem-item" ></a>' +
                           '</div></li>';
                $.template("cartservtemplate", servtemp);
                $.tmpl("cartservtemplate", serv).appendTo("#service-items");                
            }

            $('#loadminicart .article img[src=""]').attr('src', '/publishingimages/posten/cart/common/no_image.jpg');
            $('#carttotal').text(msg.TotalString);
            var lable = self._Cart_Article_Label;
            $("#cartsummary").html('<span id=\"cartsummary\">' + msg.TotalArticles + ' ' + lable + ' | ' + msg.TotalString + '</span>');
            
        },
        error: function (xhr, desc, err) {
            alert(desc);
        }
    });

}

minicart.prototype.HideLabels = function () {
    var prod = $("#product-items").children().length;
    var serv = $("#service-items").children().length;
    
    if (prod == 0 && serv == 1)
    { $('.gridHeader').hide(); $(".prod-label").hide(); $(".serv-label").hide();}
    else if (prod == 0)
    { $('.gridHeader').hide(); $(".prod-label").hide();}
    else if (serv == 1)
    { $(".serv-label").hide(); }
}

minicart.prototype.showCartSummary = function () {
    var self = this;
    $.ajax({
        type: "POST",
        url: "/_vti_bin/Posten/Cart/UIOperation.svc/GetMiniCarItems",
        data: '',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            if (msg != null) {
                var lable = self._Cart_Article_Label;
                $("#cartsummary").html('<span id=\"cartsummary\">' + msg.TotalArticles + ' ' + lable + ' | ' + msg.TotalString + '</span>');
            }
        },
        error: function (xhr, desc, err) {
            alert(desc);
        }
    });
}

minicart.prototype.RemoveProduct = function (order, parent, orderNum) {
    var self = this;
    $.ajax({
        type: "POST",
        url: "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveProduct",
        data: '{"productOrderNumber":"' + orderNum + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            if (msg != null) {
                order.fadeOut('fast', function () {
                    $(order).remove();
                    $('#carttotal').text(msg.TotalString);
                    self.HideLabels();
                    self.showCartSummary();
                });
            }
        },
        error: function (xhr, desc, err) {
            showPopUp(self._Cart_Error_Title, self._Remove_product_error_msg, "popup", false, 800, self._Close_PopUp_Button_Name);
        }
    });
}

minicart.prototype.RemoveService = function (order, parent, orderNum) {
    var self = this;
    $.ajax({
        type: "POST",
        url: "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveService",
        data: '{"serviceOrderNumber":"' + orderNum + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            if (msg != null) {
                order.fadeOut('fast', function () {
                    $(order).remove();
                    $('#carttotal').text(msg.TotalString);
                    self.HideLabels();
                    self.showCartSummary();
                });
            }
        },
        error: function (xhr, desc, err) {
            showPopUp(self._Cart_Error_Title, self._Remove_service_error_msg, "popup", false, 800, self._Close_PopUp_Button_Name);
        }
    });
}