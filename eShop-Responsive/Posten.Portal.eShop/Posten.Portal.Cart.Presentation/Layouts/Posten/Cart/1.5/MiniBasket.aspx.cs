﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages.Legacy
{
    using System;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Utilities;
    using Microsoft.SharePoint.WebControls;

    public partial class MiniBasket : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        protected string CheckoutUrl
        {
            get { return SPUrlUtility.CombineUrl(SPContext.Current.Web.ServerRelativeUrl, "/_layouts/Posten/Cart/1.5/Checkout.aspx"); }
        }

        protected BasketContext Model
        {
            get
            {
                //remove Invoice first if there is
                if (BasketContext.Current.InvoiceFee != null)
                {
                    BasketContext.Current.RemoveInvoiceFee();
                    BasketContext.ReloadCurrent();
                }
                return BasketContext.Current;
            }
        }

        #endregion Properties

        #region Methods

        protected string GetImagePath(string file)
        {
            return "/_layouts/Posten/Cart/1.5/Images/" + file;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion Methods
    }
}