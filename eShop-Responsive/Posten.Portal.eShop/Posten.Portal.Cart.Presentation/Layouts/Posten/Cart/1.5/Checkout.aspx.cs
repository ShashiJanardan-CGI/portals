﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages.Legacy
{
    using System;
    using System.Configuration;

    using Microsoft.SharePoint.WebControls;

    public partial class CheckoutPage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CartCheckoutOverrideUrl"]))
                Response.Redirect(ConfigurationManager.AppSettings["CartCheckoutOverrideUrl"]);
        }

        #endregion Methods
    }
}