﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register TagPrefix="PostenCartControls" TagName="LegacyCheckout" Src="~/_controltemplates/Posten/Cart/1.5/LegacyCheckout.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.Legacy.CheckoutPage" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ContentPlaceHolderID="PlaceHolderMiniBasket" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderLeftNavBar" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<link href="/_layouts/Posten/Cart/1.5/Styles/basket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->

<script src="/_layouts/Posten/Cart/1.5/Scripts/jquery.tools.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/jquery-avensia-plugins.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/basket-common.js" type="text/javascript"></script>


</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
<asp:Literal runat="server" Text="posten.se" />
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
<!--<asp:Literal runat="server" Text="<% $Resources: PostenCart, CheckOut_Page_Title %>" />-->
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <PostenCartControls:LegacyCheckout runat="server" />
</asp:Content>
