﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PostenCartControls" TagName="LegacyConfirmation" Src="~/_controltemplates/Posten/Cart/1.5/LegacyConfirmation.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.Legacy.ConfirmationPage" DynamicMasterPageFile="~masterurl/custom.master" EnableViewState="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderMiniBasket" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderLeftNavBar" runat="server"></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<link href="/_layouts/Posten/Cart/1.5/Styles/basket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->
<link href="/_layouts/Posten/Cart/1.5/Styles/orderconfirmation.css" type="text/css" rel="stylesheet" />
<!--[if IE 7]>
<link href="/_layouts/Posten/Cart/1.5/Styles/orderconfirmation_ie7.css" type="text/css" rel="stylesheet" />
<![endif]-->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
<asp:Literal ID="Literal1" runat="server" Text="Bekräftelse" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
<!--<asp:Literal ID="Literal2" runat="server" Text="Bekräftelse" />-->
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <PostenCartControls:LegacyConfirmation runat="server" RequireValidChecksumUrl="true" />
</asp:Content>
