﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.Cart.BusinessEntities" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MiniBasket.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.Legacy.MiniBasket" %>

<div id="minibasketlineitems" class="basket-item-wrapper">
    <div class="inner-wrapper">
        <div class="basket-item-container">
            <div class="top-border"></div>
            <div class="left-border">
                <ul class="cart-items">

    <% if (this.Model.ProductOrderItems.Count > 0) {%>
        <% foreach (IProductOrderItem item in this.Model.ProductOrderItems) { %>
        <li class="product">
            <div class="info">
                <h4><%= item.ArticleName %></h4>
                <p class="description">Art # <%= item.ArticleId %></p>
            </div>
            <div class="price-container">
                <a class="button-delete" data-orderno="<%= item.OrderNumber %>" rel="#basket_modal_delete_confirm" href="#" title="Ta bort">
                    <img src="<%= this.GetImagePath("icon-delete.gif") %>" alt="Ta bort" />
                </a>
                <div class="h4title"><span style="float:left;"><%= item.Quantity %>&nbsp;st&nbsp;</span><%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </li>
        <% } %>
    <% } %>
    <% if (this.Model.ServiceOrderItems.Count > 0) { %>
        <% foreach (IServiceOrderItem item in this.Model.ServiceOrderItems) { %>
        <li class="product">
            <% if (!string.IsNullOrEmpty(Utils.ToSafeString(item.ServiceImageUrl))) { %>
                <img class="product-image" src="<%= Utils.ToSafeString(item.ServiceImageUrl) %>" alt="<%= item.Name%>" />
            <% } %>
            <div class="info">
                <h4><%= item.Name %></h4>
                <p class="description"><%= item.FreeTextField1 %> <%= item.FreeTextField2 %></p>
            </div>
            <div class="price-container">
                <a class="button-delete" data-orderno="<%= item.OrderNumber %>" rel="#basket_modal_delete_confirm" href="#" title="Ta bort">
                    <img src="<%= this.GetImagePath("icon-delete.gif") %>" alt="Ta bort" />
                </a>
                <% if (!string.IsNullOrEmpty(item.EditUrl)) { %>
                    <a class="button-edit" data-orderno="<%= item.OrderNumber %>" href="<%= string.Format(item.EditUrl, item.OrderNumber,  Request.UrlReferrer != null ? Server.UrlEncode(Request.UrlReferrer.ToString()) : "/") %>" title="&Auml;ndra">
                        <img src="<%= this.GetImagePath("icon-edit.gif") %>" alt="&Auml;ndra" />
                    </a>
                <% } %>
                <div class="h4title"><%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </li>
        <% } %>
    <% } %>

                </ul>
            </div>
        </div>
    </div>
</div>
<div id="minibasketpanel" class="minibasket-container">
    <table class="minibasket-inner-container">
        <tbody>
            <tr>
                <td class="minibasket-spacer">
                </td>
                <td>
                    <div class="minibasket-separator">
                    </div>
                    <a id="expandLineItemTable"><span class="arrow-right-white">&nbsp;</span> <span id="minibasket_show">
                        Visa varukorg:</span> <span class="minibasket-articles"><span id="minibasket_numberofiteminbasket"><%= this.Model.NumberOfArticle %></span>&nbsp;artiklar </span><span class="clear"></span></a>
                    <div class="minibasket-separator">
                    </div>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <span class="minibasket-buttons basket-commonstyles"><a id="CheckoutButton" href="<%= this.CheckoutUrl %>"
                        class="button blue"><span><strong>Till kassan</strong></span> </a></span><span class="payment-amount">
                            <%= Utils.FormatCurrency(this.Model.TotalValue, "SEK") %>
                        </span><span class="payment-sum">Totalt:</span>
                        <!--<span>BasketId: <%= this.Model.BasketId %></span>-->
                    <div class="clear">
                    </div>
                </td>
                <td class="minibasket-spacer">
                    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
</div>
