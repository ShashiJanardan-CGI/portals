﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.Legacy.ErrorPage" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ContentPlaceHolderID="PlaceHolderMiniBasket" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<link href="/_layouts/Posten/Cart/1.5/Styles/basket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Payment Error
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
Payment Error
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div class="basket-styles">
        <h1>Error</h1>
        <asp:Label ID="ErrorMessage" runat="server" />

        <p>Go to checkout page and try again...</p>
        <p>Contact support...</p>

    </div>

</asp:Content>
