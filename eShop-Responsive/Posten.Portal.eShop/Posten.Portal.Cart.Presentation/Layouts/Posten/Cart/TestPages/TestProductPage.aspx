﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.Cart.BusinessEntities" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestProductPage.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.TestPages.TestProductPage" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ID="Content9" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.cookie("eshopUrl", document.URL, { path: '/' });
        });
    </script>
    <div>
        <p>
            <asp:Label ID="lblAddProductOrder" runat="server" Text="Add Product Order" Font-Bold="true" />
        </p>
        <p>
            <asp:Label ID="lblLineItems" AssociatedControlID="txtLineItems" runat="server" Text="Quantity:"></asp:Label><br />
            <asp:TextBox ID="txtLineItems" runat="server" Text="1"></asp:TextBox>
            <asp:RadioButtonList ID="rbProduct" runat="server">
                <asp:ListItem Selected="True" Value="Välgörenhetsfrimärke"></asp:ListItem>
                <asp:ListItem Value="Naturkraft"></asp:ListItem>
                <asp:ListItem Value="Tecknade serier"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <div>
            <asp:Button ID="btnAddProductOrder" runat="server" Text="Add Product Order to Basket" Width="250px"
                OnClick="btnAddProductOrder_Click" />
        </div>
        <br />
        <p>
            <asp:Label ID="lblUpdateProductOrder" runat="server" Text="Update Product Order"
                Font-Bold="true" />
        </p>
        <asp:Label ID="lblProductOrderNo" runat="server" Text="Product Order No: " /><br />
        <asp:TextBox ID="txtProductOrderNo" runat="server" />
        <br />
        <br />
        <div>
            <asp:Button ID="btnUpdateProductOrder" runat="server" Text="Update Product Order from Basket" Width="250px"
                OnClick="btnUpdateProductOrder_Click" />
        </div>
        <br />
        <p>
            <asp:Label ID="lblRemoveProductOrder" runat="server" Text="Remove Product Order"
                Font-Bold="true" />
        </p>
        <div>
            <asp:Button ID="btnRemoveProductOrder" runat="server" Text="Remove Product Order from Basket" Width="250px"
                OnClick="btnRemoveProductOrder_Click" />
        </div>
        <asp:Panel ID="pnlProductErrors" runat="server" Style="padding: 7px; background-color: #ff3030;
            color: #121212;">
            <asp:Label ID="lblProductErrorMessage" runat="server" />
            <p>
        </asp:Panel>
        <h3>
            Basket</h3>
        <table>
            <asp:Repeater ID="uiProductLineItems" runat="server">
                <HeaderTemplate>
                    <tr align="center">
                        <th>
                            OrderNo
                        </th>
                        <th>
                            Product Name
                        </th>
                        <th>
                            Quantity
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr align="center">
                        <td>
                            <%# Eval("OrderNumber") %>
                        </td>
                        <td>
                            <%# Eval("ArticleName") %>
                        </td>
                        <td>
                            <%# Eval("Quantity") %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>
