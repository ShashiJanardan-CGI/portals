﻿namespace Posten.Portal.Cart.Presentation.TestPages
{
    using System;

    using Microsoft.SharePoint.WebControls;

    using Posten.Portal.Cart.BusinessEntities;

    public partial class TestProductPage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Methods

        protected void btnAddProductOrder_Click(object sender, EventArgs e)
        {
            var numberOfItems = int.Parse(txtLineItems.Text);
            var order = GetProductOrder(rbProduct.SelectedValue, numberOfItems);

            var orderNo = BasketContext.Current.AddProductOrder(order);
            txtProductOrderNo.Text = orderNo;

            this.BindBasketLineItems();
        }

        protected void btnRemoveProductOrder_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtProductOrderNo.Text))
            {
                try
                {
                    BasketContext.Current.RemoveProductOrder(txtProductOrderNo.Text.Trim());
                    txtProductOrderNo.Text = string.Empty;
                }
                catch (Exception exception)
                {
                    this.OutputErrorMessage(exception.Message + Environment.NewLine + exception.StackTrace);
                }

                this.BindBasketLineItems();
            }
        }

        protected void btnUpdateProductOrder_Click(object sender, EventArgs e)
        {
            var numberOfItems = int.Parse(txtLineItems.Text);
            if (!string.IsNullOrEmpty(txtProductOrderNo.Text) && numberOfItems > -1)
            {
                var order = GetProductOrder(rbProduct.SelectedValue, numberOfItems);
                order.ArticleName += " - Updated";

                try
                {
                    BasketContext.Current.UpdateProductOrder(txtProductOrderNo.Text.Trim(), order);
                    txtProductOrderNo.Text = string.Empty;
                }
                catch (Exception exception)
                {
                    this.OutputErrorMessage(exception.Message + Environment.NewLine + exception.StackTrace);
                }

                this.BindBasketLineItems();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlProductErrors.Visible = false;
            this.BindBasketLineItems();
        }

        private static IProductOrderItem GetProductOrder(string name, int qty)
        {
            if (name.Equals("Välgörenhetsfrimärke"))
            {
                return new ProductOrderItem
                {
                    ArticleName = name,
                    ArticlePrice = 70m,
                    ArticleId = "123-ABC",
                    Vat = qty * (70m * .25m),
                    VatPercentage = .25m,
                    Quantity = qty,
                    CurrencyCode = "SEK",
                    Taxcode = "U1"
                };
            }

            if (name.Equals("Naturkraft"))
            {
                return new ProductOrderItem
                {
                    ArticleName = name,
                    ArticlePrice = 60m,
                    ArticleId = "456-DEF",
                    Vat = qty * (60m * .25m),
                    VatPercentage = .25m,
                    Quantity = qty,
                    CurrencyCode = "SEK",
                    Taxcode = "U1"
                };
            }

            // default value
            return new ProductOrderItem
            {
                ArticleName = name,
                ArticlePrice = 44m,
                ArticleId = "789-GHI",
                Vat = qty * (44m * .25m),
                VatPercentage = .25m,
                Quantity = qty,
                CurrencyCode = "SEK",
                Taxcode = "U3"
            };
        }

        private void BindBasketLineItems()
        {
            uiProductLineItems.DataSource = BasketContext.Current.ProductOrderItems;
            uiProductLineItems.DataBind();
        }

        private void OutputErrorMessage(string errorMessage)
        {
            pnlProductErrors.Visible = true;
            lblProductErrorMessage.Text = errorMessage;
        }

        #endregion Methods
    }
}