﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Posten.Portal.Cart.Presentation.WebParts.EditCart
{
    [ToolboxItemAttribute(false)]
    public class EditCart : WebPart
    {
        private Control _userControl = null;
        private const string ascx = "~/_CONTROLTEMPLATES/Posten/Cart/2.0/EditCart.ascx";

        protected override void CreateChildControls()
        {
            
            base.CreateChildControls();
            _userControl = Page.LoadControl(ascx) as ControlTemplates.EditCart;

            Controls.Add(_userControl);
        }  
    }
}
