﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Posten.Portal.Cart.BusinessEntities;

namespace Posten.Portal.Cart.Presentation.ControlTemplates.Legacy
{
    public partial class LegacyCheckout : CheckoutBase
    {
        
        protected override string LayoutVersion
        {
            get { return "1.5"; }
        }

        protected override HiddenField BasketIdControl
        {
            get { return this.BasketToCheckout; }
        }

        protected override PaymentMethodType PaymentMethod
        {
            get { return PaymentMethodType.PayPage; }
        }

        protected string TermsOfServiceUrl
        {
            get { return "http://www.posten.se/ptm/ptm_do.jsp?action=getConditionListCash"; }
        }

        protected string TermsOfSaleUrl
        {
            get { return "http://www.posten.se/img/cmt/PDF/villkor_butiken_pse.pdf"; }
        }

        protected override int Layout
        {
            get { return 1; }
        }

        protected override IBillingAccountInfo BillingInfo
        {
            get
            {
                return new BillingAccountInfo
                    {
                        Email = EmailTextBox.Text,
                        AddressLine = AddressLineTextBox.Text,
                        City = CityTextBox.Text,
                        Name = NameTextBox.Text,
                        Organization = OrganizationTextBox.Text,
                        PostalCode = PostalCodeTextBox.Text,
                        IncludeAddressInfo = FullInfoCheckBox.Checked
                    };
            }
        }

        protected override IDeliveryAccountInfo DeliveryInfo
        {
            get
            {
                return new DeliveryAccountInfo()
                {
                    Email = EmailTextBox.Text,
                    AddressLine = AddressLineTextBox.Text,
                    City = CityTextBox.Text,
                    PostalCode = PostalCodeTextBox.Text, 
                    CountryCode="SE"
                    
                }; 
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["Message"]))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorMessage", 
                    string.Format("PostenBasket.ShowPaymentErrorMessagePopUp('{0}');", 
                    Request.QueryString["Message"]),
                    true);
            }
        }

        protected override void ShowErrorMessage(string message)
        {
            this.Response.Redirect(this.Request.RawUrl + "?Message=" + HttpUtility.UrlEncode(message));
        }

        protected override bool IsOrganization
        {
            get { return false; }
        }

        protected override string SocialSecurityNumber
        {
            get { return string.Empty; }
        }

        protected override string FirstName
        {
            get { return string.Empty; }
        }

        protected override string LastName
        {
            get { return string.Empty; }
        }

        protected override string BillingCountry
        {
            get { return "SE"; }
        }

        protected override string GetCurrentUrl
        {
            get { throw new NotImplementedException(); }
        }
    }
}
