﻿using System;
using System.Web.UI;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace Posten.Portal.Cart.Presentation.ControlTemplates.Legacy
{
    public partial class MiniBasket : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ServerRelativeUrl.Text = SPUrlUtility.CombineUrl(SPContext.Current.Web.ServerRelativeUrl, "/");
        }
    }
}
