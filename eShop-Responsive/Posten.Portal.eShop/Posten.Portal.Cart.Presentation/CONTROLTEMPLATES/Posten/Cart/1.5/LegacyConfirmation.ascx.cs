﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Collections.Generic;
using Posten.Portal.Cart.BusinessEntities;

namespace Posten.Portal.Cart.Presentation.ControlTemplates.Legacy
{
    public partial class LegacyConfirmation : ConfirmationBase
    {
        protected string TermsOfServiceUrl
        {
            get { return "http://www.posten.se/ptm/ptm_do.jsp?action=getConditionListCash"; }
        }

        protected string TermsOfSaleUrl
        {
            get { return "http://www.posten.se/img/cmt/PDF/villkor_butiken_pse.pdf"; }
        }

        protected override void ShowErrorMessage(string message)
        {
            // TODO: implement
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (this.OrderContext != null && this.OrderContext.ServiceOrderItems.Count > 0)
            {
                var host =  Request.Url;//For WebSeal, get from Applicationconfig. Request.Url;
                var orders = new List<IServiceOrderItem>();
                foreach (var order in this.OrderContext.ServiceOrderItems){


                    if (order.ConfirmationUrl == null)
                    {
                        // check if skicka, if it is set the confirmation url.
                        if (order.ApplicationName== "Skicka")
                        {
                            var temporaryurl = "http://skicka/skicka/skicka.confirmation?bookingid=" +
                                               this.OrderContext.BookingId;
                            order.ConfirmationUrl = new Uri(temporaryurl);
                            
                            //only add one per applicaiton
                            if (orders.Find(x=> x.ApplicationName == order.ApplicationName) == null)
                                orders.Add(order);
                        }

                    }
                    else
                    {
                        //only add one per application
                        if (orders.Find(x => x.ApplicationName == order.ApplicationName) == null)
                            orders.Add(order);
                    }

                }

                this.ServiceOrders.DataSource = orders;  
                this.ServiceOrders.DataBind();
            }
           

        }
    }
}
