﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PostenCartControls" TagName="LegacyCheckoutBasket" Src="~/_controltemplates/Posten/Cart/1.5/CheckoutBasket.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegacyCheckout.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.Legacy.LegacyCheckout" %>

<div class="basket-styles">
    <asp:PlaceHolder ID="ErrorMessagePlc" runat="server"></asp:PlaceHolder>
    <h1>Kassan</h1>
    <h2>Din best&auml;llning</h2>
    <PostenCartControls:LegacyCheckoutBasket runat="server" />

    <h2>Dina uppgifter</h2>
    <div id="customer-info">
        <div class="full-info">
            <asp:CheckBox ID="FullInfoCheckBox" runat="server" /><asp:Label AssociatedControlID="FullInfoCheckBox" runat="server">&nbsp;Vill du ha dina uppgifter på kvittot?</asp:Label>
        </div>
        <ul class="customer-form customer-form-bright">
            <li>
                <asp:Label runat="server" AssociatedControlID="NameTextBox" Text="Namn" />
                <div>
                    <asp:TextBox ID="NameTextBox" runat="server" CssClass="enablestate"></asp:TextBox>
                </div>
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="OrganizationTextBox" Text="Företag" />
                <div>
                    <asp:TextBox ID="OrganizationTextBox" runat="server" CssClass="enablestate" />
                </div>
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="AddressLineTextBox" Text="Gatuadress" />
                <div>
                    <asp:TextBox ID="AddressLineTextBox" runat="server" CssClass="enablestate" />
                </div>
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="PostalCodeTextBox" Text="Postnummer" />
                <div>
                    <asp:TextBox ID="PostalCodeTextBox" runat="server" CssClass="postalcode enablestate" Columns="5" MaxLength="5" />
                </div>
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="CityTextBox" Text="Postort" />
                <div>
                    <asp:TextBox ID="CityTextBox" runat="server" CssClass="city enablestate" />
                </div>
            </li>
        </ul>
        <ul class="customer-form">
            <li>
                <asp:Label runat="server" AssociatedControlID="EmailTextBox" Text="E-postadress" />
                <div class="input-text customer-email">
                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Eval("Email") %>' CssClass="enablestate" />
                </div>
                <asp:RequiredFieldValidator ID="EmailValidator" ControlToValidate="EmailTextBox"
                    CssClass="error-message" Display="Dynamic" runat="server" ErrorMessage="Ange e-postadress" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="EmailTextBox"
                    CssClass="error-message" Display="Dynamic" runat="server" ErrorMessage="Ogiltig e-postadress"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
            </li>
            <li>Kvitto på ditt köp skickas till din e-postadress</li>
        </ul>
    </div>

    <!-- ACCEPT AGREEMENT -->
    <div id="customer-agreement">
        <asp:CheckBox ID="AcceptTermsCheckBox" runat="server" Text="Jag accepterar " /><a href="<%= this.TermsOfServiceUrl %>" target="_blank">tjänstevillkor</a>
        och <a href="<%= this.TermsOfSaleUrl %>" target="_blank">köpvillkor</a>
        <asp:CustomValidator ID="AcceptTermsValidator" runat="server" Display="Dynamic" ErrorMessage="<br />Du måste acceptera villkoren" ClientValidationFunction="ValidateAcceptTerms"></asp:CustomValidator>
    </div>
    <!-- // ACCEPT AGREEMENT -->

    <!-- SUBMIT BUTTON -->
    <asp:LinkButton runat="server" ID="SubmitButton" OnClick="ProcessCheckout" CssClass="basket-button button-checkout-proceed basket-button-disabled"><span>Forts&auml;tt till betals&auml;tt</span></asp:LinkButton>
    <asp:Label ID="StatusLabel" runat="server" Text="" ForeColor="Red"></asp:Label>
    <div class="clear"></div>
    <!-- // SUBMIT BUTTON -->

    <p class="payment-options-info">Du kan betala med kort eller internetbank.</p>
</div>

<asp:HiddenField ID="BasketToCheckout" runat="server" />
<script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/jquery.numeric.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    function ValidateAcceptTerms(sender, args) {
        args.IsValid = $('#<%= this.AcceptTermsCheckBox.ClientID %>').is(':checked');
    }

    jQuery(function ($) {
        var btn = $('#<%= this.SubmitButton.ClientID %>');
        if (false) {
            btn.addClass('basket-button-disabled');
            btn.click(function (e) { e.preventDefault(); e.stopPropagation(); return false; });
        }
        $('#<%= this.FullInfoCheckBox.ClientID %>').click(function (e) { $('ul.customer-form-bright').toggle(); });
        setformvaluesfromcookie();
    });

    $(".enablestate").focusout(function () {
        if (this.value != null) $.cookie(this.id, this.value, { path: "/", expires: 5 });
        else $.cookie(this.id, null);
    });

    $("#<%=PostalCodeTextBox.ClientID %>").focusout(function () {
        setCity(this.value);
    });

    $("input.postalcode").numeric();
    var currentDeleteButton = null;
    $("#s4-bodyContainer").append(PostenBasket.BasketModalTemplate);

    $(".button-delete").overlay({
        color: '#000000',
        loadSpeed: 200,
        opacity: 0.9,
        closeOnClick: false
    });

    $('.button-delete').click(function (e) {
        e.preventDefault();
        currentDeleteButton = this;
        /*

        */
    });

    $("body").click(function (event) {
        //Check first if this is an error message

        var isButtonDelete = $(event.target).is_or_hasparent(".basket-modal-ok");
        if (isButtonDelete) {

            if (window.location.search.match(/message/i)) {
                //reload the page without query string.
                window.location.href = window.location.pathname;
                return;
            }
            var orderno = $(currentDeleteButton).data("orderno");
            var child = $(currentDeleteButton).parent().parent();
            var type = $(currentDeleteButton).attr("name");
            var orderno = $(currentDeleteButton).attr("data-orderno");

            if (type == "0") {
                RemoveProduct(orderno, child);
            }
            else if (type == "1") {
                RemoveService(orderno, child);
            }

            return;
        }
    });

    //remove empty cart message if there is contents and also enable the chekout button
    if ($(".products").children().length != 0 || $(".services").children().length != 0) {
        $("p.empty-message").hide();
        $("a.basket-button").removeClass("basket-button-disabled");
    }
    else {
        //remove postback option when empty.
        $("a.basket-button-disabled").attr("href", "#");
    }

    function RemoveService(num, child) {
        $.ajax({
            type: "POST",
            url: "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveService",
            data: '{"serviceOrderNumber":"' + num + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                if (msg != null) {
                    child.parent().remove();
                    HideLabels("1");
                    RemoveItem(msg.TotalString, msg.TotalTaxString);
                }
            },
            error: function (xhr, desc, err) {
                alert(desc);
            }
        });
    }

    function RemoveProduct(num, child) {
        $.ajax({
            type: "POST",
            url: "/_vti_bin/Posten/Cart/UIOperation.svc/RemoveProduct",
            data: '{"productOrderNumber":"' + num + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                if (msg != null) {
                    child.parent().remove();
                    HideLabels("0");
                    RemoveItem(msg.TotalString, msg.TotalTaxString);
                }
            },
            error: function (xhr, desc, err) {
                alert(desc);
            }
        });
    }
    function RemoveItem(total, vat) {
        $(".title").text(total);
        $("#payment-sum .vat").text("(varav moms " + vat + ")");
    }

    function HideLabels(type) {
        var productempty, serviceempty;
        if ($(".products").children().length == 1 && type == "0") {
            $(".products").children().remove();
            productempty = true;
        }
        else if ($(".services").children().length == 1 && type == "1") {
            $(".services").children().remove();
            serviceempty = true;
        }

        //Set message when everything is empty and disable checkout button
        if ($(".products").children().length == 0 && $(".services").children().length == 0) {
            //Show empty cart message
            $("p.empty-message").show();
            $("a.basket-button").addClass("basket-button-disabled");
            $("a.basket-button-disabled").attr("href", "#");
        }
    }

    function setformvaluesfromcookie() {
        var fields = $(".enablestate");
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if ($.cookie(field.id) != null) field.value = $.cookie(field.id);
        }
    }

    function setCity(postalcode) {
        var ziplength = 5;
        if (postalcode.length <= ziplength && postalcode != "") {
            $.ajax({
                type: "POST",
                url: "/_vti_bin/Posten/Cart/UIOperation.svc/GetLocation",
                data: '{"postalcode":"' + postalcode + '","countrycode":"se" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != null) {
                        $("#<%=CityTextBox.ClientID %>").val(msg.city);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

    }
</script>