﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartConfiguration.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.CartConfiguration" %>
<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
    Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>

<div class="cart-configuration">
        <div class="payment-type">
            <div class="label">
                <asp:Label runat="server" ID="DefaultPaymentLabel" Text="Default Payment Type:" /></div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="DefaultPaymentList">
                    <asp:ListItem Text="Card" Value="CreditCard"></asp:ListItem>
                    <asp:ListItem Text="Invoice" Value="Invoice"></asp:ListItem>
                    <asp:ListItem Text="Bank" Value="OnlineBanking"></asp:ListItem>
                </asp:DropDownList></div>                      
            <div class="payment-description">
                <asp:Literal ID="Literal3" runat="server" Text="Set the default selected payment type on the checkout page."></asp:Literal></div>
        </div>
        <div class="invoice-option">            
            <div class="label">
                <asp:Label ID="InvoiceOptionLabel" runat="server" Text="Invoice Option:"/></div>
            <div class="text-box">
                <asp:DropDownList runat="server" ID="InvoiceOptionList">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                </asp:DropDownList></div>
            <div class="invoice-description">
                <asp:Literal ID="Literal1" runat="server" Text="Disable Invoice Option for anonymous users." /></div>
        </div>
        <div>
            <div class="label">Private Max Invoice Amount:</div>
            <div class="text-box"><asp:TextBox ID="PrivMaxAmountTextBox" runat="server"></asp:TextBox></div>
            <div class="invoice-description"></div>
        </div>
        <div>
            <div class="label">Company Max Invoice Amount:</div>
            <div class="text-box"><asp:TextBox ID="CompMaxAmountTextBox" runat="server"></asp:TextBox></div>
            <div class="invoice-description"></div>
        </div>




        <div class="save-type"><PostenWebControls:PostenButton ID="SaveDefaultPayment" OnClick="Save_Settings" runat="server" Text="Save" /></div>  
        <div class="save-message"><asp:Literal ID="SaveMessageText" runat="server" Text="Updates have been saved" Visible="false" /></div>
</div>

<style type="text/css">
.header-title
{
    float:left;
    width:100%;
    margin: 5px 5px 10px 10px;
    font-size: 16px;    
    font-weight: bold;
}
.cart-configuration
{
    margin: 10px;    
}
.cart-configuration .payment-type,
.cart-configuration .invoice-option
{
    float: left;
    width: 100%;
    margin: 10px 0;
}
.cart-configuration .payment-type .label,
.cart-configuration .invoice-option .label
{
    float: left;
    width: 170px;
    font-weight: bold;
}
.cart-configuration .payment-type .text-box,
.cart-configuration .invoice-option .text-box
{
    float: left;    
    width: 130px;
}
.cart-configuration .payment-type .text-box select,
.cart-configuration .invoice-option .text-box select
{
    width: 130px;
}
.cart-configuration .payment-type .payment-description,
.cart-configuration .invoice-option .invoice-description
{
    width: 100%;
    float: left;
    font-style: italic;
}
.cart-configuration .save-type,
.cart-configuration .save-message
{
    float: left;
    width: 100%;
    margin-top: 10px;
}
</style>