﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Posten.Portal.Cart.BusinessEntities;
using System.Configuration;
using Microsoft.SharePoint;

namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    public partial class EditCart : UserControl
    {

        protected BasketContext BasketContext
        {
            get
            {
                return BasketContext.GetContext(new Guid(this.BasketIdControl.Value));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                this.BasketIdControl.Value = BasketContext.Current.BasketId.ToString();

            }
            this.PopulateFields(this.BasketContext.BasketItem, this.BasketContext.ProductOrderItems, this.BasketContext.ServiceOrderItems);

        }

        public void PopulateFields(IBasketItem basketItem, ICollection<IProductOrderItem> products, ICollection<IServiceOrderItem> services)
        {


            if (products.Count == 0 && services.Count == 0 && basketItem.OrderSummaryItems.Count == 0)
            {
                this.LabelControl1.Visible = false;
                this.LabelControl2.Visible = false;
                this.emptyCartControl.Attributes.Add("style", "display:block;");
            }
            else if (products.Count == 0)
            {
                this.LabelControl1.Visible = false;
            }
            else if (services.Count == 0)
            {
                this.LabelControl2.Visible = false;
            }

            // populate repeaters.
            this.ProductsControl.DataSource = products;
            this.ProductsControl.DataBind();
            this.ServicesControl.DataSource = services;
            this.ServicesControl.DataBind();
        }
        
        public string FormatEditUrl(object editurl, string ordernumber, string changeservicelabel)
        {
            string linkEditUrl = string.Empty;
            if (editurl != null)
            {
                if (!string.IsNullOrEmpty(editurl.ToString()))
                {
                     linkEditUrl = string.Format("<a class=\"change\" href=\"{0}\">{1} <img src='/_layouts/images/Posten/Cart/edit-arrow.png' /></a>", editurl.ToString()+ordernumber, changeservicelabel);
                }
            }
            return linkEditUrl;
        }
    }
}