﻿namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;

    public partial class CartConfiguration : CartConfigurationBase
    {
        #region Properties

        protected override string DefaultPaymentValue
        {
            get { return this.DefaultPaymentList.SelectedValue; }
        }

        protected override string InvoicePaymentValue
        {
            get { return this.InvoiceOptionList.SelectedValue; }
        }

        protected override bool ShowSaveMessage
        {
            set { this.SaveMessageText.Visible = value; }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var listItem = this.GetItems;

                this.DefaultPaymentList.SelectedValue = listItem.defaultpayment;
                this.InvoiceOptionList.SelectedValue = listItem.invoiceoption;
            }
        }

        #endregion Methods
    }
}