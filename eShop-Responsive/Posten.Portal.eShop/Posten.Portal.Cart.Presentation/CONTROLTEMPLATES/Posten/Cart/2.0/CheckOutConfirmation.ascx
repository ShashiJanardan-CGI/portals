﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>

<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckOutConfirmation.ascx.cs"
    Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.CheckOutConfirmation" %>
<link href="/_layouts/Posten/Cart/Presentation/styles/Cart.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/Presentation/styles/common/common.css" type="text/css"
    rel="stylesheet" />
<link href="/_layouts/Posten/Cart/Presentation/styles/webshop_confirm.css" type="text/css"
    rel="stylesheet" />

<SharePoint:ScriptLink runat="server" ID="ScriptLink2" Name="/Posten/Cart/Presentation/js/common/jquery.ba-postmessage.min.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten/Cart/Presentation/js/common/PostenCosIFrame.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink3" Name="/Posten/Cart/Presentation/js/jqModal.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink4" Name="/Posten/Cart/Presentation/js/confirmation.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink5" Name="/Posten/Cart/Presentation/js/common/core.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>
<SharePoint:ScriptLink runat="server" ID="ScriptLink6" Name="/Posten/Cart/Presentation/js/common/jquery.cookie.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="box-1" class="box colspan-4-5">
    <div class="module webshopConfirmation headerPrimary footerSolid">
        <h3 class="theme">
            <asp:Literal ID="ltrlConfirmationTitle" runat="server" Text='<%$CartCode: Utils.GetResourceString("Confirmation_Page_Title")%>'></asp:Literal>
        </h3>
        <div class="highlight">
            <div class="p1">
                <%= Utils.GetResourceString("Confirmation_Thank_You_Label")%></div>
            <div class="p2">
                <%= Utils.GetResourceString("Confirmation_Your_Order_Number_Label")%>:&nbsp;<asp:Label ID="lblOrderNumber" runat="server" CssClass="theme"></asp:Label></div>
            <div class="p2">
                <asp:Label ID="lblTransactionIDDescription" runat="server"></asp:Label><asp:Label ID="lblTransactionId" runat="server" CssClass="theme"></asp:Label>
            </div>
            <div class="p3">
                <%= Utils.GetResourceString("Confirmation_Order_Link_1_Label")%>
                <asp:HyperLink ID="linkReceiptURL" runat="server" Target="_blank"><%= Utils.GetResourceString("Confirmation_Order_Link_Label")%></asp:HyperLink>
                <asp:Literal ID="Confirmation_Order_Link_2_Label" runat="server" Text='<%$CartCode: Utils.GetResourceString("Confirmation_Order_Link_2_Label")%>'></asp:Literal>
            </div>
        </div>
        <div class="contentConfirmation">
            <div class="header" >
                <span><%= Utils.GetResourceString("Confirmation_Your_Order_Label")%></span>
                <div class="control">
                    <%if (this.pnlYourNextStep.Visible == true) {%>
                        <a class="minimizeCollapse" href="#"><%= Utils.GetResourceString("Confirmation_Hide_Label")%><span></span></a> 
                        <a class="maximizeCollapse" href="#"><%= Utils.GetResourceString("Confirmation_View_Label")%><span></span></a>
                    <%} else { %>
                        <a class="minimize" href="#"><%= Utils.GetResourceString("Confirmation_Hide_Label")%><span></span></a> 
                        <a class="maximize" href="#"><%= Utils.GetResourceString("Confirmation_View_Label")%><span></span></a>
                    <%}%>
                </div>
            </div>
            
            <%if (this.pnlYourNextStep.Visible == true) {%>
                <div class="bodyCollapse">
            <%} else { %>
                <div class="body">
            <%}%>
                <table class="divider">
                    <tbody>
                        <asp:Panel ID="pnlProduct" runat="server">
                            <!--product label-->
                            <tr">
                                <th class="article" colspan="2">
                                    <%= Utils.GetResourceString("Cart_Product_Label")%>
                                </th>
                                <th class="quantity">
                                    <%= Utils.GetResourceString("Cart_Price_Label")%>
                                </th>
                                <th class="price">
                                    <%= Utils.GetResourceString("Cart_Price_Per_Label")%>
                                </th>
                                <th class="sum">
                                    <%= Utils.GetResourceString("Cart_Total_Price_Label")%>
                                </th>
                            </tr>
                            <asp:Repeater ID="ProductsControl" runat="server" EnableViewState="true">
                                <ItemTemplate>
                                <tr>
                                    <td class="image">
                                        <img alt="Bild" src="<%# getOrderImageUrl((string)Eval("OrderNumber")) %>">
                                    </td>
                                    <td>
                                        <span class="title"><a class="link" href="#">
                                            <%# DataBinder.Eval(Container.DataItem, "ArticleName") %></a></span> <span class="articleID">
                                                Art.#
                                                <%# DataBinder.Eval(Container.DataItem, "ArticleId")%></span> <span class="zone">
                                                    <%# DataBinder.Eval(Container.DataItem, "CurrencyCode")%></span>
                                    </td>
                                    <td class="quantity">
                                        <%# DataBinder.Eval(Container.DataItem, "Quantity")%>
                                    </td>
                                    <td class="price">
                                        <%# DataBinder.Eval(Container.DataItem, "ArticlePricewithVAT", "{0:N}")%> kr
                                    </td>
                                    <td class="sum">
                                        <%# DataBinder.Eval(Container.DataItem, "Total", "{0:N}")%> kr
                                    </td>
                                </tr>
                            </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                        <asp:Panel ID="pnlService" runat="server">
                            <!--service label-->
                            <tr>
                                <th class="article" colspan="2">
                                    <%= Utils.GetResourceString("Cart_Service_Label")%>
                                </th>
                                <th class="quantity">
                                    <%= Utils.GetResourceString("Cart_Price_Label")%>
                                </th>
                                <th class="price">
                                    <%= Utils.GetResourceString("Cart_Price_Per_Label")%>
                                </th>
                                <th class="sum">
                                    <%= Utils.GetResourceString("Cart_Total_Price_Label")%>
                                </th>
                            </tr>
                            <!--START: Produce Service details for Service goes here. This is yet to be tested
                            <tr>
                                <td colspan="5">
                                    <asp:Literal ID="ltrlServiceImageURL" runat="server"></asp:Literal><br />
                                    <asp:Literal ID="ltrlServiceDetail" runat="server"></asp:Literal>
                                </td>
                            </tr>-->
                            <asp:Repeater ID="ServicesRepeater" runat="server" EnableViewState="true">
                                <ItemTemplate>
                                <tr>
                                    <td class="image">
                                        <img alt="Bild" src="<%# getOrderImageUrl((string)Eval("OrderNumber")) %>">
                                    </td>
                                    <td>
                                        <span class="title">
                                            <%# Eval("Name")%>
                                        </span> 
                                        <span class="articleID"><%# Eval("FreeTextField1")%><br /><%# Eval("FreeTextField2")%></span> 
                                    </td>
                                    <td class="quantity">1</td>
                                    <td class="price"><%# Eval("Total", "{0:N}")%> kr</td>
                                    <td class="sum"><%# Eval("Total", "{0:N}")%> kr</td>
                                </tr>
                            </ItemTemplate>
                            </asp:Repeater>
                            <!--END: Produce Service details for Service goes here-->
                            <!-- Service Orders Repeater
                            <asp:Repeater ID="ServicesControl" runat="server" EnableViewState="true">
                                <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
							                <td class="image"><img alt="Bild" src="content/basket_1.png"></td>
							                <td>
								                <span class="title"><a class="link" href="#"><%# Eval("Name") %></a></span>
								                <span class="articleID"><%# DataBinder.Eval(Container.DataItem, "Details")%></span>
								                <span class="zone">Inrikes</span>
							                </td>
							                <td class="quantity"><%# Eval("Quantity")%></td>
							                <td class="price"><%# Eval("LinePriceString")%></td>
							                <td class="sum"><%# Eval("ListPrice")%></td>
						                </tr>

                                    </ItemTemplate>
                            </asp:Repeater>-->
                        </asp:Panel>
                    </tbody>
                </table>
            </div>
            
            <%if (this.pnlYourNextStep.Visible == true) {%>
                <div class="bodyCollapse summary">
            <%} else { %>
                <div class="body summary">
            <%}%>
                <div class="buttons">
                    <span style="display: none">
                        <button>
                            <%= Utils.GetResourceString("Confirmation_Print_Label")%></button></span>
                    
                        <PostenWebControls:PostenButton ID="btnPrintReceipt" CssClass="getLegalAddressButton"
                                                runat="server" Text='<%$CartCode: Utils.GetResourceString("Confirmation_Print_Receipt_Label")%>' />
                    
                </div>
                <div class="descriptions">
                    <asp:Panel runat="server" ID="pnlInvoiceLabel">
                        <span><asp:Literal ID="InvoiceFee_Label" runat="server"></asp:Literal>:</span>
                        <span>&nbsp;</span>
                    </asp:Panel>
                    <span><asp:Literal ID="Confirmation_Total_Including_Vat_Label" runat="server"></asp:Literal>:</span> 
                    <span><asp:Literal ID="Confirmation_VAT_Label" runat="server"></asp:Literal></span>
                    <!--<span><asp:Literal ID="Confirmation_Total_Label" runat="server"></asp:Literal></span>-->
                    
                    
                </div>
                <div class="values">
                    <asp:Panel runat="server" ID="pnlInvoiceValue">
                        <span><strong><asp:Literal ID="ltrInvoiceFee" runat="server"></asp:Literal></strong></span>
                        <span>&nbsp;</span>
                    </asp:Panel>
                    <span><strong><asp:Literal ID="Summa" runat="server"></asp:Literal></strong></span>
                    <span><strong><asp:Literal ID="Moms" runat="server"></asp:Literal></strong></span> 
                    <!--<span><asp:Literal ID="SummaMom" runat="server"></asp:Literal></span>-->
                </div>
            </div>
            <%if (this.pnlYourNextStep.Visible == true) {%>
                <div class="footerCollapse">
            <%} else { %>
                <div class="footer">
            <%}%>
             <div class="topaydescription"><span><%= Utils.GetResourceString("Confirmation_To_Pay_Label")%></span></div>
             <div class="topayvalue"><span class="theme"><asp:Literal ID="AttBetala" runat="server"></asp:Literal></span></div>
            </div>
        </div>
    </div>
    <!--Your Next Step-->
    <asp:Panel ID="pnlYourNextStep" runat="server">
        <div class="module webshopConfirmation headerPrimary footerSolid">
            <div class="contentConfirmation">
                <div class="header">
                    <span>
                        <%= Utils.GetResourceString("Confirmation_Your_Next_Step_Label")%></span>
                    <div class="control">
                        <a class="minimize" href="#"><%= Utils.GetResourceString("Confirmation_Hide_Label")%><span></span></a> 
                        <a class="maximize" href="#"><%= Utils.GetResourceString("Confirmation_View_Label")%><span></span></a>
                    </div>
                </div>
                <div class="body">
                     <!-- Section for the other services -->
                    <asp:Repeater ID="rptServicesApplication" runat="server">
                        <ItemTemplate>
                            <script type="text/javascript">
                                var src = "<%# Eval("ConfirmationUrl") %>" + "#" + encodeURIComponent(document.location.href);
                                document.write('<iframe src="' +src+ '" class="confirmation-iframe applicationFrame" scrolling="no" frameborder="0"></iframe>');
                            </script>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>  
    </asp:Panel>

   
    <!--Your Details -->
    <div class="module webshopConfirmation headerPrimary footerSolid">
        <div class="contentConfirmation">
            <div class="header">
                <span>
                    <%= Utils.GetResourceString("Confirmation_Your_Details_Label")%></span>
                <div class="control">
                    <a class="minimizeCollapse" href="#"><%= Utils.GetResourceString("Confirmation_Hide_Label")%><span></span></a>
                    <a class="maximizeCollapse" href="#"><%= Utils.GetResourceString("Confirmation_View_Label")%><span></span></a>
                </div>
            </div>
            <div class="bodyCollapse">
                <table>
                    <tbody>
                        <tr class="theme headline">
                            <td>
                                <%= Utils.GetResourceString("Your_Details_Payment_Date")%>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_PaymentDate" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="divider">
                            <td>
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Username_Label")%>:</strong>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_UserName" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="top">
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Delivery_address_Label")%>:</strong>
                            </td>
                            <td>
                                <% if (!string.IsNullOrEmpty(this.OrderContext.BasketItem.DeliveryAccountInfo.Organization))  {%>
                                  <div><%= this.OrderContext.BasketItem.DeliveryAccountInfo.Organization %></div>
                                <% } %>
                                <div><asp:Literal ID="YourDetails_DeliveryName" runat="server"></asp:Literal></div>
                                <div><asp:Literal ID="YourDetails_DeliveryAddress" runat="server"></asp:Literal></div>
                                <span><asp:Literal ID="YourDetails_DeliveryPostalCode" runat="server"></asp:Literal></span>
                                <span><asp:Literal ID="YourDetails_DeliveryCity" runat="server"></asp:Literal></span>
                                <div><asp:Literal ID="YourDetails_DeliveryCountry" runat="server"></asp:Literal></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Email")%>:</strong>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_Email" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Phone")%>:</strong>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_PhoneNo" runat="server"></asp:Literal>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_MobileNo")%>:</strong>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_MobileNo" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="divider">
                            <td>
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Payment_Options")%>:</strong>
                            </td>
                            <td>
                                <asp:Literal ID="YourDetails_PaymentOption" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="divider">
                            <td colspan="2">
                                <strong>
                                    <%= Utils.GetResourceString("Your_Details_Return_Policy")%>:</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="returnpolicy">
                                  <%= Utils.GetResourceString("Your_Details_Return_Policy_Text")%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="ext">
            <div><strong><%= Utils.GetResourceString("Your_Details_For_other_questions")%></strong></div>
            <%= Utils.GetResourceString("Your_Details_For_other_questions_text")%>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var confirmation_<%= this.ClientID %> = new confirmation();
        confirmation_<%= this.ClientID %>.init();
    });
</script>