﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="MyPagesWebControls" Namespace="Posten.Portal.MyPages.Presentation.CustomControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyPagesGlobalUserInfoControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.MyPagesGlobalUserInfoControl" %>

<div id="GlobalUserInfo">
    <ul>
        <li class="info"><span><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,UserInfo_LoggedInAs_Text %>' /> <span><MyPagesWebControls:LoginName runat="server"/></span></span></li>
		<li class="divider"></li>
        <li class="logout"><a class="link" href="/_layouts/SignOut.aspx"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,UserInfo_SignOut_Text %>' /><span class="arrow"></span></a></li>
    </ul>
</div>
