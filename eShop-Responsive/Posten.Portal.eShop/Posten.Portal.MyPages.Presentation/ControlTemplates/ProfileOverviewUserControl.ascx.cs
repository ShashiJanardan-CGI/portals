﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.Cart.Services;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class ProfileOverviewUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IAccountService service = IoC.Resolve<IAccountService>();
            var userInformation = service.GetCurrentUser(MyPagesContext.Current.Name);

            /*Populate fields*/
            this.CustomerID.Text = "";
            this.PersonalSecurityNumber.Text = userInformation.PersonalCodeNumber;
            this.EmailAddress.Text = userInformation.EmailAddress;
            this.MobileNumber.Text = userInformation.MobileNumber;
            this.FirstName.Text = userInformation.FirstName;
            this.LastName.Text = userInformation.LastName;
            this.Address.Text = string.Format("{0}", userInformation.Address.StreetAddress);
            this.PostalCode.Text = userInformation.Address.PostalCode;
            this.City.Text = userInformation.Address.City;
            this.Country.Text = userInformation.Address.Country;

        }
    }
}
