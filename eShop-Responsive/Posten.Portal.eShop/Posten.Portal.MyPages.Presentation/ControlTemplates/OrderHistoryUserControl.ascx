﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register Tagprefix="MyPagesWebControls" Namespace="Posten.Portal.MyPages.Presentation.CustomControls" Assembly="Posten.Portal.MyPages.Presentation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderHistoryUserControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.OrderHistoryUserControl" %>

<PostenWebControls:ModuleControl ID="orderHistoryModule" runat="server" CssClass="orderHistory" ModuleStyle="Table">
    <PostenWebControls:GridView runat="server" ID="gvHistory" DataSourceID="dataSource" CssClass="moduleTable" DefaultSortExpression="OrderDate" DefaultSortDirection="Descending">
        <Columns>
            <asp:TemplateField SortExpression="OrderNumber" HeaderText='<%$Resources:PostenMyPages,OrderHistory_OrderNumber_Text %>'>
                <ItemTemplate>
                    <a href="<%# Eval("OrderNumber", "OrderDetails.aspx?BookingId={0}") %>"><%# Eval("OrderNumber") %></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField SortExpression="OrderDate" DataField="OrderDate" DataFormatString="{0:d}" HeaderText='<%$Resources:PostenMyPages,OrderHistory_OrderDate_Text %>' />
            <asp:BoundField SortExpression="OrderAmount" DataField="OrderAmount" DataFormatString="{0:N}" HeaderText='<%$Resources:PostenMyPages,OrderHistory_OrderTotal_Text %>' />
        </Columns>
    </PostenWebControls:GridView>
</PostenWebControls:ModuleControl>

<asp:LinqDataSource runat="server" ID="dataSource" AutoPage="true" AutoSort="true" OnSelecting="OrderHistorySelecting"></asp:LinqDataSource>

<PostenWebControls:ModuleControl runat="server" ModuleStyle="Breaker" EdgeStyle="None" CssClass="pager">
<asp:DataPager runat="server" ID="pagerHistory" PagedControlID="gvHistory" PageSize="10">
<Fields>
<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowLastPageButton="false" ButtonCssClass="step" NextPageText="" PreviousPageText="" />
<asp:NumericPagerField ButtonType="Link" ButtonCount="12"  NumericButtonCssClass="numeric" CurrentPageLabelCssClass="current" />
</Fields>
</asp:DataPager>
</PostenWebControls:ModuleControl>

