﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetailUserControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.OrderDetailUserControl" %>

<PostenWebControls:ModuleControl ID="orderDetailModule" runat="server" ModuleStyle="Table" >
<table cellpadding="2" cellspacing="0" width="100%" class="moduleTable">
    <tr class="head even">
        <th colspan="2"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_Products_Text %>' /></th>
        <th class="center"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_Quantity_Text %>' /></th>
        <th class="label"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_PricePerUnit_Text %>' /></th>
        <th class="label"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_Total_Text %>' /></th>
    </tr>
    <asp:Repeater ID="ProductsList" runat="server">
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>
            <tr class="product odd">
                <td class="image"><div class="image"></div></td>
                <td class="info">
                    <span class="name"><a class="link" href="#"><%# Eval("ArticleName") %></a></span><br />
                    <span class="articlenumber">Art. # <%# Eval("ArticleId") %></span><br />
                    <span class="category">Utrikes</span>
                </td>
                <td class="quantity"><%# Eval("Quantity") %></td>
                <td class="price"><%# Eval("ArticlePricewithVAT", "{0:N}")%> kr</td>
                <td class="totalprice"><%# Eval("Total", "{0:N}")%> kr</td>
            </tr>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </asp:Repeater>
    <tr class="head even">
        <td colspan="5" style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_Services_Text %>' /></td>
    </tr>
    <asp:Repeater ID="ServicesList" runat="server">
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>
            <tr class="service odd">
            <td class="image"><div class="image"></div></td>
            <td class="info">
                <span class="name"><a class="link" href="#"><%# Eval("ApplicationName") %> - <%# Eval("Name") %></a></span><br />
                <span class="articlenumber">Art. # <%# Eval("OrderNumber")%></span>
            </td>
            <td class="quantity">1</td>
            <td class="price"><%# Eval("Total", "{0:N}")%> kr</td>
            <td class="totalprice"><%# Eval("Total", "{0:N}")%> kr</td>
            </tr>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </asp:Repeater>
    <tr class="footer even">
        <td colspan="2">
            <div class="button">
                <PostenWebControls:PostenButton ID="PrintReceiptButton" runat="server" Text="<%$Resources:PostenMyPages, OrderDetail_PrintReceipt_Text %>" />
            </div>
        </td>
        <td colspan="3" align="right">
            <div class="priceInformation">
                <div class="labels">
                    <div><asp:Literal ID="litTotalAmountHeader" runat="server" Text="<%$Resources:PostenMyPages,OrderDetail_Total_Text %>"></asp:Literal>:</div>
                    <div><asp:Literal ID="litTotalwoVatAmountHeader" runat="server" Text="<%$Resources:PostenMyPages,OrderDetail_TotalwoVAT_Text %>"></asp:Literal>:</div>
                    <div><asp:Literal ID="litVATHeader" runat="server" Text="<%$Resources:PostenMyPages,OrderDetail_VAT_Text %>"></asp:Literal>:</div>
                    <div><asp:Literal ID="litTotalShippingHeader" runat="server" Text="<%$Resources:PostenMyPages,OrderDetail_Shipping_Text %>"></asp:Literal>:</div>
                </div>
                <div class="values">
                    <div><asp:Literal ID="TotalAmount" runat="server"></asp:Literal></div>
                    <div><asp:Literal ID="TotalExclusiveTax" runat="server"></asp:Literal></div>
                    <div><asp:Literal ID="TotalTaxAmount" runat="server"></asp:Literal></div>
                    <div>0,00 kr</div>
                </div>
            </div>
        </td>
    </tr>
    <tr class="footer even">
        <td colspan="2"></td>
        <td colspan="3" align="right">
            <div class="priceInformation">
                <div class="labels">
                    <div>Att betala:</div>
                </div>
                <div class="values total">
                    <asp:Literal ID="Total" runat="server"></asp:Literal>
                </div>
            </div>
        </td>
    </tr>
</table>
</PostenWebControls:ModuleControl>

<PostenWebControls:ModuleControl ID="orderInfoModule" runat="server" HeaderText="<%$Resources:PostenMyPages,OrderDetail_CustomerInformation_Text %>">
<table cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td style="color:#194B96; font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_PaymentDate_Text %>' /></td>
        <td style="color:#194B96; font-weight: bold;"><asp:Literal ID="PaymentDateValue" runat="server"></asp:Literal></td>
    </tr>
    <tr class="space">
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="seperator">
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_CustomerName_Text %>' /></td>
        <td><asp:Literal ID="CustomerNameValue" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="font-weight: bold; vertical-align: top;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_DeliveryAddress_Text %>' /></td>
        <td>
            <asp:Literal ID="DeliveryCustomerName" runat="server"></asp:Literal><br />
            <asp:Literal ID="AddressValue" runat="server"></asp:Literal><br />
            <asp:Literal ID="PostalCodeValue" runat="server"></asp:Literal>
            <asp:Literal ID="CityValue" runat="server"></asp:Literal><br />
            <asp:Literal ID="CountryValue" runat="server"></asp:Literal></td><br />
    </tr>
    <tr>
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_Email_Text %>' /></td>
        <td><asp:Literal ID="EmailValue" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_PhoneNumber_Text %>' /></td>
        <td><asp:Literal ID="PhoneNumberValue" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_CellPhoneNumber_Text %>' /></td>
        <td><asp:Literal ID="MobileNumberValue" runat="server"></asp:Literal></td>
    </tr>
    <tr class="space">
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="seperator">
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_PaymentType_Text %>' /></td>
        <td><asp:Literal ID="PaymentTypeValue" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,OrderDetail_DeliveryType_Text %>' /></td>
        <td><asp:Literal ID="DeliveryShippingValue" runat="server"></asp:Literal></td>
    </tr>
</table>
</PostenWebControls:ModuleControl>

<style type="text/css">
.head .label
{
    text-align:right;
}

.product .price,
.service .price
{
    text-align: right;
}

.head .center,
.product .quantity,
.service .quantity
{
    text-align: center;
}

.product .totalprice,
.service .totalprice
{
    font-weight: bold;
    text-align: right;    
}

.serviceInformation td
{
    border-top: none;
    padding: 0;
    font-size: 0.9em;
}

.priceInformation
{
    width: 200px;
    float: right;
}

.priceInformation .labels
{
    float: left;  
    font-weight: bold;  
}

.priceInformation .values
{
    float: right; 
    text-align: right;   
}
.priceInformation .total
{
    color: #194B96;   
    font-weight: bold; 
}
.priceInformation td
{
    border-top: none;
    padding: 0;
}

.banner, .module.theme 
{
    display: none;
}
.footer .button
{
    margin-bottom: 5px;
}
.seperator
{
    background: url("/_layouts/images/posten/cart/common/dividers/horizontal_557.png") no-repeat scroll center top transparent;    
}
</style>