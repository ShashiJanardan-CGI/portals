﻿namespace Posten.Portal.MyPages.Presentation.CustomControls
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;

    [DefaultProperty("FormatString")]
    [ToolboxData("<{0}:LoginName runat=server/>")]
    public class LoginName : System.Web.UI.WebControls.LoginName
    {
        #region Properties

        protected string UserName
        {
            get
            {
                if (this.DesignMode)
                {
                    return "[UserName]";
                }

                return MyPagesContext.Current.Name;
            }
        }

        #endregion Properties

        #region Methods

        protected override void RenderContents(HtmlTextWriter writer)
        {
            string userName = this.UserName;
            if (!string.IsNullOrEmpty(userName))
            {
                userName = HttpUtility.HtmlEncode(userName);
                string formatString = this.FormatString;
                if (formatString.Length == 0)
                {
                    writer.Write(userName);
                }
                else
                {
                    try
                    {
                        writer.Write(string.Format(CultureInfo.CurrentCulture, formatString, userName));
                    }
                    catch (FormatException ex)
                    {
                        throw new FormatException("FormatString is not a valid format string.", ex);
                    }
                }
            }
        }

        #endregion Methods
    }
}