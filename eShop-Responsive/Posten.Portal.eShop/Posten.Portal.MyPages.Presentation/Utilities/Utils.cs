﻿namespace Posten.Portal.MyPages.Presentation
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    public static class Utils
    {
        #region Methods

        public static string ToOrdinal(this int item)
        {
            if (item % 100 < 20 && item % 100 > 9)
                return string.Format("{0}th", item.ToString());
            switch (item % 10)
            {
                case 1:
                    return string.Format("{0}st", item.ToString());
                case 2:
                    return string.Format("{0}nd", item.ToString());
                case 3:
                    return string.Format("{0}rd", item.ToString());
                default:
                    return string.Format("{0}th", item.ToString());
            }
        }

        #endregion Methods
    }
}