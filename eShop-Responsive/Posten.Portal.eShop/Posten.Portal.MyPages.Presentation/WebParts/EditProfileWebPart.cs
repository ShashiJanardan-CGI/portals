﻿namespace Posten.Portal.MyPages.Presentation.WebParts
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    [Guid("0ab27d11-dde0-42e0-8824-dc1a78432c7c")]
    [ToolboxItemAttribute(false)]
    public class EditProfileWebPart : WebPart
    {
        #region Fields

        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/MyPages/EditProfileUserControl.ascx";

        #endregion Fields

        #region Methods

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(AscxPath);
            Controls.Add(control);
        }

        #endregion Methods
    }
}