﻿namespace Posten.Portal.MyPages.Presentation.WebParts
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    [Guid("07d64224-0010-4784-a3a1-9330bee6a98f")]
    [ToolboxItemAttribute(false)]
    public class OrderHistoryWebPart : WebPart
    {
        #region Fields

        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/MyPages/OrderHistoryUserControl.ascx";

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Allow paging"),
        WebDescription("Allow paging of orders"),
        DefaultValue(true)]
        public bool AllowPaging
        {
            get;
            set;
        }

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Allow sorting"),
        WebDescription("Allow sorting of orders"),
        DefaultValue(true)]
        public bool AllowSorting
        {
            get;
            set;
        }

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Footer text"),
        WebDescription("Text of link in the footer"),
        DefaultValue("")]
        public string FooterText
        {
            get;
            set;
        }

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Footer url"),
        WebDescription("Url of link in the footer"),
        DefaultValue("")]
        public string FooterUrl
        {
            get;
            set;
        }

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Orders to show"),
        WebDescription("The number of orders to show on each page or in overview mode"),
        DefaultValue(10)]
        public int OrdersToShow
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        protected override void CreateChildControls()
        {
            var control = Page.LoadControl(AscxPath) as ControlTemplates.OrderHistoryUserControl;
            if (control != null)
            {
                control.HeaderText = this.Title;
                control.FooterText = this.FooterText;
                control.FooterUrl = this.FooterUrl;
                control.OrdersToShow = this.OrdersToShow;

                control.Init += delegate
                {
                    control.AllowPaging = this.AllowPaging;
                    control.AllowSorting = this.AllowSorting;
                };

                Controls.Add(control);
            }
        }

        #endregion Methods
    }
}