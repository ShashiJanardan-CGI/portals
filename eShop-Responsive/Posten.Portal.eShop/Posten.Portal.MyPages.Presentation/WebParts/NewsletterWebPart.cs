﻿namespace Posten.Portal.MyPages.Presentation.WebParts
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    [Guid("ad1fb70b-f7e5-4767-be48-d55f7588216f")]
    [ToolboxItemAttribute(false)]
    public class NewsletterWebPart : WebPart
    {
        #region Fields

        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/MyPages/NewsletterUserControl.ascx";

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Footer text"),
        WebDescription("Text of link in the footer"),
        DefaultValue("")]
        public string FooterText
        {
            get;
            set;
        }

        [WebBrowsable(true),
        Personalizable(PersonalizationScope.Shared),
        Category("Posten"),
        WebDisplayName("Footer url"),
        WebDescription("Url of link in the footer"),
        DefaultValue("")]
        public string FooterUrl
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        protected override void CreateChildControls()
        {
            var control = Page.LoadControl(AscxPath) as ControlTemplates.NewsletterUserControl;
            if (control != null)
            {
                control.HeaderText = this.Title;
                control.FooterText = this.FooterText;
                control.FooterUrl = this.FooterUrl;

                Controls.Add(control);
            }
        }

        #endregion Methods
    }
}