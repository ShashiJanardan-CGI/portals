﻿namespace Posten.Portal.MyPages.Presentation.WebParts
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    [Guid("1507e4d2-5a43-453c-bc3c-fe9a01155185")]
    [ToolboxItemAttribute(false)]
    public class OrderDetailWebPart : WebPart
    {
        #region Fields

        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/MyPages/OrderDetailUserControl.ascx";

        #endregion Fields

        #region Methods

        protected override void CreateChildControls()
        {
            var control = Page.LoadControl(AscxPath);
            if (control != null)
            {
                StringBuilder confirmationjs = new StringBuilder();
                confirmationjs.Append("<script type=\"text/javascript\">");
                confirmationjs.Append("$(document).ready(function () {");
                confirmationjs.Append("var confirmation_" + this.ClientID + "= new confirmation();");
                confirmationjs.Append("confirmation_" + this.ClientID + ".ShowGlobalCart();});</script>");

                LiteralControl confirmationJSLiteral = new LiteralControl(confirmationjs.ToString());

                Controls.Add(control);
                Controls.Add(confirmationJSLiteral);
            }
        }

        #endregion Methods
    }
}