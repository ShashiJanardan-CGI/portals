﻿using System;
using System.Runtime.InteropServices;
using Microsoft.SharePoint.Publishing;
using Microsoft.SharePoint.Utilities;

namespace Posten.Portal.MyPages.Presentation.PageLayouts
{
    [Guid("661a4956-8f79-435c-8b7a-32ffbb9f09b9")]
    public partial class MyPagesLayoutPage : PublishingLayoutPage
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            // check if user is authenticated or redirect to loginpage
            //   by returning a 403 error, webseal will catch and redirect to loginpage
            if (MyPagesContext.Current == null || !MyPagesContext.Current.IsAuthenticated)
            {
                SPUtility.EnsureAuthentication();

                // Response.StatusCode = 403; // System.Net.HttpStatusCode.Forbidden
                // Response.StatusDescription = "Forbidden";
                // Response.Status = "403 Forbidden";
                // Response.End();
                // Response.Close();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            SPUtility.EnsureAuthentication();

            base.OnLoad(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
