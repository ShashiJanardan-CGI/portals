﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
//using Posten.Portal.eShop.BusinessEntities;

namespace Posten.Portal.eShop.TestObjects.WebParts.BannerWebPart_Test
{
    [ToolboxItemAttribute(false)]
    public class BannerWebPart_Test : WebPart
    {
        public enum DisplaySize
        {
            OneColumn = 1,
            TwoColumns = 2,
            ThreeColumns = 3,
            FourColumns = 4
        }

        public enum DisplayType
        {
            Image = 1,
            Flash = 2
        }

        [WebBrowsable(true),
        Category("Banner Options"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Size"),
        DefaultValue(DisplaySize.FourColumns)]
        public DisplaySize displaySize { get; set; }

        [WebBrowsable(true),
        Category("Banner Options"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Type"),
        DefaultValue(DisplayType.Image)]
        public DisplayType displayType { get; set; }

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.Portal.eShop.TestObjects.WebParts/BannerWebPart_Test/BannerWebPart_TestUserControl.ascx";

        
        protected override void CreateChildControls()
        {
            BannerWebPart_TestUserControl control = Page.LoadControl(_ascxPath) as BannerWebPart_TestUserControl;
            if (control != null)
            {
                control.WebPart = this;
                Controls.Add(control);
            }
        }
    }
}
