﻿namespace Posten.Portal.eShop.Presentation
{
    using System.ServiceModel;
    using System.ServiceModel.Web;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUIOperation" in both code and config file together.
    [ServiceContract]
    public interface IUIOperation
    {
        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productOrderNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool AddProduct(string productNumber, string productOrderQuantity);

        #endregion Methods
    }
}