<%@ Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
  <SharePointWebControls:CssRegistration name="/_layouts/posten/eshop/1.0/eshop.css" runat="server"/>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
  <SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
  <SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
  <SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigation" RenderCurrentNodeAsLink="false" PathSeparator="" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=321 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/images/fgimg.png" HideInteriorRootNodes="true" SkipLinkText="" />
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
 <div class="box colspan-4">
		<div class="box colspan-3">
			<div class="page">
				<PublishingWebControls:EditModePanel ID="EditModePanel1" runat="server" CssClass="edit-mode-panel">
			        <SharePointWebControls:TextField ID="TextField2" runat="server" FieldName="PostenEshopCategory"/>
                    <SharePointWebControls:TextField ID="TextField3" FieldName="PostenEshopProductID" InputFieldLabel="Product ID" runat="server"></SharePointWebControls:TextField>
		        </PublishingWebControls:EditModePanel>
				<PostenWebControls:HideEmptyField FieldName="PublishingPageImage" runat="server">
					<div class="captioned-image">
						<div class="image"><PublishingWebControls:RichImageField ID="RichImageField1" FieldName="PublishingPageImage" runat="server"/></div>
						<div class="caption"><PublishingWebControls:RichHtmlField ID="RichHtmlField1" FieldName="PublishingImageCaption" AllowTextMarkup="false" AllowTables="false" AllowLists="false" AllowHeadings="false" AllowStyles="false" AllowFontColorsMenu="false" AllowParagraphFormatting="false" AllowFonts="false" AllowInsert="false" PreviewValueSize="Small" runat="server"/>
						</div>
					</div>
				</PostenWebControls:HideEmptyField>
				<PostenWebControls:HideEmptyField FieldName="Comments" runat="server">
					<div class="introduction"><SharePointWebControls:NoteField ID="NoteField1" FieldName="Comments" runat="server"/></div>
				</PostenWebControls:HideEmptyField>
				<PostenWebControls:HideEmptyField FieldName="PublishingPageContent" runat="server">
					<div class="content"><PublishingWebControls:RichHtmlField ID="RichHtmlField2" FieldName="PublishingPageContent" HasInitialFocus="True" AllowFonts="false" AllowTextMarkup="false" AllowParagraphFormatting="false" PrefixStyleSheet="posten-" MinimumEditHeight="400px" runat="server"/></div>
				</PostenWebControls:HideEmptyField>
			</div>
			<WebPartPages:WebPartZone runat="server" ID="LeftColumnZone" AllowPersonalization="False" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_LeftColumn%>" />
		</div>
		<div class="box">
			<WebPartPages:WebPartZone runat="server" ID="RightColumnZone" AllowPersonalization="False" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_RightColumn%>" />
		</div>
	</div>

  
</asp:Content>
