﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="b3954bf9-8d37-4754-9f4a-9334599eaaef" activateOnDefault="false" defaultResourceFile="PostenEshop" description="$Resources:Feature_PostenEshop_Infrastructure_Description;" featureId="b3954bf9-8d37-4754-9f4a-9334599eaaef" imageUrl="Posten/PostenFeature.gif" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:Feature_PostenEshop_Infrastructure_Title;" version="AAEAAAD/////AQAAAAAAAAAEAQAAAA5TeXN0ZW0uVmVyc2lvbgQAAAAGX01ham9yBl9NaW5vcgZfQnVpbGQJX1JldmlzaW9uAAAAAAgICAgBAAAAAAAAAAAAAAAAAAAACw==" deploymentPath="$SharePoint.Feature.FileNameWithoutExtension$_1.0" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="e39f3f19-fa02-4627-aef6-47fe34a49437" />
    <projectItemReference itemId="4a53c352-1694-482a-97d7-19966ba431a3" />
    <projectItemReference itemId="014d233b-f987-4e36-8fb6-e52d7f39a2f8" />
    <projectItemReference itemId="5893739d-cc6b-4c01-ba5b-a519f503a5e1" />
    <projectItemReference itemId="8d701ed4-16ee-4536-a3e7-1a54502d6126" />
    <projectItemReference itemId="6b759ad5-c472-42bf-b221-ea7c44d2d8a0" />
    <projectItemReference itemId="e16c271b-c80e-459f-8621-a933db36dcb8" />
  </projectItems>
</feature>