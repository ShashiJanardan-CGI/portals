﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryListUserControl.ascx.cs" Inherits="Posten.Portal.eShop.Presentation.ControlTemplates.CategoryListUserControl" %>

<PostenWebControls:ModuleControl ID="categoryListModule" runat="server" CssClass="categoryList" ModuleStyle="Table">
    <PostenWebControls:GridView runat="server" ID="gridView" DataSourceID="dataSource" CssClass="moduleTable">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText='Id' />
            <asp:BoundField DataField="Name" HeaderText='Name' />
        </Columns>
    </PostenWebControls:GridView>
</PostenWebControls:ModuleControl>

<asp:LinqDataSource runat="server" ID="dataSource" AutoPage="true" AutoSort="false" OnSelecting="OnSelecting"></asp:LinqDataSource>
