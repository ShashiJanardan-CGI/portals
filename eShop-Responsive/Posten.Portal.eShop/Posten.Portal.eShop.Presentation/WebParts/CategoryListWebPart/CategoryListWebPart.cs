﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

namespace Posten.Portal.eShop.Presentation.WebParts
{
    [ToolboxItemAttribute(false)]
    [Guid("da5d39dd-8bec-430b-8c00-e14edf61c80e")]
    public class CategoryListWebPart : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/eShop/1.0/CategoryListUserControl.ascx";

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(AscxPath);
            Controls.Add(control);
        }
    }
}
