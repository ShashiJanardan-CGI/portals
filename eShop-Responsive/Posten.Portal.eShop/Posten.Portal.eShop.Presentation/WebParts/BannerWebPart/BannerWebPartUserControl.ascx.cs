﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Services;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.eShop.Presentation.WebParts.BannerWebPart
{
    public partial class BannerWebPartUserControl : UserControl
    {
        public BannerWebPart WebPart { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DisplayBanner();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void DisplayBanner()
        {
            string bannerWidth = "100%";
            this.ImagePanel.Visible = true;
            this.FlashPanel.Visible = true;
            this.SilverLightPanel.Visible = true;

            DisplaySize size = (DisplaySize)this.WebPart.displaySize;
            var bannerService = IoC.Resolve<IBannerService>();
            ICollection<BannerItemModel> bannerItems = bannerService.GetBannersByColumnSize(size, 1);

            if (bannerItems.Count > 0)
            {
                this.BannerMessage.Visible = false;
                switch (this.WebPart.displaySize.ToString())
                {
                    case "OneColumn" :
                        bannerWidth = "181px";
                        break;
                    case "TwoColumns" :
                        bannerWidth = "362px";
                        break;
                    case "ThreeColumns" :
                        bannerWidth = "543px";
                        break;
                    case "FourColumns" :
                        bannerWidth = "725px";
                        break;
                    default:
                        bannerWidth = "100%";
                        break;    
                }
                BaseDiv.Attributes.Add("Style", string.Format("width:{0};", bannerWidth));
                
                ImageBannerControl.DataSource = bannerItems.Where(t => t.DisplayType.ToString() == "Image");
                ImageBannerControl.DataBind();

                FlashBannerControl.DataSource = bannerItems.Where(t => t.DisplayType.ToString() == "Flash");
                FlashBannerControl.DataBind();

                SilverlightBannerControl.DataSource = bannerItems.Where(t => t.DisplayType.ToString() == "Silverlight");
                SilverlightBannerControl.DataBind();

                //foreach (BannerItemModel bannerItem in bannerItems)
                //{
                //    switch (bannerItem.DisplayType.ToString())
                //    {
                //        //case "Image" :
                //        //    this.ImagePanel.Visible = true;
                //        //    this.ImageBanner.ImageUrl = bannerItem.AdUrl;
                //        //    this.ImageBanner.ToolTip = bannerItem.ActionUrl;
                //        //    this.ImageBannerURL.NavigateUrl = bannerItem.ActionUrl;
                //        //    break;
                //        case "Flash" :
                //            this.FlashPanel.Visible = true;
                //            swfLocation = bannerItem.AdUrl;
                //            break;
                //        case "SilverLight" :
                //            this.SilverLightPanel.Visible = true;
                //            break;
                //    }
                //}
            }
            else //no banner
            {
                //this.BannerMessage.Text = "Select banner size option from the webpart property."; 
                this.BannerMessage.Visible = true;
            }
        }
    }
}
