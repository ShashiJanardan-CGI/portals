﻿namespace Posten.Portal.eShop.Presentation.WebParts
{
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    [ToolboxItemAttribute(false)]
    [Guid("4dd3efca-43a1-4a38-a3a5-e5e7a316a448")]
    public class ProductListWebPart : WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/eShop/1.0/ProductListUserControl.ascx";

        #endregion Fields

        #region Methods

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(AscxPath);
            Controls.Add(control);
        }

        #endregion Methods
    }
}