﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Import Namespace="Posten.Portal.eShop.Presentation" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailsWebPartUserControl.ascx.cs"
    Inherits="Posten.Portal.eShop.Presentation.WebParts.ProductDetailsWebPart.ProductDetailsWebPartUserControl" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<link href="/_layouts/posten/eshop/1.0/productdetails.css" rel="stylesheet" type="text/css" />
<link href="/_layouts/posten/eshop/2.0/styles/jquery.jqzoom.css" rel="stylesheet" type="text/css" />
<script src="/_layouts/Posten/Cart/Presentation/js/jquery.ezpz_tooltip.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/eShop/Common/js/jquery.ad-gallery.js" type="text/javascript"></script>
<script src="/_layouts/Posten/eShop/2.0/js/jquery.jqzoom-core.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {

        //When page loads...
        $(".tab_content").hide(); //Hide all content
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content

        //On Click Event
        $("ul.tabs li").click(function () {

            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content

            var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active ID content
            return false;
        });


        $("#deliveryhelpid").ezpz_tooltip({
            contentId: 'deliveryhelp-content',
            contentPosition: 'belowRightFollow',
            stayOnContent: true,
            offset: 20,
            overrideTop: $("#deliveryhelpid").offset().top - 70,
            overrideLeft: $("#deliveryhelpid").offset().left + 30
        });

        var galleries = $('.ad-gallery').adGallery({
            loader_image: '/_layouts/images/Posten/eShop/productdetails/loader.gif',
            callbacks: {
                afterImageVisible: function () {
                    $(".ad-image-wrapper").css("overflow", "visible");
                    var imagetowrap = $('.ad-image img');
                    var largeimageurl = $(".ad-active").attr("largeimageurl");
                    imagetowrap.wrap("<a class='zoomable'" + " href='" + largeimageurl + "' />");
                    var options = {
                        zoomType: 'standard',
                        lens: true,
                        preloadImages: true,
                        alwaysOn: false,
                        zoomWidth: 250,
                        zoomHeight: 250,
                        xOffset: 20,
                        yOffset: 0,
                        title: false
                    };
                    $('.zoomable').jqzoom(options);
                    $(".ad-image-wrapper").prepend('<a href="' + largeimageurl + '" target="_blank"><button class="search zoombutton" type="button"></button></a>');
                    var zoombutton = $(".zoombutton");
                },
                beforeImageVisible: function (new_image, old_image) {
                    $(".ad-image-wrapper").css("overflow", "hidden");
                    $(".zoomWrapper").hide();
                }
            }
        });
    });
</script>
<div id="deliveryhelp-content">
    <div class="arrowtooltip">
    </div>
    <div class="helpcontent">
        <strong><%= Utils.GetResourceString("ProductDetails_DeliveryDetails_Label")%>:</strong>
        <p><%= Utils.GetResourceString("ProductDetails_DeliveryDetails_Content")%></p>
    </div>
</div>
<asp:Panel ID="ProductDetailsPanel" runat="server">
    <div class="producttitle"><asp:Literal ID="ProductName" runat="server"></asp:Literal></div>
    <div class="product">
        <div class="product_img">
            <div class="ad-gallery">
                <div class="ad-image-wrapper">
                </div>
                <div class="ad-nav">
                    <div class="ad-thumbs">
                        <ul class="ad-thumb-list">
                            <asp:Repeater ID="ThumbnailsRepeater" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# Eval("UrlImage") %>' largeimageurl='<%# Eval("UrlLarge") %>'><img src='<%# Eval("UrlThumb") %>' alt="" title="" /></a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_details">
            <div class="details"><asp:Literal ID="ProductDetailsLiteral" runat="server"></asp:Literal></div>
            <div class="details"><%= Utils.GetResourceString("ProductDetails_Denomination_Label")%> : <asp:Literal ID="DenominationLiteral" runat="server"></asp:Literal></div>
            <div class="articleno"><%= Utils.GetResourceString("ProductDetails_Article_No_Label")%> : <asp:Literal ID="ProductIdLiteral" runat="server"></asp:Literal></div>
            <br />
            <div class="details">
                <asp:Literal ID="StockAvailableLiteral" runat="server"></asp:Literal>
            </div>
            <div class="details">
                <strong><%= Utils.GetResourceString("ProductDetails_Availability_Label")%> : <asp:Literal ID="AvailabilityLiteral" runat="server"></asp:Literal></strong> <a type="text" class="delivery_help" id="deliveryhelpid"></a></div>
            <div class="price"><asp:Literal ID="PriceLiteral" runat="server"></asp:Literal>&nbsp;<asp:Literal ID="CurrencyLiteral" runat="server"></asp:Literal></div>
            <div class="incvat">Incl. VAT</div>
            <div class="buy">
                <asp:TextBox ID="QuantityTextBox" runat="server" Width="25px" Text="1"></asp:TextBox>
                &nbsp;<asp:Literal ID="MultiplyLabel" runat="server" Text="X"></asp:Literal>&nbsp;
                <asp:DropDownList ID="ProductPackagesDropDownList" runat="server" DataValueField="Quantity" DataTextField="Name">
                </asp:DropDownList>
                <br />
                <br />
                <PostenWebControls:PostenButton ID="btnBuy" runat="server" Text='<%$CartCode: Utils.GetResourceString("ProductDetails_Buy_Button_Label")%>' onclick="btnBuy_Click" />
            </div>
        </div>
        <ul class="tabs">
            <li><a href="#tab1"><%= Utils.GetResourceString("ProductDetails_Description_Label")%></a></li>
            <li><a href="#tab2"><%= Utils.GetResourceString("ProductDetails_Facts_Label")%></a></li>
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <asp:Literal ID="DescriptionLiteral" runat="server"></asp:Literal>
            </div>
            <div id="tab2" class="tab_content">
                <asp:Literal ID="FactsLiteral" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="ErrorPanel" runat="server" Visible="false">
    <asp:Literal ID="ErrorMessageLiteral" runat="server"></asp:Literal>
</asp:Panel>
