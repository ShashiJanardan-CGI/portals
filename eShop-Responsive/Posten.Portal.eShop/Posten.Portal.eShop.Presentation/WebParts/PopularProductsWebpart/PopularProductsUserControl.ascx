﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
    Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.eShop.Presentation" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopularProductsUserControl.ascx.cs" Inherits="Posten.Portal.eShop.Presentation.ControlTemplates.PopularProductsUserControl" %>

<link href="/_layouts/posten/eshop/2.0/styles/popularproducts.css" rel="stylesheet" type="text/css" />
<!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="/_layouts/posten/eshop/2.0/styles/popularproducts_ie8.css">
<![endif]-->
<script src="/_layouts/Posten/eShop/2.0/js/popularproducts.js" type="text/javascript"></script>
<script src="/_layouts/Posten/eShop/Common/js/jcarousellite_1.0.1.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/common/utils.js" type="text/javascript"></script>

<PostenWebControls:ModuleControl ID="popularProductModule" runat="server" ModuleStyle="Table">
    <div id="<%= this.ClientID %>_mainDiv" class="mainDiv">        
            <div class="product_leftNav">
                <a id="<%= this.ClientID %>_previousButton" href="#" class="previous">
                </a>
                <div class="divider_leftNav">
                    <img src="/_layouts/images/Posten/Cart/common/dividers/vertical_2.png" alt="" />
                </div>
            </div>            
            <div class="product_rightNav">
                <div class="divider_rightNav">
                    <img src="/_layouts/images/Posten/Cart/common/dividers/vertical_2.png" alt="" />
                </div>
                <a id="<%= this.ClientID %>_nextButton" class="next" href="#">
                </a>
            </div>
            <div id="<%= this.ClientID %>_productMiddle" class="product_middle">
                <ul>
                <asp:Repeater ID="PopularProductsRepeater" runat="server">
                    <ItemTemplate>
                        <li>
                        <div class="product_item" id="<%# Eval("Id") %>" >
                            <div class="product_details">
                                <div class="product_image">
                                    <a href="<%# Eval("RedirectURL") %>">
                                        <img alt="" src="<%# Eval("ImagesUri") %>"/>
                                    </a>
                                </div>
                                <div class="product_name">
                                    <a href="<%# Eval("RedirectURL") %>">
                                        <asp:Label ID="productNameLabel" runat="server" Text='<%# Eval("Name") %>' />
                                    </a>
                                </div>
                                <div class="product_description">                          
                                    <asp:Label ID="productDescriptionLabel" runat="server" Text='<%# Eval("ShortDescription") %>'/>
                                    <a class="right-arrow"></a>
                                </div>
                                <div class="product_category">
                                    <asp:Label ID="productCategoryLabel" runat="server" Text='<%# Eval("Denomination") %>' />
                                </div>                                
                            </div>
                            <div>
                                <div class="product_amount">                                
                                    <asp:Label ID="productAmountLabel" runat="server" Text='<%# Eval("Price", "{0} kr") %>'  />
                                </div>
                                <div class="product_buybutton">
                                    <span class="inputWrapper button theme">
                                        <div class="edge top">
							                <div class="left"></div>
                                            <div class="right"></div>
						                </div>
                                        <input class="add_product" type="button" value="Kӧp">
                                        <div class="edge bottom">
							                <div class="left"></div>
                                            <div class="right"></div>
						                </div>
                                    </span>
                                </div>
                                <div class="product_quantity">
                                   <PostenWebControls:PostenTextBox ID="quantityTextbox" runat="server" Text="1" Width="15px" />
                                </div>                                
                            </div>
                        </div>      
                        </li>                                          
                    </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
    </div>
</PostenWebControls:ModuleControl>
<asp:HiddenField ID="hdnsource" runat="server"/>
<script type="text/javascript" >
    var data;
    $(document).ready(function () {       
        var popularproducts_<%= this.ClientID %> = new popularproducts();  
        popularproducts_<%= this.ClientID %>.init('<%= hdnsource.ClientID %>', 
                                                  '<%= new Uri(SPContext.Current.Web.Url).AbsolutePath %>',
                                                  '<%= Utils.GetResourceString("Popular_Products_Error_Title") %>',
                                                  '<%= Utils.GetResourceString("Popular_Products_Invalid_Quantity_Value_Error_Text") %>' );
    });
</script>
