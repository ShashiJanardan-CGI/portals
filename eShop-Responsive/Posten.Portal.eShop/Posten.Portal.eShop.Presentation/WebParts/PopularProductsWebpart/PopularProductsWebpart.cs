﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Presentation.ControlTemplates;

namespace Posten.Portal.eShop.Presentation.WebParts
{
    [ToolboxItemAttribute(false)]
    [Guid("022c547b-00de-4ec8-af48-9297dfd1a2a3")]
    public class PopularProductsWebpart : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten/eShop/2.0/PopularProductsUserControl.ascx";
        private const int _defaultNumberOfItemToDisplay = 5;

        private string _webpartHeaderText;
        private int _numberOfItemsToDisplay;

        
        [WebBrowsable(true),
        Category("Popular Product Options"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Webpart Header Text")]
        public String WepartTextHeader
        {
            get
            {
                return _webpartHeaderText;
            }
            set
            {
                _webpartHeaderText = value;
            }
        }

        [WebBrowsable(true),
        Category("Popular Product Options"),
        Personalizable(PersonalizationScope.Shared),
        WebDisplayName("Number Of Items To Display")]
        public int  NumberOfItemsToDisplay
        {
            get
            {
                if (_numberOfItemsToDisplay <= 0)
                {
                    return _defaultNumberOfItemToDisplay;
                }
                else
                {
                    return _numberOfItemsToDisplay;
                }
            }
            set
            {
                _numberOfItemsToDisplay = value;
            }
        }

        protected override void CreateChildControls()
        {
            PopularProductsUserControl control = (PopularProductsUserControl) Page.LoadControl(_ascxPath);
            control.HeaderText = WepartTextHeader;
            control.NumberOfItemsToDisplay = NumberOfItemsToDisplay;
            Controls.Add(control);
        }
    }
}
