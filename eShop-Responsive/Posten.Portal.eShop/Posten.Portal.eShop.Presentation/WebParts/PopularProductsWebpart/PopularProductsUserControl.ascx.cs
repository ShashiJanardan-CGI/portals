﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.Cart;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.eShop;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Presentation.ViewData;
using Posten.Portal.eShop.Services;
using Posten.Portal.Platform.Common.Container;
using Microsoft.SharePoint;
using System.Web.Script.Serialization;
using System.Text;
using System.Web.Caching;

namespace Posten.Portal.eShop.Presentation.ControlTemplates
{
    public partial class PopularProductsUserControl : UserControl
    {
        private const string _defaultNoImageUri = "/publishingimages/posten/cart/common/no_image.jpg";
        private const string _imageLoader = "/_layouts/images/Posten/eShop/productdetails/loader.gif";
        private string _webpartHeaderText;
        private int _numberOfItemsToDisplay;

        private SPListItemCollection items;

        PopularProductResults productResults = new PopularProductResults();

        public string HeaderText
        {
            get { return _webpartHeaderText; }
            set { _webpartHeaderText = value; }
        }

        public int NumberOfItemsToDisplay
        {
            get { return _numberOfItemsToDisplay; }
            set { _numberOfItemsToDisplay = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetWebpartProperties();                
                GetProductIDList();
                DisplayPopularProducts();
                SerializeProductResults(); 
            }            
        }

        protected void SetWebpartProperties()
        {
            popularProductModule.HeaderText = HeaderText;
        }

        protected void GetProductIDList()
        {
            SPWeb curSite = SPContext.Current.Site.RootWeb;
            SPList productIdList = curSite.Lists.TryGetList("Popular Products");

            SPQuery query = new SPQuery();
            query.ViewFields = "<FieldRef Name='Title'/>" +
                "<FieldRef Name='PostenEshopProductRedirectUrl'/>";

            if (productIdList != null)
            {
                items = productIdList.GetItems(query) ;
            }
            if (Cache["CacheItemsCount"] == null)
            {
                Cache.Insert("CacheItemsCount", items.Count, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero);
            }
            if (Cache["CacheItemsCount"] != null && (Convert.ToInt32(Cache["CacheItemsCount"]) < items.Count || Convert.ToInt32(Cache["CacheItemsCount"]) > items.Count))
            {
                Cache.Remove("CacheProductList");
                Cache.Remove("CacheImageColl");
                Cache.Insert("CacheItemsCount", items.Count, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero);
            }
            if (Cache["CacheLocale"] == null)
            {
                Cache.Insert("CacheLocale", SPContext.Current.RegionalSettings.LocaleId, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero);
            }
            else
            {
                if (Convert.ToUInt32(Cache["CacheLocale"]) != SPContext.Current.RegionalSettings.LocaleId)
                {
                    Cache.Remove("CacheProductList");
                    Cache.Remove("CacheImageColl");
                }
            }
        }

        protected void DisplayPopularProducts()
        {
            List<ItemDataResults> productList = new List<ItemDataResults>();
            List<string> ImageColl = new List<string>();

            if (Cache["CacheProductList"] == null && Cache["CacheImageColl"] == null)
            {
                foreach (SPListItem item in items)
                {
                    try
                    {
                        ProductModel product = ProductContext.GetProduct(item["Title"].ToString());
                        string imageUri = string.Empty; ;

                        if (product != null)
                        {
                            ItemDataResults itemresult = new ItemDataResults();
                            itemresult.Id = product.Id;
                            itemresult.Name = product.Name;
                            itemresult.ShortDescription = product.Description;
                            itemresult.Price = product.Price.ToString("f2");
                            itemresult.Denomination = product.Value;
                            itemresult.ImagesUri = _imageLoader;

                            if (product.ImagesUri != null && product.ImagesUri.Count > 0)
                            {
                                try
                                {
                                    imageUri = (from e in product.ImagesUri
                                                where e.ToString().Contains("1_small")
                                                select e.ToString()).SingleOrDefault().ToString();
                                }
                                catch
                                {

                                }
                            }
                            if (imageUri == string.Empty)
                            {
                                imageUri = _defaultNoImageUri;
                            }

                            itemresult.RedirectURL = item["PostenEshopProductRedirectUrl"].ToString();
                            productList.Add(itemresult);
                            ImageColl.Add(imageUri);
                        }
                    }
                    catch
                    {

                    }
                }

                Cache.Insert("CacheProductList", productList, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero);
                Cache.Insert("CacheImageColl", ImageColl, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero);                
            }
            else
            {
                productList = (List<ItemDataResults>)Cache["CacheProductList"];
                ImageColl = (List<string>)Cache["CacheImageColl"];
            }

            productResults.Count = productList.Count;
            productResults.ImageCollection = ImageColl;
            productResults.ItemsToDisplay = NumberOfItemsToDisplay;

            if (productList.Count > 0)
            {
                PopularProductsRepeater.DataSource = productList;
                PopularProductsRepeater.DataBind();
            }
            
        }

        private void SerializeProductResults()
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            hdnsource.Value = json.Serialize(productResults);
        }
    }
}
