﻿namespace Posten.Portal.eShop.Presentation.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.eShop.BusinessEntities;

    public class ItemDataResults
    {
        #region Properties

        public string CurrencyCode
        {
            get; set;
        }

        public string Denomination
        {
            get; set;
        }

        public string Id
        {
            get; set;
        }

        public string ImagesUri
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Price
        {
            get; set;
        }

        public string RedirectURL
        {
            get; set;
        }

        public string ShortDescription
        {
            get; set;
        }

        public decimal Total
        {
            get; set;
        }

        #endregion Properties
    }

    public class PopularProductResults
    {
        #region Properties

        public int Count
        {
            get; set;
        }

        public IEnumerable<string> ImageCollection
        {
            get; set;
        }

        public int ItemsToDisplay
        {
            get; set;
        }

        #endregion Properties
    }
}