﻿
window.popularproducts = function () { };

popularproducts.prototype._defaultQuantityValue = 1;
popularproducts.prototype._itemsToDisplay;
popularproducts.prototype._imageCollection;
popularproducts.prototype._count;
popularproducts.prototype._currentLocale;
popularproducts.prototype._popularProductsModalError;
popularproducts.prototype._invalidQuantityValueError;

popularproducts.prototype.BindMouseEvents = function () {
    var div = $(".product_middle"), ul = $("ul", div), tLi = $("li", ul)
    tLi.each(function () {
        $(this).mouseover(function () {
            $(this).find('.product_description span').removeClass().addClass("blue");
            $(this).find('.product_category span').removeClass().addClass("blue");
            $(this).find('.product_amount span').removeClass().addClass("blue");
        });
        $(this).mouseleave(function () {
            $(this).find('.product_description span').removeClass().addClass("black");
            $(this).find('.product_category span').removeClass().addClass("black");
            $(this).find('.product_amount span').removeClass().addClass("black");
        });
    });
}

popularproducts.prototype.BindAddProduct = function () {
    var self = this;
    $('.add_product').bind("click", function () {
        var order = $(this).parent().parent();
        var buy_container = order.parent().parent();
        var productNum = buy_container.attr("id");
        var quantity = order.next('div').children().children('input').val();
        $.ajax({
            type: "POST",
            url: self._currentLocale + "/_vti_bin/Posten/eShop/UIOperation.svc/AddProduct",
            data: '{"productNumber":"' + productNum + '","productOrderQuantity":"' + quantity + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                if (msg == true) {
                    location.reload();
                }
            },
            error: function (xhr, desc, err) {
                showPopUp(self._popularProductsModalError, self._invalidQuantityValueError, "popup", false, 800, "Close");
                order.next('div').children().children('input').val(self._defaultQuantityValue);
            }
        });

    });
}

popularproducts.prototype.init = function (sourceClientId, locale, _popularProductsModalError, _invalidQuantityValueError) {

    var self = this;
    self._currentLocale = locale;
    self._popularProductsModalError = _popularProductsModalError;
    self._invalidQuantityValueError = _invalidQuantityValueError;

    if (data == undefined) {
        data = jQuery.parseJSON($('#' + sourceClientId).val());
    }
    if (data != undefined) {
        self._itemsToDisplay = data.ItemsToDisplay;
        self._imageCollection = data.ImageCollection;
        self._count = data.Count;

        if (self._count > 4) {
            $(".mainDiv .product_middle").jCarouselLite({
                btnNext: ".mainDiv .next",
                btnPrev: ".mainDiv .previous",
                speed: 100,
                visible: self._itemsToDisplay,
                imageCollection: self._imageCollection
            });
        } else {
            var div = $(".product_middle"), ul = $("ul", div), tLi = $("li", ul), imageCollectionLength = 1;
            tLi.each(function () {
                var currImage = $(this).find('img');
                currImage.attr("src", "");
                currImage.attr("src", self._imageCollection[imageCollectionLength - 1]);
                imageCollectionLength++;
            });

            $(".product_leftNav .previous").removeAttr('href');
            $(".product_rightNav .next").removeAttr('href');
        }
        self.BindMouseEvents();
        self.BindAddProduct();
    }
}