﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Microsoft.SharePoint;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Translators;

    public class CartSettingsAgent : ICartSettingsService
    {
        #region Fields

        private const string LISTNAME = "Cart Config";

        #endregion Fields

        #region Methods

        public bool AddBlockedSSn(string ssn)
        {
            bool result = true;
            try
            {
                var listItems = this.GetList();

                foreach (SPListItem item in listItems)
                {
                    if (item.Title.Equals(CartSettingTranslator.BLOCK_SSN))
                    {
                        string ssns = item[CartSettingTranslator.ValueID] == null ? "" : item[CartSettingTranslator.ValueID].ToString();
                        IList<string> listssn = ssns.ToBlockedSSNCollection();
                        listssn.Add(ssn);

                        item[CartSettingTranslator.ValueID] = listssn.ToDelimitedString();
                        item.Update();
                        break;
                    }
                }

            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public CartSettingItems GetItems()
        {
            return CartSettingTranslator.ListItemCollectionToEntity(this.GetList());
        }

        public bool RemoveBlockedSSn(string ssn)
        {
            bool result = true;
            try
            {
                var listItems = this.GetList();

                foreach (SPListItem item in listItems)
                {
                    if (item.Title.Equals(CartSettingTranslator.BLOCK_SSN))
                    {
                        string ssns = item[CartSettingTranslator.ValueID] == null ? "" : item[CartSettingTranslator.ValueID].ToString();
                        IList<string> listssn = ssns.ToBlockedSSNCollection();
                        listssn.Remove(ssn);

                        item[CartSettingTranslator.ValueID] = listssn.ToDelimitedString();
                        item.Update();
                        break;
                    }
                }

            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public bool RemoveBlockService(string id)
        {
            bool result = true;
            try
            {
                var listItems = this.GetList();
                foreach (SPListItem item in listItems)
                {
                    if (item.Title.Equals(CartSettingTranslator.BLOCK_SERVICES))
                    {
                        string services = item[CartSettingTranslator.ValueID] == null ? "" : item[CartSettingTranslator.ValueID].ToString();
                        ICollection<BlockedServices> bservices = services.ToBlockedServicesCollection();
                        ICollection<BlockedServices> newBServices = bservices.Where(s => s.ApplicationID != id).ToList<BlockedServices>();
                        item[CartSettingTranslator.ValueID] = newBServices.ToDelimitedString();
                        item.Update();
                        break;
                    }
                }

            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public bool SaveBlockService(string id, string name)
        {
            bool result = true;
            try
            {
                var listItems = this.GetList();
                BlockedServices service = new BlockedServices();
                service.ApplicationID = id;
                service.Name = name;
                foreach (SPListItem item in listItems)
                {
                    if (item.Title.Equals(CartSettingTranslator.BLOCK_SERVICES))
                    {
                        string services = item[CartSettingTranslator.ValueID] == null ? "" : item[CartSettingTranslator.ValueID].ToString();
                        ICollection<BlockedServices> bServices = services.ToBlockedServicesCollection();
                        bServices.Add(service);

                        item[CartSettingTranslator.ValueID] = bServices.ToDelimitedString();
                        item.Update();
                        break;
                    }
                }

            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public bool SaveItems(CartSettingItems items)
        {
            var response = CartSettingTranslator.EntityToListItemCollection(items, this.GetList());

            return true;
        }

        internal SPListItemCollection GetList()
        {
            SPWeb web = SPContext.Current.Site.RootWeb;

            SPList list = web.Lists.TryGetList(LISTNAME);
            var itemcollection = list.GetItems();
            return itemcollection;
        }

        #endregion Methods
    }
}