﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Platform.Common.Services;

    public class ServiceBasketAgent : ServiceAgentBase, IServiceBasketService
    {
        #region Fields

        private Proxy.ServiceBasketService.ServiceBasketClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.ServiceBasketService.ServiceBasketClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.ServiceBasketService.ServiceBasketClient, Proxy.ServiceBasketService.IServiceBasket>() as Proxy.ServiceBasketService.ServiceBasketClient;
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public bool AddServiceOrder(ref string basketId, IServiceOrderItem serviceOrderItem, ref string orderNumber)
        {
            bool result = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            orderNumber = this.ServiceClient.AddServiceOrder(ref basketId, serviceOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.RevertImpersonation(currentUser);
            return result;
        }

        public string AddServiceOrder(string basketId, IServiceOrderItem serviceOrderItem)
        {
            bool result = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            var response = this.ServiceClient.AddServiceOrder(ref basketId, serviceOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        public IServiceOrderItem GetServiceOrderItem(string basketId, string serviceOrderNumber)
        {
            Proxy.ServiceBasketService.ServiceOrderItem response = new Proxy.ServiceBasketService.ServiceOrderItem();
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetServiceOrderItem(basketId, serviceOrderNumber);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response.ToBusinessEntity();
        }

        public ICollection<IServiceOrderItem> GetServiceOrders(string basketId)
        {
            Proxy.ServiceBasketService.ServiceOrderItem[] response = new Proxy.ServiceBasketService.ServiceOrderItem[] { };
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetServiceOrders(ref basketId);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response.ToBusinessEntityCollection();
        }

        public ICollection<IServiceOrderItem> GetServiceOrdersByBookingId(string bookingId)
        {
            Proxy.ServiceBasketService.ServiceOrderItem[] items;
            string response = string.Empty;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetServiceOrdersByBookingId(bookingId, out items);
            Utilities.Utils.RevertImpersonation(currentUser);
            return items.ToBusinessEntityCollection();
        }

        public ICollection<IServiceOrderItem> GetServiceOrdersByTransactionId(long transactionId)
        {
            Proxy.ServiceBasketService.ServiceOrderItem[] items;
            string response = string.Empty;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetServiceOrdersByTransactionId(transactionId.ToString(), out items);
            Utilities.Utils.RevertImpersonation(currentUser);
            return items.ToBusinessEntityCollection();
        }

        public bool RemoveServiceOrder(string basketId, string serviceOrderNumber)
        {
            bool response = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.RemoveServiceOrder(basketId, serviceOrderNumber);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        public string UpdateServiceOrder(string basketId, IServiceOrderItem serviceOrderItem)
        {
            bool result = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            var response = this.ServiceClient.UpdateServiceOrder(basketId, serviceOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        #endregion Methods
    }
}