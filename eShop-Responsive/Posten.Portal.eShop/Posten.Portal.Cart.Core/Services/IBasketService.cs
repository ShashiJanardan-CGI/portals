﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public interface IBasketService
    {
        #region Methods

        bool AssignBillingAccount(string basketId, IBillingAccountInfo billingInfo);

        bool AssignDeliveryAccount(string basketId, IDeliveryAccountInfo deliveryInfo);

        bool AssignToKnownUser(string basketId, string knownUserId);

        bool FinalisePayment(string basketId, DateTime paymentDate, string paymentMethod, Uri receiptUrl, long transactionId, string transactionState);

        ICollection<IPaymentMethod> GetAvailablePayments(string basketId);

        ICollection<IPaymentArticle> GetBasketArticles(string basketId);

        IBasketItem GetBasketItem(string basketId);

        IBasketItem GetBasketItemByBookingId(string bookingId);

        IBasketItem GetBasketItemByTransactionId(long transactionId);

        ICollection<IBasketItem> GetBaskets();

        IBasketItem GetDefaultBasketItem();

        IBasketItem GetResolvedBasketItem(string basketid);

        bool InitialisePayment(string basketId, string[] deliveryIds, long transactionId);

        string PaymentComplete(string basketId, long transactionId, out bool result);

        string PaymentCompleteBypassQueue(string basketId, long transactionId, out bool result);

        bool PrepareForCheckout(string basketId, int layoutversion);

        IBasketItem RecalculateVAT(string basketId, string countrycode, string postalcode);

        bool ReverseInitialisePayment(string basketId, long transactionId);

        bool VerifyPaymentComplete(string basketId, long transactionId);

        #endregion Methods
    }
}