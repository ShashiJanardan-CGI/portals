﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public class ProductBasketStub : IProductBasketService
    {
        #region Methods

        public bool AddProductOrder(ref string basketId, IProductOrderItem productOrderItem, ref string orderNumber)
        {
            throw new NotImplementedException();
        }

        public string AddProductOrder(string basketId, IProductOrderItem productOrderItem)
        {
            throw new NotImplementedException();
        }

        public IProductOrderItem GetProductOrderItem(string basketId, string productOrderNumber)
        {
            throw new NotImplementedException();
        }

        public ICollection<IProductOrderItem> GetProductOrders(string basketId)
        {
            throw new NotImplementedException();
        }

        public string GetProductOrdersByBookingId(string bookingId, out ICollection<IProductOrderItem> productOrderItem)
        {
            throw new NotImplementedException();
        }

        public ICollection<IProductOrderItem> GetProductOrdersByBookingId(string bookingId)
        {
            throw new NotImplementedException();
        }

        public ICollection<IProductOrderItem> GetProductOrdersByTransactionId(long transactionId)
        {
            throw new NotImplementedException();
        }

        public bool RemoveProductOrder(string basketId, string productOrderNumber)
        {
            throw new NotImplementedException();
        }

        public string UpdateProductOrder(string basketId, IProductOrderItem productOrderItem)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}