@Echo off

rem Set the environment variables used by this script
rem *******************************************
set ServiceName=ItemNotesServiceBean
set ServiceUrl=ItemNotesService_v0200_1.wsdl
set ServiceNamespace=Posten.Portal.Cart.Services.Proxy.%ServiceName%
rem *******************************************

Rem Path to executable
Set SvcUtilExe="c:\program files (x86)\Microsoft SDKs\Windows\v7.0A\bin\SvcUtil.exe"

echo *******************************************
echo Creates proxy for %ServiceName%
echo Url: %ServiceUrl%
echo Namespace: %ServiceNamespace%
echo *******************************************
pause

%SvcUtilExe% %ServiceUrl% /out:..\Proxy\%ServiceName%Proxy.cs /config:..\Proxy\%ServiceName%Proxy.config /namespace:*,%ServiceNamespace% /serializer:XmlSerializer /tcv:Version35

pause

rem NOTE! This file must be saved with encoding CodePage 1252 (not UTF-8!)


