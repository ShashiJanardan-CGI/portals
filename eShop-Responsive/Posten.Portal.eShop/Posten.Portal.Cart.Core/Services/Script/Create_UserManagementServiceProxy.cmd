@echo off
cd /d %~dp0

rem Set the environment variables used by this script
rem *******************************************
set ServiceName=UserManagementService
set ServiceUrl=http://i1.service.utv.posten.se/UserManagementService/%ServiceName%?wsdl
set ServiceNamespace=Posten.Portal.Cart.Services.Proxy.%ServiceName%
rem *******************************************


rem Loading environment for Microsoft Visual Studio 2010 x86 tools
call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86

echo *******************************************
echo Creates proxy for %ServiceName%
echo Url: %ServiceUrl%
echo Namespace: %ServiceNamespace%
echo *******************************************
pause

svcutil %ServiceUrl% /out:..\Proxy\%ServiceName%Proxy.cs  /config:..\Proxy\%ServiceName%Proxy.config /namespace:*,%ServiceNamespace% /serializer:Auto /internal /tcv:Version35
pause

rem NOTE! This file must be saved with encoding CodePage 1252 (not UTF-8!)



