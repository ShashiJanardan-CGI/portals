﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.Cart.BusinessEntities;

    public class AccountServiceStub : IAccountService
    {
        #region Methods

        public bool CreateAccount(IAccountInfo account)
        {
            return true;
        }

        public bool DoesEmailAccountExist(string email)
        {
            switch (email)
            {
                case "jason.erana@logica.com":
                case "amadel.tandog@logica.com":
                case "gau.bundalian@logica.com":
                case "allan.mercader@logica.com":
                case "joakim.regnstrom@logica.com":
                case "anders.stuve@logica.com":
                case "jonas.karlman@logica.com":
                    return true;

                default:
                    return false;
            }
        }

        public IAccountInfo GetAccountInfo(string email, string password)
        {
            if (!this.DoesEmailAccountExist(email))
            {
                return null;
            }

            if (password != "12345")
            {
                return null;
            }

            var retval = new AccountInfo();

            retval.EmailAddress = email;
            retval.CompanyName = "Logica";

            var philippinesAddress = new Posten.Portal.Cart.BusinessEntities.Address()
            {

                StreetAddress = "2/F One World Square, Upper Road Mckinley Hill",
                City = "Taguig City",
                Country = "SE",
                PostalCode = "1634"
            };

            var swedenAddress = new Posten.Portal.Cart.BusinessEntities.Address()
            {

                StreetAddress = "21 Augustendalsvägen",
                City = "Stockholm",
                Country = "SE",
                PostalCode = "13185"
            };

            switch (email)
            {
                case "jason.erana@logica.com":
                    retval.FirstName = "Jason";
                    retval.LastName = "Erana";
                    retval.Address = philippinesAddress;
                    retval.MobileNumber = "+639178550383";
                    break;
                case "amadel.tandog@logica.com":
                    retval.FirstName = "Amadel";
                    retval.LastName = "Tandog";
                    retval.Address = philippinesAddress;
                    retval.MobileNumber = "+639162386593";

                    break;
                case "gau.bundalian@logica.com":
                    retval.FirstName = "Gau";
                    retval.LastName = "Bundalian";
                    retval.Address = philippinesAddress;
                    retval.MobileNumber = "+639164974200";
                    break;
                case "allan.mercader@logica.com":
                    retval.FirstName = "allan";
                    retval.LastName = "Mercader";
                    retval.Address = philippinesAddress;
                    retval.MobileNumber = "+6328584000";
                    break;
                case "joakim.regnstrom@logica.com":
                    retval.FirstName = "Joakim";
                    retval.LastName = "Regnstrom";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    break;
                case "anders.stuve@logica.com":
                    retval.FirstName = "Anders";
                    retval.LastName = "Stuve";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    break;
                case "jonas.karlman@logica.com":
                    retval.FirstName = "Jonas";
                    retval.LastName = "Karlman";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+123441234";
                    break;
            }

            return retval;
        }

        public IAccountInfo GetCurrentUser(string username)
        {
            var retval = new AccountInfo();
            var swedenAddress = new Posten.Portal.Cart.BusinessEntities.Address()
            {
                StreetAddress = "21 Augustendalsvägen",
                City = "Stockholm",
                Country = "SE",
                PostalCode = "13185"
            };

            retval.Address = new Posten.Portal.Cart.BusinessEntities.Address();
            retval.FirstName = "";
            retval.LastName = "";
            retval.Address.StreetAddress = " ";
            retval.Address.City = "";
            retval.Address.Country = "";
            retval.Address.PostalCode = "";
            retval.MobileNumber = "";
            retval.EmailAddress = "";
            retval.CompanyName = "";

            switch (username.ToLower())
            {
                case "int\\install":
                    retval.FirstName = "Anders";
                    retval.LastName = "Stuve";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    retval.EmailAddress="anders.stuve@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test1":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test1";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449101";
                    retval.EmailAddress="eshop.test1@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test2":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test2";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449102";
                    retval.EmailAddress = "eshop.test2@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test3":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test3";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449103";
                    retval.EmailAddress = "teshop.test3@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop Test4":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test4";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449104";
                    retval.EmailAddress = "eshop.test4@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test5":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test5";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449105";
                    retval.EmailAddress = "eshop.test5@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test6":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test6";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449106";
                    retval.EmailAddress = "eshop.test6@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test7":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test7";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449107";
                    retval.EmailAddress = "eshop.test7@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test8":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test8";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449108";
                    retval.EmailAddress = "eshop.test8@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test9":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test9";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449109";
                    retval.EmailAddress = "eshop.test9@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test10":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test10";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    retval.EmailAddress = "eshop.test10@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test11":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test11";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    retval.EmailAddress = "eshop.test11@logica.com";
                    retval.CompanyName = "Logica";
                    break;
                case "eshop test12":
                    retval.FirstName = "eShop";
                    retval.LastName = "Test12";
                    retval.Address = swedenAddress;
                    retval.MobileNumber = "+46768449100";
                    retval.EmailAddress = "eshop.test12@logica.com";
                    retval.CompanyName = "Logica";
                    break;
            }

            return retval;
        }

        #endregion Methods
    }
}