﻿namespace Posten.Portal.Cart.Services
{
    using Microsoft.SharePoint.Client;
    using Posten.Portal.Cart.Services.Proxy.ItemNotesServiceBean;
    using Posten.Portal.Platform.Common.Services;

    public class ItemNotesServiceBeanAgent : ServiceAgentBase, IItemNotesServiceBean
    {
        #region Fields

        private Proxy.ItemNotesServiceBean.ItemNotesServiceBeanClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.ItemNotesServiceBean.ItemNotesServiceBeanClient ServiceClientInstance
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = new ItemNotesServiceBeanClient(); //this.ConfigureServiceClient<Proxy.ItemNotesServiceBean.ItemNotesServiceBeanClient, Proxy.ItemNotesServiceBean.ItemNotesServiceBean>() as Proxy.ItemNotesServiceBean.ItemNotesServiceBeanClient;
                    this.serviceClientInstance.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["SkickaServiceUserName"];
                    this.serviceClientInstance.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["SkickaServiceUserPassword"];
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public void InformPaymentComplete(string[] orderNumbers, status status)
        {
            this.ServiceClientInstance.informPaymentComplete(new paymentCompleteRequest()
            {
                orderNumbers = orderNumbers,
                status = status,
                statusSpecified = true
            });
        }

        public void InformPaymentPending(string buyerEMail, string[] orderNumbers, string transactionId)
        {
            this.ServiceClientInstance.informPaymentPending(new paymentPendingRequest()
            {
                buyerEMail = buyerEMail,
                orderNumbers = orderNumbers,
                transactionId = transactionId
            });
        }

        #endregion Methods
    }
}