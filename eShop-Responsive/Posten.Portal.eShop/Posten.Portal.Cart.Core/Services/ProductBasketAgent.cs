﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services.Proxy.ProductBasketService;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Services;

    public class ProductBasketAgent : ServiceAgentBase, IProductBasketService
    {
        #region Fields

        private Proxy.ProductBasketService.ProductBasketClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.ProductBasketService.ProductBasketClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.ProductBasketService.ProductBasketClient, Proxy.ProductBasketService.IProductBasket>() as Proxy.ProductBasketService.ProductBasketClient;
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public bool AddProductOrder(ref string basketId, IProductOrderItem productOrderItem, ref string orderNumber)
        {
            bool result = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            orderNumber = this.ServiceClient.AddProductOrder(ref basketId, productOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.DropCookie("BasketItemAdded", "BasketItemAdded");
            Utilities.Utils.RevertImpersonation(currentUser);
            return result;
        }

        public string AddProductOrder(string basketId, IProductOrderItem productOrderItem)
        {
            bool result = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            var response = this.ServiceClient.AddProductOrder(ref basketId, productOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.DropCookie("BasketItemAdded", "BasketItemAdded");
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        public IProductOrderItem GetProductOrderItem(string basketId, string productOrderNumber)
        {
            Proxy.ProductBasketService.ProductOrderItem response = new Proxy.ProductBasketService.ProductOrderItem();
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetProductOrderItem(basketId, productOrderNumber);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response.ToBusinessEntity();
        }

        public ICollection<IProductOrderItem> GetProductOrders(string basketId)
        {
            Proxy.ProductBasketService.ProductOrderItem[] response = new Proxy.ProductBasketService.ProductOrderItem[] { };
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetProductOrders(ref basketId);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response.ToBusinessEntityCollection();
        }

        public string GetProductOrdersByBookingId(string bookingId, out ICollection<IProductOrderItem> productOrderItem)
        {
            Proxy.ProductBasketService.ProductOrderItem[] items;
            string response = string.Empty;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetProductOrdersByBookingId(bookingId, out items);
            productOrderItem = items.ToBusinessEntityCollection();
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        public ICollection<IProductOrderItem> GetProductOrdersByBookingId(string bookingId)
        {
            Proxy.ProductBasketService.ProductOrderItem[] items;
            string response = string.Empty;

            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetProductOrdersByBookingId(bookingId, out items);
            Utilities.Utils.RevertImpersonation(currentUser);

            return items.ToBusinessEntityCollection();
        }

        public ICollection<IProductOrderItem> GetProductOrdersByTransactionId(long transactionId)
        {
            Proxy.ProductBasketService.ProductOrderItem[] items;
            string response = string.Empty;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.GetProductOrdersByTransactionId(transactionId.ToString(), out items);
            Utilities.Utils.RevertImpersonation(currentUser);
            return items.ToBusinessEntityCollection();
        }

        public bool RemoveProductOrder(string basketId, string productOrderNumber)
        {
            bool response = false;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.RemoveProductOrder(basketId, productOrderNumber);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        public string UpdateProductOrder(string basketId, IProductOrderItem productOrderItem)
        {
            bool result = false;
            string response = string.Empty;
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.UpdateProductOrder(basketId, productOrderItem.ToProxyEntity(), out result);
            Utilities.Utils.RevertImpersonation(currentUser);
            return response;
        }

        #endregion Methods
    }
}