﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Services;

    public class UserManagementServiceAgent : ServiceAgentBase, IUserManagementService
    {
        #region Fields

        private Proxy.UserManagementService.UserManagementServiceEndpointClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.UserManagementService.UserManagementServiceEndpointClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.UserManagementService.UserManagementServiceEndpointClient, Proxy.UserManagementService.UserManagementServiceEndpoint>() as Proxy.UserManagementService.UserManagementServiceEndpointClient;
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public bool checkEmailAddress(string email)
        {
            throw new NotImplementedException();
        }

        public IAccountInfo GetAccount(string email)
        {
            var user = Utils.ImpersonateUser();
            var response = this.ServiceClient.getAccount(email.ToProxyEntity());
            Utils.RevertImpersonation(user);

            return response.ToBusinessEntity();
        }

        public ICollection<ICodeMessage> SaveAccount(IAccountInfo account)
        {
            var user = Utils.ImpersonateUser();
            var response = this.ServiceClient.saveBasicAccount(account.ToSaveProxyEntity());
            Utils.RevertImpersonation(user);

            return response.ToSaveBusinessEntityCollection();
        }

        public ICollection<ICodeMessage> UpdateAccount(IAccountInfo account)
        {
            var response = this.ServiceClient.updateBasicAccount(account.ToUpdateProxyEntity());

            return response.ToUpdateBusinessEntityCollection();
        }

        #endregion Methods
    }
}