﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    /// <summary>
    /// The i payment service.
    /// </summary>
    public interface IPaymentService
    {
        #region Methods

        IPaymentResponse DirectPayment(IDirectPaymentRequest paymentrequest);

        IPaymentResult FinalisePayment(long transactionId, bool receiptAsPdf, bool receiptAsUrl, Uri responseUrl);

        ICollection<ILegalAddress> GetLegalAddresses(string Ip, string SSNo);

        ICollection<IPaymentMethodResponse> GetPaymentMethods();

        IPaymentResult GetPaymentResult(long transactionId, bool receiptAsPdf, bool receiptAsUrl);

        /// <summary>
        /// The initialise payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <param name="responseUrl">The url that the payment provider should redirect to</param>
        /// <param name="cancelUrl">The url that the payment provider should redirect to if a user cancels the payment</param>
        /// <returns>a response object indicating where we should redirect the user etc.</returns>
        IPaymentResponse InitialisePayment(IPaymentRequest paymentRequest, Uri responseUrl, Uri cancelUrl);

        #endregion Methods
    }
}