﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public interface IServiceBasketService
    {
        #region Methods

        bool AddServiceOrder(ref string basketId, IServiceOrderItem serviceOrderItem, ref string orderNumber);

        IServiceOrderItem GetServiceOrderItem(string basketId, string serviceOrderNumber);

        ICollection<IServiceOrderItem> GetServiceOrders(string basketId);

        ICollection<IServiceOrderItem> GetServiceOrdersByBookingId(string bookingId);

        ICollection<IServiceOrderItem> GetServiceOrdersByTransactionId(long transactionId);

        bool RemoveServiceOrder(string basketId, string serviceOrderNumber);

        string UpdateServiceOrder(string basketId, IServiceOrderItem serviceOrderItem);

        #endregion Methods
    }
}