﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.ServiceModel;
    using System.Text;
    using System.Web;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services.Proxy.PaymentService;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Platform.Common.Logging;
    using Posten.Portal.Platform.Common.Services;

    using PaymentServiceException = Posten.Portal.Cart.BusinessEntities.PaymentServiceException;

    public class PaymentServiceAgent : ServiceAgentBase, IPaymentService
    {
        #region Fields

        private Proxy.PaymentService.PaymentServiceEndpointClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.PaymentService.PaymentServiceEndpointClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.PaymentService.PaymentServiceEndpointClient, Proxy.PaymentService.PaymentServiceEndpoint>() as Proxy.PaymentService.PaymentServiceEndpointClient;
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public IPaymentResponse DirectPayment(IDirectPaymentRequest paymentrequest)
        {
            var currentUser = Utilities.Utils.ImpersonateUser();
            DirectPaymentResponse paymentMethodResponse;
            try
            {
                paymentMethodResponse = this.ServiceClient.directPayment(paymentrequest.ToProxyEntity());
            }
            catch (FaultException< Proxy.PaymentService.PaymentServiceException> ex)
            {
                //Log the exception first
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
                throw new PaymentServiceException(ex.Detail);
            }

            Utilities.Utils.RevertImpersonation(currentUser);
            return paymentMethodResponse.ToBusinessEntity();
        }

        public IPaymentResult FinalisePayment(long transactionId, bool receiptAsPdf, bool receiptAsUrl, Uri responseUrl)
        {
            string url;
            if (responseUrl == null)
                url = string.Empty;
            else
                url = responseUrl.ToString();

            var request = new Proxy.PaymentService.finalisePaymentRequest
                {
                    paymentTransactionId = transactionId,
                    receiptAsPdf = receiptAsPdf,
                    receiptAsUrl = receiptAsUrl,
                    responseUrl = url
                };

            try
            {
                Proxy.PaymentService.FinalisePaymentResponse response = new Proxy.PaymentService.FinalisePaymentResponse();
                var currentUser = Utilities.Utils.ImpersonateUser();
                response = this.ServiceClient.finalisePayment(request);
                Utilities.Utils.RevertImpersonation(currentUser);

                return response.ToBusinessEntity();
            }
            catch (FaultException<Services.Proxy.PaymentService.FinalisePaymentException> exception)
            {
                // TODO: handle bad payment etc
                throw;
            }
        }

        public ICollection<ILegalAddress> GetLegalAddresses(string Ip, string SSNo)
        {
            var argRequest = new Proxy.PaymentService.getAddressesRequest
            {
                customerIp = Ip,
                socialSecNo = SSNo
            };

            Proxy.PaymentService.address[] response = new Proxy.PaymentService.address[] {};
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.getLegalAddresses(argRequest);
            Utilities.Utils.RevertImpersonation(currentUser);

            return response.ToBusinessEntityCollection();
        }

        public ICollection<IPaymentMethodResponse> GetPaymentMethods()
        {
            Proxy.PaymentService.PaymentMethod[] paymentMethodResponse = new Proxy.PaymentService.PaymentMethod[] { };
            var currentUser = Utilities.Utils.ImpersonateUser();
            paymentMethodResponse = this.ServiceClient.getPaymentMethods();
            Utilities.Utils.RevertImpersonation(currentUser);
            return paymentMethodResponse.ToBusinessEntityCollection();
        }

        public IPaymentResult GetPaymentResult(long transactionId, bool receiptAsPdf, bool receiptAsUrl)
        {
            var request = new Proxy.PaymentService.getPaymentResultRequest
                {
                    paymentTransactionId = transactionId,
                    receiptAsPdf = receiptAsPdf,
                    receiptAsUrl = receiptAsUrl
                };

            Proxy.PaymentService.GetPaymentResultResponse response = new Proxy.PaymentService.GetPaymentResultResponse();
            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.getPaymentResult(request);
            Utilities.Utils.RevertImpersonation(currentUser);

            var paymentResult = response.ToBusinessEntity();
            paymentResult.TransactionId = transactionId;
            return paymentResult;
        }

        public IPaymentResponse InitialisePayment(IPaymentRequest paymentRequest, Uri returnUrl, Uri cancelUrl)
        {
            Proxy.PaymentService.InitialisePaymentResponse response = new Proxy.PaymentService.InitialisePaymentResponse();

            var currentUser = Utilities.Utils.ImpersonateUser();
            response = this.ServiceClient.initialisePayment(paymentRequest.ToProxyEntity());
            Utilities.Utils.RevertImpersonation(currentUser);

            var paymentResponse = response.ToBusinessEntity();

            return paymentResponse;
        }

        #endregion Methods
    }

    internal class PaymentUrl
    {
        #region Fields

        private readonly bool canModify;
        private readonly NameValueCollection queryString;
        private readonly UriBuilder uriBuilder;

        #endregion Fields

        #region Constructors

        public PaymentUrl(string url)
        {
            this.uriBuilder = new UriBuilder(url);
            this.queryString = HttpUtility.ParseQueryString(this.uriBuilder.Query);
            this.canModify = string.Compare(this.CalculateMAC(), this["MAC"], true) == 0;
        }

        #endregion Constructors

        #region Properties

        public bool CanModify
        {
            get { return this.canModify; }
        }

        public Uri Uri
        {
            get { return new Uri(this.uriBuilder.ToString()); }
        }

        #endregion Properties

        #region Indexers

        public string this[string name]
        {
            get
            {
                return this.queryString[name];
            }

            set
            {
                if (!this.canModify)
                {
                    return;
                }

                this.queryString[name] = value;
                this.queryString["MAC"] = this.CalculateMAC();
                this.uriBuilder.Query = this.queryString.ToString();
            }
        }

        #endregion Indexers

        #region Methods

        public string CalculateMAC()
        {
            if (!this.queryString.HasKeys())
            {
                return string.Empty;
            }

            string input = string.Empty;
            foreach (string key in this.queryString.AllKeys.Where(item => item != "MAC"))
            {
                input += this.queryString[key];
            }

            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            // Auriga
            input += "n454e6uiiig1qclutdmxydzz91a5m4us";

            var md5 = new MD5CryptoServiceProvider();
            var ba = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            md5.Clear();

            return string.Concat(ba.Select(b => b.ToString("x2")).ToArray());
        }

        public override string ToString()
        {
            return this.Uri.ToString();
        }

        #endregion Methods
    }
}