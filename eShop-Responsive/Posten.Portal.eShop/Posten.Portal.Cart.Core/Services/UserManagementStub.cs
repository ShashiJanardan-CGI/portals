﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.Cart.BusinessEntities;

    public class UserManagementStub : IUserManagementService
    {
        #region Methods

        public bool checkEmailAddress(string email)
        {
            switch (email)
            {
                case "jason.erana@logica.com":
                case "amadel.tandog@logica.com":
                case "gau.bundalian@logica.com":
                case "allan.mercader@logica.com":
                case "joakim.regnstrom@logica.com":
                case "anders.stuve@logica.com":
                case "jonas.karlman@logica.com":
                    return true;

                default:
                    return false;
            }
        }

        public IAccountInfo GetAccount(string email)
        {
            throw new NotImplementedException();
        }

        public ICollection<ICodeMessage> SaveAccount(IAccountInfo account)
        {
            throw new NotImplementedException();
        }

        public ICollection<ICodeMessage> UpdateAccount(IAccountInfo account)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}