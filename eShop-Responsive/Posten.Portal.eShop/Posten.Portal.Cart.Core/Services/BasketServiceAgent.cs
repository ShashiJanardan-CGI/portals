﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services.Proxy.BasketService;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Services;

    public class BasketServiceAgent : ServiceAgentBase, IBasketService
    {
        #region Fields

        private Proxy.BasketService.BasketServiceClient serviceClientInstance;

        #endregion Fields

        #region Properties

        private Proxy.BasketService.BasketServiceClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.BasketService.BasketServiceClient, Proxy.BasketService.IBasketService>() as Proxy.BasketService.BasketServiceClient;
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public bool AssignBillingAccount(string basketId, IBillingAccountInfo billingInfo)
        {
            bool response = false;
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.AssignBillingAccount(basketId, billingInfo.ToProxyEntity());
            Utils.RevertImpersonation(user);

            return response;
        }

        public bool AssignDeliveryAccount(string basketId, IDeliveryAccountInfo deliveryInfo)
        {
            bool response = false;
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.AssignDeliveryAccount(basketId, deliveryInfo.ToProxyEntity());
            Utils.RevertImpersonation(user);
            return response;
        }

        public bool AssignToKnownUser(string basketId, string knownUserId)
        {
            // call web service
            bool response = false;
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.AssignToKnownUser(ref basketId, knownUserId);
            Utils.RevertImpersonation(user);

            return response;
        }

        public bool FinalisePayment(string basketId, DateTime paymentDate, string paymentMethod, Uri receiptUrl, long transactionId, string transactionState)
        {
            bool result = false;
            var user = Utils.ImpersonateUser();
            // call web service
            var response = this.ServiceClient.FinalisePayment(basketId, paymentDate, paymentMethod, receiptUrl.ToString(), transactionId.ToString(), transactionState, out result);
            Utils.RevertImpersonation(user);
            // transform to basket model
            return result;
        }

        public ICollection<IPaymentMethod> GetAvailablePayments(string basketId)
        {
            // call web service
            Proxy.BasketService.PaymentMethodItem[] response = new Proxy.BasketService.PaymentMethodItem[] { };
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.GetAvailablePayments(basketId);
            Utils.RevertImpersonation(user);
            // transform to basket model
            return response.ToBusinessEntityCollection();
        }

        public ICollection<IPaymentArticle> GetBasketArticles(string basketId)
        {
            // call web service
            ICollection<Article> response;
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetBasketArticles(basketId);
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntityCollection();
        }

        public IBasketItem GetBasketItem(string basketId)
        {
            // call web service
            Proxy.BasketService.BasketItem response = new Proxy.BasketService.BasketItem();
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetBasketItem(basketId);
            Utils.RevertImpersonation(currentUser);

            // transform to basket model
            return response.ToBusinessEntity();
        }

        public IBasketItem GetBasketItemByBookingId(string bookingId)
        {
            // call web service
            Proxy.BasketService.BasketItem response = new Proxy.BasketService.BasketItem();
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetBasketItemByBookingId(bookingId);
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntity();
        }

        public IBasketItem GetBasketItemByTransactionId(long transactionId)
        {
            Proxy.BasketService.BasketItem response = new Proxy.BasketService.BasketItem();
            // call web service
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetBasketItemByTransactionId(transactionId.ToString());
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntity();
        }

        public ICollection<IBasketItem> GetBaskets()
        {
            // call web service
            Proxy.BasketService.BasketItem[] response = new Proxy.BasketService.BasketItem[] { };
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetBaskets();
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntityCollection();
        }

        public IBasketItem GetDefaultBasketItem()
        {
            // call web service
            Proxy.BasketService.BasketItem response = new Proxy.BasketService.BasketItem();
             var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetDefaultBasketItem();
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntity();
        }

        public IBasketItem GetResolvedBasketItem(string basketid)
        {
            // call web service
            Proxy.BasketService.BasketItem response = new Proxy.BasketService.BasketItem();
            var currentUser = Utils.ImpersonateUser();
            response = this.ServiceClient.GetResolvedBasketItem(basketid);
            Utils.RevertImpersonation(currentUser);
            // transform to basket model
            return response.ToBusinessEntity();
        }

        public bool InitialisePayment(string basketId, string[] deliveryIds, long transactionId)
        {
            // call web service
            bool response = false;
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.InitialisePayment(basketId, deliveryIds, transactionId.ToString());
            Utils.RevertImpersonation(user);
            // transform to basket model
            return response;
        }

        public string PaymentComplete(string basketId, long transactionId, out bool result)
        {
            result = false;
            var user = Utils.ImpersonateUser();

            // call web service
            var response = this.ServiceClient.PaymentComplete(basketId, transactionId.ToString(), out result);
            Utils.RevertImpersonation(user);

            // transform to basket model
            return response.Message;
        }

        public string PaymentCompleteBypassQueue(string basketId, long transactionId, out bool result)
        {
            result = false;
            var responseMessage = string.Empty;
            try
            {
                var user = Utils.ImpersonateUser();

                // call web service
                var response = this.ServiceClient.PaymentCompleteBypassQueues(basketId, transactionId.ToString(), out result);
                Utils.RevertImpersonation(user);

                responseMessage = response.Message;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            // transform to basket model
            return responseMessage;
        }

        public bool PrepareForCheckout(string basketId, int layoutversion)
        {
            // call web service
            bool response = false;
            var user = Utils.ImpersonateUser();
            Posten.Portal.Cart.Services.Proxy.BasketService.CheckoutPage version = CheckoutPage.Legacy;
            if ( layoutversion == 1 )
                version = CheckoutPage.Legacy;
            if ( layoutversion ==2 )
                version = CheckoutPage.EShop;
            response = this.ServiceClient.PrepareForCheckout(basketId, version);
            Utils.RevertImpersonation(user);
            // transform to basket model
            return response;
        }

        public IBasketItem RecalculateVAT(string basketId, string countrycode, string postalcode)
        {
            var response = this.ServiceClient.RecalculateVAT(basketId,countrycode,postalcode);

            return response.ToBusinessEntity();
        }

        public bool ReverseInitialisePayment(string basketId, long transactionId)
        {
            // call web service
            bool response = false;
            var user = Utils.ImpersonateUser();
            response = this.ServiceClient.ReverseInitialisePayment(basketId, transactionId.ToString());
            Utils.RevertImpersonation(user);
            // transform to basket model
            return response;
        }

        public bool VerifyPaymentComplete(string basketId, long transactionId)
        {
            bool result = false;
            var user = Utils.ImpersonateUser();
            var response = this.ServiceClient.VerifyPaymentComplete(basketId, transactionId.ToString(), out result);
            Utils.RevertImpersonation(user);
            return result;
        }

        #endregion Methods
    }
}