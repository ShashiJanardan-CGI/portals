﻿namespace Posten.Portal.Cart.BusinessEntities
{
    public class DeliveryAccountInfo : IDeliveryAccountInfo
    {
        public string AddressLine { get; set; }

        public string City { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PostalCode { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string Telephone { get; set; }

        public string Organization { get; set; }
    }
}
