﻿using System;
using System.Collections.Generic;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class BasketOrderItem : IBasketOrderItem
    {
        #region IBasketOrderItem Members

        public string BasketId { get; set; }

        public string OrderId { get; set; }

      
        #endregion
    }
}
