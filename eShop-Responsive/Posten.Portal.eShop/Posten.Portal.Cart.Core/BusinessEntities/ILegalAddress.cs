﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface ILegalAddress : IAddress
    {
        LegalAddressType Type { get; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
