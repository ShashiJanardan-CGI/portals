﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface ICodeMessage
    {
        int Code { get; set; }

        string Message {get; set;}
    }
}
