﻿using System;
using System.Collections.Generic;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class BasketItem : IBasketItem
    {
        #region IBasketItem Members

        public string BasketId { get; set; }

        public BasketStatus BasketStatus { get; set; }

        public IBillingAccountInfo BillingAccountInfo { get; set; }

        public string BookingId { get; set; }

        public IDeliveryAccountInfo DeliveryAccountInfo { get; set; }

        public ICollection<IOrderSummaryItem> OrderSummaryItems { get; set; }

        public DateTime PaymentDate { get; set; }

        public string PaymentMethod { get; set; }

        public Uri ReceiptUrl { get; set; }

        public decimal Total { get; set; }

        public decimal TotalExclTax { get; set; }

        public string TotalExclTaxString { get; set; }

        public string TotalString { get; set; }

        public decimal TotalTax { get; set; }

        public string TotalTaxString { get; set; }

        public long TransactionId { get; set; }

        public string TransactionState { get; set; }

        #endregion
    }
}
