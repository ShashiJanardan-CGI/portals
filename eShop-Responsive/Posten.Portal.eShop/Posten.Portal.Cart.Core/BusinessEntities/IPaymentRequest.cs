﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentRequest : IPaymentRequestBase
    {
        string Currency { get; set; }

        string CustomerAddress { get; set; }

        string CustomerAtt { get; set; }

        string CustomerCity { get; set; }

        string CustomerCountry { get; set; }

        string CustomerName { get; set; }

        string CustomerVatNo { get; set; }

        string CustomerZip { get; set; }

        string Language { get; set; }

        string TransferText { get; set; }

        string UserId { get; set; }
    }
}
