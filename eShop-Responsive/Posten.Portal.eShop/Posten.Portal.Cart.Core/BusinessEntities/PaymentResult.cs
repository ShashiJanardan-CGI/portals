﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentMethodResult : IPaymentMethodResponse
    {

        public long id {get; set;}
        
        public string name{get; set;}

        public string description { get; set; }

        public PaymentMethodType type
        {
            get
            {
                PaymentMethodType result = PaymentMethodType.CreditCard;
                if (this.id == 1) result = PaymentMethodType.PayPage;
                else if (this.id == 2) result = PaymentMethodType.CreditCard;
                else if (this.id == 3) result = PaymentMethodType.OnlineBanking;
                else if (this.id == 4) result = PaymentMethodType.Invoice;
                return result;
            }
        }
        
    }
}
