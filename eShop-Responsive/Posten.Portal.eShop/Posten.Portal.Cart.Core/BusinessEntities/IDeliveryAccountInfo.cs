﻿namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IDeliveryAccountInfo
    {
        string AddressLine { get; set; }

        string City { get; set; }

        string CountryCode { get; set; }

        string CountryName { get; set; }

        string Description { get; set; }

        string Email { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string PostalCode { get; set; }

        string RegionCode { get; set; }
        
        string RegionName { get; set; }

        string Telephone { get; set; }

        string Organization { get; set; }
    }
}
