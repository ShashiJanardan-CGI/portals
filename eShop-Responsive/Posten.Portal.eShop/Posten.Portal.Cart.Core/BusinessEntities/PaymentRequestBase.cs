﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentRequestBase : IPaymentRequestBase
    {
        public long ClientId { get; set; }
        public long PaymentMethod { get; set; }
        public long TotalAmount { get; set; }
        public long TotalAmountNoTax { get; set; }
        public long TotalTax { get; set; }
        public bool CaptureNow { get; set; }
        public ICollection<IPaymentArticle> Articles { get; set; }
    }
}
