﻿using System.Runtime.Serialization;

namespace Posten.Portal.Cart.BusinessEntities
{
    [DataContract]
    public class PostalCodeInfo
    {
        [DataMember(Name = "postalcode")]
        public string PostalCode { get; set; }

        [DataMember(Name = "postalcity")]
        public string City { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "box")]
        public bool Box { get; set; }
    }
}
