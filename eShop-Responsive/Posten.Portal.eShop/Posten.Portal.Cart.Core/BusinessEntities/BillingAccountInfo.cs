﻿namespace Posten.Portal.Cart.BusinessEntities
{
    public class BillingAccountInfo : IBillingAccountInfo
    {
        public string AddressLine { get; set; }

        public string City { get; set; }

        public string Email { get; set; }

        public bool IncludeAddressInfo { get; set; }

        public string Name { get; set; }

        public string Organization { get; set; }

        public string PostalCode { get; set; }

        public string Telephone { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string LastName { get; set; }
    }
}
