﻿namespace Posten.Portal.Cart.BusinessEntities
{
    public class PrivateLegalAddress : LegalAddress, IPrivateLegalAddress 
    {

       
        #region ILegalAddress Members

        public override LegalAddressType Type
        {
            get { return LegalAddressType.Private; }
        }

        #endregion
    }
}
