﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Posten.Portal.Cart.BusinessEntities
{
    
    public abstract class LegalAddress : ILegalAddress
    {
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public abstract LegalAddressType Type { get;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
    }
}
