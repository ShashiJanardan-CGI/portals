﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IAddress
    {
         string StreetAddress { get; set; }

         string PostalCode { get; set; }

         string City { get; set; }

         string Country { get; set; }
    }
}
