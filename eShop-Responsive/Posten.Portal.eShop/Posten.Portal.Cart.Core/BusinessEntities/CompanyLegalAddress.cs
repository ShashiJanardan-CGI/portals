﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    
    public class CompanyLegalAddress : LegalAddress, ICompanyLegalAddress
    {
        public override LegalAddressType Type
        {
            get { return LegalAddressType.Company; }
        }


        #region ICompanyLegalAddress Members

        public string CompanyName { get; set; }

        #endregion



        
    }
}
