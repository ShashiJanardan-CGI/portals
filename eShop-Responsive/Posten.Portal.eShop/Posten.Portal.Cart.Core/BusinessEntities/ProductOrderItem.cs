﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class ProductOrderItem : IProductOrderItem
    {
        public string BasketId { get; set; }

        public string OrderNumber { get; set; }

        public string ArticleId { get; set; }

        public string ArticleName { get; set; }

        public decimal ArticlePrice { get; set; }

        public string CurrencyCode { get; set; }

        public int Quantity { get; set; }

        public decimal Vat { get; set; }

        public decimal VatPercentage { get; set; }

        public decimal ArticlePricewithVAT { get { return ArticlePrice*(1m + (VatPercentage)); } }

        ////public decimal Total { get; set; }

        public string Taxcode { get; set; }

        public decimal Total
        {
            get { return (ArticlePrice * (1m + (VatPercentage))) * Quantity; }
        }

        public decimal TotalExcludingVat 
        {
            get { return (ArticlePrice * Quantity); } 
        }
        
    }
}
