﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IDeliveryIdMapping
    {
        string DeliveryId { get; set; }
        
        long RowNumber { get; set; }
    }
}
