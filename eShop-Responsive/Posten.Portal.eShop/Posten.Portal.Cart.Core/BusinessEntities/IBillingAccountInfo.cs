﻿namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IBillingAccountInfo
    {
        string AddressLine { get; set; }

        string City { get; set; }

        string Email { get; set; }

        bool IncludeAddressInfo { get; set; }

        string Name { get; set; }

        string LastName { get; set; }

        string Organization { get; set; }

        string PostalCode { get; set; }

        string Telephone { get; set; }

        string CountryCode { get; set; }

        string CountryName { get; set; }
    }
}
