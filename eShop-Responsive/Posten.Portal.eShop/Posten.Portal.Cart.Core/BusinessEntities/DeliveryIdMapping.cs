﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class DeliveryIdMapping : IDeliveryIdMapping
    {
        public string DeliveryId { get; set; }

        public long RowNumber { get; set; }
    }
}
