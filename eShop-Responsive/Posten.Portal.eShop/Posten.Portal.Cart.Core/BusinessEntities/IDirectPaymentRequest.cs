﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IDirectPaymentRequest : IPaymentRequestBase
    {
        ILegalAddress LegalAddress { get; set; }

        ILegalAddress BillingAddress { get; set; }

        ICustomer Customer{ get; set; }

        string Currency{ get; set; }

        string Language{ get; set; }
    }
}
