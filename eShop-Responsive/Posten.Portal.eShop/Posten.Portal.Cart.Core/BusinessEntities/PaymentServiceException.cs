﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentServiceException : Exception
    {
        internal PaymentServiceException(Posten.Portal.Cart.Services.Proxy.PaymentService.PaymentServiceException ex):base(ex.Nodes[2].InnerText)
        {
            this.PaymentErrorCode = ex.Nodes[0].InnerText;
            this.LocalizedMessage = ex.Nodes[2].InnerText;
        }

        public PaymentServiceException(string message) : base(message)
        {
        }

        public PaymentServiceException(string message, Exception innerexception)
            : base(message, innerexception)
        {

        }

        public string PaymentErrorCode { get; set; }

        public string LocalizedMessage { get; set; }

    }
}
