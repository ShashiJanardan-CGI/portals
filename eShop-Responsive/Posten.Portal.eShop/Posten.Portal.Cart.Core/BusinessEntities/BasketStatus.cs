﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public enum BasketStatus : int
    {
        InProcess = 0,

        ReadyForCheckout = 1,

        Ordered = 2,

        PaymentFailed = 3,

        Canceled = 4,

        NewOrder = 5,

        Rejected = 6,

        Shipped = 7,

        Submitted = 8,

        InitializePaymentError = 9,

        RedirectedToPayment = 10,

        SentToFinalizePayment = 11,

        FinalizePaymentError = 12,

        SentToProduce = 13,

        ProduceError = 14,

        ProduceSuccess = 15,

        SentToConfiramtion = 16,

        ConfirmationError = 17,

        ConfirmationSuccess = 18
    }

    public enum OrderItemType : int
    {
        Product = 0,

        Service = 1,
    }

    public enum PaymentMethodType : long
    {
        PayPage = 1,
        CreditCard = 2,
        OnlineBanking = 3,
        Invoice = 4
    }
}
