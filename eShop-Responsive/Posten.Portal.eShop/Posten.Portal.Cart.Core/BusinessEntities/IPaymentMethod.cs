﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentMethod
    {
        string Description { get; set; }

        long Id { get; set; }

        string Name { get; set; }

        PaymentMethodType Type { get; set; }
    }
}
