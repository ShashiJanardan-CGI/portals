﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{

    public class BlockedServices
    {
        public string ApplicationID { get; set; }
        public string Name { get; set; }
    }


    public class CartSettingItems
    {
        public string defaultpayment { get; set; }

        public string invoiceoption { get; set; }
        public int DisplayableCartItems { get; set; }

        public decimal PrivateInvoiceMaxAmount { get; set; }
        public decimal CompanyInvoiceMaxAmount { get; set; }

        public bool LoginRequiredForInvoicePayment { get; set; }

        public bool EnableInvoicePayment { get; set; }
        public bool EnableCreditCardPayment { get; set; }
        public bool EnableInternetBankingPayment { get; set; }

        public ICollection<BlockedServices> BlockServices { get; set; }
        public IList<string> BlockSSNs { get; set; }

    }
}
