﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentMethod : IPaymentMethod
    {
        public string Description { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        public PaymentMethodType Type { get; set; }
    }
}
