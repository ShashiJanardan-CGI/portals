﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class Customer : ICustomer
    {
        public string IpAddress { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ReferenceText { get; set; }
    }
}
