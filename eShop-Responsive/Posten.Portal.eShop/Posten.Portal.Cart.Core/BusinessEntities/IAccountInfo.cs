﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IAccountInfo
    {
         string FirstName { get; set; }

         string LastName { get; set; }

         string CompanyName { get; set; }

         string EmailAddress { get; set; }

         string MobileNumber { get; set; }

         Address Address { get; set; }

         string CareOfAddress { get; set; }

         string CompanyCode { get; set; }

         string PersonalCodeNumber { get; set; }

         string SmsNumber { get; set; }

         string State { get; set; }

         string StateChangeReason { get; set; }

         string StateChangeTime { get; set; }

         string StateChangedBy { get; set; }

         string Type { get; set; }

         string Password { get; set; }
    }
}
