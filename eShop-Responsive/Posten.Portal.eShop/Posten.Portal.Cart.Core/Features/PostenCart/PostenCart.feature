﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="62435a25-17e3-4e36-b284-778ff013dfc4" defaultResourceFile="PostenCart" description="$Resources:Feature_PostenCart_Description;" featureId="62435a25-17e3-4e36-b284-778ff013dfc4" imageUrl="Posten/PostenFeature.gif" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.f2bb2562-223d-444b-8a83-c13fa563864d.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:Feature_PostenCart_Title;" version="AAEAAAD/////AQAAAAAAAAAEAQAAAA5TeXN0ZW0uVmVyc2lvbgQAAAAGX01ham9yBl9NaW5vcgZfQnVpbGQJX1JldmlzaW9uAAAAAAgICAgCAAAAAAAAAAAAAAAAAAAACw==" upgradeActionsReceiverAssembly="$SharePoint.Project.AssemblyFullName$" upgradeActionsReceiverClass="$SharePoint.Type.f2bb2562-223d-444b-8a83-c13fa563864d.FullName$" deploymentPath="$SharePoint.Feature.FileNameWithoutExtension$_2.0" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="ce4ea2cf-5f38-40e0-9a41-62caec31b328" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="6a5eb74c-9de8-4f75-a637-0731d114182d" />
    <projectItemReference itemId="ead65860-0f20-4774-b18d-b204b3ee3ca1" />
    <projectItemReference itemId="02fb47a2-4471-4e7a-8bfe-6bc1556e0ce0" />
    <projectItemReference itemId="42106d4b-1c60-4837-b1eb-4311c6c4e45d" />
    <projectItemReference itemId="14578b12-b6fe-4c48-93a8-aa0da178e681" />
  </projectItems>
</feature>