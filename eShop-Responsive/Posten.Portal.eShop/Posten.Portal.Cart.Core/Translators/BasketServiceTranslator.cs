﻿using System;
using System.Collections.Generic;
using System.Linq;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Services.Proxy.BasketService;
using Posten.Portal.Cart.Utilities;
using BasketItem = Posten.Portal.Cart.BusinessEntities.BasketItem;
using BasketStatus = Posten.Portal.Cart.BusinessEntities.BasketStatus;
using BillingAccountInfo = Posten.Portal.Cart.BusinessEntities.BillingAccountInfo;
using DeliveryAccountInfo = Posten.Portal.Cart.BusinessEntities.DeliveryAccountInfo;
using OrderItemType = Posten.Portal.Cart.BusinessEntities.OrderItemType;
using OrderSummaryItem = Posten.Portal.Cart.BusinessEntities.OrderSummaryItem;

namespace Posten.Portal.Cart.Translators
{
    internal static class BasketServiceTranslator
    {
        internal static IPaymentMethod ToBusinessEntity(this Services.Proxy.BasketService.PaymentMethodItem item)
        {
            return new PaymentMethod
            {
                Description = Utils.Trim(item.Description), 
                Id = item.Id,
                Name = Utils.Trim(item.Name),
                Type =
                    Enum.IsDefined(typeof(PaymentMethodType), item.Id)
                        ? (PaymentMethodType)item.Id
                        : PaymentMethodType.PayPage
            };
        }

        internal static IOrderSummaryItem ToBusinessEntity(this Services.Proxy.BasketService.OrderSummaryItem item)
        {
            return new OrderSummaryItem
            {
                Created = item.Created,
                CurrencyCode = item.CurrencyCode,
                Details = Utils.Trim(item.Details),
                FreeTextField1 = Utils.Trim(item.FreeTextField1),
                FreeTextField2 = Utils.Trim(item.FreeTextField2),
                LinePrice = item.LinePrice,
                LinePriceString = item.LinePriceString,
                LineTaxString = item.LineTaxString,
                ListPrice = item.LinePrice,
                Name = Utils.Trim(item.Name),
                OrderItemImageUrl = item.OrderItemImageUrl.ToUri(),
                OrderItemType = (OrderItemType)item.OrderItemType,
                OrderNumber = item.OrderNo,
                Quantity = item.Quantity,
                Vat = item.Vat
            };
        }

        internal static Services.Proxy.BasketService.BillingAccountInfo ToProxyEntity(this IBillingAccountInfo item)
        {
            return new Services.Proxy.BasketService.BillingAccountInfo
            {
                AddressLine = item.AddressLine,
                City = item.City,
                Email = item.Email,
                IncludeAddressInfo = item.IncludeAddressInfo,
                Name = item.Name,
                Organization = item.Organization,
                PostalCode = item.PostalCode,
                Telephone = item.Telephone, 
                CountryCode = item.CountryCode, 
                CountryName= item.CountryName, 
                LastName = item.LastName
            };
        }

        internal static Services.Proxy.BasketService.DeliveryAccountInfo ToProxyEntity(this IDeliveryAccountInfo item)
        {
            return new Services.Proxy.BasketService.DeliveryAccountInfo
            {
                AddressLine = item.AddressLine,
                City = item.City,
                CountryCode = item.CountryCode,
                CountryName = item.CountryName,
                Description = item.Description,
                Email = item.Email,
                FirstName = item.FirstName,
                LastName = item.LastName,
                PostalCode = item.PostalCode,
                RegionCode = item.RegionCode,
                RegionName = item.RegionName,
                Telephone = item.Telephone, 
                Organization = item.Organization
            };
        }

        internal static IBasketItem ToBusinessEntity(this Services.Proxy.BasketService.BasketItem item)
        {
            return new BasketItem
            {
                BasketId = item.BasketId,
                BasketStatus = (BasketStatus)item.BasketStatus,
                BillingAccountInfo = item.BillingAccountInfo.ToBusinessEntity(),
                BookingId = item.BookingId,
                DeliveryAccountInfo = item.DeliveryAccountInfo.ToBusinessEntity(),
                OrderSummaryItems = item.OrderSummaryItems.ToBusinessEntityCollection(),
                PaymentDate = item.PaymentDate,
                PaymentMethod = item.PaymentMethod,
                ReceiptUrl = item.ReceiptUrl.ToUri(),
                Total = item.Total,
                TotalExclTax = item.TotalExclTax,
                TotalExclTaxString = item.TotalExclTaxString,
                TotalString = item.TotalString,
                TotalTax = item.TotalTax,
                TotalTaxString = item.TotalTaxString,
                TransactionId = Utils.ToLong(item.TransactionId),
                TransactionState = item.TransactionState
            };
        }

        internal static IDeliveryAccountInfo ToBusinessEntity(this Services.Proxy.BasketService.DeliveryAccountInfo item)
        {
            return new DeliveryAccountInfo
            {
                AddressLine = Utils.Trim(item.AddressLine),
                City = Utils.Trim(item.City),
                CountryCode = Utils.Trim(item.CountryCode),
                CountryName = Utils.Trim(item.CountryName),
                Description = Utils.Trim(item.Description),
                Email = Utils.Trim(item.Email),
                FirstName = Utils.Trim(item.FirstName),
                LastName = Utils.Trim(item.LastName),
                PostalCode = Utils.Trim(item.PostalCode),
                RegionCode = Utils.Trim(item.RegionCode),
                RegionName = Utils.Trim(item.RegionName),
                Telephone = Utils.Trim(item.Telephone),
                Organization = Utils.Trim(item.Organization)
            };
        }

        internal static IBillingAccountInfo ToBusinessEntity(this Services.Proxy.BasketService.BillingAccountInfo item)
        {
            return new BillingAccountInfo
            {
                AddressLine = Utils.Trim(item.AddressLine),
                City = Utils.Trim(item.City),
                Email = Utils.Trim(item.Email),
                IncludeAddressInfo = item.IncludeAddressInfo,
                Name = Utils.Trim(item.Name),
                Organization = Utils.Trim(item.Organization),
                PostalCode = Utils.Trim(item.PostalCode),
                Telephone = Utils.Trim(item.Telephone),
                CountryCode = Utils.Trim(item.CountryCode),
                CountryName = Utils.Trim(item.CountryName),
                LastName = Utils.Trim(item.LastName),
            };
        }

        internal static ICollection<IPaymentMethod> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.BasketService.PaymentMethodItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICollection<IOrderSummaryItem> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.BasketService.OrderSummaryItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICollection<IBasketItem> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.BasketService.BasketItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICollection<IPaymentArticle> ToBusinessEntityCollection(this IEnumerable<Article> articles)
        {
            return articles.Select(item => item.ToBusinessEntity()).Cast<IPaymentArticle>().ToList();
        }

        internal static PaymentArticle ToBusinessEntity(this Article article)
        {
            PaymentArticle returnvalue =new PaymentArticle()
                       {
                           ArticleDescription = article.ArticleDescription,
                           ArticleId = article.ArticleId,
                           ArticleName =  article.ArticleName,
                           ArticlePrice = Convert.ToInt64(Math.Round(article.ArticlePrice * 100m)),
                           CompanyCode = article.CompanyCode,
                           CustomerId=article.CustomerId,
                           FootnoteText=article.FootnoteText,
                           OrderNumber=article.OrderNumber,
                           Quantity = Convert.ToInt64( article.Quantity),
                           QuantityDecimals = 0,
                           Tax = Convert.ToInt64( article.Tax*100),
                           TaxCode =article.TaxCode,
                           TaxCountry = article.TaxCountry,
                           TaxRate=Convert.ToInt32(article.TaxRate*10000),
                           TotalPrice= Convert.ToInt64(article.TotalPrice*100),
                           TotalPriceVatIncluded = Convert.ToInt64((article.TotalPrice + article.Tax)* 100)
                           
                       };

            //Dochecks for Products
            if (article.IsProduct)
            {
                //set company code and customerid
                returnvalue.CustomerId = "11052892";
                returnvalue.CompanyCode = "F092";
                returnvalue.TaxCountry = "SE";
            }

            return returnvalue;
        }
    }
}
