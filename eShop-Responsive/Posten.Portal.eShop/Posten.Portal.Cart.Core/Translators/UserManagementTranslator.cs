﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.Cart.Services.Proxy.UserManagementService;
using System.Collections.ObjectModel;
using Posten.Portal.Cart.BusinessEntities;
using System.Configuration;

namespace Posten.Portal.Cart.Translators
{
    internal static class UserManagementTranslator
    {
        internal static getAccountRequest ToProxyEntity(this string item)
        {
            return new getAccountRequest
            {
                key = "EMAIL", //TODO
                value = item
            };
        }

        internal static IAccountInfo ToBusinessEntity(this GetAccountResponse item)
        {
            return new AccountInfo
            {
                FirstName = item.firstName,
                LastName = item.lastName,
                EmailAddress = item.email,
                CompanyCode = item.companyCode,
                CompanyName = item.companyName,
                MobileNumber = item.phoneNumber,
                PersonalCodeNumber = item.personalCodeNumber,
                SmsNumber = item.smsNumber,
                StateChangedBy = item.stateChangedBy,
                Type = item.type,
                StateChangeTime = item.stateChangeTime,
                CareOfAddress = item.careOfAddress,
                StateChangeReason = item.stateChangeReason,
                Address = new Posten.Portal.Cart.BusinessEntities.Address { City = item.city, Country = item.country, StreetAddress = item.streetAddress, PostalCode = item.postalCode },
                State = item.state
            };
        }

        internal static updateBasicAccountRequest ToUpdateProxyEntity(this IAccountInfo item)
        {
            return new updateBasicAccountRequest
            {
                city = item.Address.City,
                coAddress = item.CareOfAddress,
                country = item.Address.Country,
                email = item.EmailAddress,
                emailOfUpdater = item.EmailAddress,
                firstName = item.FirstName,
                language = "en", //TODO
                lastName = item.LastName,
                personalCodeNumber = item.PersonalCodeNumber,
                phoneNumber = item.MobileNumber,
                postalCode = item.Address.PostalCode,
                smsNumber = item.SmsNumber,
                streetAddress = item.Address.StreetAddress
            };
        }

        internal static saveBasicAccountRequest ToSaveProxyEntity(this IAccountInfo item)
        {
            return new saveBasicAccountRequest
            {
                activationUrl = ConfigurationManager.AppSettings["TemporaryActivationUrl"].ToString(), //TODO
                city = item.Address.City,
                coAddress = item.Address.StreetAddress,
                companyCode = item.CompanyCode,
                conditionsAccepted = true,
                country = item.Address.Country,
                email = item.EmailAddress,
                firstName = item.FirstName,
                language = "", //TODO
                languageForWelcomeEmail = "", //TODO
                lastName = item.LastName,
                legalName = item.FirstName,
                password = item.Password,
                personalCodeNumber = item.PersonalCodeNumber,
                popularName = "", //TODO
                postalCode = item.Address.PostalCode,
                smsNumber = item.SmsNumber,
                streetAddress = item.Address.StreetAddress,
                type = item.Type
            };
        }

        internal static ICollection<ICodeMessage> ToUpdateBusinessEntityCollection(this UpdateBasicAccountResponse items)
        {
            return items.codeMessages.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICollection<ICodeMessage> ToSaveBusinessEntityCollection(this SaveBasicAccountResponse items)
        {
            return items.codeMessages.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ICodeMessage ToBusinessEntity(this codeMessage item)
        {
            return new CodeMessage
            {
                Code = item.code,
                Message = item.message
            };
        }

    }
}
