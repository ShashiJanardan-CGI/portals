﻿using System.Collections.Generic;
using System.Linq;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Utilities;
using ProxyProductOrderItem = Posten.Portal.Cart.Services.Proxy.ProductBasketService.ProductOrderItem;

namespace Posten.Portal.Cart.Translators
{
    internal static class ProductBasketTranslator
    {
        internal static ProxyProductOrderItem ToProxyEntity(this IProductOrderItem item)
        {
            return new ProxyProductOrderItem
            {
                ArticleId = Utils.Trim(item.ArticleId), 
                ArticleName = Utils.Trim(item.ArticleName), 
                ArticlePrice = item.ArticlePrice, 
                BasketId = item.BasketId, 
                CurrencyCode = item.CurrencyCode, 
                OrderNo = item.OrderNumber, 
                Quantity = item.Quantity, 
                Total = item.Total, 
                Vat = item.Vat, 
                VatPercentage = item.VatPercentage
            };
        }

        internal static IProductOrderItem ToBusinessEntity(this ProxyProductOrderItem item)
        {
            return new ProductOrderItem
            {
                ArticleId = Utils.Trim(item.ArticleId), 
                ArticleName = Utils.Trim(item.ArticleName), 
                ArticlePrice = item.ArticlePrice, 
                BasketId = item.BasketId, 
                CurrencyCode = item.CurrencyCode, 
                OrderNumber = item.OrderNo, 
                Quantity = item.Quantity, 
                Vat = item.Vat, 
                VatPercentage = item.VatPercentage,
                Taxcode=null
            };
        }

        internal static ProxyProductOrderItem[] ToProxyEntityArray(this IEnumerable<IProductOrderItem> items)
        {
            return items.Select(item => item.ToProxyEntity()).ToArray();
        }

        internal static ICollection<IProductOrderItem> ToBusinessEntityCollection(this IEnumerable<ProxyProductOrderItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }
    }
}
