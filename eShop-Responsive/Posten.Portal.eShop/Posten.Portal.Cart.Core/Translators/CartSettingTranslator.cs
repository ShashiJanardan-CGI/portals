﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.Cart.BusinessEntities;
using Microsoft.SharePoint;

namespace Posten.Portal.Cart.Translators
{
    internal static class CartSettingTranslator
    {
        public static readonly Guid ValueID = new Guid("DEAC0379-74E2-4447-915A-7A80E8AA4F96");
        private const string INVOICE_TITLE = "Invoice Option";
        private const string PAYMENT_TITLE = "Default Payment Type";
        private const string SERVICES_MAX_ITEMS = "Cart Max Items";
        private const string PRIVATE_MAX_AMOUNT_TITLE = "PrivateInvoiceMaxAmount";
        private const string COMPANY_MAX_AMOUNT_TITLE = "CompanyInvoiceMaxAmount";
        private const string ENABLE_CREDIT_CARD_TITLE = "EnableCreditCardPayment";
        private const string ENABLE_INVOICE_TITLE = "EnableInvoicePayment";
        private const string ENABLE_INTERNET_BANKING_TITLE = "EnableInternetBankingPayment";
        public const string BLOCK_SERVICES = "BlockServices";
        public const string BLOCK_SSN = "Blocked SSN";


        internal static string ToDelimitedString(this ICollection<BlockedServices> bServices)
        {
            StringBuilder str = new StringBuilder();
            foreach (var item in bServices)
            {
                str.Append(string.Format("{0}:{1},", item.ApplicationID, item.Name));
            }
            if (str.Length >0)  str.Remove(str.Length - 1, 1);
            return str.ToString();  
        }

        internal static string ToDelimitedString(this IList<string> ssns)
        {
            StringBuilder str = new StringBuilder();
            foreach (var item in ssns)
            {
                str.Append(string.Format("{0},", item));
            }
            if (str.Length > 0) str.Remove(str.Length - 1, 1);
            return str.ToString();
        }

        internal static ICollection<BlockedServices> ToBlockedServicesCollection(this string configValue)
        {
            List<BlockedServices> services = new List<BlockedServices>();
            string[] rawServices = configValue.Split(',');
            foreach (var service in rawServices)
            {
                if (service != string.Empty)
                {
                    BlockedServices bservice = new BlockedServices();
                    string[] serviceValue = service.Split(':');
                    bservice.ApplicationID = serviceValue[0];
                    bservice.Name = serviceValue[1];
                    services.Add(bservice);
                }
            }
            return services;
        }

        internal static IList <string> ToBlockedSSNCollection(this string configValue)
        {
            if (string.IsNullOrEmpty(configValue))
            {
                return new List<string>();
            }

            string[] SSNs = configValue.Split(',');
            return SSNs.ToList();
        }


        internal static CartSettingItems ListItemCollectionToEntity(SPListItemCollection items)
        {
            CartSettingItems entity = new CartSettingItems();

            foreach (SPListItem item in items)
            {
                decimal maxValue = 0;
                int maxcartitems = 0;
                string configValue = (item[ValueID] == null) ? string.Empty : item[ValueID].ToString();


                switch (item.Title)
                {
                    case PAYMENT_TITLE:
                        entity.defaultpayment = configValue;
                        break;
                    case INVOICE_TITLE:
                        entity.invoiceoption = configValue;
                        entity.LoginRequiredForInvoicePayment = configValue.ToLower() == "yes" ? true : false;
                        break;
                    case PRIVATE_MAX_AMOUNT_TITLE:
                        decimal.TryParse(configValue, out maxValue);
                        entity.PrivateInvoiceMaxAmount = maxValue;
                        break;
                    case COMPANY_MAX_AMOUNT_TITLE:
                        decimal.TryParse(configValue, out maxValue);
                        entity.CompanyInvoiceMaxAmount = maxValue;
                        break;
                    case ENABLE_CREDIT_CARD_TITLE:
                        entity.EnableCreditCardPayment = configValue.ToLower() == "yes" ? true : false; ;
                        break;
                    case ENABLE_INVOICE_TITLE:
                        entity.EnableInvoicePayment = configValue.ToLower() == "yes" ? true : false; ;
                        break;
                    case ENABLE_INTERNET_BANKING_TITLE:
                        entity.EnableInternetBankingPayment = configValue.ToLower() == "yes" ? true : false; ;
                        break;
                    case BLOCK_SERVICES:
                        entity.BlockServices = configValue.ToBlockedServicesCollection();
                        break;
                    case BLOCK_SSN:
                        entity.BlockSSNs  = configValue.ToBlockedSSNCollection();
                        break;
                    case SERVICES_MAX_ITEMS:
                        Int32.TryParse(configValue, out maxcartitems);
                        entity.DisplayableCartItems = maxcartitems;
                        break;
                }
            }

            return entity;

        }

        internal static SPListItemCollection EntityToListItemCollection(CartSettingItems entity, SPListItemCollection items)
        {
            foreach (SPListItem item in items)
            {
                switch (item.Title)
                {
                    case PAYMENT_TITLE:
                        item[ValueID] = entity.defaultpayment;
                        break;
                    case INVOICE_TITLE:
                        item[ValueID] = entity.invoiceoption;
                        break;
                    case PRIVATE_MAX_AMOUNT_TITLE:
                        item[ValueID] = entity.PrivateInvoiceMaxAmount;
                        break;
                    case COMPANY_MAX_AMOUNT_TITLE:
                        item[ValueID] = entity.CompanyInvoiceMaxAmount;
                        break;
                    case ENABLE_CREDIT_CARD_TITLE:
                        item[ValueID] = entity.EnableCreditCardPayment ? "Yes" : "No";
                        break;
                    case ENABLE_INTERNET_BANKING_TITLE:
                        item[ValueID] = entity.EnableInternetBankingPayment ? "Yes" : "No";
                        break;
                    case ENABLE_INVOICE_TITLE:
                        item[ValueID] = entity.EnableInvoicePayment ? "Yes" : "No";
                        break;
                    case SERVICES_MAX_ITEMS:
                        item[ValueID] = entity.DisplayableCartItems;
                        break;
                }
                item.Update();
            }

            return items;
        }


    }
}
