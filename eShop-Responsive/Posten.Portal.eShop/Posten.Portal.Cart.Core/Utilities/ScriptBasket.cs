﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Posten.Portal.Cart.Utilities
{
    public enum ScriptBasketOrderItemType : int
    {
        Service = 1,
        Product = 2
    }

    public interface IScriptBasketOrderItem
    {
        string OrderNumber { get; }

        ScriptBasketOrderItemType Type { get; }

        string Id { get; }

        string Name { get; }

        string Price { get; }

        string Description { get; }
    }

    public interface IScriptBasketServiceOrderItem : IScriptBasketOrderItem
    {
        string EditUrl { get; }
    }

    public interface IScriptBasketProductOrderItem : IScriptBasketOrderItem
    {
        int Quantity { get; }
    }

    public interface IScriptBasket
    {
        Guid BasketId { get; }

        string ValueIncludingVat { get; }

        string ValueExcludingVat { get; }

        int NumberOfOrderItems { get; }

        IScriptBasketOrderItem[] OrderItems { get; }
    }

    public class ScriptBasketServiceOrderItem : IScriptBasketServiceOrderItem
    {
        public string OrderNumber { get; internal set; }

        public ScriptBasketOrderItemType Type
        {
            get { return ScriptBasketOrderItemType.Service; }
        }

        public string Id { get; internal set; }

        public string Name { get; internal set; }

        public string Price { get; internal set; }

        public string Description { get; internal set; }

        public string EditUrl { get; internal set; }
    }

    public class ScriptBasketProductOrderItem : IScriptBasketProductOrderItem
    {
        public string OrderNumber { get; internal set; }

        public ScriptBasketOrderItemType Type
        {
            get { return ScriptBasketOrderItemType.Product; }
        }

        public string Id { get; internal set; }

        public string Name { get; internal set; }

        public string Price { get; internal set; }

        public string Description { get; internal set; }

        public int Quantity { get; internal set; }
    }

    public class ScriptBasket : IScriptBasket
    {
        internal static readonly ScriptBasket Empty = new ScriptBasket { BasketId = Guid.Empty, ValueExcludingVat = 0m.ToString("C"), ValueIncludingVat = 0m.ToString("C") };
        private ICollection<IScriptBasketOrderItem> orderItems;

        public Guid BasketId { get; internal set; }

        public string ValueIncludingVat { get; internal set; }

        public string ValueExcludingVat { get; internal set; }

        public int NumberOfOrderItems
        {
            get { return this.OrderItems.Length; }
        }

        public IScriptBasketOrderItem[] OrderItems
        {
            get
            {
                if (this.orderItems == null)
                {
                    this.orderItems = new List<IScriptBasketOrderItem>();
                }

                return this.orderItems.ToArray();
            }

            internal set
            {
                this.orderItems = value.ToList();
            }
        }
    }
}
