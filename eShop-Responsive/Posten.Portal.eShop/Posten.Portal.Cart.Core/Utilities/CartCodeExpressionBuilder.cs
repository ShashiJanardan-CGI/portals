﻿using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;

namespace Posten.Portal.Cart.Presentation
{
    [ExpressionPrefix("Code")]
    public class CartCodeExpressionBuilder : ExpressionBuilder 
    {
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        {
            return new CodeSnippetExpression(entry.Expression);
        }
    }
}
