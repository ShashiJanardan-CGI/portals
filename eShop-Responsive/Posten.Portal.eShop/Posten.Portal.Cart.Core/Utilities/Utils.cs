﻿using System;
using System.Configuration;

namespace Posten.Portal.Cart.Utilities
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using Posten.Portal.Platform.Common.Logging;
    using System.Reflection;
    using System.Security.Principal;
    using System.Web;

    public static class Utils
    {
        public static string Trim(string value)
        {
            return (value ?? string.Empty).Trim();
        }

        public static long ToLong(string value)
        {
            return ToLong(value, 0);
        }

        public static long ToLong(string value, long defaultValue)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return defaultValue;
                }

                return Convert.ToInt64(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Guid ToGuid(this string value)
        {
            try
            {
                return new Guid(value);
            }
            catch
            {
                return Guid.Empty;
            }
        }

        public static T ToEnum<T>(this string value, T defaultValue)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return defaultValue;
                }

                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Uri ToUri(this string value)
        {
            try
            {
                return new Uri(value);
            }
            catch
            {
                return null;
            }
        }

        internal static void CheckNullValue(string name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(name);
            }
        }

        internal static void CheckNullValue(string name, object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(name);
            }
        }

        public static void RecursiveLog(this Exception ex)
        {
            string logLessage = string.Format("{0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace);
            MethodBase currentMethod = MethodInfo.GetCurrentMethod();
            
            PostenLogger.LogError(
                logLessage,
                PostenLogger.ApplicationLogCategory,
                currentMethod != null ? currentMethod.DeclaringType : null,
                currentMethod != null ? currentMethod.Name : null
            );

            if (ex.InnerException != null) {                
                ex.InnerException.RecursiveLog();
            }
        }

        public static IPrincipal ImpersonateUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated) return HttpContext.Current.User;
            var tempUser = HttpContext.Current.User;
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity("user"), null);
            return tempUser;
        }

        public static void RevertImpersonation(IPrincipal user)
        {
            if (!HttpContext.Current.User.Equals(user)) HttpContext.Current.User = user;
        }

        public static string ToSafeString(this Uri value)
        {
            if (value == null)
                return string.Empty;

            return value.ToString();
        }

        public static void DropCookie(string name, string value)
        {
            //Check if this is an httprequest
            if (HttpContext.Current == null)
                return;

            HttpContext.Current.Response.Cookies[name].Value=value;
            HttpContext.Current.Response.Cookies[name].HttpOnly = false;
            HttpContext.Current.Response.Cookies[name].Expires = DateTime.Now.AddDays(1);
            HttpContext.Current.Response.Cookies[name].Domain = ConfigurationManager.AppSettings["CookieDomain"];
        
        }

        public static Uri ToWebSealUrl(this Uri value)
        {
            var websealhost = ConfigurationManager.AppSettings["CartWebSealUrl"];
            if (string.IsNullOrEmpty(websealhost))
                return value;

            UriBuilder ub = new UriBuilder();
            ub.Host = websealhost;
            ub.Fragment = value.Fragment;
            ub.Query = value.Query;
            ub.Scheme = value.Scheme;
            ub.Path = value.AbsolutePath;
            return ub.Uri;
                
        }

        public static string GetResourceString(string key)
        {
            return Posten.Portal.Platform.Common.Services.ResourceService.GetString(key, "PostenCart");
        }
    }
}
