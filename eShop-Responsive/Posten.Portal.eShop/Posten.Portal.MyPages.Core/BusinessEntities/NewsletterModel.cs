﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.MyPages.BusinessEntities
{
    public class NewsletterModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
