﻿using System;
using System.Web;
using Microsoft.SharePoint;

namespace Posten.Portal.MyPages
{
    public class MyPagesContext
    {
        private const string HttpContextItemKey = "PostenMyPagesContext";

        private const string PropertyKeyUsername = "username";
        private const string PropertyKeyMail = "mail";
        private const string PropertyKeyFirstName = "firstname";
        private const string PropertyKeyLastName = "lastname";

        private MyPagesContext()
        {
        }

        public static MyPagesContext Current
        {
            get
            {
                MyPagesContext context = null;
                if (HttpContext.Current != null)
                {
                    try
                    {
                        context = GetContext(HttpContext.Current);
                    }
                    catch
                    {
                        // TODO: logging
                    }
                }

                return context;
            }
        }

        public IUserProfile CurrentUser
        {
            get { return null; }
        }

        public bool IsAuthenticated
        {
            get
            {
                // HttpContext.Current.User.Identity.IsAuthenticated
                return SPContext.Current != null && 
                       SPContext.Current.Web != null && 
                       SPContext.Current.Web.CurrentUser != null;
            }
        }

        public string Name
        {
            get
            {
                string name = string.Empty;

                SPWeb web = SPContext.Current.Web;
                if (((web != null) && (web.CurrentUser != null)) && !string.IsNullOrEmpty(web.CurrentUser.Name))
                {
                    name = web.CurrentUser.Name;
                }

                HttpContext current = HttpContext.Current;
                if (((name == string.Empty) && (current.User != null)) && ((current.User.Identity != null) && !string.IsNullOrEmpty(current.User.Identity.Name)))
                {
                    name = current.User.Identity.Name;
                }

                return name;
            }
        }

        public string Username
        {
            get
            {
                string username = string.Empty;

                SPWeb web = SPContext.Current.Web;
                if (((web != null) && (web.CurrentUser != null)) && !string.IsNullOrEmpty(web.CurrentUser.LoginName))
                {
                    username = web.CurrentUser.LoginName;
                }

                HttpContext current = HttpContext.Current;
                if (((username == string.Empty) && (current.User != null)) && ((current.User.Identity != null) && !string.IsNullOrEmpty(current.User.Identity.Name)))
                {
                    username = current.User.Identity.Name;
                }
                
                return username;
            }
        }

        public static MyPagesContext GetContext(HttpContext httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            MyPagesContext context = httpContext.Items[HttpContextItemKey] as MyPagesContext;
            if (context == null)
            {
                context = new MyPagesContext();
                httpContext.Items[HttpContextItemKey] = context;
            }

            return context;
        }

        public object GetProperty(string key)
        {
            // TODO: implement
            return null;
        }
    }
}
