﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.MyPages
{
    public enum UserType
    {
        BasicPrivate,
        BasicCompany,
        PrimaryCompany
    }

    public interface IUserProfile
    {
        string UserId { get; }

        string Username { get; }

        string Mail { get; }

        string FirstName { get; }

        string LastName { get; }

        UserType UserType { get; }

        IAddress Address { get; }
    }

    /*
     * 
user_id
old PSE user name
user name
password
first name
last name
number of faulty logins
SMS number
email
language
general terms
     * 
     *  
c/o address
street address
house number
postal code
city
country
     * 
     * 
legal name
popular name
company code
     * 
     * 
personal code number
     * 
     */
}
