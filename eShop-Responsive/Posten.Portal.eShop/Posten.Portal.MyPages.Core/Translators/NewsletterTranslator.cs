﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.MyPages.BusinessEntities;
using Posten.Portal.MyPages.Services.Proxy.NewsletterService;
using System.Collections.ObjectModel;

namespace Posten.Portal.MyPages.Translators
{
    internal static class NewsletterTranslator
    {
        internal static NewsletterModel ToBusinessEntity(this Services.Proxy.NewsletterService.NewsletterItem item)
        {
            return new NewsletterModel
            {
                Id = item.Id,
                Name = item.Name
            };
        }

        internal static ICollection<NewsletterModel> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.NewsletterService.NewsletterItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static NewsletterItem[] ToProxyEntityCollection(this Collection<NewsletterModel> items)
        {
            return items.Select(item => item.ToProxyEntity()).ToArray();
        }

        internal static NewsletterItem ToProxyEntity(this NewsletterModel item)
        {
            return new NewsletterItem
            {
                Id = item.Id,
                Name = item.Name
            };
        }
    }
}
