﻿using System.Collections.Generic;
using Posten.Portal.MyPages.BusinessEntities;
using Posten.Portal.MyPages.Translators;
using Posten.Portal.Platform.Common.Services;

namespace Posten.Portal.MyPages.Services
{
    public class OrderHistoryAgent : ServiceAgentBase, IOrderHistoryService
    {
        private Proxy.OrderHistoryService.OrderHistoryServiceClient serviceClientInstance;

        private Proxy.OrderHistoryService.OrderHistoryServiceClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.OrderHistoryService.OrderHistoryServiceClient, Proxy.OrderHistoryService.IOrderHistoryService>() as Proxy.OrderHistoryService.OrderHistoryServiceClient;
                }

                return this.serviceClientInstance;
            }
        }

        public ICollection<OrderHistoryModel> GetOrderHistory(string username)
        {
            // Call web-service
            var response = this.ServiceClient.GetOrderHistory();

            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }
    }
}
