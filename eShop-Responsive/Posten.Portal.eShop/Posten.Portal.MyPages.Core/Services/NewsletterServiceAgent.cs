﻿using System.Collections.Generic;
using System.Linq;
using Posten.Portal.MyPages.BusinessEntities;
using Posten.Portal.MyPages.Translators;
using Posten.Portal.Platform.Common.Services;
using System.Collections.ObjectModel;

namespace Posten.Portal.MyPages.Services
{
    public class NewsletterServiceAgent : ServiceAgentBase, INewsletterService
    {
        private Proxy.NewsletterService.NewsletterServiceClient serviceClientInstance;

        private Proxy.NewsletterService.NewsletterServiceClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.NewsletterService.NewsletterServiceClient, Proxy.NewsletterService.INewsletterService>() as Proxy.NewsletterService.NewsletterServiceClient;
                }

                return this.serviceClientInstance;
            }
        }

        public ICollection<NewsletterModel> GetNewsletterInterests()
        {
            // Call web-service
            var response = this.ServiceClient.GetNewsletters();

            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }

        public ICollection<NewsletterModel> GetUserNewsletter()
        {
            // Call web-service
            var response = this.ServiceClient.GetUserNewsletters();

            // Translate to a list with extention method
            return response.ToBusinessEntityCollection();
        }

        public bool RegisterUserNewsletter(Collection<NewsletterModel> interests)
        {
            // Call web-service
            var response = this.ServiceClient.RegisterNewsletter(interests.ToProxyEntityCollection());

            // Translate to a list with extention method
            return response;            
        }
    }
}
