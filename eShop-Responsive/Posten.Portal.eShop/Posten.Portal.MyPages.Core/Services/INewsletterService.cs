﻿using System.Collections.Generic;
using System.ComponentModel;
using Posten.Portal.MyPages.BusinessEntities;
using System.Collections.ObjectModel;

namespace Posten.Portal.MyPages.Services
{
    [Category("My Pages"), Description("This service is used to store newsletter subscriptions infromation in commerce server")]
    public interface INewsletterService
    {
        ICollection<NewsletterModel> GetNewsletterInterests();

        ICollection<NewsletterModel> GetUserNewsletter();

        bool RegisterUserNewsletter(Collection<NewsletterModel> interests);
    }
}
