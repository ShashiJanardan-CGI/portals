using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Posten.Portal.Platform.Common.Utilities;

namespace Posten.Portal.MyPages.EventReceivers
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid("bb967841-a25a-4a91-ba86-e4efad777570")]
    public class WebConfigEventReceiver : WebConfigEngine
    {
        protected override string OwnerModif
        {
            get { return "Posten.Portal.MyPages"; }
        }

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            // Add mapping to order-history webservice
            AddUnityMapping(typeof(Services.IOrderHistoryService), typeof(Services.OrderHistoryAgent), typeof(Services.OrderHistoryStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.OrderHistoryService.IOrderHistoryService), "http://localhost:8777/OrderHistoryService.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            // Add mapping to newsletter webservice
            AddUnityMapping(typeof(Services.INewsletterService), typeof(Services.NewsletterServiceAgent), typeof(Services.NewsletterServiceStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.NewsletterService.INewsletterService), "http://localhost:8777/NewsletterService.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            // Apply changes
            ApplyChangesInWebConfig(properties);
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            // Roll-back web.config changes
            RemoveChangesInWebConfig(properties);
        }

        public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, IDictionary<string, string> parameters)
        {
        }
    }
}
