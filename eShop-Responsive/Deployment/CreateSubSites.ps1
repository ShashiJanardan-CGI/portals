param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml"
)
if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

[xml]$xml = (Get-Content $InputFile)

$variationsubsites=$xml.Deployment.Webapplication.SiteCollection.EshopVariations.Split(",")
$environment = $xml.Deployment.Webapplication.Url

foreach($variationlanguagepair in $variationsubsites)
{
	$variation = $variationlanguagepair.Split(";")[1]
	$subsitelanguage= $variationlanguagepair.Split(";")[0]
	$subsiteName= $variationlanguagepair.Split(";")[2]

	try
	{
		$subwebtocreate=$environment + "/" + $variation +"/eshop"
		$web = get-spweb $subwebtocreate -ErrorAction SilentlyContinue
	}
	catch
	{ 
		$web = $null
	}

	if ($web -eq $null)
	{
		Write-host "Subsite does not exist. Creating:" $subwebtocreate;
		New-SpWeb -url $subwebtocreate -Name $subsiteName -Language $subsitelanguage -Template "POSTENESHOP#0"
		Write-Host "Done"
	}
}