
if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

$SourcePath ="EshopManagedProperties.xml"

function CreateMapping
([string]$managedpropertyname,
	[string]$crawledpropertyname, [int] $type, [int]$enabledforscoping)
{
	$crawledproperty = Get-SPEnterpriseSearchMetadataCrawledProperty -Name $crawledpropertyname -SearchApplication $searchapp  -ErrorAction silentlycontinue

	$managedproperty = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $managedpropertyname  -ErrorAction silentlycontinue
	
	if($managedproperty -eq $null)
	{
		$managedproperty = New-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Name $managedpropertyname -Type $type -EnabledForScoping $enabledforscoping
	}
	
	if($crawledproperty -ne $null)
	{
		New-SPEnterpriseSearchMetadataMapping -SearchApplication $searchapp -ManagedProperty $managedproperty -CrawledProperty $crawledproperty -ErrorAction silentlycontinue
	}
	else
	{
		Write-Host $crawledpropertyname -nonewline
		Write-Host " was not found metadatamapping will not be created."
	}
}

Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content $SourcePath))		


$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name


$Metadatanodes = $xmldata.SelectNodes("SearchApplication/ManagedProperties/Mapping")
foreach($node in $Metadatanodes)
{	
	Write-Host "Creating ManagedProperty: " $node.ManagedProperty -NoNewline
	Write-Host "using crawledproperty: " $node.CrawledProperty
	CreateMapping $node.ManagedProperty $node.CrawledProperty $node.Type $node.EnabledForScoping
}

Write-Host "ManagedProperties creation is completed."