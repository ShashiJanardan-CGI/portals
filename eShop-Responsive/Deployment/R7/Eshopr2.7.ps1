param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml"
)

if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

[xml]$xml = (Get-Content $InputFile)
$environment = $xml.Deployment.Webapplication.Url
write-host $environment
$enurl =$environment + "/en/eshop" 
$enexists = (Get-SPWeb $enurl -ErrorAction SilentlyContinue) 

if($enexists -eq $null)
{
	Write-Host "Creating English Subsite..." -NoNewline
	New-SPWeb $enurl -Template "POSTENESHOP#0" -language 1033
	Write-Host "DONE."
}

$svurl = $environment +"/sv/eshop"

$svexists = (Get-SPWeb $svurl  -ErrorAction SilentlyContinue) 

if($svexists -eq $null)
{
	Write-Host "Creating swedish Subsite..." -NoNewline
	New-SPWeb $svurl -Template "POSTENESHOP#0" -language 1053
	Write-Host "DONE."
}