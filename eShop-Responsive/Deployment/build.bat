@echo off
cd /d %~dp0

set config=%1
IF "%1"=="" GOTO Continue
set config=Release
set outdir=..\..\Deployment\out\

call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86

%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.eShop.Dependencies\Posten.Portal.eShop.Dependencies.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.Cart.Core\Posten.Portal.Cart.Core.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.Cart.Presentation\Posten.Portal.Cart.Presentation.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.eShop.Core\Posten.Portal.eShop.Core.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.eShop.Search\Posten.Portal.eShop.Search.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.eShop.Presentation\Posten.Portal.eShop.Presentation.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.MyPages.Core\Posten.Portal.MyPages.Core.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%
%FrameworkDir%%FrameworkVersion%\MSBuild.exe /p:Configuration=%config% /m /v:m ..\Posten.Portal.eShop\Posten.Portal.MyPages.Presentation\Posten.Portal.MyPages.Presentation.csproj /t:Clean /t:Package /p:BasePackagePath=%outdir%

rem Copy wsp-files to wsp-dir
mkdir Packages
copy out\*.wsp Packages /y
copy out\*.Cart.Core.dll Packages /y
copy ..\Dependencies\*.wsp Packages /y
del /q /s out
rmdir out

pause
