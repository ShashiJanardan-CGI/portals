
Write-Host "Loading Powershell addin ..." -nonewline
Add-PSSnapin Microsoft.SharePoint.PowerShell
Write-host "done"

#read variables from EshopManagedProperties.xml
Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content "EshopManagedProperties.xml"))		

$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name

$searchconfigurationnode = $xmldata.SelectSingleNode("SearchApplication/SearchConfiguration")
$ServiceContextUrl = $searchconfigurationnode.Url
$ServiceApplicationProxyGroupName = $searchconfigurationnode.ServiceApplicationProxyGroup
$SearchServiceApplicationUrl = $searchconfigurationnode.CatalogServiceUrl

Write-Host "retrieving Business Connectivity Services..." -nonewline
$bcsService = Get-SPServiceInstance | where-object {$_.TypeName -eq "Business Data Connectivity Service"} 
write-host "done"

if($bcsService.Status -ne "Online")
{
	
	Start-SPServiceInstance $bcsService
	Write-Host "Starting BCS Service" -nonewline	
	
	#refresh $bcsService to check if status is online
	while($bcsService.Status -ne "Online")
	{
		Write-Host "." -nonewline
		Start-Sleep -s 10
		$bcsService = Get-SPServiceInstance | where-object {$_.TypeName -eq "Business Data Connectivity Service"} 
	}
	write-host "done"
}

write-host "Adding BCS Service into the default Application Proxies..." -nonewline
$bcsServiceproxy = Get-SPServiceApplicationProxy | where { $_.Name -eq "Business Data Connectivity Service" }
if($bcsServiceproxy -ne $null)
{
	Add-SPServiceApplicationProxyGroupMember $searchconfigurationnode.ServiceApplicationProxyGroup -Member $bcsServiceproxy
}
write-host "done"

write-host "Cleaning up existing models..." -nonewline
$model = Get-SPBusinessDataCatalogMetadataObject -Name "ProductsModel" -BdcObjectType Model -ServiceContext $ServiceContextUrl

if($model -ne $null)
{
	Remove-SPBusinessDataCatalogModel �Identity $model
}
write-host "Done"
#Activate the feature that will add the BCS Model
# Set the farm properties that will be needed by the application
$farm = Get-SPFarm
write-host "Setting Farm properties..." -nonewline
$farm.Properties["SearchServiceApplicationUrl"] = $SearchServiceApplicationUrl
$farm.Properties["SiteUrlforProductBcs"] = $ServiceContextUrl

$farm.update()
write-host "done"


Disable-SPFeature "EshopSearchScopes_1.0" -Force -ErrorAction silentlycontinue
Enable-SPFeature "EshopSearchScopes_1.0"


$model = Get-SPBusinessDataCatalogMetadataObject -Name "ProductsModel" -BdcObjectType Model -ServiceContext $ServiceContextUrl

#add allauthenticated users to view data
$allAuthUsers = New-SPClaimsPrincipal -EncodedClaim "c:0(.s|true"

foreach($bcsidentitytogiveaccess in $xmldata.SelectNodes("SearchApplication/AccessControlList/AccessControlEntry"))
{
	
	$claim= New-SPClaimsPrincipal -Identity $bcsidentitytogiveaccess.Principal -IdentityType $bcsidentitytogiveaccess.type
	foreach( $right in $bcsidentitytogiveaccess.SelectNodes("Right"))
	{
		write-host "Grant Access to the Model:", $model.Name, " to ", $bcsidentitytogiveaccess.Principal, "..."  -nonewline
	
		Grant-SPBusinessDataCatalogMetadataObject �Identity $Model �Principal $claim �Right $right.BdcRight
		write-host "Done"
	}
	
}

write-host "Grant Access to the Model:" , $model.Name, " to All Authenticated Users..." -nonewline
Grant-SPBusinessDataCatalogMetadataObject �Identity $model �Principal $allAuthUsers �Right Execute
write-host "Done"





#addaccess to all entities for good emasure
foreach($entity in $model.AllEntities)
{
	
	foreach($bcsidentitytogiveaccess in $xmldata.SelectNodes("SearchApplication/AccessControlList/AccessControlEntry"))
	{
		
		$claim= New-SPClaimsPrincipal -Identity $bcsidentitytogiveaccess.Principal -IdentityType $bcsidentitytogiveaccess.type
		
		
		
		foreach( $right in $bcsidentitytogiveaccess.SelectNodes("Right"))
		{
			write-host "Grant Access to the Entity:" , $entity.Name, " to ", $bcsidentitytogiveaccess.Principal, "..." -nonewline
			Grant-SPBusinessDataCatalogMetadataObject �Identity $entity �Principal $claim �Right $right.BdcRight
			write-host "Done"
		}
		
		
	}
	
	write-host "Grant Access to the Entity:" , $entity.Name, " to All Authenticated Users..." -nonewline
	Grant-SPBusinessDataCatalogMetadataObject �Identity $entity �Principal $allAuthUsers �Right Execute
	write-host "Done"
	
	#copy permissions to children	
	write-host "Propagating Access to the Entity:" , $entity.Name, " to Allchildren" -nonewline
	Copy-SPBusinessDataCatalogAclToChildren -MetadataObject $entity
}





$ServiceApplicationProxyGroupName="COS2 Proxy Group"
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name
#Create content source
$ssa=Get-SPEnterPriseSearchServiceApplication -Identity $searchapp
$ssaContent = new-object Microsoft.Office.Server.Search.Administration.Content($ssa)


$lobSystems = @("ProductsLobSystem","ProductLobSystemInstance")
$proxyGroup = Get-SPServiceApplicationProxyGroup $ServiceApplicationProxyGroupName


if ($ssaContent.ContentSources["ProductsContentSource"] -ne $null)
{
	Get-SPEnterpriseSearchCrawlContentSource -Identity "ProductsContentSource" -SearchApplication $searchapp | Remove-SPEnterpriseSearchCrawlContentSource
}

$ProductsContentSource = New-SPEnterpriseSearchCrawlContentSource -name "ProductsContentSource" -searchapplication $ssa -Type Business -LOBSystemSet $lobSystems -BDCApplicationProxyGroup $proxyGroup


Write-Host "The Content Sources have been created. Please follow the next steps, they will have to be done manually."
Write-Host "Go to Central Administration -> Manage Service Applications ->" $SearchAppnode.Name  -BackgroundColor Red
Write-Host "On the Left Navigation select Content Sources, Edit ProductsContentSource" -BackgroundColor Red
Write-Host "On the External Datasource column, Check ProductLobSystemInstance and click OK " -BackgroundColor Red
Write-Host "Press any key when all of the above has been completed to continue ..." -BackgroundColor Red

$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#reget the content shource since it was updated
$ProductsContentSource = Get-SPEnterpriseSearchCrawlContentSource -Identity "ProductsContentSource" -SearchApplication $searchapp 

write-host "Restarting Search Service..." -nonewline
Restart-Service -displayname "Sharepoint Server Search 14"
Write-Host "Done" 
Write-Host "Starting Crawl" 
$ProductsContentSource.StartFullCrawl()
Start-Sleep -s 5
Write-Host "." -nonewline 
while( $ProductsContentSource.CrawlStatus -ne "Idle")
{
	Write-Host "." -nonewline
	Start-Sleep -s 10
}
Write-Host "Done."  

Write-Host "Crawl Completed." 

Write-Host "Creating Managed Properties" 
.\CreateEshopManagedProperties.ps1
Write-Host "Creating Managed Properties Completed" 