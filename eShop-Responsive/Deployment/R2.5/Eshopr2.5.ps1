param ([string]$environment)

if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}


$enexists = (Get-SPWeb $environment/en/eshop  -ErrorAction SilentlyContinue) -ne $null

if(!$enexists)
{
	Write-Host "Creating English Subsite..." -NoNewline
	New-SPWeb $environment/en/eshop -Template "POSTENESHOP#0" -language 1033
	Write-Host "DONE."
}

$svexists = (Get-SPWeb $environment/sv/eshop  -ErrorAction SilentlyContinue) -ne $null

if(!$svexists)
{
	Write-Host "Creating swedish Subsite..." -NoNewline
	New-SPWeb $environment/sv/eshop -Template "POSTENESHOP#0" -language 1053
	Write-Host "DONE."
}