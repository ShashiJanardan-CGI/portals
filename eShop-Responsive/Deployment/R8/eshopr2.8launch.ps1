param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml"
)

if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

[xml]$xml = (Get-Content $InputFile)
$environment = $xml.Deployment.Webapplication.Url

function AddPage([string]$plName,[string]$pageName, [string]$currentenvironment)
{
	try
	{
		$web= get-spweb $currentenvironment
	}
	Catch [system.exception]
	{
		return
	}
	
	$psite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($web.Site)
	$pWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)
	$pl = $psite.GetPageLayouts($false) | Where { $_.Name -eq $plName }
	$pagesListName = $pweb.PagesListName
 
	if (-not($pl)) {
		throw "Page Layout '$plName' not found"
	}
 
	[Microsoft.SharePoint.Publishing.PublishingPage]$page = $null
	$file = $web.GetFile("$pagesListName/$pageName")
	if (-not($file.Exists)) {
		Write-Host "Page $pageName not found. Creating..." -NoNewline
		$page = $pweb.AddPublishingPage($pageName, $pl)
		Write-Host "DONE" -ForegroundColor Green
		
		Write-Host "publishing $pageName..." -NoNewline
		$page.ListItem.File.CheckIn("")
		$page.ListItem.File.Publish("")
		Write-Host "DONE" -ForegroundColor Green
	}
	
	
}

#check input parameters
if($environment -eq '')
{
	Write-Host "Please Provide the correct web (e.g. Http://samfunk3.utv.posten.se)" -ForegroundColor Red
	return
}

Write-Host "Loading Powershell addin ..." -nonewline
Add-PSSnapin Microsoft.SharePoint.PowerShell
Write-host "done"

#check if valid site collection
$web = get-spsite $environment -ErrorAction SilentlyContinue

if($web -eq $null)
{
	Write-Host "Please Provide the correct web (e.g. Http://samfunk3.utv.posten.se)"  -ForegroundColor Red
	return
}

#deactivate features for minicart.
Write-Host "Deactivating features for minicart on current subsite:" -nonewline
Write-Host $environment + "/sv/eshop ..." -nonewline
$currentenvironment = $environment +"/sv/eshop" 
Disable-SPFeature -identity "941b3d90-1fab-4754-8226-c1d957f8950e" -Url $currentenvironment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

Write-Host "Deactivating features for minicart on current subsite:" -nonewline
Write-Host "$environment/en/eshop" -nonewline
$currentenvironment = $environment +"/sv/eshop" 
Disable-SPFeature -identity "941b3d90-1fab-4754-8226-c1d957f8950e" -Url $currentenvironment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

#Activate feature for all web.
Write-Host "Activating feature for minicart to be enabled on all subsites:" -nonewline
Write-Host $environment -nonewline
Enable-SPFeature -identity "29f68ef7-a974-48eb-857c-92929e3bdace" -Url $environment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

#Create search pages
Write-Host "Create Search Pages for swedish site..." -nonewline
$currentenvironment = "$environment/sv/eshop" 

AddPage "SearchPageLayout.aspx" "Search.aspx" $currentenvironment

Write-Host "Done" -ForegroundColor Green

Write-Host "Create Search Pages for English site..." -nonewline
$currentenvironment = "$environment/en/eshop" 
AddPage "SearchPageLayout.aspx" "Search.aspx" $currentenvironment

Write-Host "Done" -ForegroundColor Green


Write-Host "Editing Search Scopes for English site to include Product Pages..." -nonewline
$currentenvironment = "$environment/en/eshop" 
AddPage "SearchPageLayout.aspx" "Search.aspx" $currentenvironment

Write-Host "Done" -ForegroundColor Green
