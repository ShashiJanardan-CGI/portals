Write-Host "Loading Powershell addin ..." -nonewline
Add-PSSnapin Microsoft.SharePoint.PowerShell
Write-host "done"

#read variables from EshopManagedProperties.xml
Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content "EshopManagedProperties.xml"))		
$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name
$searchconfigurationnode = $xmldata.SelectSingleNode("SearchApplication/SearchConfiguration")
$ServiceContextUrl = $searchconfigurationnode.Url
$ServiceApplicationProxyGroupName = $searchconfigurationnode.ServiceApplicationProxyGroup


write-host "Updating Point Search scope: se_sv" 
$newscope = Get-SPEnterpriseSearchQueryScope -Identity "se_sv" -SearchApplication $searchapp -Url $url -ErrorAction silentlycontinue
if($newscope -ne $null)
{
	New-SPEnterpriseSearchQueryScopeRule -RuleType PropertyQuery -ManagedProperty "ContentSource" -PropertyValue "ProductsContentSource" -FilterBehavior "Include" -scope $newscope -SearchApplication $searchapp -Url $ServiceContextUrl
	New-SPEnterpriseSearchQueryScopeRule -RuleType PropertyQuery -ManagedProperty "ProductLanguage" -PropertyValue "en-US" -FilterBehavior "Exclude" -scope $newscope -SearchApplication $searchapp -Url $ServiceContextUrl
}
write-host "done" 

write-host "Updating Point Search scope: se_en" 

$newscope = Get-SPEnterpriseSearchQueryScope -Identity "se_en" -SearchApplication $searchapp -Url $url -ErrorAction silentlycontinue
if($newscope -ne $null)
{
	New-SPEnterpriseSearchQueryScopeRule -RuleType PropertyQuery -ManagedProperty "ContentSource" -PropertyValue "ProductsContentSource" -FilterBehavior "Include" -scope $newscope -SearchApplication $searchapp -Url $ServiceContextUrl
	New-SPEnterpriseSearchQueryScopeRule -RuleType PropertyQuery -ManagedProperty "ProductLanguage" -PropertyValue "sv-SE" -FilterBehavior "Exclude" -scope $newscope -SearchApplication $searchapp -Url $ServiceContextUrl
}
write-host "done" 

