param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml"
)

if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

[xml]$xml = (Get-Content $InputFile)
$environment = $xml.Deployment.Webapplication.Url

$swedishcontent = "<div class='tmpEshop'>     <a href='/sv/Sidor/iframes/ebutiken/e-butik.aspx'>e-butiken</a></div> <style>.tmpEshop {    position: absolute;    width: 119px;    background: url('/PublishingImages/ebutik_btn_rnd.png') no-repeat;    height: 47px;    top: -114px;    left: 622px;    text-align: right;} #Head {    z-index: auto;} .tmpEshop a {    display: block;    margin-top: 14px;    margin-right: 12px;    color: #fff !important;    z-index: 100;}</style>"
$englishcontent = "<div class='tmpEshop'>     <a href='/sv/Sidor/iframes/ebutiken/e-butik.aspx'>e-shop</a></div> <style>.tmpEshop {    position: absolute;    width: 119px;    background: url('/PublishingImages/ebutik_btn_rnd.png') no-repeat;    height: 47px;    top: -114px;    left: 622px;    text-align: right;} #Head {    z-index: auto;} .tmpEshop a {    display: block;    margin-top: 14px;    margin-right: 12px;    color: #fff !important;    z-index: 100;}</style>"
$webpartTitle ="L�tSt�_L�nk_Eshop"

function AddContentEditorWebpart([string]$zone,[string]$webpartTitle, [string]$content, $page)
{
	#checkout page before adding webpart. 
	$page.Checkout()
	try
	{
		$wpm= $page.GetLimitedWebPartManager([System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
	}
	Catch [system.exception]
	{
		return
	}
	
	#Assume that the webpart does not exist.
	#xml to import
	$currentpath = $pwd.path + "\fakeminicart.dwp"
	$xmlreader = New-Object System.Xml.XmlTextReader($currentpath)
	$outputtext=""
	$webpart=$wpm.ImportWebPart($xmlreader,[ref] $outputtext)
	
	$xmlDoc = New-Object System.Xml.XmlDocument
	$xmlElement = $xmlDoc.CreateElement("MyElement")
	$xmlElement.InnerText = $content
	
	$webpart.Content= $xmlElement
	
	
	$wpm.AddWebPart($webpart, $zone, 1)
	$wpm.SaveChanges($webpart)
	$page.CheckIn("")
	$page.Publish("")
	
}

#check input parameters
if($environment -eq '')
{
	Write-Host "Please Provide the correct web (e.g. Http://samfunk3.utv.posten.se)" -ForegroundColor Red
	return
}

#check if valid site collection
$web = get-spsite $environment -ErrorAction SilentlyContinue

if($web -eq $null)
{
	Write-Host "Please Provide the correct web (e.g. Http://samfunk3.utv.posten.se)"  -ForegroundColor Red
	return
}

#deactivate features for minicart.
Write-Host "Activating features for minicart on current subsite:" -nonewline
Write-Host $environment + "/sv/eshop ..." -nonewline
$currentenvironment = $environment +"/sv/eshop" 
Enable-SPFeature -identity "941b3d90-1fab-4754-8226-c1d957f8950e" -Url $currentenvironment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

Write-Host "Activating features for minicart on current subsite:" -nonewline
Write-Host "$environment/en/eshop" -nonewline
$currentenvironment = $environment +"/sv/eshop" 
Enable-SPFeature -identity "941b3d90-1fab-4754-8226-c1d957f8950e" -Url $currentenvironment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

#Deactivate feature for all web.
Write-Host "Deactivating feature for minicart to be enabled on all subsites:" -nonewline
Write-Host $environment -nonewline
Disable-SPFeature -identity "29f68ef7-a974-48eb-857c-92929e3bdace" -Url $environment -ErrorAction SilentlyContinue
Write-host "done" -ForegroundColor Green

#add webparts
#title L�tSt�_L�nk_Eshop zone 4


Write-Host "Re-adding fake minicart to /sv..." -nonewline
$currentweb = $environment + "/sv"
$rootweb = get-spweb $currentweb -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage= $rootweb.getfile("sidor/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $swedishcontent  $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /en..." -nonewline
$currentweb = $environment + "/en"
$rootweb = get-spweb $currentweb -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("pages/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $englishcontent $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /en/Private..." -nonewline
$currentweb = $environment + "/en/Private"
$rootweb = get-spweb  $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("pages/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $englishcontent $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /en/Business-Mail..." -nonewline
$currentweb =$environment + "/en/Business-Mail"
$rootweb = get-spweb $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("pages/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $englishcontent $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /en/Market-Communication..." -nonewline
$currentweb = $environment + "/en/Market-Communication"
$rootweb = get-spweb $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("pages/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $englishcontent $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /en/Logistics..." -nonewline
$currentweb =  $environment + "/en/Logistics"
$rootweb = get-spweb $currentweb -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("pages/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $englishcontent $currentpage
}
Write-host "done" -ForegroundColor Green




Write-Host "Re-adding fake minicart to /sv/Privat..." -nonewline
$currentweb = $environment + "/sv/Privat"
$rootweb = get-spweb $currentweb -ErrorAction SilentlyContinue 
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("sidor/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $swedishcontent  $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /sv/Foretagspost..." -nonewline
$currentweb = $environment + "/sv/Foretagspost"
$rootweb = get-spweb $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("sidor/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $swedishcontent  $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /sv/Marknadskommunikation..." -nonewline
$currentweb = $environment + "/sv/Marknadskommunikation"
$rootweb = get-spweb $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("sidor/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $swedishcontent  $currentpage
}
Write-host "done" -ForegroundColor Green

Write-Host "Re-adding fake minicart to /sv/Logistik..." -nonewline
$currentweb = $environment + "/sv/Logistik"
$rootweb = get-spweb $currentweb  -ErrorAction SilentlyContinue
if($rootweb -ne $null)
{
$currentpage=$rootweb.getfile("sidor/home.aspx")
AddContentEditorWebpart "Zone4" $webpartTitle  $swedishcontent  $currentpage
}
Write-host "done" -ForegroundColor Green

