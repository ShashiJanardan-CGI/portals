param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml",
	[switch] $WhatIf = $false,
	[string] $Action = "Deploy"
)

function WaitForSolutionJob
{
	param
	(
		[Parameter(Position=0, Mandatory=$true)]
		$Identity
	)

	Write-Host -NoNewLine "Waiting to finish "
	while ((Get-SPSolution -Identity $Identity).JobExists)
	{
		Write-Host -NoNewLine .
		Start-Sleep -Seconds 2
	}
	Write-Host " done!"
}

function Get-MD5([System.IO.FileInfo] $file = $(throw 'Usage: Get-MD5 [System.IO.FileInfo]'))
{
	$stream = $null
	$cryptoServiceProvider = [System.Security.Cryptography.MD5CryptoServiceProvider];
	$hashAlgorithm = New-Object $cryptoServiceProvider
	$stream = $file.OpenRead()
	$hashByteArray = $hashAlgorithm.ComputeHash($stream)
	$stream.Close()
	## We have to be sure that we close the file stream if any exceptions are thrown.
	trap
	{
		if ($stream -ne $null)
		{
			$stream.Close();
		}
		break;
	}
	return [string]$hashByteArray;
}

function UninstallSolution
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Solution,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $Solution) { Return }

		CheckRequiredAttribute $Solution -Name "Name"

		$solutionName = $Solution.Name
		$solutionWebApplication = ($Solution.WebApplication -eq $true)          # defaults to false
		$solutionDeployOnce = ($Solution.DeployOnce -eq $true)                  # defaults to false
		$solutionLocal = ($Solution.Local -eq $true)                            # defaults to false
		$solutionLanguage = $Solution.Language
		if (-not $solutionLanguage) { $solutionLanguage = 0 }

		# Skipp if solution does not exist in farm
		$targetSolution = Get-SPSolution -Identity $solutionName -ErrorAction SilentlyContinue
		if (-not $targetSolution)
		{
			Write-Host "Solution $solutionName not found on farm, skipping uninstall!"
			Return
		}

		# Skipp if solution does not exist in farm
		if ($WebApplication)
		{
			$targetWebApp = Get-SPWebApplication -Identity $WebApplication -ErrorAction SilentlyContinue
			if (-not $targetWebApp)
			{
				Write-Host "Web Application $WebApplication not found, skipping solution uninstall!"
				Return
			}
		}

		# Wait for existing jobs
		if ($targetSolution -and $targetSolution.JobExists)
		{
			WaitForSolutionJob -Identity $targetSolution.Name
		}

		# Uninstalls or updates globaly deployed solutions
		if ($targetSolution)
		{
			if ($targetSolution.Deployed)
			{
				if ($solutionDeployOnce)
				{
					Write-Host "Solution $solutionName is deployed but marked as DeployOnce=True, skipping uninstall!"
				}
				else
				{
					Write-Host "Uninstalling solution $solutionName" -ForegroundColor White
					if ($targetSolution.ContainsWebApplicationResource)
					{
						Uninstall-SPSolution -Identity $targetSolution -AllWebApplications -Language $solutionLanguage -Local:$solutionLocal -Confirm:$false -WhatIf:$WhatIf
					}
					else
					{
						Uninstall-SPSolution -Identity $targetSolution -Language $solutionLanguage -Local:$solutionLocal -Confirm:$false -WhatIf:$WhatIf
					}
					WaitForSolutionJob -Identity $targetSolution.Name
				}
			}
		}

		$targetSolution = Get-SPSolution -Identity $solutionName -ErrorAction SilentlyContinue
		if ($targetSolution -and -not $targetSolution.Deployed)
		{
			$backupSolutionPath = $globalBackupDir + "\" + $solutionName
			$targetSolution.SolutionFile.SaveAs($backupSolutionPath)

			Write-Host "Removing solution $solutionName" -ForegroundColor White
			Remove-SPSolution -Identity $targetSolution -Language $solutionLanguage -Confirm:$false -WhatIf:$WhatIf
		}
	}
}

function InstallOrUpdateSolution
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Solution,
		[string] $WebApplication,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $Solution) { Return }

		CheckRequiredAttribute $Solution -Name "Name"

		$solutionName = $Solution.Name
		$solutionWebApplication = ($Solution.WebApplication -eq $true)          # defaults to false
		$solutionDeployOnce = ($Solution.DeployOnce -eq $true)                  # defaults to false
		$solutionCASPolicies = !($Solution.CASPolicies -eq $false)              # defaults to true
		$solutionGACDeployment = !($Solution.GACDeployment -eq $false)          # defaults to true
		$solutionForce = ($Solution.Force -eq $true)                            # defaults to false
		$solutionLocal = ($Solution.Local -eq $true)                            # defaults to false
		$solutionLanguage = $Solution.Language
		if (-not $solutionLanguage) { $solutionLanguage = 0 }

		# Check if solution file exists
		$packagePath = Resolve-Path -LiteralPath ".\Packages\$SolutionName" -ErrorAction SilentlyContinue
		if (-not $packagePath)
		{
			Write-Host "ERROR: Solution file $solutionName was not found, aborting!" -ForegroundColor Red
			Exit
		}

		# Check if web application exists
		if ($WebApplication)
		{
			$targetWebApp = Get-SPWebApplication -Identity $WebApplication -ErrorAction SilentlyContinue
			if (-not $targetWebApp)
			{
				Write-Host "ERROR: Web application at $WebApplication was not found, aborting!" -ForegroundColor Red
				Exit
			}
		}

		# Add solution file if it does not exists in the farm
		$targetSolution = Get-SPSolution -Identity $solutionName -ErrorAction SilentlyContinue
		if (-not $targetSolution)
		{
			if ($WhatIf)
			{
				Write-Host "Will add $solutionName to Farm" -ForegroundColor White
			}
			else
			{
				Write-Host "Adding $solutionName to Farm" -ForegroundColor White
				$targetSolution = Add-SPSolution -LiteralPath $packagePath -Language $solutionLanguage -WhatIf:$WhatIf
				if (-not $targetSolution)
				{
					Write-Host "ERROR: Failed adding $solutionName to farm, aborting!" -ForegroundColor Red
					Exit
				}
			}
		}

		# Now the solution should be added if not a WhatIf-run
		$targetSolution = Get-SPSolution -Identity $solutionName -ErrorAction SilentlyContinue
		if (-not $targetSolution -and -not $WhatIf)
		{
			Write-Host "ERROR: Solution $solutionName does not exist in farm, aborting!" -ForegroundColor Red
			Exit
		}

		# Wait for existing jobs
		if ($targetSolution -and $targetSolution.JobExists)
		{
			WaitForSolutionJob -Identity $targetSolution.Name
		}
		
		$backupSolutionPath = $globalBackupDir + "\" + $solutionName
		$samePackage = $false
		if ($targetSolution)
		{
			$targetSolution.SolutionFile.SaveAs($backupSolutionPath)
			if ((Get-MD5 $($packagePath -as [String] -as [System.IO.FileInfo])) -eq (Get-MD5 $backupSolutionPath))
			{
				$samePackage = $true
				Remove-Item -LiteralPath $backupSolutionPath
			}
		}

		# Installs or updates globaly deployed solutions
		if ($targetSolution -and -not $targetSolution.ContainsWebApplicationResource -and -not $WebApplication)
		{
			if ($targetSolution.Deployed)
			{
				if ($solutionDeployOnce -or $samePackage)
				{
					Write-Host "Solution $solutionName is already deployed, skipping upgrade!"
				}
				else
				{
					Write-Host "Updating solution $solutionName" -ForegroundColor White
					Update-SPSolution -Identity $targetSolution -LiteralPath $packagePath -GACDeployment:$solutionGACDeployment -CASPolicies:$solutionCASPolicies -Force:$solutionForce -Local:$solutionLocal -WhatIf:$WhatIf
					WaitForSolutionJob -Identity $targetSolution.Name
				}
			}
			if (-not $targetSolution.Deployed)
			{
				Write-Host "Installing solution $solutionName" -ForegroundColor White
				Install-SPSolution -Identity $targetSolution -Language $solutionLanguage -GACDeployment:$solutionGACDeployment -CASPolicies:$solutionCASPolicies -Force:$solutionForce -Local:$solutionLocal -WhatIf:$WhatIf
				WaitForSolutionJob -Identity $targetSolution.Name
			}
		}

		# Installs or updates web application solutions
		if ($targetSolution -and $targetSolution.ContainsWebApplicationResource -and $WebApplication)
		{
			if ($targetSolution.Deployed)
			{
				if ($solutionDeployOnce -or $samePackage)
				{
					Write-Host "Solution $solutionName is already deployed on $WebApplication, skipping upgrade!"
				}
				else
				{
					Write-Host "Updating solution $solutionName on $WebApplication" -ForegroundColor White
					Update-SPSolution -Identity $targetSolution -LiteralPath $packagePath -GACDeployment:$solutionGACDeployment -CASPolicies:$solutionCASPolicies -Force:$solutionForce -Local:$solutionLocal -WhatIf:$WhatIf
					WaitForSolutionJob -Identity $targetSolution.Name
				}
			}
			if (-not $targetSolution.Deployed)
			{
				Write-Host "Installing solution $solutionName on $WebApplication" -ForegroundColor White
				Install-SPSolution -Identity $targetSolution -WebApplication $WebApplication -Language $solutionLanguage -GACDeployment:$solutionGACDeployment -CASPolicies:$solutionCASPolicies -Force:$solutionForce -Local:$solutionLocal -WhatIf:$WhatIf
				WaitForSolutionJob -Identity $targetSolution.Name
			}
		}
	}
}

function ValidateFeature
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Node
	)
	process
	{
		if (-not $Node) { Return }

		$Feature = $Node.Node
		$featureIdentity = $Feature.Id
		if (-not $featureIdentity) {
			$featureIdentity = $Feature.Name
		}
		$featureName = $Feature.Name
		if (-not $featureName) {
			$featureName = $featureIdentity
		}

		# Check if required parameters is set
		if (-not $featureIdentity)
		{
			Write-Host "ERROR: Missing required Id or Name attribute of Feature-tag, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Exit
		}

		$targetFeature = Get-SPFeature -Identity $featureIdentity -ErrorAction SilentlyContinue
		if (-not $targetFeature)
		{
			Write-Host "ERROR: Feature $featureName was not found, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Exit
		}

		$featureName = $targetFeature.DisplayName

		if ($targetFeature.Scope -eq "Farm" -and $Feature.ParentNode.get_Name() -ne "Farm")
		{
			Write-Host "ERROR: Feature $featureName is not a Farm scope feature, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Write-Host "ParentNode: " $Feature.ParentNode.get_Name()
			Exit
		}

		if ($targetFeature.Scope -eq "WebApplication" -and $Feature.ParentNode.get_Name() -ne "WebApplication")
		{
			Write-Host "ERROR: Feature $featureName is not a WebApplication scope feature, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Write-Host "ParentNode: " $Feature.ParentNode.get_Name()
			Exit
		}

		if ($targetFeature.Scope -eq "Site" -and $Feature.ParentNode.get_Name() -ne "SiteCollection")
		{
			Write-Host "ERROR: Feature $featureName is not a Site scope feature, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Write-Host "ParentNode: " $Feature.ParentNode.get_Name()
			Exit
		}

		if ($targetFeature.Scope -eq "Web" -and -not $Feature.ParentNode.get_Name() -like "Site*")
		{
			Write-Host "ERROR: Feature $featureName is not a Web scope feature, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Write-Host "ParentNode: " $Feature.ParentNode.get_Name()
			Exit
		}
	}
}

function EnableOrDisableFeature
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Feature,
		[Parameter(Position=1, Mandatory=$true)]
		[Microsoft.SharePoint.SPFeatureScope] $Scope,
		[string] $Url,
		[switch] $Delayed = $false,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $Feature) { Return }
	
		
		if ($Delayed -and -not ($Feature.Action -match "Delayed*")) { Return }
		if (-not $Delayed -and $Feature.Action -match "Delayed*") { Return }

		$featureEnableAction = !($Feature.Action -eq "Disable" -or $Feature.Action -eq "DelayedDisable")
		$featureIdentity = $Feature.Id
		if (-not $featureIdentity) {
			$featureIdentity = $Feature.Name
		}
		$featureName = $Feature.Name
		if (-not $featureName) {
			$featureName = $featureIdentity
		}
		
		

		# Check if required parameters is set
		if (-not $featureIdentity)
		{
			Write-Host "ERROR: Missing required Id or Name attribute of Feature-tag, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Exit
		}

		$targetFeature = Get-SPFeature -Identity $featureIdentity -ErrorAction SilentlyContinue
		if (-not $targetFeature)
		{
			Write-Host "ERROR: Feature $featureName was not found, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Feature.OuterXml
			Exit
		}

		# Check if feature already is enabled/disabled
		$enabledFeature = $null
		switch ($targetFeature.Scope)
		{
			Farm { $enabledFeature = Get-SPFeature -Identity $targetFeature -Farm -ErrorAction SilentlyContinue }
			WebApplication { $enabledFeature = Get-SPFeature -Identity $targetFeature -WebApplication $Url -ErrorAction SilentlyContinue }
			Site { $enabledFeature = Get-SPFeature -Identity $targetFeature -Site $Url -ErrorAction SilentlyContinue }
			Web { $enabledFeature = Get-SPFeature -Identity $targetFeature -Web $Url -ErrorAction SilentlyContinue }
		}

		if ($featureEnableAction)
		{
			if (-not $enabledFeature)
			{
				Write-Host "Enable feature $featureName on $Scope $Url" -ForegroundColor White
				Enable-SPFeature -Identity $targetFeature -Url $Url
			}
			else
			{
				Write-Host "Feature $featureName is already enabled on $Scope $Url"
			}
		}
		else
		{
			if ($enabledFeature)
			{
				Write-Host "Disable feature $featureName on $Scope $Url" -ForegroundColor White
				Disable-SPFeature -Identity $targetFeature -Url $Url
			}
			else
			{
				Write-Host "Feature $featureName is already disabled on $Scope $Url"
			}
		}
	}
}

function CreateWebApplication
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$WebApplication,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $WebApplication) { Return }

		CheckRequiredAttribute $WebApplication -Name "Url"
		CheckRequiredAttribute $WebApplication -Name "Port"
		CheckRequiredAttribute $WebApplication -Name "Name"
		CheckRequiredAttribute $WebApplication -Name "ApplicationPoolAccount"
		CheckRequiredAttribute $WebApplication -Name "DatabaseName"
		
		$webAppName = $WebApplication.Name
		$webAppUrl = $WebApplication.Url
		$webAppPort = $WebApplication.Port
		$webAppApplPoolAccount = $WebApplication.ApplicationPoolAccount
		$webAppApplPoolName = $webAppName + " Application Pool"
		$webAppDatabaseName = $WebApplication.DatabaseName
		
		# TODO: Check Claims, url, port etc

		$targetWebApp = Get-SPWebApplication -Identity $webAppUrl -ErrorAction SilentlyContinue
		if (-not $targetWebApp)
		{
			Write-Host "Creating Web Application at $webAppUrl" -ForegroundColor White

			$AuthProvider = New-SPAuthenticationProvider -UseWindowsIntegratedAuthentication -AllowAnonymous
   			New-SPWebApplication -Name $webAppName -ApplicationPoolAccount $webAppApplPoolAccount -ApplicationPool $webAppApplPoolName -DatabaseName $webAppDatabaseName -Url $webAppUrl -Port $webAppPort -AuthenticationProvider $AuthProvider
		}
	}
}

function RemoveWebApplication
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$WebApplication,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $WebApplication) { Return }

		CheckRequiredAttribute $WebApplication -Name "Url"

		$webAppName = $WebApplication.Name
		$webAppUrl = $WebApplication.Url

		$targetWebApp = Get-SPWebApplication -Identity $webAppUrl -ErrorAction SilentlyContinue
		if ($targetWebApp)
		{
			Write-Host "Removing Web Application at $webAppUrl!" -ForegroundColor White
			Remove-SPWebApplication -Identity $webAppUrl -DeleteIISSite:$true -RemoveContentDatabases:$true -WhatIf:$WhatIf
		}
	}
}

function DeployWebApplication
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$WebApplication,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $WebApplication) { Return }

		CheckRequiredAttribute $WebApplication -Name "Url"

		$webAppName = $WebApplication.Name
		$webAppUrl = $WebApplication.Url

		$targetWebApp = Get-SPWebApplication -Identity $webAppUrl -ErrorAction SilentlyContinue
		if (-not $targetWebApp)
		{
			Write-Host "ERROR: Web Application as $webAppUrl was not found, aborting!" -ForegroundColor Red
			Write-Host "XML: " $WebApplication.OuterXml
			Exit
		}

		$webAppUrl = $targetWebApp.Url

		# Deploy Web Applicaiton solutions to this Web Application
		# $xml.Deployment.Farm.Solution | InstallOrUpdateSolution -WebApplication $webAppUrl -WhatIf:$WhatIf

		# Enable or Disable all features for this Web Application
		$WebApplication.Feature | EnableOrDisableFeature -Scope WebApplication -Url $webAppUrl -Delayed:$false -WhatIf:$WhatIf

		# Create all Site Collections
		$WebApplication.SiteCollection | DeploySiteCollection -ParentUrl $webAppUrl -WhatIf:$WhatIf
			
		# Enable or Disable all features for this Web Application
		$WebApplication.Feature | EnableOrDisableFeature -Scope WebApplication -Url $webAppUrl -Delayed:$true -WhatIf:$WhatIf
	}
}

function CheckRequiredAttribute
{
	param
	(
		[Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true)]
		$Xml,
		[Parameter(Position=1, Mandatory=$true)]
		[string] $Name
	)
	process
	{
		$Value = $Xml.$Name
		if (-not $Value -or -not $Value.Trim())
		{
			$tagName = $Xml.get_Name()
			Write-Host "ERROR: Missing required $Name attribute of $tagName-tag, aborting!" -ForegroundColor Red
			Write-Host "XML: " $Xml.OuterXml
			Exit
		}
	}
}

function DeploySiteCollection
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$SiteCollection,
		[Parameter(Position=1, Mandatory=$true)]
		[string] $ParentUrl,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $SiteCollection) { Return }

		CheckRequiredAttribute $SiteCollection -Name "Url"
		CheckRequiredAttribute $SiteCollection -Name "Name"

		$siteName = $SiteCollection.Name
		$siteUrl = $($ParentUrl.Trim("/") + "/" + $SiteCollection.Url.Trim("/")).Trim("/")
		$siteTemplate = $SiteCollection.Template
		$siteOwnerAlias = $env:USERDOMAIN + "\" + $env:USERNAME
		$siteLanguage = $SiteCollection.Language
		$siteVariations = $SiteCollection.Variations
		
		# Check Language-attribute
		if (-not $siteLanguage) { $siteLanguage = [Microsoft.SharePoint.SPRegionalSettings]::GlobalServerLanguage.LCID }
		$installedLanguage = [Microsoft.SharePoint.SPRegionalSettings]::GlobalInstalledLanguages | Where-Object { $_.LCID -eq $siteLanguage }
		if (-not $installedLanguage)
		{
			Write-Host "ERROR: Language pack is not installed for LCID $siteLanguage, aborting!" -ForegroundColor Red
			Write-Host "URL: $siteUrl"
			Exit
		}
		
		$targetSite = Get-SPSite -Identity $siteUrl -ErrorAction SilentlyContinue
		if (-not $targetSite)
		{
			# TODO: check template
			# TODO: content database

			# Create site collection
			Write-Host "Creating new site collection on $siteUrl" -ForegroundColor White
			$targetSite = New-SPSite -Url $siteUrl -OwnerAlias $siteOwnerAlias -Name $siteName -Template $siteTemplate -Language $siteLanguage -WhatIf:$WhatIf
		}

		if ($targetSite)
		{
			$siteUrl = $targetSite.Url
		}
		
		$subSiteRoot = $siteUrl
		if ($siteVariations)
		{
			# Set variation languages
			SetVariationLanguages -Languages $siteVariations -Url $siteUrl
			
			# Append root site variation to site url so subsites gets created correctly
			$subSiteRoot = $siteUrl + "/" + $siteVariations.Split(',')[0]
		}

		# Enable or Disable all features for this Web Application
		$SiteCollection.Feature | EnableOrDisableFeature -Scope Site -Url $siteUrl -Delayed:$false -WhatIf:$WhatIf

		# Create all sub sites
		$SiteCollection.Site | DeploySite -ParentUrl $subSiteRoot -Language $siteLanguage -WhatIf:$WhatIf

		# Enable or Disable all features for this Web Application
		$SiteCollection.Feature | EnableOrDisableFeature -Scope Site -Url $siteUrl -Delayed:$true -WhatIf:$WhatIf
	}
}

function DeploySite
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Site,
		[Parameter(Position=1, Mandatory=$true)]
		[string] $ParentUrl,
		[int] $Language,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $Site) { Return }

		CheckRequiredAttribute $Site -Name "Url"
		CheckRequiredAttribute $Site -Name "Name"

		$webName = $Site.Name
		$webUrl = $($ParentUrl.Trim("/") + "/" + $Site.Url.Trim("/")).Trim("/")
		$webTemplate = $Site.Template

		$targetWeb = Get-SPWeb -Identity $webUrl -ErrorAction SilentlyContinue
		if (-not $targetWeb)
		{
			# TODO: check template

			# Create site collection
			Write-Host "Creating new sub-site on $webUrl" -ForegroundColor White
			$targetWeb = New-SPWeb -Url $webUrl -Name $webName -Template $webTemplate -Language $Language -WhatIf:$WhatIf
		}

		if ($targetWeb)
		{
			$webUrl = $targetWeb.Url
		}

		# Enable or Disable all features for this site
		$Site.Feature | EnableOrDisableFeature -Scope Web -Url $webUrl -Delayed:$false -WhatIf:$WhatIf
			
		# Set publishing properties for this site
		$Site.PublishingWebProperty | SetPublishingWebProperty -Url $webUrl -WhatIf:$WhatIf

		# Enable or Disable all features for this site
		$Site.Feature | EnableOrDisableFeature -Scope Web -Url $webUrl -Delayed:$true -WhatIf:$WhatIf
	}
}

function SetVariationLanguages
{
	param
	(
		[string]$Languages = "",
		[string]$Url = $(Throw "No Url provided")
	)
	process
	{
		try
		{
			$locales = $Languages.Split(",")

			Write-Host "Checking the language parameter list "
			foreach ($locale in $locales)
			{
				# Check if language pack is installed
				$installedLanguage = [Microsoft.SharePoint.SPRegionalSettings]::GlobalInstalledLanguages | Where-Object { [System.Globalization.CultureInfo]::GetCultureInfo($_.LCID).Name -eq $locale }
				if (-not $installedLanguage)
				{
					Throw ("The language pack for " + $locale + " is not installed!")
				}
			}

			Write-Host "Getting the root web for:" $url
			$targetSite = Get-SPSite -Identity $Url
			$rootWeb = $targetSite.RootWeb

			if ($rootWeb.Locale.Name -ne $locales[0])
			{
				Throw ("You must set the first language in the parameter list to the same as the site collection language, you have " + $locales[0] + " as the first and the site collection is in " + $rootWeb.Locale.Name);
			}

			Write-Host "Setting the PostenLabels property to:" $Languages
			$rootWeb.AllProperties["PostenLabels"] = $Languages;
			$rootWeb.Update();

			Write-Host "Done";
		}
		catch [Exception]
		{
			Write-Host $_.Exception.ToSTring();
		}
	}
}

function SetPublishingWebProperty
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$Property,
		[Parameter(Position=1, Mandatory=$true)]
		[string] $Url,
		[switch] $WhatIf = $false
	)
	process
	{
		if (-not $Property) { Return }

		CheckRequiredAttribute $Property -Name "Name"
		CheckRequiredAttribute $Property -Name "Value"

		$targetWeb = Get-SPWeb -Identity $Url -ErrorAction SilentlyContinue
		if (-not $targetWeb)
		{
			Write-Host "WARNING: Site on $Url was not found, skipping!"
			Return
		}

		$targetPublishingWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($targetWeb)
		if (-not $targetPublishingWeb)
		{
			Write-Host "WARNING: Publishing Web on $Url was not found, skipping!"
			Return
		}

		$propName = $Property.Name
		$propValue = $Property.Value
		Write-Host "Setting $propName to '$propValue' on Publishing Web at $Url" -ForegroundColor White

		$propObj = $targetPublishingWeb

		if ($propName -eq "WelcomePageUrl")
		{
			$propName = "DefaultPage"
			$targetFile = $targetWeb.GetFile($targetPublishingWeb.PagesListName + "/" + $propValue)

			$propValue = $targetFile
		}

		# TODO: cleanup code

		$arr = $propName.Split('.')
		for ($i = 0; $i -lt $arr.Length; $i++)
		{
			$propName = $arr[$i]
			#Write-Host "propName: $propName"
			if ($i -lt $arr.Length - 1)
			{
				$propObj = $propObj.$propName
			}
		}

		$prevValue = $propObj.$propName
		Write-Host "prevValue: $prevValue" $prevValue.GetType()
		if ($prevValue -is [bool])
		{
			$propValue = ($propValue -eq $true)
		}
		Write-Host "propValue: $propValue" $propValue.GetType()
		$propObj.$propName = $propValue
		$targetPublishingWeb.Update()
		$targetWeb.Update()
	}
}

function InstallOrUpdateWebAppSolution
{
	param
	(
		[Parameter(Position=0, ValueFromPipeline=$true)]
		$WebApplication,
		[switch] $WhatIf
	)
	process
	{
		if (-not $WebApplication) { Return }

		CheckRequiredAttribute $WebApplication -Name "Url"

		$webAppUrl = $WebApplication.Url

		$targetWebApp = Get-SPWebApplication -Identity $webAppUrl -ErrorAction SilentlyContinue
		if (-not $targetWebApp)
		{
			Write-Host "WARNING: Web Application as $webAppUrl was not found, skipping!"
			Return
		}

		$webAppUrl = $targetWebApp.Url

		# Deploy Web Applicaiton solutions to this Web Application
		$xml.Deployment.Farm.Solution | InstallOrUpdateSolution -WebApplication $webAppUrl -WhatIf:$WhatIf
	}
}

function Exec-SPShell($cmd)
{
	$spaddin = @(". 'C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\CONFIG\POWERSHELL\Registration\sharepoint.ps1'; cd '$pwd'")
	$cmdlist = [string]::Join(";", $cmd)
	$startcommand = "-Command """ + ($spaddin+";"+$cmdlist) + """"
	#Write-Host "Calling: " $startcommand
	Start-Process Powershell -ArgumentList $startcommand -NoNewWindow -Wait
}


# Check if deployment file exists
$filePath = Resolve-Path -LiteralPath $InputFile -ErrorAction SilentlyContinue
if (-not $filePath)
{
	Write-Host "ERROR: Input file $InputFile was not found, aborting!" -ForegroundColor Red
	Exit
}

# Globally update all instances of "localhost" in the input file to actual local server name
[xml]$xml = (Get-Content $filePath) -replace ("localhost", $env:COMPUTERNAME)

#ValidateInputFile $xml

# Load SharePoint snapin
Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue

$globalBackupDir = $PWD.Path + "\Backup\" + (Get-Date -Format yyyyMMdd_HHmmss)

if (-not (Get-Item -LiteralPath $globalBackupDir -ErrorAction SilentlyContinue))
{
	$dummy = New-Item $globalBackupDir -Type Directory
}

if ($Action -eq "Uninstall")
{
	# TODO: Confirm removal of webapps and uninstall of solutions
	
	# Remove Web Applications
	# -DeleteIISSite:$true -RemoveContentDatabases:$true
	$xml.Deployment.WebApplication | RemoveWebApplication -WhatIf:$WhatIf

	# Remove Solutions
	$xml.Deployment.Farm.Solution | UninstallSolution -WhatIf:$WhatIf
}
elseif ($Action -eq "InternalInstall")
{
	Write-Host "Installing solutions" -ForegroundColor White

	# TODO: validate solutions and web applications

	# Deploy solutions
	$xml.Deployment.Farm.Solution | InstallOrUpdateSolution -WhatIf:$WhatIf
		
	# Create web applications
	$xml.Deployment.WebApplication | CreateWebApplication -WhatIf:$WhatIf
	
	# Deploy web application solutions		
	$xml.Deployment.WebApplication | InstallOrUpdateWebAppSolution -WhatIf:$WhatIf

	# Restart the SPTimerV4 service if it's running
	If ((Get-Service -Name SPTimerV4).Status -eq "Running")
	{
		Write-Host "Restarting SharePoint Timer Service..." -ForegroundColor White
		Restart-Service SPTimerV4
	}
}
elseif ($Action -eq "InternalDeploy")
{
	Write-Host "Creating site collections and activating features" -ForegroundColor White

	# TODO: validate solutions and web applications

	# Activate features and create site collections
	$xml | Select-Xml '//Feature' | ValidateFeature
	$xml.Deployment.Farm.Feature | EnableOrDisableFeature -Scope Farm -Delayed:$false -WhatIf:$WhatIf
	$xml.Deployment.WebApplication | DeployWebApplication -WhatIf:$WhatIf
	$xml.Deployment.Farm.Feature | EnableOrDisableFeature -Scope Farm -Delayed:$true -WhatIf:$WhatIf
}
elseif ($Action -eq "Deploy")
{
	# TODO: validate solutions and web applications

	$cmd = ".\Deployment-v3.ps1 -InputFile '$filePath' -Action InternalInstall"
	Exec-SPShell $cmd

	# TODO: check if install was successfull

	$cmd = ".\Deployment-v3.ps1 -InputFile '$filePath' -Action InternalDeploy"
	Exec-SPShell $cmd
}
else
{
	Write-Host "Unknown Action: $Action" -ForegroundColor Red
}

if ((Get-ChildItem -LiteralPath $globalBackupDir).Count -eq 0)
{
	Remove-Item -LiteralPath $globalBackupDir
}
