
(function($) {
    var userAgent = navigator.userAgent.toLowerCase();

    $.browser = {
        version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],
        safari: /webkit/.test( userAgent ),
        opera: /opera/.test( userAgent ),
        msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
        mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
    };

})(jQuery);
/*
$('.module .content').append(
	'<div class="corner tl"></div>' +
	'<div class="corner tr"></div>' +
	'<div class="corner bl"></div>' +
	'<div class="corner br"></div>'
)
*/
/**************************************************************************************
***** DropDown                                                                   ******
**************************************************************************************/
function openDropDown( dropDown ) {
	if( dropDown.is(":visible") ) {
		dropDown.fadeOut();
		return false;
	}
	
	$(".dropDown:visible").fadeOut();
	dropDown.fadeIn();
}

jQuery(function($) {

	// DropDown Close Button
	$(".dropDown .close").click( function() {
//		$("#GlobalMenu .active").removeClass("active")
		$(this).closest(".dropDown").hide()
	})
	
	// DropDown MouseOut
	$(".dropDown").mouseleave( function() {
//		$("#GlobalMenu .active").removeClass("active")
//		$(this).closest(".dropDown").fadeOut()
	})
	
/**************************************************************************************
***** Global Search                                                              ******
**************************************************************************************/
	
	$(".search input[type='text']").focus(function(){
		this.select()
		$("#AutoComplete").show();
		$(this).prev().hide()
	}).blur(function(){
		if(this.value.length == 0)
			$(this).prev().show();
		
		$("#AutoComplete").hide();
	})
	
/**************************************************************************************
***** Global Menu                                                                ******
**************************************************************************************/
	
	$("#GlobalMenu .dropDownMenu a").mouseover( function() {
		var orderNo = $(this).parent()[0].className.split(" ")[1];
		var dropDown = $("#GlobalMenu .dropDown." + orderNo);
		
		setTimeout(function() { openDropDown( dropDown ) }, 200);
		
		/*
		// Close dropdown when clicking on an active pulldown
		if( $(this).parent("li.active").length > 0 ) {
			$("#GlobalMenu .dropDown").fadeOut()
			$(this).parent().removeClass("active")
			
			return
		}
		
		// Close any open dropdown 
		if( $("#GlobalMenu li.active").length > 0 ) {
			$("#GlobalMenu li.active").removeClass("active")
			$("#GlobalMenu .dropDown").fadeOut()
			
			var tmp = this
			setTimeout(function() { openGlobalDropDown( tmp ) }, 200)
			
			return
		}
		
		openGlobalDropDown(this)*/
	})
	


/**************************************************************************************
***** WebShop                                                                    ******
**************************************************************************************/
	
	$(".webShop .shoppingBasket a").click( function() {
		var dropDown = $(".webShop .dropDown");
		
		setTimeout(function() { openDropDown( dropDown ) }, 200);
		$(".webShop .indent").toggleClass("active");
		
		return false;
	})
	
	$(".basketUpdate").click( function() {
		var notifcation = $(".webShop .notification").show();
		
		setTimeout(function() { notifcation.fadeOut(1500) }, 2000);
		
		return false;
	})
	

/**************************************************************************************
***** Control                                                               ******
**************************************************************************************/
	$(".control a").click( function() {
		$(this).parent().children().toggle();
		$(this).parent().parent().siblings().toggle();
		return false;
	})
	
/**************************************************************************************
***** Quick Finder                                                               ******
**************************************************************************************/
	
	$('#QuickFinder .headline').click(function() {
		
		if( $(this).next().is(':visible') )
			return false;
		
		$(this).parent().children('.area:visible').slideUp();
		$(this).next().slideDown();
		
		$(this).parent().children('.headline.active').removeClass('active');
		$(this).toggleClass('active');
		
//		$(this).parent().find('.down').toggleClass('down').toggleClass('right');
//		$(this).find('.arrow').toggleClass('right').toggleClass('down');
		
		return false;
	})
	
/**************************************************************************************
***** TreeView                                                                   ******
**************************************************************************************/

	$(".treeView .node").click(function() {
		
		$(this).parent().toggleClass("expanded");
		
		return false;
//		$(this).find('.arrow').toggleClass('down').toggleClass('right');
	});
	
/**************************************************************************************
***** Related Content                                                            ******
**************************************************************************************/
	
	$('.relatedContent .control').click(function() {
	
	$(this).toggleClass('active')
	
	$(this).find(".arrow").toggleClass('primary').toggleClass('down').toggleClass('theme').toggleClass('up')
	
	$(this).parent().siblings('.content').find('.expansion').toggleClass('hidden');
	
//	$(this).sibling('.expansion').toggleClass('hidden');
	
	return false;
	
		if( $(this).hasClass('down')) {
			$(this).removeClass('down')
			$(this).addClass('up')
//			$(this).children('.answer').slideUp()
			return
		}
		
		
		return false;
		
		$(this).parent().children('li.active').children('.answer').slideUp()
		$(this).parent().children('li.active').removeClass('active')
		
		$(this).addClass('active')
		$(this).children('.answer').slideDown()
		
		return false;
	})
	
/**************************************************************************************
***** Main Campaign Module                                                       ******
**************************************************************************************/
	
	// Pagination for multi-banners
	function bannerPagination() {
		var index = $(this).parent().children("span").index(this);
		var ulPages = $(this).parent().parent().children("ul");
		
		$(".paginator span").removeClass("active").unbind("click");
		$(".paginator span").eq(index).addClass("active");
		$(".paginator span").addClass("loading");
		
		ulPages.children("li:visible").fadeToggle(300, function() {
			ulPages.children("li").eq(index).fadeToggle(300);
			$(".paginator span:not(.active)").bind("click", bannerPagination);
			$(".paginator span").removeClass("loading");
		});
	}
	$(".paginator span:not(.active)").click(bannerPagination);
	
/**************************************************************************************
***** FAQ                                                                        ******
**************************************************************************************/
	
	$('.faq .question a').click(function() {
		var li = $(this).parent().parent();
		
		if( $(li).hasClass('active')) {
			$(li).removeClass('active')
			$(li).children('.answer').slideUp()
			
			return false;
		}
		
		$(li).addClass('active')
		$(li).children('.answer').slideDown()
		
		return false;
	})
	
/**************************************************************************************
***** Center Console Menu                                                        ******
**************************************************************************************/
	
	$("#CenterConsoleMenu .slide").click( function() {
		
		var orderNo = $(this).closest(".column")[0].className.split(" ")[1];
		var dropDown = $("#CenterConsoleMenu .dropDown." + orderNo);
		
		$(this).parent().toggleClass("active");
		
		setTimeout(function() { openDropDown( dropDown ) }, 200);
		
		return false;
		/*
		// Close dropdown when clicking on an active pulldown
		if( $(this).parent(".active").length > 0 ) {
			$("#CenterConsoleMenu .dropDown").fadeOut()
			$(this).removeClass("active")
			$(this).parent().removeClass("active")
			return false;
		}
		
		// Close any open dropdown 
		if( $("#CenterConsoleMenu .active").length > 0 ) {
			$("#CenterConsoleMenu .active").removeClass("active")
			$("#CenterConsoleMenu .dropDown").fadeOut()
			
			
			var tmp = this
			setTimeout(function() { openConsoleDropDown( tmp ) }, 200)
			return false;
		}
		
		openConsoleDropDown(this)
		
		return false;*/
	})
	

	/*
	$("#CenterConsoleMenu .dropDown .close").click( function() {
		$("#CenterConsoleMenu .active").removeClass("active")
		$("#CenterConsoleMenu .dropDown").hide()
	})
	
	$("#CenterConsoleMenu .dropDown").mouseleave( function() {
		$("#CenterConsoleMenu .active").removeClass("active")
		$("#CenterConsoleMenu .dropDown").fadeOut()
	})*/
	
/**************************************************************************************
***** Tab                                                                        ******
**************************************************************************************/	
	
	$(".tabs a").click(function() {
		$(this).parent().parent().children(".active").removeClass("active");
		$(this).parent().addClass("active");
		
		var tab = $(this).parent()
		var tabList = $(this).parent().parent().children(".tab")
		var contentList = $(this).parent().parent().siblings(".content")
		var index = tabList.index(tab)
		
		$(contentList).removeClass("active");
		$(contentList).eq(index).addClass("active");
//		

		return false;
	})
	
/**************************************************************************************
***** jQuery Transform                                                           ******
**************************************************************************************/
	
	$('a.linkBtn').each(function() {
		var $this = $(this);
		$this.addClass('jqTransformButton').wrapInner('<span><span/></span>')
		.hover(function(){$this.addClass('jqTransformButton_hover');}, function(){$this.removeClass('jqTransformButton_hover')})
		.focus(function(){$this.addClass('jqTransformButton_hover');})
		.blur(function(){$this.removeClass('jqTransformButton_hover')})
		.mousedown(function(){$this.addClass('jqTransformButton_click')})
		.mouseup(function(){$this.removeClass('jqTransformButton_click')});
	});
	
//	$('.customCheckbox').jqTransform() 

})

/*
// Open the associated dropdown
function openGlobalDropDown(obj) {
	
	var orderNo = $(obj).parent()[0].className.split(" ")[1];
	
	$("#GlobalMenu .dropDown." + orderNo).fadeIn()
	
	$(obj).parent().addClass("active")
	$(obj).blur()
}

// Open the associated dropdown
function openConsoleDropDown(obj) {
	
	var orderNo = $(obj).parent().parent().parent()[0].className.split(" ")[1];
	
	$("#CenterConsoleMenu .dropDown." + orderNo).fadeIn()
	
	$(obj).addClass("active")
	$(obj).parent().addClass("active")
	$(obj).blur()
}
*/