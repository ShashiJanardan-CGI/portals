﻿	if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
		Add-PSSnapin "Microsoft.SharePoint.PowerShell"
	}


	function DeploySolution {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName"),
		[string]$WebApplicationName = $(Throw "Parameter cannot be null: WebApplicationName")
	  )
	  AddSolution $SolutionName
	  InstallSolution $SolutionName $WebApplicationName
	  WaitForTimerJob $SolutionName
	  Write-Host $SolutionName" deployment completed successfully!" -f "Green"
	}

	function AddSolution {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName")
	  )
	  Write-Host "Adding solution: " $SolutionName
	  $CurrentLocation = Get-Location
	  Add-SPSolution -LiteralPath $CurrentLocation"\"$SolutionName
	  Write-Host $SolutionName" successfully added to the farm!" -f "Green"
	}

	function InstallSolution {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName"),
		[string]$WebApplicationName = $(Throw "Parameter cannot be null: WebApplicationName")
	  )
	  $Solution = Get-SPSolution | Where-Object { $_.Name -match $SolutionName }
	  if($Solution.ContainsWebApplicationResource)
	  {        
		Write-Host "Installing $SolutionName for the given webapplication:"    
		Install-SPSolution -Identity $SolutionName -WebApplication $WebApplicationName -GACDeployment -Force
	  }
	  else
	  {            
		Write-Host "Deploying $SolutionName Globally:"    
		Install-SPSolution -Identity $SolutionName -GACDeployment -Force
	  }	
	}

	function WaitForTimerJob {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName")
	  )
	  $Solution = Get-SPSolution | Where-Object { $_.Name -eq $SolutionName }
	  if ($Solution -ne $null) {
		Write-Host "Waiting to finish deployment job..."
		$Counter = 1
		$Maximum = 100 # Seconds
		$SleepTime = 2 # Seconds
		while(($Solution.JobExists) -and ($Counter -lt $Maximum)) {     
		  $JobStatus = $Solution | Select-Object JobStatus;
		  Start-Sleep -s $SleepTime
		  $Counter++
		}
		Write-Host "Finished the solution timer job."
	  }
	}

	function RetractSolution {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName")
	  )
	  UninstallSolution $SolutionName
	  WaitForTimerJob $SolutionName
	  RemoveSolution $SolutionName
	  WaitForTimerJob $SolutionName
	}

	function UninstallSolution {
	  param (
		[string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName")
	  )
	  Write-Host "Uninstalling Solution: $SolutionName"
	  $solution = Get-SPSolution | Where { $_.Name -match $SolutionName }
	  try {
		if($solution.ContainsWebApplicationResource) {                  
		  Uninstall-SPSolution -identity $SolutionName -allwebapplications -Confirm:$False  
		}
		else {       
		  Uninstall-SPSolution -identity $SolutionName -Confirm:$false
		}
	  }
	  catch {
		Write-Host "Uninstallation failed for solution $SolutionName." -f Red
	  }
	}

	function RemoveSolution {
	  param (
		  [string]$SolutionName = $(Throw "Parameter cannot be null: SolutionName")
	  )
	  Write-Host "Removing solution: $SolutionName"
	  Remove-SPSolution -Identity $SolutionName -Confirm:$False -Force
	  Write-Host $SolutionName" removed successfully!" -F Green
	}

	function Reset-IISOnServers {
	  param (
		[bool]$DisplayStatus = $False
	  )
	  $OutputIfSuccess = "Attempting stop...Internet services successfully stopped" + 
						 "Attempting start...Internet services successfully restarted"
	  $Farm = Get-SPFarm  
	  foreach ($Server in $Farm.Servers) 
	  { 
		$ServerName = $Server.Name 
		$ServerRole = $Server.Role
		if ($ServerRole -ne "Invalid") { 
		  Write-Host "Restarting IIS on $ServerName..." -F White
		  $Output = iisreset $ServerName | Out-String
		  $OutputActual = $Output.Trim().Replace("`r`n", "")
		  if($OutputActual -ne $OutputIfSuccess) {
			Write-Host "IIS Reset Failure:" -F "Red"
			Write-Host $Output -F "Red"
		  }
		  if($DisplayStatus) {
			iisreset $ServerName /status
		  }
		} 
	  } 
	}

	function Reset-TimerServiceOnServers {
	  param (
		[bool]$DisplayStatus = $False
	  )
	  $OutputIfSuccess = "Attempting stop...Internet services successfully stopped" + 
						 "Attempting start...Internet services successfully restarted"
	  $Farm = Get-SPFarm  
	  foreach ($Server in $Farm.Servers) 
	  { 
		$ServerName = $Server.Name 
		$ServerRole = $Server.Role
		if ($ServerRole -ne "Invalid") { 
		  Write-Host "Restarting Timer Service on $ServerName..." -F "White"
		  $Service = Get-WmiObject -Computer $ServerName Win32_Service -Filter "Name='SPTimerV4'"
		  if ($Service -ne $Null)
		  {
			$StopResult = $Service.InvokeMethod('StopService', $Null)
			if($StopResult -ne 0) {
			  Write-Host "Error stopping Timer Services on $ServerName." -F "Red"
			}
			Start-Sleep -S 7
			$StartResult = $Service.InvokeMethod('StartService', $Null)
			if($StartResult -ne 0) {
			  Write-Host "Error starting Timer Services on $ServerName." -F "Red"
			}
			Start-Sleep -S 7
		  }
		} 
	  } 
	}

	function DeactivateFarmFeatures{
	param($featureIds = $(Throw "Value cannot be null: featureIds"))
		
		  if ($featureIds -ne $null) {

		  foreach($featureId in $featureIds)
			{
				Write-Host "Deactivating feature: $featureId"
				$farmFeature = Get-SPFeature -Farm | Where {$_.ID -eq $featureId}
				if ($farmFeature -ne $null)
				{
					Disable-SPFeature –identity $featureId -Force -Confirm:$false
				}
				else
				 {
					 Write-Host " Feature is not active."
				 }
			}	 
	  }
	}


	function ActivateFarmFeatures {
	  param (
		$featureIds = $(Throw "Value cannot be null: featureIds"))
		
		if ($featureIds -ne $null) 
		{
			foreach($featureId in $featureIds)
			{ 
			  Write-Host "Activating Farm Feature: $featureId"
			  $farmFeature = Get-SPFeature -Farm | Where {$_.ID -eq $featureId} 
			  if ($farmFeature -eq $null)
			  { 
				Enable-SPFeature –identity $featureId -Force -Confirm:$false
			  }
			  else
			  {
				Write-Host " Feature already active"
			  }
			}
		}
	}

function NewSPGroup {[CmdletBinding()]
Param(
		[Microsoft.SharePoint.PowerShell.SPWebPipeBind]$Web,
		[string]$GroupAlias,
		[string]$OwnerName,
		[string]$MemberName,
		[string]$Role,
		[string]$Description
	 )
	 
	$SPWeb = $Web.Read()
	
	if ($SPWeb.SiteGroups[$GroupAlias] -ne $null){
		throw  "Group $GroupAlias already exists!"	
	}

	if ($Role) {
		$roleDefinition = $SPWeb.RoleDefinitions[$Role]
		if (!$roleDefinition) {
			throw "Role Definition $Role doesn't exist!"
		}
	}

	if ($SPWeb.Site.WebApplication.UseClaimsAuthentication){
		$op = New-SPClaimsPrincipal $OwnerName -IdentityType WindowsSamAccountName
		$mp = New-SPClaimsPrincipal $MemberName -IdentityType WindowsSamAccountName
		$owner = $SPWeb | Get-SPUser $op
		$member = $SPWeb | Get-SPUser $mp
	}
	else {
	$owner = $SPWeb | Get-SPUser $OwnerName
		if ($MemberName) {
			$member = $SPWeb | Get-SPUser $MemberName
		}
	}

	$SPWeb.SiteGroups.Add($GroupAlias, $owner, $member, $Description)
	$SPGroup = $SPWeb.SiteGroups[$GroupAlias]	
	$roleAssignment = new-object Microsoft.SharePoint.SPRoleAssignment($SPGroup)
	if ($Role) {
		$roleAssignment.RoleDefinitionBindings.Add($roleDefinition)
	}
	$SPWeb.RoleAssignments.Add($roleAssignment)
	$SPWeb.Dispose()
	return $SPGroup
}


function IsNotNull($objectToCheck) {
    if (!$objectToCheck) {
        return $false
    }
 
    if ($objectToCheck -is [String] -and $objectToCheck -eq [String]::Empty) {
        return $false
    }
 
    return $true
}

function Pause
{
    param([string] $pauseKey,
            [ConsoleModifiers] $modifier,
            [string] $prompt,
            [bool] $hideKeysStrokes)
             
    Write-Host -NoNewLine "Press $prompt to continue . . . "
    do
    {
        $key = [Console]::ReadKey($hideKeysStrokes)
    } 
    while(($key.Key -ne $pauseKey) -or ($key.Modifiers -ne $modifer))   
     
    Write-Host
}
